Imports Microsoft.VisualBasic

Public Class ARPSIS
#Region "Propiedades de la Clase"

    Private _objectId As String
    Private _nomSubTramo As String
    Private _nomASPF As String
    Private _fechaUltInun As String
    Private _numInun As String
    Private _demarcacion As String
    Private _origen As String
    Private _criterio As String
    Private _estado As String
    Private _codDemarcacion As String
    Private _codSubTramo As String



    Public Property ObjectId() As String
        Get
            ObjectId = _objectId
        End Get
        Set(ByVal mObjectId As String)
            _objectId = mObjectId
        End Set
    End Property
    Public Property NomSubTramo() As String
        Get
            NomSubTramo = _nomSubTramo
        End Get
        Set(ByVal mNomSubTramo As String)
            _nomSubTramo = mNomSubTramo
        End Set
    End Property
    Public Property NomASPF() As String
        Get
            NomASPF = _nomASPF
        End Get
        Set(ByVal mNomASPF As String)
            _nomASPF = mNomASPF
        End Set
    End Property
    Public Property FechaUltInun() As String
        Get
            FechaUltInun = _fechaUltInun
        End Get
        Set(ByVal aFechaUltInun As String)
            _fechaUltInun = aFechaUltInun
        End Set
    End Property

    Public Property Demarcacion() As String
        Get
            Demarcacion = _demarcacion
        End Get
        Set(ByVal aDemarcacion As String)
            _demarcacion = aDemarcacion
        End Set
    End Property
    Public Property CodDemarcacion() As String
        Get
            CodDemarcacion = _codDemarcacion
        End Get
        Set(ByVal aCodDemarcacion As String)
            _codDemarcacion = aCodDemarcacion
        End Set
    End Property

    Public Property Origen() As String
        Get
            Origen = _origen
        End Get
        Set(ByVal aOrigen As String)
            _origen = aOrigen
        End Set
    End Property
    Public Property Criterio() As String
        Get
            Criterio = _criterio
        End Get
        Set(ByVal aCriterio As String)
            _criterio = aCriterio
        End Set
    End Property

    Public Property Estado() As String
        Get
            Estado = _estado
        End Get
        Set(ByVal aEstado As String)
            _estado = aEstado
        End Set
    End Property

    Public Property NumInun() As String
        Get
            NumInun = _numInun
        End Get
        Set(ByVal aNumInun As String)
            _numInun = aNumInun
        End Set
    End Property
    Public Property CodSubTramo() As String
        Get
            CodSubTramo = _codSubTramo
        End Get
        Set(ByVal aCodSubTramo As String)
            _codSubTramo = aCodSubTramo
        End Set
    End Property
#End Region

End Class