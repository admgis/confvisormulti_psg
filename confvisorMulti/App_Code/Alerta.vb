Imports Microsoft.VisualBasic
Imports Service

Public Class Alerta
#Region "Propiedades de la Clase"
    Private _id As Long
    Private _descripcion As String
    Private _identificador As String
    Private _fecha As String
    Private _fechaGeneracion As String
    Private _comarcas As Boolean
    Private _usuario As String
    Private _enferm As Enfermedad
    Private _especies As Especie()
    Private _radios As Radio()
    Private _puntos As PuntoInfeccion()

    Public Property id() As Long
        Get
            id = _id
        End Get
        Set(ByVal aId As Long)
            _id = aId
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDesc As String)
            _descripcion = aDesc
        End Set
    End Property
    Public Property identificador() As String
        Get
            identificador = _identificador
        End Get
        Set(ByVal aIdentificador As String)
            _identificador = aIdentificador
        End Set
    End Property

    Public Property fecha() As String
        Get
            fecha = _fecha
        End Get
        Set(ByVal aFecha As String)
            _fecha = aFecha
        End Set
    End Property
    Public Property fechaGeneracion() As String
        Get
            fechaGeneracion = _fechaGeneracion
        End Get
        Set(ByVal aFecha As String)
            _fechaGeneracion = aFecha
        End Set
    End Property

    Public Property comarcas() As Boolean
        Get
            comarcas = _comarcas
        End Get
        Set(ByVal aCom As Boolean)
            _comarcas = aCom
        End Set
    End Property

    Public Property usuario() As String
        Get
            usuario = _usuario
        End Get
        Set(ByVal aUser As String)
            _usuario = aUser
        End Set
    End Property

    Public Property enferm() As Enfermedad
        Get
            enferm = _enferm
        End Get
        Set(ByVal aEnferm As Enfermedad)
            _enferm = aEnferm
        End Set
    End Property
    Public Property especies() As Especie()
        Get
            especies = _especies
        End Get
        Set(ByVal aEspecies As Especie())
            _especies = aEspecies
        End Set
    End Property
    Public Property radios() As Radio()
        Get
            radios = _radios
        End Get
        Set(ByVal aRadios As Radio())
            _radios = aRadios
        End Set
    End Property

    Public Property puntos() As PuntoInfeccion()
        Get
            puntos = _puntos
        End Get
        Set(ByVal value As PuntoInfeccion())
            _puntos = value
        End Set
    End Property




#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As Long) As Alerta
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow

        strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
        strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
        strQuery = "SELECT ALE_CO_ID, ALE_DS_DESCRIPCION, TO_CHAR(ALE_DT_FECHA,'dd/MM/yyyy') AS ALE_DT_FECHA,ALE_BO_COMARCAS, ALE_DS_USUARIO, ENF_CO_ID, TO_CHAR(ALE_DT_GENERACION,'dd/MM/yyyy') AS ALE_DT_GENERACION, ALE_DS_FOCO FROM " & _
                    strTableName & " WHERE ALE_CO_ID='" & id & "'"
        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

        For Each aRow In aDS.Tables(0).Rows
            Me.id = IIf(aRow("ALE_CO_ID").Equals(System.DBNull.Value), "", aRow("ALE_CO_ID"))
            Me.descripcion = IIf(aRow("ALE_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("ALE_DS_DESCRIPCION"))
            Me.fecha = IIf(aRow("ALE_DT_FECHA").Equals(System.DBNull.Value), "", aRow("ALE_DT_FECHA"))
            Me.fechaGeneracion = IIf(aRow("ALE_DT_GENERACION").Equals(System.DBNull.Value), "", aRow("ALE_DT_GENERACION"))
            Me.comarcas = IIf(aRow("ALE_BO_COMARCAS").Equals("S"), True, False)
            Me.usuario = IIf(aRow("ALE_DS_USUARIO").Equals(System.DBNull.Value), "", aRow("ALE_DS_USUARIO"))
            Me.identificador = IIf(aRow("ALE_DS_FOCO").Equals(System.DBNull.Value), "", aRow("ALE_DS_FOCO"))
            Me.enferm = New Enfermedad()
            Me.enferm.Cargar(aRow("ENF_CO_ID"))
        Next
        'Cargamos las especies afectadas por la alerta
        strTableName = "SAN_AES_ALERTAS_ESPECIES"
        strDatasetName = "SAN_AES_ALERTAS_ESPECIES"
        strQuery = "SELECT ALE_CO_ID, ESP_CO_ID FROM SAN_AES_ALERTAS_ESPECIES WHERE ALE_CO_ID='" & id & "'"
        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

        ReDim Me.especies(aDS.Tables(0).Rows.Count - 1)
        Dim i As Integer
        i = 0
        For Each aRow In aDS.Tables(0).Rows
            Dim aId As String
            aId = IIf(aRow("ESP_CO_ID").Equals(System.DBNull.Value), "", aRow("ESP_CO_ID"))
            Me.especies(i) = New Especie()
            Me.especies(i).Cargar(aId)
            i += 1
        Next
        'Cargamos los radios de la alerta
        strTableName = "SAN_RAD_RADIOS_AFECCION"
        strDatasetName = "SAN_RAD_RADIOS_AFECCION"
        strQuery = "SELECT ALE_CO_ID, RAD_CO_ID, RAD_NM_LONGITUD,RAD_DS_COLOR FROM SAN_RAD_RADIOS_AFECCION WHERE ALE_CO_ID='" & id & "' ORDER BY RAD_NM_LONGITUD ASC"
        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

        ReDim Me.radios(aDS.Tables(0).Rows.Count - 1)
        i = 0
        For Each aRow In aDS.Tables(0).Rows
            Dim aId As Long
            aId = IIf(aRow("RAD_CO_ID").Equals(System.DBNull.Value), "", aRow("RAD_CO_ID"))
            Me.radios(i) = New Radio()
            Me.radios(i).Cargar(aId)
            i += 1
        Next

        'Cargamos los puntos de la alerta
        strTableName = "SAN_PTO_PUNTOS_ALERTAS"
        strDatasetName = "SAN_PTO_PUNTOS_ALERTAS"
        strQuery = "SELECT PTO_CO_ID FROM SAN_PTO_PUNTOS_ALERTAS WHERE ALE_CO_ID='" & id & "'"
        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

        ReDim Me.puntos(aDS.Tables(0).Rows.Count - 1)
        i = 0
        For Each aRow In aDS.Tables(0).Rows
            Dim aId As Long
            aId = IIf(aRow("PTO_CO_ID").Equals(System.DBNull.Value), "", aRow("PTO_CO_ID"))
            Me.puntos(i) = New PuntoInfeccion()
            Me.puntos(i).Cargar(aId)
            i += 1
        Next
        Return Me

        Return Me
    End Function
#End Region
End Class
