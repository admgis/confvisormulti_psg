Imports Microsoft.VisualBasic

Public Class Anillo
    Private _longitud As Long
    Private _subexplotaciones() As Subexplotacion
#Region "Propiedades de la Clase"
    Public Property longitud() As Long
        Get
            longitud = _longitud
        End Get
        Set(ByVal value As Long)
            _longitud = value
        End Set
    End Property
    Public Property subexplotaciones() As Subexplotacion()
        Get
            subexplotaciones = _subexplotaciones
        End Get
        Set(ByVal value As Subexplotacion())
            _subexplotaciones = value
        End Set
    End Property
#End Region
End Class
