Imports Microsoft.VisualBasic

Public Class Aprovechamiento
#Region "Propiedades de la Clase"
    Private _id As String
    Private _nombreCent As String
    Private _salto As String
    Private _potencia As String
    Private _corriente As String

    Public Property ID() As String
        Get
            ID = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property Salto() As String
        Get
            Salto = _salto
        End Get
        Set(ByVal mSalto As String)
            _salto = mSalto
        End Set
    End Property
    Public Property Corriente() As String
        Get
            Corriente = _corriente
        End Get
        Set(ByVal mCorriente As String)
            _corriente = mCorriente
        End Set
    End Property
    Public Property NOMBRECENT() As String
        Get
            NOMBRECENT = _nombreCent
        End Get
        Set(ByVal aNombrecent As String)
            _nombrecent = aNombreCent
        End Set
    End Property
    Public Property Potencia() As String
        Get
            Potencia = _potencia
        End Get
        Set(ByVal mPotencia As String)
            _potencia = mPotencia
        End Set
    End Property

#End Region

End Class
