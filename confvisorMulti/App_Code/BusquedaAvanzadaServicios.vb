Imports Microsoft.VisualBasic

Public Class BusquedaAvanzadaServicios
#Region "Propiedades de la Clase"
    Private _codId As String
    Private _descAlias As String
    Private _infoPdf As String
    Private _uuid(0) As String
    Public Property codId() As String
        Get
            codId = _codId
        End Get
        Set(ByVal cId As String)
            _codId = cId
        End Set
    End Property
    Public Property descAlias() As String
        Get
            descAlias = _descAlias
        End Get
        Set(ByVal desc As String)
            _descAlias = desc
        End Set
    End Property
    Public Property infoPdf() As String
        Get
            infoPdf = _infoPdf
        End Get
        Set(ByVal aInfoPdf As String)
            _infoPdf = aInfoPdf
        End Set
    End Property
    Public Property uuid() As String()
        Get
            uuid = _uuid
        End Get
        Set(ByVal aUuid() As String)
            _uuid = aUuid
        End Set
    End Property
#End Region

End Class
