Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service

Public Class Campo
    Private _Id As String
    Private _nombre As String
    Private _clave As Boolean
    Private _descripcion As String
    Private _visible As String
    Private _filtroSeleccion As String
    Private _claveSeleccion As String
    Private _formato As String

#Region "Propiedades de la Clase"
    Public Property Id() As String
        Get
            Id = _Id
        End Get
        Set(ByVal aId As String)
            _Id = aId
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property clave() As Boolean
        Get
            clave = _clave
        End Get
        Set(ByVal esClave As Boolean)
            _clave = esClave
        End Set
    End Property
    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDescripcion As String)
            _descripcion = aDescripcion
        End Set
    End Property

    Public Property visible() As String
        Get
            visible = _visible
        End Get
        Set(ByVal avisible As String)
            _visible = avisible
        End Set
    End Property

    Public Property filtroSeleccion() As String
        Get
            filtroSeleccion = _filtroSeleccion
        End Get
        Set(ByVal value As String)
            _filtroSeleccion = value
        End Set
    End Property
    Public Property claveSeleccion() As String
        Get
            claveSeleccion = _claveSeleccion
        End Get
        Set(ByVal value As String)
            _claveSeleccion = value
        End Set
    End Property
    Public Property formato() As String
        Get
            formato = _formato
        End Get
        Set(ByVal aFormato As String)
            _formato = aFormato
        End Set
    End Property
#End Region

#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As String, ByVal codIdioma As String) As Campo
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "VIS_CAM_CAMPOS"
            strDatasetName = "VIS_CAM_CAMPOS"
            strQuery = "SELECT VIS_CAM_CAMPOS.CAM_CO_ID, VIS_CAM_CAMPOS.CAM_DS_NOMBRE, VIS_CAM_CAMPOS.CAM_BOL_CLAVE, CASE WHEN VIS_CMD_DESC_CAMPOS.CAM_DS_DESCRIPCION IS NULL THEN VIS_CAM_CAMPOS.CAM_DS_DESCRIPCION ELSE VIS_CMD_DESC_CAMPOS.CAM_DS_DESCRIPCION END as CAM_DS_DESCRIPCION, VIS_CAM_CAMPOS.CAM_DS_VISIBLE, VIS_CAM_CAMPOS.CAM_BOL_FILTROSELECCION, VIS_CAM_CAMPOS.CAM_BOL_CLAVESELECCION, VIS_CAM_CAMPOS.CAM_DS_FORMATO " & _
   " FROM VIS_CAM_CAMPOS LEFT JOIN VIS_CMD_DESC_CAMPOS ON VIS_CAM_CAMPOS.CAM_CO_ID= VIS_CMD_DESC_CAMPOS.CAM_CO_ID AND VIS_CMD_DESC_CAMPOS.IDI_DS_CODIGO='" & codIdioma & "'" & _
   "WHERE VIS_CAM_CAMPOS.CAM_CO_ID ='" & id & "'"


            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            For Each aRow In aDS.Tables(0).Rows
                Me.Id = aRow("CAM_CO_ID")
                Me.nombre = aRow("CAM_DS_NOMBRE")
                Me.clave = IIf(aRow("CAM_BOL_CLAVE") = 1, True, False)
                Me.descripcion = aRow("CAM_DS_DESCRIPCION")
                Me.visible = IIf(aRow("CAM_DS_VISIBLE") Is DBNull.Value, "", aRow("CAM_DS_VISIBLE"))
                Me.filtroSeleccion = IIf(aRow("CAM_BOL_FILTROSELECCION") Is DBNull.Value, "", aRow("CAM_BOL_FILTROSELECCION"))
                Me.claveSeleccion = IIf(aRow("CAM_BOL_CLAVESELECCION") Is DBNull.Value, "", aRow("CAM_BOL_CLAVESELECCION"))
                Me.formato = IIf(aRow("CAM_DS_FORMATO") IS DBNull.Value, "", aRow("CAM_DS_FORMATO"))
            Next
            Return Me
        Catch
            Return Nothing
        End Try

    End Function
#End Region

End Class
