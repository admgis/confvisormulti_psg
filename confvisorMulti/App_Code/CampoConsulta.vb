Imports Microsoft.VisualBasic

Public Class CampoConsulta
#Region "Propiedades de la Clase"
    Private _nombre As String
    Private _descripcion As String
    Private _origen As Consulta.tipoOrigen
    Private _exportar As Boolean = True

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDesc As String)
            _descripcion = aDesc
        End Set
    End Property

    Public Property exportar() As Boolean
        Get
            exportar = _exportar
        End Get
        Set(ByVal aExp As Boolean)
            _exportar = aExp
        End Set
    End Property

    Public Property origen() As Consulta.tipoOrigen
        Get
            origen = _origen
        End Get
        Set(ByVal aOrigen As Consulta.tipoOrigen)
            _origen = aOrigen
        End Set
    End Property

#End Region

#Region "Funciones de la Clase"

#End Region


End Class
