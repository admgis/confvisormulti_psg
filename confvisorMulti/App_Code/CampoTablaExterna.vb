Imports Microsoft.VisualBasic

Public Class CampoTablaExterna

	Private _Id As Integer
	Private _idTabla As Integer
	Private _nombre As String
	Private _descripcion As String
	Private _clave As String
	Private _numerico As String

	Public Property Id() As Integer
		Get
			Id = _Id
		End Get
		Set(ByVal aId As Integer)
			_Id = aId
		End Set
	End Property

	Public Property idTabla() As Integer
		Get
			idTabla = _idTabla
		End Get
		Set(ByVal aIdTabla As Integer)
			_idTabla = aIdTabla
		End Set
	End Property

	Public Property nombre() As String
		Get
			nombre = _nombre
		End Get
		Set(ByVal aNombre As String)
			_nombre = aNombre
		End Set
	End Property

	Public Property descripcion() As String
		Get
			descripcion = _descripcion
		End Get
		Set(ByVal aDescripcion As String)
			_descripcion = aDescripcion
		End Set
	End Property

	Public Property clave() As String
		Get
			clave = _clave
		End Get
		Set(ByVal aClave As String)
			_clave = aClave
		End Set
	End Property

	Public Property numerico() As String
		Get
			numerico = _numerico
		End Get
		Set(ByVal aNumerico As String)
			_numerico = aNumerico
		End Set
	End Property

End Class
