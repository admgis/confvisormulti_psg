Imports Microsoft.VisualBasic

Public Class CampoTematico
    ''Public Enum tipoCampo
    '    Numerico = 1
    '    Caracter = 2
    '    Fecha = 3
    '    Booleano = 4
    '    Otros = 5
    'End Enum


    Private _nombre As String
    Private _tipo As String

#Region "Propiedades de la Clase"
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property tipo() As String
        Get
            tipo = _tipo
        End Get
        Set(ByVal aTipo As String)
            _tipo = aTipo
        End Set
    End Property
#End Region
End Class
