Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service
Public Class Capa

    Private _Id As String
    Private _nombre As String
    Private _descripcion As String
    Private _consulta As String
    Private _descConsulta As String
    Private _identificable As Boolean
    Private _leyendaVisible As Boolean
    Private _imgLeyenda As String
    Private _areaMaxima As String
    Private _areaMaximaSeleccion As String
    Private _MaximoElementosSeleccion As String
#Region "Propiedades de la Clase"
    Public Property Id() As String
        Get
            Id = _Id
        End Get
        Set(ByVal aId As String)
            _Id = aId
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDescripcion As String)
            _descripcion = aDescripcion
        End Set
    End Property

    Public Property consulta() As String
        Get
            consulta = _consulta
        End Get
        Set(ByVal aConsulta As String)
            _consulta = aConsulta
        End Set
    End Property
    Public Property descConsulta() As String
        Get
            descConsulta = _descConsulta
        End Get
        Set(ByVal adescConsulta As String)
            _descConsulta = adescConsulta
        End Set
    End Property
    Public Property identificable() As Boolean
        Get
            identificable = _identificable
        End Get
        Set(ByVal aidentificable As Boolean)
            _identificable = aidentificable
        End Set
    End Property
    Public Property leyendavisible() As Boolean
        Get
            leyendavisible = _leyendaVisible
        End Get
        Set(ByVal value As Boolean)
            _leyendaVisible = value
        End Set
    End Property
    Public Property imgLeyenda() As String
        Get
            imgLeyenda = _imgLeyenda
        End Get
        Set(ByVal value As String)
            _imgLeyenda = value
        End Set
    End Property
    Public Property areaMaxima() As String
        Get
            areaMaxima = _areaMaxima
        End Get
        Set(ByVal aareaMaxima As String)
            _areaMaxima = aareaMaxima
        End Set
    End Property
    Public Property areaMaximaSeleccion() As String
        Get
            areaMaximaSeleccion = _areaMaximaSeleccion
        End Get
        Set(ByVal value As String)
            _areaMaximaSeleccion = value
        End Set
    End Property

    Public Property maximoElementosSeleccion() As String
        Get
            maximoElementosSeleccion = _MaximoElementosSeleccion
        End Get
        Set(ByVal value As String)
            _MaximoElementosSeleccion = value
        End Set
    End Property
#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As String, ByVal codIdioma As String, Optional ByVal codVisor As String = "") As Capa
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "VIS_CAP_CAPAS"
            strDatasetName = "VIS_CAP_CAPAS"
            If codVisor = "" Then
                'strQuery = "SELECT VIS_CEV_CAPASEXTRAIBLESVISOR.CEV_NM_AREAMAXEXTRACCION, VIS_CAP_CAPAS.CAP_CO_ID, VIS_CAP_CAPAS.CAP_DS_NOMBRE, DECODE(VIS_CDS_DESC_CAPAS.CAP_DS_DESCRIPCION,NULL,VIS_CAP_CAPAS.CAP_DS_DESCRIPCION,VIS_CDS_DESC_CAPAS.CAP_DS_DESCRIPCION) as CAP_DS_DESCRIPCION, VIS_CAP_CAPAS.CAP_BO_LEYENDAVISIBLE , VIS_CAP_CAPAS.CAP_DS_IMAGENLEYENDA, " & _
                '"VIS_CAP_CAPAS.CAP_DS_CONSULTA, VIS_CAP_CAPAS.CAP_DS_DESC_CONSULTA, VIS_CAP_CAPAS.CAP_BO_IDENTIFICABLE FROM VIS_CAP_CAPAS LEFT JOIN VIS_CEV_CAPASEXTRAIBLESVISOR ON VIS_CAP_CAPAS.CAP_CO_ID = VIS_CEV_CAPASEXTRAIBLESVISOR.CAP_CO_ID LEFT JOIN VIS_CDS_DESC_CAPAS ON VIS_CAP_CAPAS.CAP_CO_ID= VIS_CDS_DESC_CAPAS.CAP_CO_ID " & _
                '" AND VIS_CDS_DESC_CAPAS.IDI_DS_CODIGO='" & codIdioma & "'" & _
                '"WHERE VIS_CAP_CAPAS.CAP_CO_ID='" & id & "'"

                strQuery = "SELECT 0 CEV_NM_AREAMAXEXTRACCION, 0 CSV_NM_AREAMAXEXTRACCION, 0 CSV_NM_NUMEROMAXIMOELEMENTOS, VIS_CAP_CAPAS.CAP_CO_ID, VIS_CAP_CAPAS.CAP_DS_NOMBRE, CASE WHEN VIS_CDS_DESC_CAPAS.CAP_DS_DESCRIPCION IS NULL THEN VIS_CAP_CAPAS.CAP_DS_DESCRIPCION ELSE VIS_CDS_DESC_CAPAS.CAP_DS_DESCRIPCION END as CAP_DS_DESCRIPCION, VIS_CAP_CAPAS.CAP_BO_LEYENDAVISIBLE , VIS_CAP_CAPAS.CAP_DS_IMAGENLEYENDA, " & _
                "VIS_CAP_CAPAS.CAP_DS_CONSULTA, VIS_CAP_CAPAS.CAP_DS_DESC_CONSULTA, VIS_CAP_CAPAS.CAP_BO_IDENTIFICABLE FROM VIS_CAP_CAPAS LEFT JOIN VIS_CDS_DESC_CAPAS ON VIS_CAP_CAPAS.CAP_CO_ID= VIS_CDS_DESC_CAPAS.CAP_CO_ID " & _
                " AND VIS_CDS_DESC_CAPAS.IDI_DS_CODIGO='" & codIdioma & "'" & _
                "WHERE VIS_CAP_CAPAS.CAP_CO_ID='" & id & "'"

            Else
                strQuery = "SELECT VIS_CEV_CAPASEXTRAIBLESVISOR.CEV_NM_AREAMAXEXTRACCION, VIS_CSV_CAPASSELECCIONESVISOR.CSV_NM_AREAMAXEXTRACCION, VIS_CSV_CAPASSELECCIONESVISOR.CSV_NM_NUMEROMAXIMOELEMENTOS,  VIS_CAP_CAPAS.CAP_CO_ID, VIS_CAP_CAPAS.CAP_DS_NOMBRE, CASE WHEN VIS_CDS_DESC_CAPAS.CAP_DS_DESCRIPCION IS NULL THEN VIS_CAP_CAPAS.CAP_DS_DESCRIPCION ELSE VIS_CDS_DESC_CAPAS.CAP_DS_DESCRIPCION END as CAP_DS_DESCRIPCION, VIS_CAP_CAPAS.CAP_BO_LEYENDAVISIBLE , VIS_CAP_CAPAS.CAP_DS_IMAGENLEYENDA, " & _
                "VIS_CAP_CAPAS.CAP_DS_CONSULTA, VIS_CAP_CAPAS.CAP_DS_DESC_CONSULTA, VIS_CAP_CAPAS.CAP_BO_IDENTIFICABLE FROM VIS_CAP_CAPAS " & _
                " LEFT JOIN VIS_CEV_CAPASEXTRAIBLESVISOR ON VIS_CAP_CAPAS.CAP_CO_ID = VIS_CEV_CAPASEXTRAIBLESVISOR.CAP_CO_ID AND VIS_CEV_CAPASEXTRAIBLESVISOR.VIS_CO_ID = " & codVisor & _
                " LEFT JOIN VIS_CSV_CAPASSELECCIONESVISOR ON VIS_CAP_CAPAS.CAP_CO_ID = VIS_CSV_CAPASSELECCIONESVISOR.CAP_CO_ID AND VIS_CSV_CAPASSELECCIONESVISOR.VIS_CO_ID = " & codVisor & _
                " LEFT JOIN VIS_CDS_DESC_CAPAS ON VIS_CAP_CAPAS.CAP_CO_ID= VIS_CDS_DESC_CAPAS.CAP_CO_ID " & _
                " AND VIS_CDS_DESC_CAPAS.IDI_DS_CODIGO='" & codIdioma & "'" & _
                "WHERE VIS_CAP_CAPAS.CAP_CO_ID='" & id & "'"
            End If


            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            For Each aRow In aDS.Tables(0).Rows
                Me.Id = aRow("CAP_CO_ID")
                Me.nombre = aRow("CAP_DS_NOMBRE")
                Me.descripcion = aRow("CAP_DS_DESCRIPCION")
                Me.consulta = IIf(aRow("CAP_DS_CONSULTA").Equals(System.DBNull.Value), "", aRow("CAP_DS_CONSULTA"))
                Me.descConsulta = IIf(aRow("CAP_DS_DESC_CONSULTA").Equals(System.DBNull.Value), "", aRow("CAP_DS_DESC_CONSULTA"))
                Me.identificable = IIf(aRow("CAP_BO_IDENTIFICABLE").Equals(System.DBNull.Value), True, IIf(aRow("CAP_BO_IDENTIFICABLE").Equals("Y"), True, False))
                Me.leyendavisible = IIf(aRow("CAP_BO_LEYENDAVISIBLE").Equals(System.DBNull.Value), True, IIf(aRow("CAP_BO_LEYENDAVISIBLE").Equals("Y"), True, False))
                Me.imgLeyenda = IIf(aRow("CAP_DS_IMAGENLEYENDA").Equals(System.DBNull.Value), "", aRow("CAP_DS_IMAGENLEYENDA"))
                Me.areaMaxima = IIf(aRow("CEV_NM_AREAMAXEXTRACCION").Equals(System.DBNull.Value), "0", aRow("CEV_NM_AREAMAXEXTRACCION").ToString)
                Me.areaMaximaSeleccion = IIf(aRow("CSV_NM_AREAMAXEXTRACCION").Equals(System.DBNull.Value), "0", aRow("CSV_NM_AREAMAXEXTRACCION").ToString)
                Me.maximoElementosSeleccion = IIf(aRow("CSV_NM_NUMEROMAXIMOELEMENTOS").Equals(System.DBNull.Value), "0", aRow("CSV_NM_NUMEROMAXIMOELEMENTOS").ToString)
            Next

            Return Me
        Catch
            Return Nothing
        End Try

    End Function
#End Region

End Class
