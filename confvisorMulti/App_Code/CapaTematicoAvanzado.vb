﻿Imports Microsoft.VisualBasic

Public Class CapaTematicoAvanzado
#Region "Propiedades de la Clase"

    Private _id As String
    Private _descripcion As String
    Private _workspaceName As String
    Private _dataSource As String
    Private _campoClave As String
    Private _camposVisibles As String
    Private _permitirEtiquetado As Boolean

    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Public Property WorkspaceName() As String
        Get
            WorkspaceName = _workspaceName
        End Get
        Set(ByVal value As String)
            _workspaceName = value
        End Set
    End Property

    Public Property dataSource() As String
        Get
            dataSource = _dataSource
        End Get
        Set(ByVal value As String)
            _dataSource = value
        End Set
    End Property

    Public Property campoClave() As String
        Get
            campoClave = _campoClave
        End Get
        Set(ByVal value As String)
            _campoClave = value
        End Set
    End Property

    Public Property camposVisibles() As String
        Get
            Return _camposVisibles
        End Get
        Set(ByVal value As String)
            _camposVisibles = value
        End Set
    End Property

    Public Property permitirEtiquetado() As Boolean
        Get
            Return _permitirEtiquetado
        End Get
        Set(ByVal value As Boolean)
            _permitirEtiquetado = value
        End Set
    End Property

#End Region
End Class
