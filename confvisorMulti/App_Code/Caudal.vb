Imports Microsoft.VisualBasic

Public Class Caudal
#Region "Propiedades de la Clase"
    Private _id As String
    Private _nombre As String
    Private _tipo As String
    Private _idtipo As String
    Private _cuenca As String
    Private _idPunto As String
    Private _codDemar As String
    Private _nomDemar As String
    Private _xUtm30 As Double
    Private _yUtm30 As Double
    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property IdPunto() As String
        Get
            IdPunto = _idPunto
        End Get
        Set(ByVal mIdPunto As String)
            _idPunto = mIdPunto
        End Set
    End Property
    Public Property CodDemar() As String
        Get
            CodDemar = _codDemar
        End Get
        Set(ByVal mCodDemar As String)
            _codDemar = mCodDemar
        End Set
    End Property
    Public Property NomDemar() As String
        Get
            NomDemar = _nomDemar
        End Get
        Set(ByVal mNomDemar As String)
            _nomDemar = mNomDemar
        End Set
    End Property
    Public Property Cuenca() As String
        Get
            Cuenca = _cuenca
        End Get
        Set(ByVal mCuenca As String)
            _cuenca = mCuenca
        End Set
    End Property
    Public Property Tipo() As String
        Get
            Tipo = _tipo
        End Get
        Set(ByVal mTipo As String)
            _tipo = mTipo
        End Set
    End Property

    Public Property IdTipo() As String
        Get
            IdTipo = _idtipo
        End Get
        Set(ByVal mIdTipo As String)
            _idtipo = mIdTipo
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property XUtm30() As Double
        Get
            XUtm30 = _xUtm30
        End Get
        Set(ByVal mXUtm30 As Double)
            _xUtm30 = mXUtm30
        End Set
    End Property
    Public Property YUtm30() As Double
        Get
            YUtm30 = _yUtm30
        End Get
        Set(ByVal mYUtm30 As Double)
            _yUtm30 = mYUtm30
        End Set
    End Property
#End Region

End Class
