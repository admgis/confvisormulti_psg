Imports Microsoft.VisualBasic
Imports Service

Public Class Comarca
#Region "Propiedades de la Clase"
    Private _id As String
    Private _idProv As Integer
    Private _nombre As String
    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property IdProv() As Integer
        Get
            IdProv = _idProv
        End Get
        Set(ByVal mIdProv As Integer)
            _idProv = mIdProv
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As String) As Comarca
        Dim strQuery As String
        Dim strTabla As String, strTabla2 As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow

        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaComarcasSanitarias")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdComarcasSanitarias")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreComarcasSanitarias")
            strPrefijo = System.Configuration.ConfigurationManager.AppSettings("PrefijoIDComarcasSanitarias")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " = '" + id + "'" + " AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 "
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)                        
            For Each aRow In aDS.Tables(0).Rows
                Dim aComarca As New Comarca()
                Me.Id = aRow(strCampoId)
                Me.IdProv = Mid(Me.Id, 3, 2)
                Me.nombre = aRow(strCampoNombre)                
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

End Class
