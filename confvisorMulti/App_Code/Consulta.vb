Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Xml
Imports Service
Imports System.Data
Imports System.Data.OracleClient
Imports System.Data.OleDb

Public Class Consulta
#Region "Propiedades de la Clase"

    Public Enum tipoOrigen
        source = 1
        database = 2
    End Enum
    Private _nombre As String
    Private _descripcion As String
    Private _titulo As String
    Private _sqlbase As String
    Private _xsl As String
    Private _connectionKey As String
    Private _parametros() As Parametro
    Private _resourceBases() As RecursoBase
    Private _fields() As CampoConsulta
    Private _resources() As Recurso
    Private _consultas() As ConsultaAnidada

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDescripcion As String)
            _descripcion = aDescripcion
        End Set
    End Property

    Public Property titulo() As String
        Get
            titulo = _titulo
        End Get
        Set(ByVal aTitulo As String)
            _titulo = aTitulo
        End Set
    End Property

    Public Property sqlbase() As String
        Get
            sqlbase = _sqlbase
        End Get
        Set(ByVal aSQL As String)
            _sqlbase = aSQL
        End Set
    End Property

    Public Property connectionKey() As String
        Get
            connectionKey = _connectionKey
        End Get
        Set(ByVal connectionKey As String)
            If Not String.IsNullOrEmpty(connectionKey) Then
                _connectionKey = connectionKey
            Else
                _connectionKey = "bbdd"
            End If
        End Set
    End Property

    Public Property xsl() As String
        Get
            xsl = _xsl
        End Get
        Set(ByVal aXsl As String)
            _xsl = aXsl
        End Set
    End Property

    Public Property listaCampos() As CampoConsulta()
        Get
            listaCampos = _fields
        End Get
        Set(ByVal aLista As CampoConsulta())
            _fields = aLista
        End Set
    End Property

    Public Property resourceBases() As RecursoBase()
        Get
            resourceBases = _resourceBases
        End Get
        Set(ByVal aRecursoBases As RecursoBase())
            _resourceBases = aRecursoBases
        End Set
    End Property

    Public Property resources() As Recurso()
        Get
            resources = _resources
        End Get
        Set(ByVal aRecursos As Recurso())
            _resources = aRecursos
        End Set
    End Property

    Public Property anidadas() As ConsultaAnidada()
        Get
            anidadas = _consultas
        End Get
        Set(ByVal aCons As ConsultaAnidada())
            _consultas = aCons
        End Set
    End Property

    Public Property Parametros() As Parametro()
        Get
            Parametros = _parametros
        End Get
        Set(ByVal aParametros As Parametro())
            _parametros = aParametros
        End Set
    End Property
#End Region

#Region "Funciones de la Clase"
    Public Function Cargar(ByVal nombre As String, ByVal codIdioma As String) As Consulta
        Try
            Dim m_xmld As XmlDocument
            Dim m_nodelist As XmlNodeList, m_nodelist2 As XmlNodeList
            Dim m_node As XmlNode, m_node2 As XmlNode
            Dim pathConsulta As String, pathBase As String
            Dim i As Integer, j As Integer
            'Creamos el "XML Document"
            m_xmld = New XmlDocument()

            pathBase = System.AppDomain.CurrentDomain.BaseDirectory()
            pathConsulta = System.Configuration.ConfigurationManager.AppSettings("PathConsultas")
            'Cargamos el archivo

            If (codIdioma.ToLower = "es") Then
                m_xmld.Load(pathBase + pathConsulta + nombre + ".xml")
            Else
                Try
                    m_xmld.Load(pathBase + pathConsulta + nombre + "_" + codIdioma + ".xml")
                Catch ex As FileNotFoundException
                    m_xmld.Load(pathBase + pathConsulta + nombre + ".xml")
                End Try
            End If


            'Obtenemos la lista de los nodos "consulta"
            m_nodelist = m_xmld.SelectNodes("/consulta")

            'Iniciamos el ciclo de lectura
            For Each m_node In m_nodelist
                'Obtenemos el atributo del codigo
                Me.nombre = nombre
                Me.descripcion = m_node.Attributes.GetNamedItem("name").Value
                Me.titulo = m_node.Attributes.GetNamedItem("title").Value
            Next

            'Obtenemos ahora el sqlBase
            m_nodelist = m_xmld.SelectNodes("/consulta/sqlbase")
            For Each m_node In m_nodelist
                Dim connectionKeyStr As String = ""
                If IsNothing(m_node.Attributes.GetNamedItem("connectionKey")) = False Then
                    connectionKeyStr = m_node.Attributes.GetNamedItem("connectionKey").Value
                End If
                Me.connectionKey = connectionKeyStr
                Me.sqlbase = m_node.ChildNodes.Item(0).InnerText
            Next
            m_nodelist = m_xmld.SelectNodes("/consulta/params/param")
            If Not (m_nodelist Is Nothing) Then
                ReDim Me.Parametros(m_nodelist.Count - 1)
                i = 0
                For Each m_node In m_nodelist
                    Dim aParam As New Parametro
                    aParam.Nombre = m_node.Attributes.GetNamedItem("name").Value
                    aParam.Query = m_node.Attributes.GetNamedItem("query").Value
                    Me.Parametros(i) = aParam
                    i += 1
                Next
            Else
                Me.Parametros = Nothing
            End If
            m_nodelist = m_xmld.SelectNodes("/consulta/xsl")
            For Each m_node In m_nodelist
                Me.xsl = m_node.Attributes.GetNamedItem("url").Value
            Next
            'Obtenemos los paths con los recursos
            m_nodelist = m_xmld.SelectNodes("/consulta/resourcepath")
            ReDim Me.resourceBases(m_nodelist.Count - 1)
            i = 0
            For Each m_node In m_nodelist
                Dim nuevoRec As New RecursoBase
                nuevoRec.nombre = m_node.Attributes.GetNamedItem("name").Value
                nuevoRec.path = m_node.Attributes.GetNamedItem("base").Value
                Me.resourceBases(i) = nuevoRec
                i += 1
            Next

            'Obtenemos ahora los campos 
            m_nodelist = m_xmld.SelectNodes("/consulta/display/field")
            ReDim Me.listaCampos(m_nodelist.Count - 1)
            i = 0
            For Each m_node In m_nodelist
                Dim nuevoCampo As New CampoConsulta

                'agm 15/12/2010 a�adido para poder elegir si exportamos o no los campos a EXCEL � PDF
                If m_node.Attributes.GetNamedItem("export") Is Nothing = False Then
                    If UCase(m_node.Attributes.GetNamedItem("export").Value) = "NO" Then
                        nuevoCampo.exportar = False
                    End If
                End If
                Select Case UCase(m_node.Attributes.GetNamedItem("source").Value)
                    Case "REQUEST"
                        nuevoCampo.origen = tipoOrigen.source
                    Case "DATABASE"
                        nuevoCampo.origen = tipoOrigen.database
                End Select
                nuevoCampo.nombre = m_node.Attributes.GetNamedItem("name").Value
                nuevoCampo.descripcion = m_node.Attributes.GetNamedItem("alias").Value
                Me.listaCampos(i) = nuevoCampo
                i += 1
            Next

            m_nodelist = m_xmld.SelectNodes("/consulta/display/resource")
            ReDim Me.resources(m_nodelist.Count - 1)
            i = 0
            For Each m_node In m_nodelist
                Dim nuevoRecurso As New Recurso
                Select Case UCase(m_node.Attributes.GetNamedItem("source").Value)
                    Case "REQUEST"
                        nuevoRecurso.origen = tipoOrigen.source
                    Case "DATABASE"
                        nuevoRecurso.origen = tipoOrigen.database
                End Select
                nuevoRecurso.base = m_node.Attributes.GetNamedItem("base").Value
                nuevoRecurso.extension = m_node.Attributes.GetNamedItem("extension").Value
                nuevoRecurso.tipo = m_node.Attributes.GetNamedItem("type").Value
                nuevoRecurso.nombre = m_node.Attributes.GetNamedItem("name").Value
                nuevoRecurso.descripcion = m_node.Attributes.GetNamedItem("alias").Value
                Me.resources(i) = nuevoRecurso
                i += 1
            Next

            'Obtenemos ahora las consultas anidadas correspondientes
            'Existen de dos tipos, las que son para toda la consulta y las que son para cada una de las filas
            'Debemos seleccionar las que son por fila y las que son globales
            'Globales
            m_nodelist = m_xmld.SelectNodes("/consulta/nested/nestedGlobal")
            ReDim Me.anidadas(m_nodelist.Count - 1)
            i = 0
            For Each m_node In m_nodelist
                Dim nuevaConsulta As New ConsultaAnidada
                nuevaConsulta.nombre = m_node.Attributes.GetNamedItem("nestedConsulta").Value
                nuevaConsulta.descripcion = m_node.Attributes.GetNamedItem("linkmessage").Value
                nuevaConsulta.tipo = ConsultaAnidada.tipoAnidada.unica
                m_nodelist2 = m_node.SelectNodes("mappings/mapping")
                j = 0
                ReDim nuevaConsulta.mappings(m_nodelist2.Count - 1)
                For Each m_node2 In m_nodelist2
                    Dim nuevoMap As New Mapping
                    nuevoMap.desde = m_node2.Attributes.GetNamedItem("from").Value
                    nuevoMap.hacia = m_node2.Attributes.GetNamedItem("to").Value
                    Select Case UCase(m_node2.Attributes.GetNamedItem("source").Value)
                        Case "REQUEST"
                            nuevoMap.origen = tipoOrigen.source
                        Case "DATABASE"
                            nuevoMap.origen = tipoOrigen.database
                    End Select
                    nuevaConsulta.mappings(j) = nuevoMap
                    j += 1
                Next
                Me.anidadas(i) = nuevaConsulta
                i += 1
            Next

            m_nodelist = m_xmld.SelectNodes("/consulta/nested/nestedByRow")
            i = Me.anidadas.Length
            ReDim Preserve Me.anidadas(Me.anidadas.Length + m_nodelist.Count - 1)

            For Each m_node In m_nodelist
                Dim nuevaConsulta As New ConsultaAnidada
                nuevaConsulta.nombre = m_node.Attributes.GetNamedItem("nestedConsulta").Value
                nuevaConsulta.descripcion = m_node.Attributes.GetNamedItem("linkmessage").Value
                nuevaConsulta.tipo = ConsultaAnidada.tipoAnidada.fila
                m_nodelist2 = m_node.SelectNodes("/consulta/nested/nestedByRow/mappings/mapping")
                j = 0
                ReDim nuevaConsulta.mappings(m_nodelist2.Count - 1)
                For Each m_node2 In m_nodelist2
                    Dim nuevoMap As New Mapping
                    nuevoMap.desde = m_node2.Attributes.GetNamedItem("from").Value
                    nuevoMap.hacia = m_node2.Attributes.GetNamedItem("to").Value
                    Select Case UCase(m_node2.Attributes.GetNamedItem("source").Value)
                        Case "REQUEST"
                            nuevoMap.origen = tipoOrigen.source
                        Case "DATABASE"
                            nuevoMap.origen = tipoOrigen.database
                    End Select
                    nuevaConsulta.mappings(j) = nuevoMap
                    j += 1
                Next
                Me.anidadas(i) = nuevaConsulta
                i += 1
            Next

            Return Me


        Catch ex As Exception
            Throw ex
            'Error trapping
6:          Console.Write(ex.ToString())
            Return Nothing
        End Try
    End Function

    Public Function Ejecutar(ByVal nombres As String(), ByVal values As String(), ByVal params As String()) As Ficha
        Dim strQuery As String
        Dim strNomConsulta As String
        Dim i As Integer, j As Integer
        Dim aDS As Data.DataSet
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Dim primerValor As Boolean
        Try
            'Debemos encontrar los nombres de los par�metros
            strQuery = UCase(Me.sqlbase)

            If Not (Me.Parametros Is Nothing) Then
                'Hacemos la sustituci�n de valores
                If Not (params Is Nothing) Then
                    For i = 0 To params.Length - 1
                        For j = 0 To Me.Parametros.Length - 1
                            If UCase(Me.Parametros(j).Nombre) = UCase(params(i)) Then
                                strQuery += Me.Parametros(j).Query + " "
                            End If
                        Next
                    Next
                End If
            End If
            Dim longitud As Integer
            If (values.Length > nombres.Length) Then
                longitud = nombres.Length
            Else
                longitud = values.Length
            End If
            ''agm: para evitar SQL Injection
            'For i = 0 To longitud - 1
            '    strQuery = strQuery.Replace("%" + UCase(nombres(i)) + "%", ":" + Replace(UCase(nombres(i)), ".", "_", ))
            'Next
            For i = 0 To longitud - 1
                strQuery = strQuery.Replace("%" + UCase(nombres(i)) + "%", "'" + values(i) + "'")
            Next

            'Hemos generado la query
            aDS = ExecuteQuery(strQuery, Me.nombre, Me.nombre, Me.connectionKey)

            ''agm: para evitar SQL Injection
            'Dim aConn As OracleConnection
            'Try
            '    Dim strConn As String
            '    strConn = System.Configuration.ConfigurationManager.AppSettings("bbdd")

            '    aConn = New OracleConnection(strConn)
            '    aConn.Open()

            '    Dim mAdapter As OracleDataAdapter = New OracleDataAdapter

            '    mAdapter.SelectCommand = New OracleCommand(strQuery, aConn)

            '    For i = 0 To longitud - 1
            '        Dim par As OracleParameter = mAdapter.SelectCommand.Parameters.Add(Replace(UCase(nombres(i)), ".", "_", ), OracleType.NVarChar, 100)
            '        par.Value = values(i)
            '    Next

            '    Dim aDataSet As Data.DataSet = New Data.DataSet(Me.nombre)
            '    mAdapter.Fill(aDataSet, Me.nombre)

            '    aConn.Close()
            '    aDS = aDataSet

            'Catch ex As Exception
            '    aConn.Close()
            '    Throw New Exception(ex.Message, ex)
            'End Try

            Dim nuevaFicha As New Ficha
            nuevaFicha.nombre = Me.nombre
            nuevaFicha.Titulo = Me.titulo
            nuevaFicha.descripcion = Me.descripcion
            nuevaFicha.xsl = Me.xsl
            ReDim nuevaFicha.resultados(aDS.Tables(0).Rows.Count - 1)
            i = 0
            primerValor = True
            For Each aRow In aDS.Tables(0).Rows
                'Resultados
                Dim aRes As New Resultado
                Dim aCampo As CampoConsulta
                'Dim j As Integer
                Dim k As Integer
                j = 0
                ReDim aRes.valores(Me.listaCampos.Length - 1)
                For Each aCampo In Me.listaCampos
                    Dim aValor As New Valor
                    'El nombre del valor es la descripci�n (aqu� s�lo sacamos el alias)
                    aValor.nombre = aCampo.descripcion
                    'agm 15/12/2010 a�adido para elegir si exportar a EXCEL y PDF
                    aValor.exportar = aCampo.exportar
                    Select Case aCampo.origen
                        Case tipoOrigen.database
                            aValor.valor = aRow(aCampo.nombre).ToString
                        Case tipoOrigen.source
                            For k = 0 To nombres.Length - 1
                                If UCase(nombres(k)) = UCase(aCampo.nombre) Then
                                    aValor.valor = values(k)
                                    Exit For
                                End If
                            Next
                    End Select
                    aRes.valores(j) = aValor
                    j += 1
                Next
                'Anidadas
                Dim aCons As ConsultaAnidada
                k = 0
                Dim numAnidadasUnica As Integer
                numAnidadasUnica = 0
                For Each aCons In Me.anidadas
                    If (aCons.tipo = ConsultaAnidada.tipoAnidada.fila) Then
                        ReDim Preserve aRes.anidadas(k)
                        Dim aNest As New Nested
                        Dim aMap As Mapping
                        Dim l As Integer, m As Integer
                        aNest.nombre = aCons.nombre
                        aNest.descripcion = aCons.descripcion
                        ReDim aNest.parametros(aCons.mappings.Length - 1)
                        l = 0
                        For Each aMap In aCons.mappings
                            Dim aVal As New Valor
                            aVal.nombre = aMap.hacia
                            If (aMap.origen = tipoOrigen.database) Then
                                aVal.valor = aRow(aMap.desde).ToString
                            Else
                                For m = 0 To nombres.Length - 1
                                    If UCase(nombres(m)) = UCase(aMap.desde) Then
                                        aVal.valor = values(m)
                                        Exit For
                                    End If
                                Next
                            End If
                            aNest.parametros(l) = aVal
                            l += 1
                        Next
                        aRes.anidadas(k) = aNest
                        k += 1
                    Else
                        If primerValor Then
                            'Anidadas




                            If (aCons.tipo = ConsultaAnidada.tipoAnidada.unica) Then

                                ReDim Preserve nuevaFicha.nested(numAnidadasUnica)

                                Dim aNest As New Nested
                                Dim aMap As Mapping
                                Dim l As Integer, m As Integer
                                aNest.nombre = aCons.nombre
                                aNest.descripcion = aCons.descripcion
                                ReDim aNest.parametros(aCons.mappings.Length - 1)
                                l = 0
                                For Each aMap In aCons.mappings
                                    Dim aVal As New Valor
                                    aVal.nombre = aMap.hacia
                                    If (aMap.origen = tipoOrigen.database) Then
                                        aVal.valor = aRow(aMap.desde).ToString
                                    Else
                                        For m = 0 To nombres.Length - 1
                                            If UCase(nombres(m)) = UCase(aMap.desde) Then
                                                aVal.valor = values(m)
                                                Exit For
                                            End If
                                        Next
                                    End If
                                    aNest.parametros(l) = aVal
                                    l += 1
                                Next
                                nuevaFicha.nested(numAnidadasUnica) = aNest
                                numAnidadasUnica += 1
                            End If


                        End If
                    End If

                Next

                'Recursos
                Dim aRecurso As Recurso
                j = 0
                ReDim aRes.links(Me.resources.Length - 1)
                For Each aRecurso In Me.resources
                    Dim aRecBase As RecursoBase
                    Dim aLink As New Link
                    Dim strResultado As String, strObtenido As String
                    aLink.tipo = aRecurso.tipo
                    aLink.descripcion = aRecurso.descripcion
                    strResultado = ""
                    'Obtenemos la base del recurso
                    For Each aRecBase In Me.resourceBases
                        If aRecurso.base = aRecBase.nombre Then
                            strResultado = aRecBase.path
                        End If
                    Next
                    strObtenido = ""
                    Select Case aRecurso.origen
                        Case tipoOrigen.database
                            If Not (aRow(aRecurso.nombre).Equals(System.DBNull.Value)) And Not (aRow(aRecurso.nombre).Equals("")) Then
                                strObtenido += aRow(aRecurso.nombre)
                            End If
                        Case tipoOrigen.source
                            Dim m As Integer
                            For m = 0 To nombres.Length - 1
                                If UCase(nombres(m)) = UCase(aRecurso.nombre) Then
                                    strObtenido += values(m)
                                    Exit For
                                End If
                            Next
                    End Select
                    If (strObtenido <> "") Then
                        strObtenido += aRecurso.extension
                        aLink.valor = strResultado + strObtenido
                        aRes.links(j) = aLink
                        j += 1
                    End If
                Next
                nuevaFicha.resultados(i) = aRes
                primerValor = False
                i += 1
            Next



            Return nuevaFicha
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
            Return Nothing
        End Try
    End Function

    'ByVal nombres As String(), ByVal values As String()) As Ficha
    Public Function EjecutarPost(ByVal xmlConsulta As XmlDocument) As Ficha
        Dim strQuery As String
        Dim strNomConsulta As String
        Dim i As Integer, jj As Integer, kk As Integer, ll As Integer
        Dim aDS As Data.DataSet
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Dim primerValor As Boolean
        Dim m_nodelist As XmlNodeList, m_nodelist2 As XmlNodeList, m_nodelist3 As XmlNodeList, m_nodelist4 As XmlNodeList
        Dim m_node As XmlNode, m_node2 As XmlNode, m_node3 As XmlNode, m_node4 As XmlNode, m_node5 As XmlNode
        Dim nombres As String()
        Dim values As String()
        Try
            'Debemos encontrar los nombres de los par�metros
            strQuery = UCase(Me.sqlbase)
            m_nodelist = xmlConsulta.SelectNodes("/consulta/claves")
            ReDim nombres(m_nodelist.Count - 1)
            ReDim values(m_nodelist.Count - 1)
            For i = 0 To m_nodelist.Count - 1

                'Debemos averiguar si para cada clave corresponde un valor o varios
                m_node = m_nodelist(i)
                m_nodelist2 = m_node.SelectNodes("clave")
                For jj = 0 To m_nodelist2.Count - 1
                    Dim strNombre As String
                    m_node3 = m_nodelist2(jj)
                    m_node4 = m_node3.SelectSingleNode("name")
                    strNombre = m_node4.InnerText
                    m_nodelist3 = m_node3.SelectNodes("values")
                    For kk = 0 To m_nodelist3.Count - 1
                        m_node4 = m_nodelist3(kk)
                        m_nodelist4 = m_node4.SelectNodes("value")
                        If (m_nodelist4.Count = 1) Then
                            m_node5 = m_nodelist4(0)
                            Dim strValue As String
                            strValue = m_node5.InnerText
                            nombres(i) = strNombre
                            values(i) = strValue
                            strQuery = strQuery.Replace("%" + UCase(strNombre) + "%", "'" + strValue + "'")
                        Else
                            Dim strValues As String
                            strValues = ""
                            For ll = 0 To m_nodelist4.Count - 1
                                m_node5 = m_nodelist4(ll)
                                nombres(i) = strNombre

                                If (strValues.Length > 0) Then
                                    strValues += ","
                                Else
                                    values(i) = m_node5.InnerText
                                End If
                                strValues += "'" + m_node5.InnerText + "'"
                            Next ll
                            strQuery = strQuery.Replace("%" + UCase(strNombre) + "%", strValues)
                        End If
                    Next kk
                Next jj



            Next i

            'Hemos generado la query
            aDS = ExecuteQuery(strQuery, Me.nombre, Me.nombre)
            Dim nuevaFicha As New Ficha
            nuevaFicha.nombre = Me.nombre
            nuevaFicha.Titulo = Me.titulo
            nuevaFicha.descripcion = Me.descripcion
            nuevaFicha.xsl = Me.xsl
            ReDim nuevaFicha.resultados(aDS.Tables(0).Rows.Count - 1)
            i = 0
            primerValor = True
            For Each aRow In aDS.Tables(0).Rows
                'Resultados
                Dim aRes As New Resultado
                Dim aCampo As CampoConsulta
                Dim j As Integer
                Dim k As Integer
                j = 0
                ReDim aRes.valores(Me.listaCampos.Length - 1)
                For Each aCampo In Me.listaCampos
                    Dim aValor As New Valor
                    'El nombre del valor es la descripci�n (aqu� s�lo sacamos el alias)
                    aValor.nombre = aCampo.descripcion
                    Select Case aCampo.origen
                        Case tipoOrigen.database
                            aValor.valor = aRow(aCampo.nombre).ToString
                        Case tipoOrigen.source
                            For k = 0 To nombres.Length - 1
                                If UCase(nombres(k)) = UCase(aCampo.nombre) Then
                                    aValor.valor = values(k)
                                    Exit For
                                End If
                            Next
                    End Select
                    aRes.valores(j) = aValor
                    j += 1
                Next
                'Anidadas
                Dim aCons As ConsultaAnidada
                k = 0
                Dim numAnidadasUnica As Integer
                numAnidadasUnica = 0
                For Each aCons In Me.anidadas
                    If (aCons.tipo = ConsultaAnidada.tipoAnidada.fila) Then
                        ReDim Preserve aRes.anidadas(k)
                        Dim aNest As New Nested
                        Dim aMap As Mapping
                        Dim l As Integer, m As Integer
                        aNest.nombre = aCons.nombre
                        aNest.descripcion = aCons.descripcion
                        ReDim aNest.parametros(aCons.mappings.Length - 1)
                        l = 0
                        For Each aMap In aCons.mappings
                            Dim aVal As New Valor
                            aVal.nombre = aMap.hacia
                            If (aMap.origen = tipoOrigen.database) Then
                                aVal.valor = aRow(aMap.desde).ToString
                            Else
                                For m = 0 To nombres.Length - 1
                                    If UCase(nombres(m)) = UCase(aMap.desde) Then
                                        aVal.valor = values(m)
                                        Exit For
                                    End If
                                Next
                            End If
                            aNest.parametros(l) = aVal
                            l += 1
                        Next
                        aRes.anidadas(k) = aNest
                        k += 1
                    Else
                        If primerValor Then
                            'Anidadas




                            If (aCons.tipo = ConsultaAnidada.tipoAnidada.unica) Then

                                ReDim Preserve nuevaFicha.nested(numAnidadasUnica)

                                Dim aNest As New Nested
                                Dim aMap As Mapping
                                Dim l As Integer, m As Integer
                                aNest.nombre = aCons.nombre
                                aNest.descripcion = aCons.descripcion
                                ReDim aNest.parametros(aCons.mappings.Length - 1)
                                l = 0
                                For Each aMap In aCons.mappings
                                    Dim aVal As New Valor
                                    aVal.nombre = aMap.hacia
                                    If (aMap.origen = tipoOrigen.database) Then
                                        aVal.valor = aRow(aMap.desde).ToString
                                    Else
                                        For m = 0 To nombres.Length - 1
                                            If UCase(nombres(m)) = UCase(aMap.desde) Then
                                                aVal.valor = values(m)
                                                Exit For
                                            End If
                                        Next
                                    End If
                                    aNest.parametros(l) = aVal
                                    l += 1
                                Next
                                nuevaFicha.nested(numAnidadasUnica) = aNest
                                numAnidadasUnica += 1
                            End If


                        End If
                    End If

                Next

                'Recursos
                Dim aRecurso As Recurso
                j = 0
                ReDim aRes.links(Me.resources.Length - 1)
                For Each aRecurso In Me.resources
                    Dim aRecBase As RecursoBase
                    Dim aLink As New Link
                    Dim strResultado As String, strObtenido As String
                    aLink.tipo = aRecurso.tipo
                    aLink.descripcion = aRecurso.descripcion
                    strResultado = ""
                    'Obtenemos la base del recurso
                    For Each aRecBase In Me.resourceBases
                        If aRecurso.base = aRecBase.nombre Then
                            strResultado = aRecBase.path
                        End If
                    Next
                    strObtenido = ""
                    Select Case aRecurso.origen
                        Case tipoOrigen.database
                            If Not (aRow(aRecurso.nombre).Equals(System.DBNull.Value)) And Not (aRow(aRecurso.nombre).Equals("")) Then
                                strObtenido += aRow(aRecurso.nombre)
                            End If
                        Case tipoOrigen.source
                            Dim m As Integer
                            For m = 0 To nombres.Length - 1
                                If UCase(nombres(m)) = UCase(aRecurso.nombre) Then
                                    strObtenido += values(m)
                                    Exit For
                                End If
                            Next
                    End Select
                    If (strObtenido <> "") Then
                        strObtenido += aRecurso.extension
                        aLink.valor = strResultado + strObtenido
                        aRes.links(j) = aLink
                        j += 1
                    End If
                Next
                nuevaFicha.resultados(i) = aRes
                primerValor = False
                i += 1
            Next



            Return nuevaFicha
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

End Class
