Imports Microsoft.VisualBasic

Public Class ConsultaAnidada
#Region "Propiedades de la Clase"
    Public Enum tipoAnidada
        unica = 1
        fila = 2
    End Enum

    Private _tipo As tipoAnidada
    Private _nombre As String
    Private _descripcion As String
    Private _mappings() As Mapping

    Public Property tipo() As tipoAnidada
        Get
            tipo = _tipo
        End Get
        Set(ByVal aTipo As tipoAnidada)
            _tipo = aTipo
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDesc As String)
            _descripcion = aDesc
        End Set
    End Property

    Public Property mappings() As Mapping()
        Get
            mappings = _mappings
        End Get
        Set(ByVal aMappings As Mapping())
            _mappings = aMappings
        End Set
    End Property
#End Region
#Region "Funciones de la Clase"

#End Region
End Class
