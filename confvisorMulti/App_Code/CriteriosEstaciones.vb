Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service

Public Class CriteriosEstaciones
	Private _maxAltitud As String
	Private _minAltitud As String
	Private _maxTemperatura As String
	Private _minTemperatura As String
	Private _maxPrecicitacion As String
	Private _minPrecicitacion As String
	Private _colCriteriosEstaciones() As CriteriosEstaciones
	
#Region "Propiedades de la clase"
	Public Property maxAltitud() As String
		Get
			maxAltitud = _maxAltitud
		End Get
		Set(ByVal amaxAltitud As String)
			_maxAltitud = amaxAltitud
		End Set
	End Property

	Public Property minAltitud() As String
		Get
			minAltitud = _minAltitud
		End Get
		Set(ByVal aminAltitud As String)
			_minAltitud = aminAltitud
		End Set
	End Property

	Public Property maxTemperatura() As String
		Get
			maxTemperatura = _maxTemperatura
		End Get
		Set(ByVal amaxTemperatura As String)
			_maxTemperatura = amaxTemperatura
		End Set
	End Property

	Public Property minTemperatura() As String
		Get
			minTemperatura = _minTemperatura
		End Get
		Set(ByVal aminTemperatura As String)
			_minTemperatura = aminTemperatura
		End Set
	End Property

	Public Property maxPrecicitacion() As String
		Get
			maxPrecicitacion = _maxPrecicitacion
		End Get
		Set(ByVal amaxPrecicitacion As String)
			_maxPrecicitacion = amaxPrecicitacion
		End Set
	End Property

	Public Property minPrecicitacion() As String
		Get
			minPrecicitacion = _minPrecicitacion
		End Get
		Set(ByVal aminPrecicitacion As String)
			_minPrecicitacion = aminPrecicitacion
		End Set
	End Property

	Public Property criteriosEstaciones() As CriteriosEstaciones()
		Get
			criteriosEstaciones = _colCriteriosEstaciones
		End Get
		Set(ByVal colCriteriosEstaciones As CriteriosEstaciones())
			_colCriteriosEstaciones = colCriteriosEstaciones
		End Set
	End Property


#End Region
End Class
