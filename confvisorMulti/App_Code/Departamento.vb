Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service

Public Class Departamento
    Private _Id As Integer
    Private _nombre As String
    Private _IdPadre As Integer
    Private _colDepartamento() As Departamento
    Private _colServicios() As Servicio
    Private _nNombreTabla As String = "VIS_DEP_DEPARTAMENTOS"

#Region "Propiedades de la clase"
    Public Property Id() As Integer
        Get
            Id = _Id
        End Get
        Set(ByVal aId As Integer)
            _Id = aId
        End Set
    End Property
    Public Property IdPadre() As Integer
        Get
            IdPadre = _IdPadre
        End Get
        Set(ByVal aIdPadre As Integer)
            _IdPadre = aIdPadre
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property departamentos() As Departamento()
        Get
            departamentos = _colDepartamento
        End Get
        Set(ByVal colDepartamento As Departamento())
            _colDepartamento = colDepartamento
        End Set
    End Property

    Public Property servicios() As Servicio()
        Get
            servicios = _colServicios
        End Get
        Set(ByVal colServicios As Servicio())
            _colServicios = colServicios
        End Set
    End Property
#End Region

#Region "Funciones de la Clase"
    Public Function CargarDepartamentoSinServicios(ByVal id As Integer, ByVal codIdioma As String) As Departamento
        Dim strQuery As String

        Dim aDS As Data.DataSet
        Dim strTableName As String, strDatasetName As String
        Try

            strTableName = "VIS_DEP_DEPARTAMENTOS"
            strDatasetName = "VIS_DEP_DEPARTAMENTOS"

            strQuery = "SELECT VIS_DEP_DEPARTAMENTOS.DEP_CO_ID,VIS_DEP_DEPARTAMENTOS.DEP_CO_IDPADRE,CASE WHEN VIS_DED_DESC_DEPARTAMENTOS.DEP_DS_DESCRIPCION IS NULL THEN VIS_DEP_DEPARTAMENTOS.DEP_DS_DESCRIPCION ELSE VIS_DED_DESC_DEPARTAMENTOS.DEP_DS_DESCRIPCION END AS DEP_DS_DESCRIPCION" & _
           " FROM VIS_DEP_DEPARTAMENTOS LEFT JOIN VIS_DED_DESC_DEPARTAMENTOS ON  VIS_DEP_DEPARTAMENTOS.DEP_CO_ID= VIS_DED_DESC_DEPARTAMENTOS.DEP_CO_ID AND VIS_DED_DESC_DEPARTAMENTOS.IDI_DS_CODIGO='" & codIdioma & "'" & _
           " WHERE VIS_DEP_DEPARTAMENTOS.DEP_CO_ID='" & id & "'"


            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow
            For Each aRow In aDS.Tables(0).Rows
                Me.Id = aRow("DEP_CO_ID")
                Me.nombre = aRow("DEP_DS_DESCRIPCION")
                Me.IdPadre = IIf(aRow("DEP_CO_IDPADRE").Equals(System.DBNull.Value), 0, aRow("DEP_CO_IDPADRE"))
            Next

            'Obtenemos los hijos
            strTableName = "VIS_DEP_DEPARTAMENTOS"
            strDatasetName = "VIS_DEP_DEPARTAMENTOS"
            strQuery = "SELECT DEP_CO_ID FROM VIS_DEP_DEPARTAMENTOS WHERE DEP_CO_IDPADRE='" & id & "' order by VIS_DEP_DEPARTAMENTOS.DEP_NM_ORDEN ASC"
            'strQuery = "SELECT TEM_CO_ID FROM VIS_TEM_TEMAS WHERE TEM_CO_IDPADRE=" & id 
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim Preserve Me.departamentos(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim nDepartamento As New Departamento
                nDepartamento.CargarDepartamentoSinServicios(aRow("DEP_CO_ID"), codIdioma)
                Me.departamentos.SetValue(nDepartamento, i)
                i += 1
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    Public Function CargarDepartamento(ByVal id As Integer, ByVal strRoles As String, ByVal codIdioma As String) As Departamento
        Dim strQuery As String

        Dim aDS As Data.DataSet
        Dim strTableName As String, strDatasetName As String
        Try

            strTableName = "VIS_DEP_DEPARTAMENTOS"
            strDatasetName = "VIS_DEP_DEPARTAMENTOS"

            strQuery = "SELECT VIS_DEP_DEPARTAMENTOS.DEP_CO_ID,VIS_DEP_DEPARTAMENTOS.DEP_CO_IDPADRE,CASE WHEN VIS_DED_DESC_DEPARTAMENTOS.DEP_DS_DESCRIPCION IS NULL THEN VIS_DEP_DEPARTAMENTOS.DEP_DS_DESCRIPCION ELSE VIS_DED_DESC_DEPARTAMENTOS.DEP_DS_DESCRIPCION END AS DEP_DS_DESCRIPCION" & _
           " FROM VIS_DEP_DEPARTAMENTOS LEFT JOIN VIS_DED_DESC_DEPARTAMENTOS ON  VIS_DEP_DEPARTAMENTOS.DEP_CO_ID= VIS_DED_DESC_DEPARTAMENTOS.DEP_CO_ID AND VIS_DED_DESC_DEPARTAMENTOS.IDI_DS_CODIGO='" & codIdioma & "'" & _
           " WHERE VIS_DEP_DEPARTAMENTOS.DEP_CO_ID='" & id & "'"


            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow
            For Each aRow In aDS.Tables(0).Rows
                Me.Id = aRow("DEP_CO_ID")
                Me.nombre = aRow("DEP_DS_DESCRIPCION")
                Me.IdPadre = IIf(aRow("DEP_CO_IDPADRE").Equals(System.DBNull.Value), 0, aRow("DEP_CO_IDPADRE"))
            Next

            'Obtenemos los Servicios (s�lo los que disponemos permisos sobre ellos)
            strTableName = "VIS_SER_SERVICIOS"
            strDatasetName = "VIS_SER_SERVICIOS"

            strQuery = "select vis_ser_servicios.ser_co_id from vis_rol_roles_servicio,vis_ser_servicios, vis_hos_hosts " & _
                " where (vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
                " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                " and (vis_ser_servicios.dep_co_id=" + id.ToString + ") and(vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
                " order by(vis_ser_servicios.ser_ds_descripcion)"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim Me.servicios(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim nServicio As New Servicio
                nServicio.Cargar(aRow("SER_CO_ID"), codIdioma)
                Me.servicios(i) = nServicio
                i += 1
            Next
            'Obtenemos los hijos
            strTableName = "VIS_DEP_DEPARTAMENTOS"
            strDatasetName = "VIS_DEP_DEPARTAMENTOS"
            strQuery = "SELECT DEP_CO_ID FROM VIS_DEP_DEPARTAMENTOS WHERE DEP_CO_IDPADRE='" & id & "' order by VIS_DEP_DEPARTAMENTOS.DEP_NM_ORDEN ASC"
            'strQuery = "SELECT TEM_CO_ID FROM VIS_TEM_TEMAS WHERE TEM_CO_IDPADRE=" & id 
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim Preserve Me.departamentos(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim nDepartamento As New Departamento
                nDepartamento.CargarDepartamento(aRow("DEP_CO_ID"), strRoles, codIdioma)
                Me.departamentos.SetValue(nDepartamento, i)
                i += 1
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
   
#End Region

End Class
