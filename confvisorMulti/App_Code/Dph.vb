Imports Microsoft.VisualBasic

Public Class Dph
#Region "Propiedades de la Clase"
    Private _idEstudio As String
    Private _idTramo As String
    Private _objectId As String
    Private _estudio As String
    Private _cauce As String
    Private _tipoEstudio As String
    Private _disponible As String
    Private _organismo As String
    Private _provTramo As String
    Private _codProvTramo As String

    Public Property ObjectId() As String
        Get
            ObjectId = _objectId
        End Get
        Set(ByVal mObjectId As String)
            _objectId = mObjectId
        End Set
    End Property
    Public Property Cauce() As String
        Get
            Cauce = _cauce
        End Get
        Set(ByVal mCauce As String)
            _cauce = mCauce
        End Set
    End Property
    Public Property IdEstudio() As String
        Get
            IdEstudio = _idEstudio
        End Get
        Set(ByVal mIdEstudio As String)
            _idEstudio = mIdEstudio
        End Set
    End Property
    Public Property IdTramo() As String
        Get
            IdTramo = _idTramo
        End Get
        Set(ByVal mIdTramo As String)
            _idTramo = mIdTramo
        End Set
    End Property
    Public Property ProvTramo() As String
        Get
            ProvTramo = _provTramo
        End Get
        Set(ByVal aProvTramo As String)
            _provTramo = aProvTramo
        End Set
    End Property

    Public Property CodProvTramo() As String
        Get
            CodProvTramo = _codProvTramo
        End Get
        Set(ByVal aCodProvTramo As String)
            _codProvTramo = aCodProvTramo
        End Set
    End Property

    Public Property Estudio() As String
        Get
            estudio = _estudio
        End Get
        Set(ByVal aEstudio As String)
            _estudio = aEstudio
        End Set
    End Property
    Public Property TipoEstudio() As String
        Get
            TipoEstudio = _tipoEstudio
        End Get
        Set(ByVal mTipoEstudio As String)
            _tipoEstudio = mTipoEstudio
        End Set
    End Property
    Public Property Disponible() As String
        Get
            Disponible = _disponible
        End Get
        Set(ByVal mDisponible As String)
            _disponible = mDisponible
        End Set
    End Property
    Public Property Organismo() As String
        Get
            Organismo = _organismo
        End Get
        Set(ByVal mOrganismo As String)
            _organismo = mOrganismo
        End Set
    End Property
#End Region

End Class
