Imports Microsoft.VisualBasic

Public Class EmbalsesCX
#Region "Propiedades de la Clase"
    Private _ref_ceh As String
    Private _nombreEmbalse As String
    Private _corriente As String
    Private _propietario As String
    Private _nombreMunicipio As String


    Public Property ref_ceh() As String
        Get
            ref_ceh = _ref_ceh
        End Get
        Set(ByVal strref_ceh As String)
            _ref_ceh = strref_ceh
        End Set
    End Property

    Public Property nomEmbalse() As String
        Get
            nomEmbalse = _nombreEmbalse
        End Get
        Set(ByVal strNomEmbalse As String)
            _nombreEmbalse = strNomEmbalse
        End Set
    End Property


    Public Property corriente() As String
        Get
            corriente = _corriente
        End Get
        Set(ByVal strCorriente As String)
            _corriente = strCorriente
        End Set
    End Property


    Public Property propietario() As String
        Get
            propietario = _propietario
        End Get
        Set(ByVal strPropietario As String)
            _propietario = strPropietario
        End Set
    End Property



    Public Property nombreMunicipio() As String
        Get
            nombreMunicipio = _nombreMunicipio
        End Get
        Set(ByVal strNombreMunicipio As String)
            _nombreMunicipio = strNombreMunicipio
        End Set
    End Property

#End Region

End Class
