Imports Microsoft.VisualBasic
Imports Service

Public Class Enfermedad
    Private _Id As Long
    Private _nombre As String
#Region "Propiedades de la Clase"
    Public Property Id() As Long
        Get
            Id = _Id
        End Get
        Set(ByVal aId As Long)
            _Id = aId
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As Long) As Enfermedad
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "SAN_ENF_ENFERMEDADES"
            strDatasetName = "SAN_ENF_ENFERMEDADES"
            strQuery = "SELECT ENF_CO_ID, ENF_DS_DESCRIPCION FROM " & _
                        strTableName & " WHERE ENF_CO_ID='" & id & "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                Me.Id = IIf(aRow("ENF_CO_ID").Equals(System.DBNull.Value), "", aRow("ENF_CO_ID"))
                Me.nombre = IIf(aRow("ENF_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("ENF_DS_DESCRIPCION"))
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

End Class
