Imports Microsoft.VisualBasic
Imports Service
Public Class Especie
#Region "Propiedades de la Clase"
    Private _Id As String
    Private _idFamilia As String
    Private _nombre As String
    Public Property Id() As String
        Get
            Id = _Id
        End Get
        Set(ByVal aId As String)
            _Id = aId
        End Set
    End Property
    Public Property IdFamilia() As String
        Get
            IdFamilia = _idFamilia
        End Get
        Set(ByVal aId As String)
            _idFamilia = aId
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As String) As Especie
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        strTableName = "TC_ESPECIE_REGA"
        strDatasetName = "TC_ESPECIE_REGA"
        strQuery = "SELECT ID, ES_CO_FAMILIA, DESCRIPCION FROM DGG.TC_ESPECIE_REGA WHERE ID='" & id & "'"
        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

        For Each aRow In aDS.Tables(0).Rows
            Me.Id = IIf(aRow("ID").Equals(System.DBNull.Value), "", aRow("ID"))
            Me.IdFamilia = IIf(aRow("ES_CO_FAMILIA").Equals(System.DBNull.Value), "", aRow("ES_CO_FAMILIA"))
            Me.nombre = IIf(aRow("DESCRIPCION").Equals(System.DBNull.Value), "", aRow("DESCRIPCION"))
        Next
        Return Me
    End Function
#End Region



End Class
