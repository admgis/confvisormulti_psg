Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service

Public Class Estacion
	Private _Id As String
	Private _nombre As String
	Private _colEstaciones() As Estacion
	Private _nNombreTabla As String = "DGA.CH_ESTACIONES"

#Region "Propiedades de la clase"
	Public Property Id() As String
		Get
			Id = _Id
		End Get
		Set(ByVal aId As String)
			_Id = aId
		End Set
	End Property

	Public Property nombre() As String
		Get
			nombre = _nombre
		End Get
		Set(ByVal aNombre As String)
			_nombre = aNombre
		End Set
	End Property

	Public Property estaciones() As Estacion()
		Get
			estaciones = _colEstaciones
		End Get
		Set(ByVal colEstaciones As Estacion())
			_colEstaciones = colEstaciones
		End Set
	End Property


#End Region
End Class
