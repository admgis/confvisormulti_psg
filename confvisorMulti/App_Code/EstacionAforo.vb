Imports Microsoft.VisualBasic

Public Class EstacionAforo
#Region "Propiedades de la Clase"

    Private _id As String = ""
    Private _nombreConfederacion As String = ""
    Private _estado As String = ""
    Private _situacionEstacion As String = ""
    Private _codSituacionEstacion As String = ""
    Private _codEstacion As String = ""
    Private _nombreEstacion As String = ""
    Private _UTMX_H30_ETRS89 As Double
    Private _UTMY_H30_ETRS89 As Double
    Private _fotografia As String = ""
    Private _seccion As String = ""
    Private _plano As String = ""
    Private _codDemarcacion As String = ""


    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property nombreConfederacion() As String
        Get
            nombreConfederacion = _nombreConfederacion
        End Get
        Set(ByVal aNombreConfederacion As String)
            _nombreConfederacion = aNombreConfederacion
        End Set
    End Property

    Public Property estado() As String
        Get
            estado = _estado
        End Get
        Set(ByVal aEstado As String)
            _estado = aEstado
        End Set
    End Property

    Public Property situacionEstacion() As String
        Get
            situacionEstacion = _situacionEstacion
        End Get
        Set(ByVal aSituacionEstacion As String)
            _situacionEstacion = aSituacionEstacion
        End Set
    End Property

    Public Property codSituacionEstacion() As String
        Get
            codSituacionEstacion = _codSituacionEstacion
        End Get
        Set(ByVal aCodSituacionEstacion As String)
            _codSituacionEstacion = aCodSituacionEstacion
        End Set
    End Property

    Public Property codEstacion() As String
        Get
            codEstacion = _codEstacion
        End Get
        Set(ByVal aCodEstacion As String)
            _codEstacion = aCodEstacion
        End Set
    End Property

    Public Property nombreEstacion() As String
        Get
            nombreEstacion = _nombreEstacion
        End Get
        Set(ByVal aNombreEstacion As String)
            _nombreEstacion = aNombreEstacion
        End Set
    End Property

    Public Property UTMX_H30_ETRS89() As Double
        Get
            UTMX_H30_ETRS89 = _UTMX_H30_ETRS89
        End Get
        Set(ByVal aUTMX_H30_ETRS89 As Double)
            _UTMX_H30_ETRS89 = aUTMX_H30_ETRS89
        End Set
    End Property

    Public Property UTMY_H30_ETRS89() As Double
        Get
            UTMY_H30_ETRS89 = _UTMY_H30_ETRS89
        End Get
        Set(ByVal aUTMY_H30_ETRS89 As Double)
            _UTMY_H30_ETRS89 = aUTMY_H30_ETRS89
        End Set
    End Property

    Public Property fotografia() As String
        Get
            fotografia = _fotografia
        End Get
        Set(ByVal aFotografia As String)
            _fotografia = aFotografia
        End Set
    End Property

    Public Property seccion() As String
        Get
            seccion = _seccion
        End Get
        Set(ByVal aSeccion As String)
            _seccion = aSeccion
        End Set
    End Property

    Public Property plano() As String
        Get
            plano = _plano
        End Get
        Set(ByVal aPlano As String)
            _plano = aPlano
        End Set
    End Property

    Public Property CodDemarcacion() As String
        Get
            CodDemarcacion = _codDemarcacion
        End Get
        Set(ByVal mCodDem As String)
            _codDemarcacion = mCodDem
        End Set
    End Property
#End Region

End Class
