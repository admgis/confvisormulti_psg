Imports Microsoft.VisualBasic

Public Class ExcepcionAlerta
#Region "Propiedades de la Clase"


    Private _comarcas As Comarca()
    Private _municipios As Municipio()
    Private _subexplotaciones As Subexplotacion()

    Public Property comarcas() As Comarca()
        Get
            comarcas = _comarcas
        End Get
        Set(ByVal value As Comarca())
            _comarcas = value
        End Set
    End Property
    Public Property municipios() As Municipio()
        Get
            municipios = _municipios
        End Get
        Set(ByVal value As Municipio())
            _municipios = value
        End Set
    End Property
    Public Property subexplotaciones() As Subexplotacion()
        Get
            subexplotaciones = _subexplotaciones
        End Get
        Set(ByVal value As Subexplotacion())
            _subexplotaciones = value
        End Set
    End Property

#End Region
End Class
