Imports Microsoft.VisualBasic
Imports Service

Public Class Familia
#Region "Propiedades de la Clase"
    Private _id As String
    Private _descripcion As String
    Private _especies As Especie()

    Public Property id() As String
        Get
            id = _id
        End Get
        Set(ByVal aId As String)
            _id = aId
        End Set
    End Property
    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDesc As String)
            _descripcion = aDesc
        End Set
    End Property
    Public Property especies() As Especie()
        Get
            especies = _especies
        End Get
        Set(ByVal colEspecies As Especie())
            _especies = colEspecies
        End Set
    End Property

#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As String) As Familia
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow


        strTableName = "TC_FAMILIA_REGA"
        strDatasetName = "TC_FAMILIA_REGA"
        strQuery = "SELECT ID,DESCRIPCION FROM DGG.TC_FAMILIA_REGA WHERE ID='" & id & "'"
        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

        For Each aRow In aDS.Tables(0).Rows
            Me.id = IIf(aRow("ID").Equals(System.DBNull.Value), "", aRow("ID"))
            Me.descripcion = IIf(aRow("DESCRIPCION").Equals(System.DBNull.Value), "", aRow("DESCRIPCION"))
        Next
        'Cargamos las especies de la Familia
        strTableName = "TC_ESPECIE_REGA"
        strDatasetName = "TC_ESPECIE_REGA"
        strQuery = "SELECT ID FROM DGG.TC_ESPECIE_REGA WHERE ES_CO_FAMILIA='" & id & "'"
        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

        ReDim Me.especies(aDS.Tables(0).Rows.Count - 1)
        Dim i As Integer
        i = 0
        For Each aRow In aDS.Tables(0).Rows
            Dim aId As String
            aId = IIf(aRow("ID").Equals(System.DBNull.Value), "", aRow("ID"))
            Me.especies(i) = New Especie()
            Me.especies(i).Cargar(aId)
            i += 1
        Next
        Return Me

    End Function

#End Region
End Class
