Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service

Public Class FamiliaAlimentacion
	Private _Id As Integer
	Private _nombre As String
	Private _colFamilias() As FamiliaAlimentacion
	Private _nNombreTabla As String = "DGIAA.CD_FAM_FAMILIAS"

#Region "Propiedades de la clase"
	Public Property Id() As Integer
		Get
			Id = _Id
		End Get
		Set(ByVal aId As Integer)
			_Id = aId
		End Set
	End Property

	Public Property nombre() As String
		Get
			nombre = _nombre
		End Get
		Set(ByVal aNombre As String)
			_nombre = aNombre
		End Set
	End Property

	Public Property familiasAlimentacion() As FamiliaAlimentacion()
		Get
			familiasAlimentacion = _colFamilias
		End Get
		Set(ByVal colFamilias As FamiliaAlimentacion())
			_colFamilias = colFamilias
		End Set
	End Property


#End Region

End Class
