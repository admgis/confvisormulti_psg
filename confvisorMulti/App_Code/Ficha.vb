Imports Microsoft.VisualBasic

Public Class Ficha
#Region "Propiedades de la Clase"
    Private _nombre As String
    Private _titulo As String
    Private _descripcion As String
    Private _xsl As String
    Private _resultados() As Resultado
    Private _nested() As Nested

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDesc As String)
            _descripcion = aDesc
        End Set
    End Property
    Public Property xsl() As String
        Get
            xsl = _xsl
        End Get
        Set(ByVal aXsl As String)
            _xsl = aXsl
        End Set
    End Property
    Public Property resultados() As Resultado()
        Get
            resultados = _resultados
        End Get
        Set(ByVal aRes As Resultado())
            _resultados = aRes
        End Set
    End Property
    Public Property nested() As Nested()
        Get
            nested = _nested
        End Get
        Set(ByVal aNest As Nested())
            _nested = aNest
        End Set
    End Property
    Public Property Titulo() As String
        Get
            Titulo = _titulo
        End Get
        Set(ByVal value As String)
            _titulo = value
        End Set
    End Property
#End Region




End Class
