Imports Microsoft.VisualBasic

Public Class FichaAlerta
    Private _descripcion As String
    Private _identificador As String
    Private _focos() As PuntoInfeccion
    Private _anillos() As Anillo
#Region "Propiedades de la Clase"
    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property
    Public Property identificador() As String
        Get
            identificador = _identificador
        End Get
        Set(ByVal value As String)
            _identificador = value
        End Set
    End Property
    Public Property focos() As PuntoInfeccion()
        Get
            focos = _focos
        End Get
        Set(ByVal value As PuntoInfeccion())
            _focos = value
        End Set
    End Property
    Public Property anillos() As Anillo()
        Get
            anillos = _anillos
        End Get
        Set(ByVal value As Anillo())
            _anillos = value
        End Set
    End Property
#End Region
#Region "Funciones de la Clase"

#End Region

End Class
