Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service

Public Class Funcionalidad
    Private _Id As String
    Private _nombre As String
    Private _descripcion As String

#Region "Propiedades de la Clase"
    Public Property Id() As String
        Get
            Id = _Id
        End Get
        Set(ByVal aId As String)
            _Id = aId
        End Set
    End Property
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDescripcion As String)
            _descripcion = aDescripcion
        End Set
    End Property
#End Region

#Region "Funciones de la Clase"
    Public Function Cargar(ByVal idFuncionalidad As String) As Funcionalidad
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "VIS_FUN_FUNCIONALIDADES"
            strDatasetName = "VIS_FUN_FUNCIONALIDADES"
            strQuery = "SELECT FUN_CO_ID, FUN_DS_NOMBRE, FUN_DS_DESCRIPCION FROM VIS_FUN_FUNCIONALIDADES WHERE FUN_CO_ID=" & idFuncionalidad
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            For Each aRow In aDS.Tables(0).Rows
                Me.Id = aRow("FUN_CO_ID")
                Me.nombre = aRow("FUN_DS_NOMBRE")
                Me.descripcion = aRow("FUN_DS_DESCRIPCION")
            Next
            Return Me
        Catch
            Return Nothing
        End Try
    End Function
#End Region
End Class
