Imports Microsoft.VisualBasic

Public Class InfraestructuraGISPE

#Region "Propiedades de la Clase"
    Private _cod As String
    Private _id As String
    Private _nombre As String
    'Private _demarcacion As String
    'Private _provincia As String
    'Private _municipio As String
    Private _cauce As String
    Private _titular As String
    Private _tipoInf As String
    Public Property Cod() As String
        Get
            Cod = _cod
        End Get
        Set(ByVal mCod As String)
            _cod = mCod
        End Set
    End Property
    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property Cauce() As String
        Get
            Cauce = _cauce
        End Get
        Set(ByVal aCauce As String)
            _cauce = aCauce
        End Set
    End Property
    Public Property Titular() As String
        Get
            Titular = _titular
        End Get
        Set(ByVal value As String)
            _titular = value
        End Set
    End Property
    'Public Property Demarcacion() As String
    '    Get
    '        Demarcacion = _demarcacion
    '    End Get
    '    Set(ByVal mDemarcacion As String)
    '        _demarcacion = mDemarcacion
    '    End Set
    'End Property
    'Public Property Provincia() As String
    '    Get
    '        Provincia = _provincia
    '    End Get
    '    Set(ByVal aProvincia As String)
    '        _provincia = aProvincia
    '    End Set
    'End Property
    'Public Property Municipio() As String
    '    Get
    '        Municipio = _municipio
    '    End Get
    '    Set(ByVal aMunicipio As String)
    '        _municipio = aMunicipio
    '    End Set
    'End Property
    Public Property TipoInf() As String
        Get
            TipoInf = _tipoInf
        End Get
        Set(ByVal aTipoInf As String)
            _tipoInf = aTipoInf
        End Set
    End Property
#End Region

End Class