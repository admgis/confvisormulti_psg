﻿
' JCP
Public Class ItemArbolServicios
    Private _Tema As Tema
    Private _Servicio As Servicio

    Public Enum ItemArbolType
        Tema = 1
        Servicio = 2
    End Enum

    Private _mTipo As ItemArbolType

    Public ReadOnly Property Tipo() As ItemArbolType
        Get
            Tipo = _mTipo
        End Get
    End Property

    Public Property Tema() As Tema
        Get
            Tema = _Tema
        End Get
        Set(ByVal aTema As Tema)
            _Tema = aTema
            _mTipo = ItemArbolType.Tema
        End Set
    End Property


    Public Property Servicio() As Servicio
        Get
            Servicio = _Servicio
        End Get
        Set(ByVal aServicio As Servicio)
            _Servicio = aServicio
            _mTipo = ItemArbolType.servicio
        End Set
    End Property

End Class
