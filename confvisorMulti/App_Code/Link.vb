Imports Microsoft.VisualBasic

Public Class Link
#Region "Propiedades de la Clase"
    Private _tipo As String
    Private _valor As String
    Private _alias As String

    Public Property tipo() As String
        Get
            tipo = _tipo
        End Get
        Set(ByVal aTipo As String)
            _tipo = aTipo
        End Set
    End Property

    Public Property valor() As String
        Get
            valor = _valor
        End Get
        Set(ByVal value As String)
            _valor = value
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _alias
        End Get
        Set(ByVal aDesc As String)
            _alias = aDesc
        End Set
    End Property
#End Region
    
End Class
