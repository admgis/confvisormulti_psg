Imports Microsoft.VisualBasic

Public Class Mapping
#Region "Propiedades de la Clase"
    Private _source As Consulta.tipoOrigen
    Private _from As String
    Private _to As String

    Public Property origen() As Consulta.tipoOrigen
        Get
            origen = _source
        End Get
        Set(ByVal aSource As Consulta.tipoOrigen)
            _source = aSource
        End Set
    End Property

    Public Property desde() As String
        Get
            desde = _from
        End Get
        Set(ByVal aFrom As String)
            _from = aFrom
        End Set
    End Property

    Public Property hacia() As String
        Get
            hacia = _to
        End Get
        Set(ByVal aTo As String)
            _to = aTo
        End Set
    End Property
#End Region
End Class
