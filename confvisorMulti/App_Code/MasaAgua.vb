Imports Microsoft.VisualBasic

Public Class MasaAgua

#Region "Propiedades de la Clase"

    Private _codMSBTDM As String
    Private _nombre As String

    Public Property CodMSBTDM() As String
        Get
            CodMSBTDM = _codMSBTDM
        End Get
        Set(ByVal mCodMSBTDM As String)
            _codMSBTDM = mCodMSBTDM
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
#End Region


End Class
