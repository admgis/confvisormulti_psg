Imports Microsoft.VisualBasic
Imports Service

Public Class Municipio
#Region "Propiedades de la Clase"
    Private _id As Integer
    Private _idProv As Integer
    Private _idString As String
    Private _idProvString As String
    Private _nombre As String
    Public Property Id() As Integer
        Get
            Id = _id
        End Get
        Set(ByVal mId As Integer)
            _id = mId
        End Set
    End Property
    Public Property IdProv() As Integer
        Get
            IdProv = _idProv
        End Get
        Set(ByVal mIdProv As Integer)
            _idProv = mIdProv
        End Set
    End Property
    Public Property IdString() As String
        Get
            IdString = _idString
        End Get
        Set(ByVal mIdString As String)
            _idString = mIdString
        End Set
    End Property
    Public Property IdProvString() As String
        Get
            IdProvString = _idProvString
        End Get
        Set(ByVal mIdProvString As String)
            _idProvString = mIdProvString
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As Integer) As Municipio
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaMunicipios")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdMunicipios")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipios")
            strCampoIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvincias")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + "," + strCampoIdProvincia + " FROM " + strTabla + " WHERE " + strCampoId + "='" + id.ToString + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)

            For Each aRow In aDS.Tables(0).Rows

                Me.Id = aRow(strCampoId)
                Me.IdProv = aRow(strCampoIdProvincia)
                Me.nombre = aRow(strCampoNombre)
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
#End Region

End Class
