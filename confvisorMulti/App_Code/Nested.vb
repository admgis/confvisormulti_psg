Imports Microsoft.VisualBasic

Public Class Nested
#Region "Propiedades de la Clase"
    Private _nombre As String
    Private _alias As String
    Private _parametros() As Valor

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _alias
        End Get
        Set(ByVal aDesc As String)
            _alias = aDesc
        End Set
    End Property

    Public Property parametros() As Valor()
        Get
            parametros = _parametros
        End Get
        Set(ByVal value As Valor())
            _parametros = value
        End Set
    End Property

#End Region

End Class
