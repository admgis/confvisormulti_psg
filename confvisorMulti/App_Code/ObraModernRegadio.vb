Imports Microsoft.VisualBasic

Public Class ObraModernRegadio
#Region "Propiedades de la Clase"
    Private _id As String
    Private _idDemarcacion As String
    Private _nombre As String
    Private _nombreLargo As String

    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property nombreLargo() As String
        Get
            nombreLargo = _nombreLargo
        End Get
        Set(ByVal value As String)
            _nombreLargo = value
        End Set
    End Property
    #End Region

End Class
