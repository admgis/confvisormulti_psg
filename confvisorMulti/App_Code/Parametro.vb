Imports Microsoft.VisualBasic

Public Class Parametro
    Private _nombre As String
    Private _query As String

    Public Property Nombre() As String
        Get
            Nombre = _nombre
        End Get
        Set(ByVal mNombre As String)
            _nombre = mNombre
        End Set
    End Property
    Public Property Query() As String
        Get
            Query = _query
        End Get
        Set(ByVal mQuery As String)
            _query = mQuery
        End Set
    End Property

End Class
