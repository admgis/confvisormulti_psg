Imports Microsoft.VisualBasic

Public Class Piezometro
   

#Region "Propiedades de la Clase"
    Private _id As String
    Private _nombre As String
    Private _descripcion As String
    Private _idProvincia As Integer
    Private _provincia As String
    Private _idMunicipio As Integer
    Private _municipio As String
    Private _idMasa As String
    Private _masa As String
    Private _idDemar As String
    Private _demarcacion As String
    Private _cota As Double
    Private _fechaInicio As String
    Private _numMedidas As Integer
    Private _xUtmET As Double
    Private _yUtmET As Double
    Private _xUtm30 As Double
    Private _yUtm30 As Double
    Private _urlFicha As String
    Private _nivel As Double
    Private _fechaNivel As String
    Private _profundidad As String
    Private _idUH As String
    Private _UH As String



    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Descripcion = _descripcion
        End Get
        Set(ByVal aDescripcion As String)
            _descripcion = aDescripcion
        End Set
    End Property    
    Public Property IdDemar() As String
        Get
            IdDemar = _idDemar
        End Get
        Set(ByVal mIdDemar As String)
            _idDemar = mIdDemar
        End Set
    End Property
    Public Property Demarcacion() As String
        Get
            Demarcacion = _demarcacion
        End Get
        Set(ByVal mDemarcacion As String)
            _demarcacion = mDemarcacion
        End Set
    End Property
    Public Property IdProvincia() As Integer
        Get
            IdProvincia = _idProvincia
        End Get
        Set(ByVal midProvincia As Integer)
            _idProvincia = midProvincia
        End Set
    End Property
    Public Property Provincia() As String
        Get
            Provincia = _provincia
        End Get
        Set(ByVal mProvincia As String)
            _provincia = mProvincia
        End Set
    End Property
    Public Property IdMunicipio() As Integer
        Get
            IdMunicipio = _idMunicipio
        End Get
        Set(ByVal midMunicipio As Integer)
            _idMunicipio = midMunicipio
        End Set
    End Property
    Public Property Municipio() As String
        Get
            Municipio = _municipio
        End Get
        Set(ByVal mMunicipio As String)
            _municipio = mMunicipio
        End Set
    End Property
    Public Property FechaInicio() As String
        Get
            FechaInicio = _fechaInicio
        End Get
        Set(ByVal mFechaInicio As String)
            _fechaInicio = mFechaInicio
        End Set
    End Property
    Public Property IdUH() As String
        Get
            IdUH = _idUH
        End Get
        Set(ByVal midUH As String)
            _idUH = midUH
        End Set
    End Property
    Public Property UH() As String
        Get
            UH = _UH
        End Get
        Set(ByVal mUH As String)
            _UH = mUH
        End Set
    End Property
    Public Property Cota() As Double
        Get
            Cota = _cota
        End Get
        Set(ByVal mCota As Double)
            _cota = mCota
        End Set
    End Property
    Public Property IdMasa() As String
        Get
            IdMasa = _idMasa
        End Get
        Set(ByVal midMasa As String)
            _idMasa = midMasa
        End Set
    End Property
    Public Property Masa() As String
        Get
            Masa = _masa
        End Get
        Set(ByVal mMasa As String)
            _masa = mMasa
        End Set
    End Property
    Public Property NumMedidas() As Integer
        Get
            NumMedidas = _numMedidas
        End Get
        Set(ByVal mNumMedidas As Integer)
            _numMedidas = mNumMedidas
        End Set
    End Property
    Public Property XUtmET() As Double
        Get
            XUtmET = _xUtmET
        End Get
        Set(ByVal mXUtmET As Double)
            _xUtmET = mXUtmET
        End Set
    End Property
    Public Property YUtmET() As Double
        Get
            YUtmET = _yUtmET
        End Get
        Set(ByVal mYUtmET As Double)
            _yUtmET = mYUtmET
        End Set
    End Property

    Public Property XUtm30() As Double
        Get
            XUtm30 = _xUtm30
        End Get
        Set(ByVal mXUtm30 As Double)
            _xUtm30 = mXUtm30
        End Set
    End Property
    Public Property YUtm30() As Double
        Get
            YUtm30 = _yUtm30
        End Get
        Set(ByVal mYUtm30 As Double)
            _yUtm30 = mYUtm30
        End Set
    End Property
    Public Property URLFicha() As String
        Get
            URLFicha = _urlFicha
        End Get
        Set(ByVal mURLFicha As String)
            _urlFicha = mURLFicha
        End Set
    End Property
    Public Property Nivel() As Double
        Get
            Nivel = _nivel
        End Get
        Set(ByVal mNivel As Double)
            _nivel = mNivel
        End Set
    End Property
    Public Property FechaNivel() As String
        Get
            FechaNivel = _fechaNivel
        End Get
        Set(ByVal mFechaNivel As String)
            _fechaNivel = mFechaNivel
        End Set
    End Property
    Public Property Profundidad() As String
        Get
            Profundidad = _profundidad
        End Get
        Set(ByVal mProfundidad As String)
            _profundidad = mProfundidad
        End Set
    End Property
#End Region




End Class
