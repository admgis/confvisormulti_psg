Imports Microsoft.VisualBasic

Public Class Plantilla
    Private _id As Long
    Private _idServicio As Long
    Private _nombre As String
    Private _mxd As String

#Region "Propiedades de la Clase"
    Public Property id() As Long
        Get
            id = _id
        End Get
        Set(ByVal aId As Long)
            _id = aId
        End Set
    End Property
    Public Property idServicio() As Long
        Get
            idServicio = _idServicio
        End Get
        Set(ByVal aId As Long)
            _idServicio = aId
        End Set
    End Property
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property mxd() As String
        Get
            mxd = _mxd
        End Get
        Set(ByVal aMxd As String)
            _mxd = aMxd
        End Set
    End Property
#End Region
End Class
