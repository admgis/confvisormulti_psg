﻿Imports Microsoft.VisualBasic

Public Class Playa

#Region "Propiedades de la Clase"

    Private _id As String
    Private _nombre As String
    Private _provincia As String
    Private _municipio As String
    Private _codProvincia As String
    Private _codMunicipio As String
    Private _codCCAA As String
    Private _xUtmET As Double
    Private _yUtmET As Double

    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property provincia() As String
        Get
            provincia = _provincia
        End Get
        Set(ByVal aProvincia As String)
            _provincia = aProvincia
        End Set
    End Property

    Public Property municipio() As String
        Get
            municipio = _municipio
        End Get
        Set(ByVal aMunicipio As String)
            _municipio = aMunicipio
        End Set
    End Property
    Public Property codProvincia() As String
        Get
            codProvincia = _codProvincia
        End Get
        Set(ByVal aCodProvincia As String)
            _codProvincia = acodProvincia
        End Set
    End Property

    Public Property codMunicipio() As String
        Get
            codMunicipio = _codMunicipio
        End Get
        Set(ByVal aCodMunicipio As String)
            _codMunicipio = aCodMunicipio
        End Set
    End Property
    Public Property codCCAA() As String
        Get
            codCCAA = _codCCAA
        End Get
        Set(ByVal acodCCAA As String)
            _codCCAA = acodCCAA
        End Set
    End Property
    Public Property XUtmET() As Double
        Get
            XUtmET = _xUtmET
        End Get
        Set(ByVal mXUtmET As Double)
            _xUtmET = mXUtmET
        End Set
    End Property
    Public Property YUtmET() As Double
        Get
            YUtmET = _yUtmET
        End Get
        Set(ByVal mYUtmET As Double)
            _yUtmET = mYUtmET
        End Set
    End Property

#End Region

End Class
