Imports Microsoft.VisualBasic

Public Class PresaAdmtivo
#Region "Propiedades de la Clase"
    Private _id As String
    Private _idDemarcacion As String
    Private _nombre As String
    Private _fase As String
    Private _titular As String
    Private _demarcacion As String
    Private _provincia As String
    Private _IdProvincia As String
    Private _tipologia As String
    Private _cauce As String
    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property cauce() As String
        Get
            cauce = _cauce
        End Get
        Set(ByVal aCauce As String)
            _cauce = aCauce
        End Set
    End Property
    Public Property Fase() As String
        Get
            Fase = _fase
        End Get
        Set(ByVal mFase As String)
            _fase = mFase
        End Set
    End Property
    Public Property Titular() As String
        Get
            Titular = _titular
        End Get
        Set(ByVal aTitular As String)
            _titular = aTitular
        End Set
    End Property
    Public Property Demarcacion() As String
        Get
            Demarcacion = _demarcacion
        End Get
        Set(ByVal mDemarcacion As String)
            _demarcacion = mDemarcacion
        End Set
    End Property
    Public Property IdDemarcacion() As String
        Get
            IdDemarcacion = _idDemarcacion
        End Get
        Set(ByVal mIdDemarcacion As String)
            _idDemarcacion = mIdDemarcacion
        End Set
    End Property
    Public Property Provincia() As String
        Get
            Provincia = _provincia
        End Get
        Set(ByVal aProvincia As String)
            _provincia = aProvincia
        End Set
    End Property
    Public Property IdProvincia() As String
        Get
            IdProvincia = _IdProvincia
        End Get
        Set(ByVal aIdProvincia As String)
            _IdProvincia = aIdProvincia
        End Set
    End Property
    Public Property Tipologia() As String
        Get
            Tipologia = _tipologia
        End Get
        Set(ByVal aTipologia As String)
            _tipologia = aTipologia
        End Set
    End Property
#End Region

End Class
