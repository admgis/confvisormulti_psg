Imports Microsoft.VisualBasic

Public Class Provincia
#Region "Propiedades de la Clase"
    Private _id As Integer
    Private _idString As String
    Private _nombre As String
    Public Property Id() As Integer
        Get
            Id = _id
        End Get
        Set(ByVal mId As Integer)
            _id = mId
        End Set
    End Property
    Public Property IdString() As String
        Get
            IdString = _idString
        End Get
        Set(ByVal mIdString As String)
            _idString = mIdString
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

#End Region
#Region "Funciones de la Clase"

#End Region
End Class
