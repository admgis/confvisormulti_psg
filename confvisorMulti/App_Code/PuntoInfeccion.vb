Imports Microsoft.VisualBasic
Imports Service

Public Class PuntoInfeccion
    Private _id As Long
    Private _idAlerta As Long
    Private _idExplotacion As String
    Private _latitud As Double
    Private _longitud As Double
    Private _descripcion As String
    Private _idEspecie As String
    Private _especie As String
    Private _idExplotacionSinEspecie As String
    Private _censo As String

#Region "Propiedades de la Clase"
    Public Property id() As Long
        Get
            id = _id
        End Get
        Set(ByVal value As Long)
            _id = value
        End Set
    End Property
    Public Property idAlerta() As Long
        Get
            idAlerta = _idAlerta
        End Get
        Set(ByVal value As Long)
            _idAlerta = value
        End Set
    End Property
    Public Property idExplotacion() As String
        Get
            idExplotacion = _idExplotacion
        End Get
        Set(ByVal value As String)
            _idExplotacion = value
        End Set
    End Property
    Public Property latitud() As Double
        Get
            latitud = _latitud
        End Get
        Set(ByVal value As Double)
            _latitud = value
        End Set
    End Property

    Public Property longitud() As Double
        Get
            longitud = _longitud
        End Get
        Set(ByVal value As Double)
            _longitud = value
        End Set
    End Property
    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property
    Public Property idEspecie() As String
        Get
            idEspecie = _idEspecie
        End Get
        Set(ByVal value As String)
            _idEspecie = value
        End Set
    End Property
    Public Property especie() As String
        Get
            especie = _especie
        End Get
        Set(ByVal value As String)
            _especie = value
        End Set
    End Property

    Public Property idExplotacionSinEspecie() As String
        Get
            idExplotacionSinEspecie = _idExplotacionSinEspecie
        End Get
        Set(ByVal value As String)
            _idExplotacionSinEspecie = value
        End Set
    End Property

    Public Property censo() As String
        Get
            censo = _censo
        End Get
        Set(ByVal value As String)
            _censo = value
        End Set
    End Property

#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As Long) As PuntoInfeccion
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "SAN_PTO_PUNTOS_ALERTAS"
            strDatasetName = "SAN_PTO_PUNTOS_ALERTAS"
            strQuery = "SELECT PTO_CO_ID, ALE_CO_ID, PTO_CO_SUBEXPLOTACION, PTO_NM_LATITUD, PTO_NM_LONGITUD,PTO_DS_IDENTIFICACION " & _
                " , SE_CE_EXPLOT,SE_CO_ESPECIE,ESPECIE," & _
                " SUM(COALESCE(CNV.CN_CENSO,0) )  + SUM(COALESCE(CVAC.CN_MACHOS_12M,0)) + SUM(COALESCE(CVAC.CN_HEMBRAS_12M,0)) + " & _
                " SUM(COALESCE(CVAC.CN_MACHOS_12_24M,0)) + SUM(COALESCE(CVAC.CN_HEMBRAS_12_24M,0)) + SUM(COALESCE(CVAC.CN_MACHOS_24M,0)) + SUM(COALESCE(CVAC.CN_HEMBRAS_24M,0)) CENSO FROM " & _
                strTableName & _
                " LEFT JOIN DGG.REGA_MV_UBICACION_ESPECIES  ON DGG.REGA_MV_UBICACION_ESPECIES.SE_CO_SUBEXPLOT =  PTO_CO_SUBEXPLOTACION " & _
                " LEFT JOIN DGG.REGA_MV_T_CENSONOVAC CNV ON CNV.CN_CE_SUBEXP =  DGG.REGA_MV_UBICACION_ESPECIES.SE_CE_EXPLOT AND CNV.CN_CO_ESPECIE = DGG.REGA_MV_UBICACION_ESPECIES.SE_CO_ESPECIE " & _
                " LEFT JOIN DGG.REGA_MV_T_CENSOVAC CVAC ON CVAC.CN_CE_SUBEXP =  DGG.REGA_MV_UBICACION_ESPECIES.SE_CE_EXPLOT AND CVAC.CN_CO_ESPECIE = DGG.REGA_MV_UBICACION_ESPECIES.SE_CO_ESPECIE " & _
                " WHERE PTO_CO_ID='" & id & "'  GROUP BY PTO_CO_ID, ALE_CO_ID, PTO_CO_SUBEXPLOTACION, SE_CE_EXPLOT,PTO_NM_LATITUD, PTO_NM_LONGITUD,PTO_DS_IDENTIFICACION, SE_CO_ESPECIE, ESPECIE"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                Me.id = IIf(aRow("PTO_CO_ID").Equals(System.DBNull.Value), "", aRow("PTO_CO_ID"))
                Me.idAlerta = IIf(aRow("ALE_CO_ID").Equals(System.DBNull.Value), "", aRow("ALE_CO_ID"))
                Me.idExplotacion = IIf(aRow("PTO_CO_SUBEXPLOTACION").Equals(System.DBNull.Value), "", aRow("PTO_CO_SUBEXPLOTACION"))
                Me.latitud = IIf(aRow("PTO_NM_LATITUD").Equals(System.DBNull.Value), "", aRow("PTO_NM_LATITUD"))
                Me.longitud = IIf(aRow("PTO_NM_LONGITUD").Equals(System.DBNull.Value), "", aRow("PTO_NM_LONGITUD"))
                Me.descripcion = IIf(aRow("PTO_DS_IDENTIFICACION").Equals(System.DBNull.Value), "", aRow("PTO_DS_IDENTIFICACION"))
                Me.idEspecie = IIf(aRow("SE_CO_ESPECIE").Equals(System.DBNull.Value), "", aRow("SE_CO_ESPECIE"))
                Me.especie = IIf(aRow("ESPECIE").Equals(System.DBNull.Value), "", aRow("ESPECIE"))
                Me.idExplotacionSinEspecie = IIf(aRow("SE_CE_EXPLOT").Equals(System.DBNull.Value), "", aRow("SE_CE_EXPLOT"))
                Me.censo = IIf(aRow("CENSO").Equals(System.DBNull.Value), "", aRow("CENSO"))
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
#End Region
    
End Class
