Imports Microsoft.VisualBasic
Imports Service

Public Class Radio
    Private _id As Long
    Private _idAlerta As Long
    Private _longitud As Double
    Private _color As String
    Private _excepciones As ExcepcionAlerta
#Region "Propiedades de la Clase"
    Public Property id() As Long
        Get
            id = _id
        End Get
        Set(ByVal aId As Long)
            _id = aId
        End Set
    End Property
    Public Property idAlerta() As Long
        Get
            idAlerta = _idAlerta
        End Get
        Set(ByVal aId As Long)
            _idAlerta = aId
        End Set
    End Property
    Public Property longitud() As Double
        Get
            longitud = _longitud
        End Get
        Set(ByVal aLong As Double)
            _longitud = aLong
        End Set
    End Property

    Public Property color() As String
        Get
            color = _color
        End Get
        Set(ByVal aColor As String)
            _color = aColor
        End Set
    End Property

    Public Property excepciones() As ExcepcionAlerta
        Get
            excepciones = _excepciones
        End Get
        Set(ByVal value As ExcepcionAlerta)
            _excepciones = value
        End Set
    End Property
#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal id As Long) As Radio
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Dim z As Integer
        Try
            strTableName = "SAN_RAD_RADIOS_AFECCION"
            strDatasetName = "SAN_RAD_RADIOS_AFECCION"
            strQuery = "SELECT ALE_CO_ID, RAD_CO_ID, RAD_NM_LONGITUD,RAD_DS_COLOR FROM " & _
                        strTableName & " WHERE RAD_CO_ID='" & id & "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                Me.id = IIf(aRow("RAD_CO_ID").Equals(System.DBNull.Value), "", aRow("RAD_CO_ID"))
                Me.idAlerta = IIf(aRow("ALE_CO_ID").Equals(System.DBNull.Value), "", aRow("ALE_CO_ID"))
                Me.longitud = IIf(aRow("RAD_NM_LONGITUD").Equals(System.DBNull.Value), "", aRow("RAD_NM_LONGITUD"))
                Me.color = IIf(aRow("RAD_DS_COLOR").Equals(System.DBNull.Value), "", aRow("RAD_DS_COLOR"))
            Next
            Me.excepciones = New ExcepcionAlerta()
            For z = 1 To 3
                'Obtenemos ahora las excepciones para el radio de la comarca
                strTableName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
                strDatasetName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
                strQuery = "SELECT REX_CO_ID_EXCEPCION,TEX_CO_ID FROM " & _
                            strTableName & " WHERE RAD_CO_ID='" & id & "' AND TEX_CO_ID='" + z.ToString() + "'"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                If (z = 1) Then
                    If (aDS.Tables(0).Rows.Count = 0) Then
                        Me.excepciones.comarcas = Nothing
                        'Exit For
                    Else
                        ReDim Me.excepciones.comarcas(aDS.Tables(0).Rows.Count - 1)
                    End If
                ElseIf (z = 2) Then
                    If (aDS.Tables(0).Rows.Count = 0) Then
                        Me.excepciones.municipios = Nothing
                        ' Exit For
                    Else
                        ReDim Me.excepciones.municipios(aDS.Tables(0).Rows.Count - 1)
                    End If
                Else
                    If (aDS.Tables(0).Rows.Count = 0) Then
                        Me.excepciones.subexplotaciones = Nothing
                        ' Exit For
                    Else
                        ReDim Me.excepciones.subexplotaciones(aDS.Tables(0).Rows.Count - 1)
                    End If
                End If
                Dim i As Integer
                i = 0
                For Each aRow In aDS.Tables(0).Rows
                    If (z = 1) Then
                        Dim aComarca As Comarca
                        aComarca = New Comarca()
                        aComarca.Cargar(aRow("REX_CO_ID_EXCEPCION"))
                        Me.excepciones.comarcas(i) = aComarca
                        i += 1
                    ElseIf (z = 2) Then
                        Dim aMunicipio As Municipio
                        aMunicipio = New Municipio()
                        aMunicipio.Cargar(aRow("REX_CO_ID_EXCEPCION"))
                        Me.excepciones.municipios(i) = aMunicipio
                        i += 1
                    Else
                        Dim aSubexp As Subexplotacion
                        aSubexp = New Subexplotacion()
                        aSubexp.Cargar(aRow("REX_CO_ID_EXCEPCION"))
                        Me.excepciones.subexplotaciones(i) = aSubexp
                        i += 1
                    End If
                Next
            Next z
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region
End Class
