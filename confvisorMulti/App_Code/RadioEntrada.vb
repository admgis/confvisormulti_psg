Imports Microsoft.VisualBasic
Imports Service

Public Class RadioEntrada
#Region "Propiedades de la Clase"
    Private _id As String
    Private _excepciones As ExcepcionAlerta
    'Private _excepciones As String()

    Public Property id() As String
        Get
            id = _id
        End Get
        Set(ByVal value As String)
            _id = value
        End Set
    End Property

    Public Property excepciones() As ExcepcionAlerta
        Get
            excepciones = _excepciones
        End Get
        Set(ByVal value As ExcepcionAlerta)
            _excepciones = value
        End Set
    End Property
#End Region
#Region "Funciones de la Clase"
    Public Function modificarRadio() As Boolean
        'Borramos todas las entradas correspondientes a las excepciones 
        'y luego a�adimos las nuevas que se han generado
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim i As Integer
        Try
           
            'Obtenemos ahora las excepciones para el radio de la comarca
            strTableName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
            strDatasetName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
            strQuery = "DELETE FROM " & _
                        strTableName & " WHERE RAD_CO_ID='" & Me.id & "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            For i = 0 To Me.excepciones.comarcas.Length - 1
                strTableName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
                strDatasetName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
                strQuery = "INSERT INTO " & _
                            strTableName & " (RAD_CO_ID,REX_CO_ID_EXCEPCION,TEX_CO_ID) VALUES ('" & Me.id & "','" & Me.excepciones.comarcas(i).Id & "','1')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next

            For i = 0 To Me.excepciones.municipios.Length - 1
                strTableName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
                strDatasetName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
                strQuery = "INSERT INTO " & _
                            strTableName & " (RAD_CO_ID,REX_CO_ID_EXCEPCION,TEX_CO_ID) VALUES ('" & Me.id & "','" & Me.excepciones.municipios(i).Id & "','2')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next

            For i = 0 To Me.excepciones.subexplotaciones.Length - 1
                strTableName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
                strDatasetName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
                strQuery = "INSERT INTO " & _
                            strTableName & " (RAD_CO_ID,REX_CO_ID_EXCEPCION,TEX_CO_ID) VALUES ('" & Me.id & "','" & Me.excepciones.subexplotaciones(i).id & "','3')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region
End Class
