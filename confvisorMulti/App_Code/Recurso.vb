Imports Microsoft.VisualBasic

Public Class Recurso
#Region "Propiedades de la clase"
    
    Private _tipo As String
    Private _origen As Consulta.tipoOrigen
    Private _base As String
    Private _extension As String
    Private _nombre As String
    Private _alias As String

    Public Property tipo() As String
        Get
            tipo = _tipo
        End Get
        Set(ByVal aTipo As String)
            _tipo = aTipo
        End Set
    End Property

    Public Property origen() As Consulta.tipoOrigen
        Get
            origen = _origen
        End Get
        Set(ByVal aOrigen As Consulta.tipoOrigen)
            _origen = aOrigen
        End Set
    End Property

    Public Property base() As String
        Get
            base = _base
        End Get
        Set(ByVal aBase As String)
            _base = aBase
        End Set
    End Property

    Public Property extension() As String
        Get
            extension = _extension
        End Get
        Set(ByVal aExt As String)
            _extension = aExt
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _alias
        End Get
        Set(ByVal aDesc As String)
            _alias = aDesc
        End Set
    End Property
#End Region

End Class
