Imports Microsoft.VisualBasic

Public Class RecursoBase
#Region "Propiedades de la Clase"
    Private _nombre As String
    Private _path As String

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property path() As String
        Get
            path = _path
        End Get
        Set(ByVal aPath As String)
            _path = aPath
        End Set
    End Property
#End Region

End Class
