﻿Imports Microsoft.VisualBasic

Public Class Regadio
#Region "Propiedades de la Clase"
    Private _id As String
    Private _nombre As String
    Private _documento As String

    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property documento() As String
        Get
            documento = _documento
        End Get
        Set(ByVal value As String)
            _documento = value
        End Set
    End Property
#End Region
End Class
