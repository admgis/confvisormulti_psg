Imports Microsoft.VisualBasic

Public Class Resultado
#Region "Propiedades de la Clase"
    Private _valores() As Valor
    Private _nested() As Nested
    Private _links() As Link

    Public Property valores() As Valor()
        Get
            valores = _valores
        End Get
        Set(ByVal value As Valor())
            _valores = value
        End Set
    End Property

    Public Property anidadas() As Nested()
        Get
            anidadas = _nested
        End Get
        Set(ByVal aNest As Nested())
            _nested = aNest
        End Set
    End Property

    Public Property links() As Link()
        Get
            links = _links
        End Get
        Set(ByVal aLinks As Link())
            _links = aLinks
        End Set
    End Property

#End Region

End Class
