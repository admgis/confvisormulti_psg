Imports Microsoft.VisualBasic
Imports Service

Public Class SeleccionLista
    Private _Id As Integer
    Private _descripcion As String
    Private _fecha As String
    Private _definitionExp As String
    Private _codCapa As String
    Private _objectIds As String
    Private _codServicio As String
    Private _definitionExpFiltro As String
    Private _codCapaFiltro As String
    Private _codServicioFiltro As String


#Region "Propiedades de la Clase"
    Public Property Id() As Integer
        Get
            Id = _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Public Property fecha() As String
        Get
            fecha = _fecha
        End Get
        Set(ByVal value As String)
            _fecha = value
        End Set
    End Property


    Public Property definitionExp() As String
        Get
            definitionExp = _definitionExp
        End Get
        Set(ByVal value As String)
            _definitionExp = value
        End Set
    End Property

    Public Property objectIds() As String
        Get
            Return _objectIds
        End Get
        Set(ByVal value As String)
            _objectIds = value
        End Set
    End Property

    Public Property codCapa() As String
        Get
            codCapa = _codCapa
        End Get
        Set(ByVal value As String)
            _codCapa = value
        End Set
    End Property

    Public Property codServicio() As String
        Get
            codServicio = _codServicio
        End Get
        Set(ByVal value As String)
            _codServicio = value
        End Set
    End Property

    Public Property codCapaFiltro() As String
        Get
            codCapaFiltro = _codCapaFiltro
        End Get
        Set(ByVal value As String)
            _codCapaFiltro = value
        End Set
    End Property

    Public Property codServicioFiltro() As String
        Get
            codServicioFiltro = _codServicioFiltro
        End Get
        Set(ByVal value As String)
            _codServicioFiltro = value
        End Set
    End Property
    Public Property definitionExpFiltro() As String
        Get
            definitionExpFiltro = _definitionExpFiltro
        End Get
        Set(ByVal value As String)
            _definitionExpFiltro = value
        End Set
    End Property
#End Region

#Region "Funciones de la Clase"
    Public Function Cargar(ByVal idTematico As Integer, ByVal usuario As String) As SeleccionLista
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            '            strTableName = "VIS_TAD_TEMATICOSDEMANDA"
            '            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            '
            'strQuery = "SELECT TAD_CO_ID, TAD_DS_FICHERO_ENTRADA, TAD_DS_TABLA_ENTRADA, TAD_DS_CAMPO_TEM_ENTRADA, TIT_CO_ID, " & _
            '     "EST_CO_ID, TAD_DS_NOMBRE_ANALISIS, TAD_DS_CAMPO_TEM_JOIN, TAD_DS_RESULTADO, TPR_CO_ID, TAD_DS_USUARIO, TAD_DS_DESCRIPCION, TAD_DS_FECHA " & _
            '     "FROM VIS_TAD_TEMATICOSDEMANDA WHERE TAD_CO_ID=" + idTematico.ToString
            'aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            'For Each aRow In aDS.Tables(0).Rows
            'Me.Id = aRow("TAD_CO_ID")
            'Me.nombre = IIf(aRow("TAD_DS_NOMBRE_ANALISIS").Equals(System.DBNull.Value), "", aRow("TAD_DS_NOMBRE_ANALISIS"))
            'Me.privacidad = IIf(aRow("TPR_CO_ID").Equals(System.DBNull.Value), 1, aRow("TPR_CO_ID"))
            ' Me.autor = IIf(aRow("TAD_DS_USUARIO").Equals(System.DBNull.Value), "", aRow("TAD_DS_USUARIO"))
            ' Me.idLayer = IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), "1", aRow("TIT_CO_ID").ToString())
            'Me.descripcion = IIf(aRow("TAD_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TAD_DS_DESCRIPCION"))
            ' Me.fecha = IIf(aRow("TAD_DS_FECHA").Equals(System.DBNull.Value), "", aRow("TAD_DS_FECHA"))

            'Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

End Class
