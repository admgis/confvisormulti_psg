Imports System.Web
Imports System.Web.Security
Imports System.Web.Services
Imports System.Web.Services.Protocols

Imports System.Data
Imports System.Data.OracleClient
Imports System.Data.OleDb
'Imports System.Data.Odbc

Imports System.DirectoryServices.Protocols
Imports System.DirectoryServices
Imports System.Xml

Imports System.IO
Imports System.Collections.Generic
Imports Tema
Imports Servicio
Imports Devart.Data.PostgreSql


<WebService(Namespace:="http://ims4.mapa.mapya.es/confvisor")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Service
    Inherits System.Web.Services.WebService

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios", EnableSession:=True)> _
Public Function ObtenerServicios(ByVal codIdioma As String) As Tema()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrTemas() As Tema

			strRoles = ObtenerAutenticacion()
			If codIdioma.Equals(System.DBNull.Value) Or codIdioma.Equals("") Then

				codIdioma = "es"

			End If


			'Obtenemos en primer lugar todo el �rbol de temas
			strTableName = "vis_tem_temas"
			strDatasetName = "vis_tem_temas"
			strQuery = "select tem_co_id from vis_tem_temas where tem_co_idpadre is null order by tem_nm_orden asc"
			aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
			Dim aRow As Data.DataRow

			ReDim Preserve arrTemas(aDS.Tables(0).Rows.Count - 1)
			i = 0
			For Each aRow In aDS.Tables(0).Rows
				Dim mTema As New Tema
				mTema.CargarTema(aRow("TEM_CO_ID"), strRoles, codIdioma)
				arrTemas(i) = mTema
				i += 1
			Next
			Return arrTemas

		Catch ex As Exception
			Return Nothing
		End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios", EnableSession:=True)> _
 Public Function ObtenerServiciosFlex(ByVal codIdioma As String, ByVal usuario As UserInfo) As Tema()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrTemas() As Tema

            strRoles = ObtenerAutenticacionFlex(usuario)
            If codIdioma.Equals(System.DBNull.Value) Or codIdioma.Equals("") Then

                codIdioma = "es"

            End If


            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"
            strQuery = "select tem_co_id from vis_tem_temas where tem_co_idpadre is null order by tem_nm_orden asc"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrTemas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mTema As New Tema
                mTema.CargarTema(aRow("TEM_CO_ID"), strRoles, codIdioma)
                arrTemas(i) = mTema
                i += 1
            Next
            Return arrTemas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function CargarItemServicio(ByVal idServicio As Integer, ByVal codIdioma As String) As Servicio
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "VIS_SER_SERVICIOS"
            strDatasetName = "VIS_SER_SERVICIOS"

            strQuery = "select hos_ds_nombre,ser_ds_nombre,SER_DS_DESCCOMPLETA,CASE WHEN VIS_SDS_DESC_SERVICIOS.ser_ds_descripcion IS null THEN vis_ser_servicios.SER_DS_DESCRIPCION ELSE vis_sds_desc_servicios.SER_DS_DESCRIPCION END as ser_ds_descripcion,ser_ds_url,ser_ds_path,tem_co_id," & _
    "vis_tis_tipo_servicio.TIS_DS_DESCRIPCION,ser_ds_info, ser_co_alpha, VIS_SDS_DESC_SERVICIOS.ser_ds_alias " & _
    "from ((vis_ser_servicios left join VIS_SDS_DESC_SERVICIOS on vis_ser_servicios.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id " & _
    "and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') inner join vis_hos_hosts on vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
    "inner join vis_tis_tipo_servicio on vis_ser_servicios.tis_co_id=vis_tis_tipo_servicio.tis_co_id " & _
    " where (vis_ser_servicios.ser_co_id=" + idServicio.ToString + ") "



            Dim aServicio As Servicio

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            If aDS.Tables(0).Rows.Count > 0 Then
                aServicio = New Servicio
                For Each aRow In aDS.Tables(0).Rows
                    aServicio.id = idServicio
                    aServicio.host = IIf(aRow("HOS_DS_NOMBRE").Equals(System.DBNull.Value), "", aRow("HOS_DS_NOMBRE"))
                    aServicio.nombre = IIf(aRow("SER_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("SER_DS_DESCRIPCION"))
                    aServicio.descripcion = IIf(aRow("SER_DS_DESCCOMPLETA").Equals(System.DBNull.Value), "", aRow("SER_DS_DESCCOMPLETA"))
                    aServicio.url = IIf(aRow("SER_DS_URL").Equals(System.DBNull.Value), "", aRow("SER_DS_URL"))
                    aServicio.metodo = IIf(aRow("SER_DS_PATH").Equals(System.DBNull.Value), "", aRow("SER_DS_PATH"))
                    aServicio.info = IIf(aRow("SER_DS_INFO").Equals(System.DBNull.Value), "", aRow("SER_DS_INFO"))
                    aServicio.tipo = IIf(aRow("TIS_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TIS_DS_DESCRIPCION"))
                    aServicio.alpha = IIf(aRow("SER_CO_ALPHA").Equals(System.DBNull.Value), 100, aRow("SER_CO_ALPHA"))
                    aServicio.nombrelargo = IIf(aRow("SER_DS_ALIAS").Equals(System.DBNull.Value), aServicio.nombre, aRow("SER_DS_ALIAS"))
                    aServicio.IdPadre = IIf(aRow("TEM_CO_ID").Equals(System.DBNull.Value), -1, aRow("TEM_CO_ID"))
                Next
                aServicio.Privacidad = aServicio.GetPrivacidad(Me.Session("Usuario"))
                Return aServicio
            Else
                Return Nothing
            End If






        Catch
            Return Nothing
        End Try

    End Function

    Public Function CargarItemTema(ByVal id As Integer, ByVal strRoles As String, ByVal codIdioma As String) As Tema
        Dim strQuery As String

        Dim aDS As Data.DataSet
        Dim strTableName As String, strDatasetName As String
        Try

            strTableName = "VIS_TEM_TEMAS"
            strDatasetName = "VIS_TEM_TEMAS"

            strQuery = "SELECT VIS_TEM_TEMAS.TEM_CO_ID,VIS_TEM_TEMAS.TEM_DS_DESCCOMPLETA, CASE WHEN VIS_TDS_DESC_TEMAS.TEM_DS_DESCRIPCION IS NULL THEN VIS_TEM_TEMAS.TEM_DS_DESCRIPCION ELSE VIS_TDS_DESC_TEMAS.TEM_DS_DESCRIPCION END AS TEM_DS_DESCRIPCION, TEM_CO_IDPADRE" & _
           " FROM VIS_TEM_TEMAS LEFT JOIN VIS_TDS_DESC_TEMAS ON  VIS_TEM_TEMAS.TEM_CO_ID= VIS_TDS_DESC_TEMAS.TEM_CO_ID AND VIS_TDS_DESC_TEMAS.IDI_DS_CODIGO='" & codIdioma & "'" & _
           " WHERE VIS_TEM_TEMAS.TEM_CO_ID='" & id & "'"

            Dim aTema As Tema
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            If aDS.Tables(0).Rows.Count > 0 Then
                For Each aRow In aDS.Tables(0).Rows
                    aTema = New Tema

                    aTema.Id = aRow("TEM_CO_ID")
                    If Not IsDBNull(aRow("TEM_DS_DESCRIPCION")) Then
                        aTema.nombre = aRow("TEM_DS_DESCRIPCION")
                    End If
                    If Not IsDBNull(aRow("TEM_CO_IDPADRE")) Then
                        aTema.IdPadre = aRow("TEM_CO_IDPADRE")
                    End If
                    If Not IsDBNull(aRow("TEM_DS_DESCCOMPLETA")) Then
                        aTema.descripcion = aRow("TEM_DS_DESCCOMPLETA")
                    End If
                    aTema.CargarNumTemasHijo(aTema.Id, strRoles, codIdioma)
                    aTema.CargarNumServiciosHijo(aTema.Id, strRoles, codIdioma)
                Next

                Return aTema
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CargarItemOrganismo(ByVal id As Integer, ByVal strRoles As String, ByVal codIdioma As String) As Tema
        Dim strQuery As String

        Dim aDS As Data.DataSet
        Dim strTableName As String, strDatasetName As String
        Try

            strTableName = "vis_dep_departamentos"
            strDatasetName = "vis_dep_departamentos"


            strQuery = "SELECT vis_dep_departamentos.DEP_CO_ID,vis_dep_departamentos.DEP_DS_DESCRIPCION, CASE WHEN VIS_DED_DESC_DEPARTAMENTOS.DEP_DS_DESCRIPCION IS NULL THEN vis_dep_departamentos.DEP_DS_DESCRIPCION ELSE VIS_DED_DESC_DEPARTAMENTOS.DEP_DS_DESCRIPCION END AS DEP_DS_DESCRIPCION, DEP_CO_IDPADRE " & _
            " FROM vis_dep_departamentos LEFT JOIN VIS_DED_DESC_DEPARTAMENTOS ON  vis_dep_departamentos.DEP_CO_ID= VIS_DED_DESC_DEPARTAMENTOS.DEP_CO_ID AND VIS_DED_DESC_DEPARTAMENTOS.IDI_DS_CODIGO='" & codIdioma & "'" & _
            " WHERE vis_dep_departamentos.DEP_CO_ID='" & id & "'"

            Dim aTema As Tema
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            If aDS.Tables(0).Rows.Count > 0 Then
                For Each aRow In aDS.Tables(0).Rows
                    aTema = New Tema

                    aTema.Id = aRow("DEP_CO_ID")
                    If Not IsDBNull(aRow("DEP_DS_DESCRIPCION")) Then
                        aTema.nombre = aRow("DEP_DS_DESCRIPCION")
                    End If
                    If Not IsDBNull(aRow("DEP_CO_IDPADRE")) Then
                        aTema.IdPadre = aRow("DEP_CO_IDPADRE")
                    End If
                    If Not IsDBNull(aRow("DEP_DS_DESCRIPCION")) Then
                        aTema.descripcion = aRow("DEP_DS_DESCRIPCION")
                    End If

                    aTema.CargarNumDptosHijo(aTema.Id, strRoles, codIdioma)
                Next

                Return aTema
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Buscar Servicios", EnableSession:=True)> _
        Public Function FindServicios(ByVal aNombre As String, ByVal codIdioma As String) As ItemArbolServicios()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrITEMS() As ItemArbolServicios

            strRoles = ObtenerAutenticacion()
            If codIdioma.Equals(System.DBNull.Value) Or codIdioma.Equals("") Then

                codIdioma = "es"

            End If

            Dim aRow As Data.DataRow
            Dim numTemas As Integer
            Dim numServicios As Integer



            '-------------------------
            strTableName = "VIS_SER_SERVICIOS"
            strDatasetName = "VIS_SER_SERVICIOS"




            strQuery = "select vis_ser_servicios.ser_co_id from vis_rol_roles_servicio,vis_ser_servicios, vis_hos_hosts " & _
                " where (vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
                " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                " and (upper(vis_ser_servicios.ser_ds_descripcion)) like '%" + UCase(aNombre) + "%'" & _
                " order by(vis_ser_servicios.ser_ds_descripcion)"
            'LFMC: Lo quitamos, ahora devolvemos el servicio con su privacidad " and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            numServicios = aDS.Tables(0).Rows.Count

            ReDim Preserve arrITEMS(numTemas + numServicios - 1)

            For Each aRow In aDS.Tables(0).Rows
                Dim mItem As New ItemArbolServicios
                mItem.Servicio = CargarItemServicio(aRow("SER_CO_ID"), codIdioma)
                arrITEMS(i) = mItem
                i += 1
            Next

            Return arrITEMS

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion del Arbol de Servicios", EnableSession:=True)> _
    Public Function ObtenerArbolCategorias(ByVal raiz As String, ByVal codIdioma As String) As ItemArbolServicios()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrITEMS() As ItemArbolServicios

            strRoles = ObtenerAutenticacion()
            If codIdioma.Equals(System.DBNull.Value) Or codIdioma.Equals("") Then

                codIdioma = "es"

            End If

            Dim aRow As Data.DataRow
            Dim numTemas As Integer
            Dim numServicios As Integer


            ' temas 
            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"

            If raiz = "" Then
                strQuery = "select tem_co_id from vis_tem_temas where tem_co_idpadre is null order by tem_nm_orden asc"
            Else
                strQuery = "select tem_co_id from vis_tem_temas where tem_co_idpadre = " + raiz + " order by tem_nm_orden asc"
            End If

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            numTemas = aDS.Tables(0).Rows.Count


            ReDim Preserve arrITEMS(numTemas - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mItem As New ItemArbolServicios
                mItem.Tema = CargarItemTema(aRow("TEM_CO_ID"), strRoles, codIdioma)
                arrITEMS(i) = mItem
                i += 1
            Next

            '-------------------------
            strTableName = "VIS_SER_SERVICIOS"
            strDatasetName = "VIS_SER_SERVICIOS"



            If raiz = "" Then
                strQuery = "select vis_ser_servicios.ser_co_id from vis_rol_roles_servicio,vis_ser_servicios, vis_hos_hosts " & _
                    " where (vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
                    " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                    " and (vis_ser_servicios.tem_co_id is null) " & _
                    " order by(vis_ser_servicios.ser_ds_descripcion)" 'LFMC: Quitado. Ahora devolvemos todos con su privacidad and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
            Else
                strQuery = "select vis_ser_servicios.ser_co_id from vis_rol_roles_servicio,vis_ser_servicios, vis_hos_hosts " & _
                    " where (vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
                    " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                    " and (vis_ser_servicios.tem_co_id=" + raiz.ToString + ") " & _
                    " order by(vis_ser_servicios.ser_ds_descripcion)" 'LFMC: Quitado. Ahora devolvemos todos con su privacidad and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
            End If

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            numServicios = aDS.Tables(0).Rows.Count

            ReDim Preserve arrITEMS(numTemas + numServicios - 1)

            For Each aRow In aDS.Tables(0).Rows
                Dim mItem As New ItemArbolServicios
                mItem.Servicio = CargarItemServicio(aRow("SER_CO_ID"), codIdioma)
                arrITEMS(i) = mItem
                i += 1
            Next

            Return arrITEMS

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion del Arbol de Servicios", EnableSession:=True)> _
        Public Function ObtenerArbolOrganigrama(ByVal raiz As String, ByVal codIdioma As String) As ItemArbolServicios()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrITEMS() As ItemArbolServicios

            strRoles = ObtenerAutenticacion()
            If codIdioma.Equals(System.DBNull.Value) Or codIdioma.Equals("") Then

                codIdioma = "es"

            End If

            Dim aRow As Data.DataRow
            Dim numTemas As Integer
            Dim numServicios As Integer


            ' temas 
            strTableName = "vis_dep_departamentos"
            strDatasetName = "vis_dep_departamentos"

            If raiz = "" Then
                strQuery = "select dep_co_id from vis_dep_departamentos where dep_co_idpadre is null"
            Else
                strQuery = "select dep_co_id from vis_dep_departamentos where dep_co_idpadre = " + raiz + " "
            End If

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            numTemas = aDS.Tables(0).Rows.Count


            ReDim Preserve arrITEMS(numTemas - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mItem As New ItemArbolServicios
                mItem.Tema = CargarItemOrganismo(aRow("DEP_CO_ID"), strRoles, codIdioma)
                arrITEMS(i) = mItem
                i += 1
            Next

            '-------------------------
            strTableName = "VIS_SER_SERVICIOS"
            strDatasetName = "VIS_SER_SERVICIOS"



            If raiz = "" Then
                ' ''strQuery = "select vis_ser_servicios.ser_co_id from vis_rol_roles_servicio,vis_ser_servicios, vis_hos_hosts " & _
                ' ''    " where (vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
                ' ''    " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                ' ''    " and (vis_ser_servicios.dep_co_id is null) and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
                ' ''    " order by(vis_ser_servicios.ser_ds_descripcion)"

            Else
                strQuery = "select vis_ser_servicios.ser_co_id from vis_rol_roles_servicio,vis_ser_servicios, vis_hos_hosts " & _
                    " where (vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
                    " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                    " and (vis_ser_servicios.dep_co_id=" + raiz.ToString + ") " & _
                    " order by(vis_ser_servicios.ser_ds_descripcion)" 'LFMC: Quitado. Ahora devolvemos todos con su privacidad and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _

                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

                numServicios = aDS.Tables(0).Rows.Count

                ReDim Preserve arrITEMS(numTemas + numServicios - 1)

                For Each aRow In aDS.Tables(0).Rows
                    Dim mItem As New ItemArbolServicios
                    mItem.Servicio = CargarItemServicio(aRow("SER_CO_ID"), codIdioma)
                    arrITEMS(i) = mItem
                    i += 1
                Next

            End If



            Return arrITEMS

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicio por Nombre", EnableSession:=True)> _
        Public Function ObtenerServicioId(ByVal idBBDD As Integer, ByVal codIdioma As String) As Servicio
        Try
            Dim strRoles As String
            Dim mServicio As New Servicio

            strRoles = ObtenerAutenticacion()
            If codIdioma.Equals(System.DBNull.Value) Or codIdioma.Equals("") Then

                codIdioma = "es"

            End If
            mServicio.Cargar(idBBDD, codIdioma)

            Return mServicio

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Obtencion de Servicio por Nombre", EnableSession:=True)> _
    Public Function ObtenerServicio(ByVal strDescripcion As String, ByVal codIdioma As String) As Servicio
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim mServicio As New Servicio

            strRoles = ObtenerAutenticacion()
            If codIdioma.Equals(System.DBNull.Value) Or codIdioma.Equals("") Then

                codIdioma = "es"

            End If


            'Obtenemos en primer lugar id del servicio
            strTableName = "vis_ser_servicios"
            strDatasetName = "vis_ser_servicios"
            strQuery = "select ser_co_id from vis_ser_servicios where ser_ds_descripcion ='" + strDescripcion + "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            For Each aRow In aDS.Tables(0).Rows
                mServicio.Cargar(aRow("SER_CO_ID"), codIdioma)
            Next
            Return mServicio

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion del Organigrama", EnableSession:=True)> _
Public Function ObtenerOrganigrama(ByVal codIdioma As String) As Departamento()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrDepart() As Departamento

            strRoles = ObtenerAutenticacion()


            'Obtenemos en primer lugar todo el �rbol del organigrama
            strTableName = "vis_dep_departamentos"
            strDatasetName = "vis_dep_departamentos"
            strQuery = "select dep_co_id from vis_dep_departamentos where dep_co_idpadre is null"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrDepart(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mDepartamento As New Departamento
                mDepartamento.CargarDepartamentoSinServicios(aRow("DEP_CO_ID"), codIdioma)
                arrDepart(i) = mDepartamento
                i += 1
            Next
            Return arrDepart

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Obtencion de Temas", EnableSession:=True)> _
   Public Function ObtenerTemas(ByVal codIdioma As String) As Tema()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrTemas() As Tema

            strRoles = ObtenerAutenticacion()


            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"
			strQuery = "select tem_co_id from vis_tem_temas where tem_co_idpadre is null order by tem_nm_orden asc"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrTemas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mTema As New Tema
                mTema.CargarTemaSinServicios(aRow("TEM_CO_ID"), codIdioma)
                arrTemas(i) = mTema
                i += 1
            Next
            Return arrTemas

        Catch ex As Exception
            Return Nothing
        End Try
	End Function

	<System.Web.Services.WebMethod(Description:="Obtencion de Temas de Visor", EnableSession:=True)> _
	  Public Function ObtenerTemasVisor(ByVal codIdioma As String, ByVal idVisor As Integer) As Tema()
		Try

			Dim strQuery As String, strTableName As String, strDatasetName As String
			Dim aDS As Data.DataSet
			Dim strRoles As String
			Dim i As Integer
			Dim arrTemas() As Tema

			strRoles = ObtenerAutenticacion()


			'Obtenemos en primer lugar todo el �rbol de temas
			strTableName = "vis_tem_temas"
			strDatasetName = "vis_tem_temas"
            strQuery = "select t.tem_co_id from vis_tem_temas t inner join vis_vtm_visores_tema v on t.tem_co_id=v.tem_co_id where v.tem_co_idpadre is null and v.vis_co_id=" & idVisor & "  order by v.vtm_nm_orden asc"
			aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
			Dim aRow As Data.DataRow

			ReDim Preserve arrTemas(aDS.Tables(0).Rows.Count - 1)
			i = 0
			For Each aRow In aDS.Tables(0).Rows
				Dim mTema As New Tema
                mTema.CargarTemaSinServicios(aRow("TEM_CO_ID"), codIdioma, idVisor)
				arrTemas(i) = mTema
				i += 1
			Next
			Return arrTemas

		Catch ex As Exception
			Return Nothing
		End Try
	End Function

    Public Shared Function ExecuteNonQuery(ByVal strSQL As String, _
                                        Optional ByVal connectionKey As String = "bbdd") As Boolean

        Dim aConn As PgSqlConnection = Nothing
        Dim trans As PgSqlTransaction = Nothing
        Dim res As Boolean = False
        Try
            'Dim strConn As String
            'If connectionKey = "bbdd" Then
            Dim strConn As String = System.Configuration.ConfigurationManager.AppSettings(connectionKey)
            aConn = New PgSqlConnection(strConn)
            'Else
            'aConn = New PgSqlConnection(connectionKey)
            'End If
            aConn.Open()
            trans = aConn.BeginTransaction()
            Dim mCommand As PgSqlCommand = New PgSqlCommand(strSQL, aConn, trans)

            mCommand.ExecuteNonQuery()
            trans.Commit()

            aConn.Close()

            res = True

        Catch ex As Exception
            If trans Is Nothing = False Then
                trans.Rollback()
            End If
            aConn.Close()
            Throw New Exception(ex.Message, ex)
        End Try

        Return res

    End Function

    <System.Web.Services.WebMethod(Description:="Almacenamiento de tem�tico de usuario", EnableSession:=True)> _
    Public Function GuardarTematicoAvanzado(ByVal Id As Integer, ByVal nombre As String, _
        ByVal fecha As String, ByVal idTipo As String, ByVal layerDrawingOptions As String, _
        ByVal layerDataSource As String, ByVal pointClustering As Boolean, ByVal renderer As String, _
        ByVal definitionExp As String, ByVal properties As String, ByVal userInfo As UserInfo) As Boolean
        Dim res As Boolean = False

        Dim aConn As PgSqlConnection

        Try

            Dim usuario As String = userInfo.nombre
            Dim connectionKey As String = "bbddComun"
            If usuario Is Nothing Then Return res
            Dim rolAdmATematicos As String = "'ADMTEMATICOSAV'"
            Dim strRoles As String = ObtenerAutenticacion()

            'Dim strConn As String = System.Configuration.ConfigurationManager.AppSettings(connectionKey)
            Dim strPointClustering = "Y"

            If pointClustering = False Then strPointClustering = "N"

            Dim strConn As String = System.Configuration.ConfigurationManager.AppSettings(connectionKey)
            Dim cmdParam1, cmdParam2, cmdParam3, cmdParam4, cmdParam5 As OracleParameter
            Dim cmd As PgSqlCommand
            aConn = New PgSqlConnection(strConn)
            'cmd = New PgSqlCommand("UPDATE VIS_TAD_TEMATICOSDEMANDA SET TAD_DS_RESULTADO = :Resultado, EST_CO_ID=2 WHERE TAD_CO_ID = " & idTematico, aConn)

            If Id < 1 Then
                Dim strQuery As String = "SELECT SEQ_TAV_CO_ID.NEXTVAL FROM DUAL"
                Dim aDS As DataSet = ExecuteQuery(strQuery, "T", "T", "bbddComun")

                For Each aRow As Data.DataRow In aDS.Tables(0).Rows
                    Id = CType(aRow(0), Integer)
                Next

                cmd = New PgSqlCommand("INSERT INTO VIS_TAV_TEMATICOS_AVANZADOS (TAV_CO_ID, USU_DS_NOMBRE, TAV_DS_NOMBRE, TAV_DS_FECHA, TTA_CO_ID, TAV_DS_LAYERDRAWINGOPTIONS, " & _
                                    "TAV_DS_LAYERDATASOURCE, TAV_BOL_POINTCLUSTERING, TAV_DS_JSON_RENDERER, TAV_DS_DEFINITIONEXPRESSION, TAV_DS_PROPERTIES) VALUES (" & _
                                   Id & ",'" & usuario & "','" & nombre & "', '" & fecha & "'," & idTipo & ",:layerDrawingOptions,:layerDataSource" & _
                                   ",'" & strPointClustering & "',:renderer,:definitionExp,:properties)", aConn)


                'ExecuteNonQuery("INSERT INTO VIS_TAV_TEMATICOS_AVANZADOS (TAV_CO_ID, USU_DS_NOMBRE, TAV_DS_NOMBRE, TAV_DS_FECHA, TTA_CO_ID, TAV_DS_LAYERDRAWINGOPTIONS, " & _
                '                   "TAV_DS_LAYERDATASOURCE, TAV_BOL_POINTCLUSTERING, TAV_DS_JSON_RENDERER, TAV_DS_DEFINITIONEXPRESSION, TAV_DS_PROPERTIES) VALUES (" & _
                '                 Id & ",'" & usuario & "','" & nombre & "', '" & fecha & "'," & idTipo & ",'" & layerDrawingOptions & "','" & layerDataSource & _
                '                  "','" & strPointClustering & "','" & renderer & "','" & definitionExp & "','" & properties & "')", connectionKey)

            Else
                If strRoles.IndexOf(rolAdmATematicos) <> -1 Then

                    cmd = New PgSqlCommand("UPDATE VIS_TAV_TEMATICOS_AVANZADOS SET TAV_DS_NOMBRE ='" & nombre & "', TAV_DS_LAYERDRAWINGOPTIONS=:layerDrawingOptions" & _
                                    ", TAV_DS_LAYERDATASOURCE=:layerDataSource, TAV_BOL_POINTCLUSTERING='" & strPointClustering & "' " & _
                                    ", TAV_DS_JSON_RENDERER=:renderer, TAV_DS_DEFINITIONEXPRESSION=:definitionExp, TAV_DS_PROPERTIES=:properties WHERE TAV_CO_ID=" & Id, aConn)

                    'ExecuteNonQuery("UPDATE VIS_TAV_TEMATICOS_AVANZADOS SET TAV_DS_NOMBRE ='" & nombre & "', TAV_DS_LAYERDRAWINGOPTIONS='" & layerDrawingOptions & _
                    '                "', TAV_DS_LAYERDATASOURCE='" & layerDataSource & "', TAV_BOL_POINTCLUSTERING='" & strPointClustering & "' " & _
                    '                ", TAV_DS_JSON_RENDERER='" & renderer & "', TAV_DS_DEFINITIONEXPRESSION='" & definitionExp & "', TAV_DS_PROPERTIES='" & properties & "' WHERE TAV_CO_ID=" & Id, connectionKey)
                Else
                    cmd = New PgSqlCommand("UPDATE VIS_TAV_TEMATICOS_AVANZADOS SET TAV_DS_NOMBRE ='" & nombre & "', TAV_DS_LAYERDRAWINGOPTIONS=:layerDrawingOptions" & _
                                    ", TAV_DS_LAYERDATASOURCE=:layerDataSource, TAV_BOL_POINTCLUSTERING='" & strPointClustering & "' " & _
                                    ", TAV_DS_JSON_RENDERER=:renderer, TAV_DS_DEFINITIONEXPRESSION=:definitionExp, TAV_DS_PROPERTIES=:properties WHERE TAV_CO_ID=" & Id & " AND USU_DS_NOMBRE='" & usuario & "' ", aConn)

                    'ExecuteNonQuery("UPDATE VIS_TAV_TEMATICOS_AVANZADOS SET TAV_DS_NOMBRE ='" & nombre & "', TAV_DS_LAYERDRAWINGOPTIONS='" & layerDrawingOptions & _
                    '                "', TAV_DS_LAYERDATASOURCE='" & layerDataSource & "', TAV_BOL_POINTCLUSTERING='" & strPointClustering & "' " & _
                    '                ", TAV_DS_JSON_RENDERER='" & renderer & "', TAV_DS_DEFINITIONEXPRESSION='" & definitionExp & "', TAV_DS_PROPERTIES='" & properties & "' WHERE TAV_CO_ID=" & Id & " AND USU_DS_NOMBRE='" & usuario & "' ", connectionKey)
                End If

            End If

            If layerDrawingOptions.Length = 0 Then
                cmdParam1 = New OracleParameter(":layerDrawingOptions", System.Data.OracleClient.OracleType.Clob, layerDrawingOptions.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, DBNull.Value)
            Else
                cmdParam1 = New OracleParameter(":layerDrawingOptions", System.Data.OracleClient.OracleType.Clob, layerDrawingOptions.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, layerDrawingOptions)
            End If

            If layerDataSource.Length = 0 Then
                cmdParam2 = New OracleParameter(":layerDataSource", System.Data.OracleClient.OracleType.Clob, layerDataSource.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, DBNull.Value)
            Else
                cmdParam2 = New OracleParameter(":layerDataSource", System.Data.OracleClient.OracleType.Clob, layerDataSource.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, layerDataSource)
            End If
            If renderer.Length = 0 Then
                cmdParam3 = New OracleParameter(":renderer", System.Data.OracleClient.OracleType.Clob, renderer.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, DBNull.Value)
            Else
                cmdParam3 = New OracleParameter(":renderer", System.Data.OracleClient.OracleType.Clob, renderer.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, renderer)
            End If

            If definitionExp.Length = 0 Then
                cmdParam4 = New OracleParameter(":definitionExp", System.Data.OracleClient.OracleType.Clob, definitionExp.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, DBNull.Value)
                'cmdParam4.
            Else
                cmdParam4 = New OracleParameter(":definitionExp", System.Data.OracleClient.OracleType.Clob, definitionExp.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, definitionExp)
            End If

            If properties.Length = 0 Then
                cmdParam5 = New OracleParameter(":properties", System.Data.OracleClient.OracleType.Clob, properties.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, DBNull.Value)
            Else
                cmdParam5 = New OracleParameter(":properties", System.Data.OracleClient.OracleType.Clob, properties.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, properties)
            End If

            cmd.Parameters.Add(cmdParam1)
            cmd.Parameters.Add(cmdParam2)
            cmd.Parameters.Add(cmdParam3)
            cmd.Parameters.Add(cmdParam4)
            cmd.Parameters.Add(cmdParam5)
            aConn.Open()
            cmd.ExecuteNonQuery()

            res = True

            Return res

        Catch ex As Exception
            aConn.Close()
            Return res
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Almacenamiento de Selecci�n de usuario", EnableSession:=True)> _
    Public Function GuardarSeleccion(ByVal codSeleccion As Integer, ByVal descripcion As String, _
        ByVal codCapa As String, ByVal definitionExp As String, ByVal codCapaFiltro As String, _
        ByVal definitionExpFiltro As String, _
        ByVal objectIds As String, _
        ByVal userInfo As UserInfo) As SeleccionLista
        Dim resultado As SeleccionLista

        Try

            Dim usuario As String = userInfo.nombre
            Dim connectionKey As String = "bbdd"
            If usuario Is Nothing Then Return resultado
            Dim sqlStr As String
            Dim sqlStrValues As String
            'Dim strConn As String = System.Configuration.ConfigurationManager.AppSettings(connectionKey)

            If codSeleccion < 1 Then
                Dim strQuery As String = "SELECT SEQ_SEL_CO_ID.NEXTVAL FROM DUAL"
                Dim aDS As DataSet = ExecuteQuery(strQuery, "T", "T")

                For Each aRowCodigo As Data.DataRow In aDS.Tables(0).Rows
                    codSeleccion = CType(aRowCodigo(0), Integer)
                Next

                sqlStr = "INSERT INTO VIS_SEL_SELECCION (SEL_CO_ID, SEL_DS_DESCRIPCION, CAP_CO_ID, USU_DS_NOMBRE,SEL_DS_DEFINITIONEXP, SEL_DS_OBJECTIDS "
                sqlStrValues = "VALUES (" & _
                               codSeleccion & ",'" & descripcion & "', '" & codCapa & "','" & usuario & "','" & definitionExp.Replace("'", "''") & "','" & objectIds & "'"
                If codCapaFiltro <> "" Then
                    sqlStr &= ", FILTRO_CAP_CO_ID "
                    sqlStrValues &= ",'" & codCapaFiltro & "'"
                End If
                If definitionExpFiltro <> "" Then
                    sqlStr &= ", FILTRO_DS_DEFINITIONEXP"
                    sqlStrValues &= ",'" & definitionExpFiltro.Replace("'", "''") & "'"
                End If
                sqlStr &= ")"
                sqlStrValues &= ")"


                ExecuteNonQuery(sqlStr & sqlStrValues, connectionKey)

            Else
                sqlStr = "UPDATE VIS_SEL_SELECCION SET CAP_CO_ID ='" & codCapa & "', SEL_DS_DEFINITIONEXP='" & definitionExp.Replace("'", "''") & _
                                "', SEL_DS_DESCRIPCION='" & descripcion & "', SEL_DS_OBJECTIDS='" & objectIds & "'"

                If codCapaFiltro <> "" Then
                    sqlStr &= ", FILTRO_CAP_CO_ID ='" & codCapaFiltro & "'"
                End If
                If definitionExpFiltro <> "" Then
                    sqlStr &= ", FILTRO_DS_DEFINITIONEXP = '" & definitionExpFiltro.Replace("'", "''") & "'"
                End If
                sqlStr &= " WHERE SEL_CO_ID=" & codSeleccion

                ExecuteNonQuery(sqlStr, connectionKey)
            End If

            Dim strQueryRes As String = "SELECT VIS_SEL_SELECCION.SEL_CO_ID SEL_CO_ID, VIS_SEL_SELECCION.SEL_DS_DESCRIPCION, VIS_SEL_SELECCION.SEL_DS_FECHA, VIS_SEL_SELECCION.CAP_CO_ID,  VIS_CAP_CAPAS.SER_CO_ID, VIS_SEL_SELECCION.SEL_DS_DEFINITIONEXP, " & _
              "VIS_SEL_SELECCION.FILTRO_CAP_CO_ID, VIS_CAP_CAPAS_FILTRO.SER_CO_ID SER_CO_ID_FILTRO, VIS_SEL_SELECCION.FILTRO_DS_DEFINITIONEXP, SEL_DS_OBJECTIDS " & _
              " FROM VIS_SEL_SELECCION INNER JOIN VIS_CAP_CAPAS ON VIS_SEL_SELECCION.CAP_CO_ID = VIS_CAP_CAPAS.CAP_CO_ID " & _
              " LEFT JOIN VIS_CAP_CAPAS VIS_CAP_CAPAS_FILTRO ON VIS_SEL_SELECCION.FILTRO_CAP_CO_ID = VIS_CAP_CAPAS_FILTRO.CAP_CO_ID  " & _
              " WHERE " & _
              "	VIS_SEL_SELECCION.SEL_CO_ID=" & codSeleccion


            Dim aDSRes As DataSet = ExecuteQuery(strQueryRes, "selecc", "selecc", connectionKey)
            Dim aRow As DataRow = aDSRes.Tables(0).Rows(0)
            resultado = New SeleccionLista
            resultado.Id = aRow("SEL_CO_ID")
            resultado.descripcion = IIf(aRow("SEL_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("SEL_DS_DESCRIPCION"))
            resultado.fecha = IIf(aRow("SEL_DS_FECHA").Equals(System.DBNull.Value), "", aRow("SEL_DS_FECHA"))
            resultado.codCapa = aRow("CAP_CO_ID")
            resultado.codServicio = aRow("SER_CO_ID")
            resultado.objectIds = IIf(aRow("SEL_DS_OBJECTIDS").Equals(System.DBNull.Value), "", aRow("SEL_DS_OBJECTIDS"))
            resultado.definitionExp = IIf(aRow("SEL_DS_DEFINITIONEXP").Equals(System.DBNull.Value), "", aRow("SEL_DS_DEFINITIONEXP"))
            resultado.codCapaFiltro = IIf(aRow("FILTRO_CAP_CO_ID").Equals(System.DBNull.Value), "", aRow("FILTRO_CAP_CO_ID"))
            resultado.codServicioFiltro = IIf(aRow("SER_CO_ID_FILTRO").Equals(System.DBNull.Value), "", aRow("SER_CO_ID_FILTRO"))
            resultado.definitionExpFiltro = IIf(aRow("FILTRO_DS_DEFINITIONEXP").Equals(System.DBNull.Value), "", aRow("FILTRO_DS_DEFINITIONEXP"))


            'res = codSeleccion

            Return resultado

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion las Capas Extraibles del Visor", EnableSession:=True)> _
    Public Function ObtenerCapasExtraiblesVisor(ByVal codIdioma As String, ByVal idVisor As Integer, ByVal idServ As Integer) As Capa()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            'Dim strRoles As String
            Dim i As Integer
            Dim strRoles As String
            Dim arrCapas() As Capa

            strRoles = ObtenerAutenticacion()

            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_cap_capas"
            strDatasetName = "vis_cap_capas"
            strQuery = "select c.cap_co_id from vis_ser_servicios s inner join vis_cap_capas c on s.ser_co_id=c.ser_co_id inner join vis_cev_capasextraiblesvisor v on c.cap_co_id=v.cap_co_id where v.vis_co_id =" & idVisor & " and s.ser_co_id = " & idServ
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrCapas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mCapa As New Capa
                mCapa.Cargar(aRow("CAP_CO_ID"), codIdioma, idVisor)
                arrCapas(i) = mCapa
                i += 1
            Next
            Return arrCapas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion las Capas Seleccionables del Visor", EnableSession:=True)> _
    Public Function ObtenerCapasSeleccionesVisor(ByVal codIdioma As String, ByVal idVisor As Integer, ByVal idServ As Integer) As Capa()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            'Dim strRoles As String
            Dim i As Integer
            Dim strRoles As String
            Dim arrCapas() As Capa

            strRoles = ObtenerAutenticacion()

            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_cap_capas"
            strDatasetName = "vis_cap_capas"
            strQuery = "select c.cap_co_id from vis_ser_servicios s inner join vis_cap_capas c on s.ser_co_id=c.ser_co_id inner join VIS_CSV_CAPASSELECCIONESVISOR v on c.cap_co_id=v.cap_co_id where v.vis_co_id =" & idVisor & " and s.ser_co_id = " & idServ
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrCapas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mCapa As New Capa
                mCapa.Cargar(aRow("CAP_CO_ID"), codIdioma, idVisor)
                arrCapas(i) = mCapa
                i += 1
            Next
            Return arrCapas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion las Capas en Filtros del Visor", EnableSession:=True)> _
    Public Function ObtenerCapasEnFiltrosVisor(ByVal codIdioma As String, ByVal idVisor As Integer, ByVal idServ As Integer) As Capa()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            'Dim strRoles As String
            Dim i As Integer
            Dim strRoles As String
            Dim arrCapas() As Capa

            strRoles = ObtenerAutenticacion()

            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_cap_capas"
            strDatasetName = "vis_cap_capas"
            strQuery = "select c.cap_co_id from vis_ser_servicios s inner join vis_cap_capas c on s.ser_co_id=c.ser_co_id inner join VIS_CFV_CAPASFILTROSVISOR v on c.cap_co_id=v.cap_co_id where v.vis_co_id =" & idVisor & " and s.ser_co_id = " & idServ
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrCapas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mCapa As New Capa
                mCapa.Cargar(aRow("CAP_CO_ID"), codIdioma, idVisor)
                arrCapas(i) = mCapa
                i += 1
            Next
            Return arrCapas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de los Serviciso con Capas Extraibles del Visor", EnableSession:=True)> _
    Public Function ObtenerServiciosConCapasExtraiblesVisor(ByVal codIdioma As String, ByVal idVisor As Integer) As Servicio()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            'Dim strRoles As String
            Dim i As Integer
            Dim arrServicios() As Servicio

            'strRoles = ObtenerAutenticacion()


            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_ser_servicios"
            strDatasetName = "vis_ser_servicios"
            strQuery = "select distinct s.ser_co_id from vis_ser_servicios s inner join vis_cap_capas c on s.ser_co_id=c.ser_co_id inner join vis_cev_capasextraiblesvisor v on c.cap_co_id=v.cap_co_id where v.vis_co_id= " & idVisor
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrServicios(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mServicio As New Servicio
                mServicio.Cargar(aRow("SER_CO_ID"), codIdioma)
                arrServicios(i) = mServicio
                i += 1
            Next
            Return arrServicios

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de los Serviciso con Capas Seleccionables del Visor", EnableSession:=True)> _
    Public Function ObtenerServiciosConCapasSeleccionablesVisor(ByVal codIdioma As String, ByVal idVisor As Integer) As Servicio()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            'Dim strRoles As String
            Dim i As Integer
            Dim arrServicios() As Servicio

            'strRoles = ObtenerAutenticacion()


            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_ser_servicios"
            strDatasetName = "vis_ser_servicios"
            strQuery = "select distinct s.ser_co_id from vis_ser_servicios s inner join vis_cap_capas c on s.ser_co_id=c.ser_co_id inner join VIS_CSV_CAPASSELECCIONESVISOR v on c.cap_co_id=v.cap_co_id where v.vis_co_id= " & idVisor
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrServicios(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mServicio As New Servicio
                mServicio.Cargar(aRow("SER_CO_ID"), codIdioma)
                arrServicios(i) = mServicio
                i += 1
            Next
            Return arrServicios

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de los Serviciso con Capas en Filtro del Visor", EnableSession:=True)> _
    Public Function ObtenerServiciosConCapasEnFiltroVisor(ByVal codIdioma As String, ByVal idVisor As Integer) As Servicio()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            'Dim strRoles As String
            Dim i As Integer
            Dim arrServicios() As Servicio

            'strRoles = ObtenerAutenticacion()


            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_ser_servicios"
            strDatasetName = "vis_ser_servicios"
            strQuery = "select distinct s.ser_co_id from vis_ser_servicios s inner join vis_cap_capas c on s.ser_co_id=c.ser_co_id inner join VIS_CFV_CAPASFILTROSVISOR v on c.cap_co_id=v.cap_co_id where v.vis_co_id= " & idVisor
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrServicios(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mServicio As New Servicio
                mServicio.Cargar(aRow("SER_CO_ID"), codIdioma)
                arrServicios(i) = mServicio
                i += 1
            Next
            Return arrServicios

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios para una Tema determinado", EnableSession:=True)> _
    Public Function ObtenerServiciosID(ByVal idTema As Integer, ByVal codIdioma As String) As Tema()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrTemas() As Tema

            strRoles = ObtenerAutenticacion()


            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"
            strQuery = "select tem_co_id from vis_tem_temas where tem_co_id='" & idTema & "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrTemas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mTema As New Tema
                mTema.CargarTema(aRow("TEM_CO_ID"), strRoles, codIdioma)
                arrTemas(i) = mTema
                i += 1
            Next
            Return arrTemas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios para una Tema determinado", EnableSession:=True)> _
    Public Function ObtenerTemaID(ByVal idTema As Integer, ByVal codIdioma As String) As Tema()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrTemas() As Tema

            strRoles = ObtenerAutenticacion()


            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"
            strQuery = "select tem_co_id from vis_tem_temas where tem_co_id='" & idTema & "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrTemas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mTema As New Tema
                mTema.CargarTema(aRow("TEM_CO_ID"), strRoles, codIdioma, "", False)
                arrTemas(i) = mTema
                i += 1
            Next
            Return arrTemas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios para una Tema determinado", EnableSession:=True)> _
    Public Function ObtenerServiciosIDFlex(ByVal idTema As Integer, ByVal codIdioma As String, ByVal usuario As UserInfo) As Tema()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrTemas() As Tema

            strRoles = ObtenerAutenticacionFlex(usuario)

            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"
            strQuery = "select tem_co_id from vis_tem_temas where tem_co_id='" & idTema & "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrTemas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mTema As New Tema
                mTema.CargarTema(aRow("TEM_CO_ID"), strRoles, codIdioma)
                arrTemas(i) = mTema
                i += 1
            Next
            Return arrTemas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios para una Tema determinado", EnableSession:=True)> _
    Public Function ObtenerTemaIDFlex(ByVal idTema As Integer, ByVal codIdioma As String, ByVal usuario As UserInfo) As Tema()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrTemas() As Tema

            strRoles = ObtenerAutenticacionFlex(usuario)

            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"
            strQuery = "select tem_co_id from vis_tem_temas where tem_co_id='" & idTema & "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrTemas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mTema As New Tema
                mTema.CargarTema(aRow("TEM_CO_ID"), strRoles, codIdioma, "", False)
                arrTemas(i) = mTema
                i += 1
            Next
            Return arrTemas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios para una Tema determinado", EnableSession:=True)> _
    Public Function ObtenerTemaIDFlexVisor(ByVal idTema As Integer, ByVal codIdioma As String, _
    ByVal usuario As UserInfo, ByVal idVisor As Integer) As Tema()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrTemas() As Tema

            strRoles = ObtenerAutenticacionFlex(usuario)

            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"
            strQuery = "select tem_co_id from vis_tem_temas where tem_co_id='" & idTema & "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrTemas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim mTema As New Tema
                mTema.CargarTema(aRow("TEM_CO_ID"), strRoles, codIdioma, usuario.nombre, False, , idVisor, False)
                arrTemas(i) = mTema
                i += 1
            Next
            Return arrTemas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios por Tema", EnableSession:=True)> _
    Public Function ObtenerServiciosTema(ByVal nombreTema As String, ByVal codIdioma As String) As Servicio()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim arrServicios() As Servicio
            strRoles = ObtenerAutenticacion()

            'Obtenemos en primer lugar todo el �rbol de temas
            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"
            strQuery = "select tem_co_id from vis_tem_temas where upper(tem_ds_descripcion)='" + UCase(nombreTema) + "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            ReDim Preserve arrServicios(aDS.Tables(0).Rows.Count - 1)
            'i = 0
            Dim mTema As New Tema
            For Each aRow In aDS.Tables(0).Rows

                mTema.CargarTema(aRow("TEM_CO_ID"), strRoles, codIdioma)
                'arrServicios(i) = mTema.servicios
                'i += 1
            Next
            Return mTema.servicios

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Chequeo de C�digos de Subexplotaciones", EnableSession:=True)> _
    Public Function CheckSubexplotacion(ByVal strCodigo As String) As Subexplotacion()
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim i As Integer
        Dim colSubexplotaciones() As Subexplotacion
        Try
            'agm 23/03/2012 Migraci�n Ganader�a REGA
            strQuery = "SELECT * from DGG.REGA_MV_UBICACION_ESPECIES WHERE SE_CO_SUBEXPLOT = '" + strCodigo + "'"
            'strQuery = "SELECT * from DGG.VUB_MV_UBICACION_ESPECIES WHERE SE_CLAVE = '" + strCodigo + "'"
            strTableName = "VUC_UBICACION_ESPECIES_COMP"
            strDatasetName = "VUC_UBICACION_ESPECIES_COMP"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)


            'Debemos a�adir los elementos a la colecci�n de Id's de explotaciones
            ReDim colSubexplotaciones(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim aSubexp As Subexplotacion
                aSubexp = New Subexplotacion()
                aSubexp.id = IIf(aRow("SE_CO_SUBEXPLOT").Equals(System.DBNull.Value), 0, aRow("SE_CO_SUBEXPLOT"))
                aSubexp.idExplotacion = IIf(aRow("SE_CE_EXPLOT").Equals(System.DBNull.Value), "", aRow("SE_CE_EXPLOT"))
                'aSubexp.direccion = IIf(aRow("SE_DIREC").Equals(System.DBNull.Value), "", aRow("SE_DIREC"))
                'aSubexp.codPostal = IIf(aRow("SE_CP").Equals(System.DBNull.Value), "", aRow("SE_CP"))
                'aSubexp.estado = IIf(aRow("ESTADO").Equals(System.DBNull.Value), "", aRow("ESTADO"))
                'aSubexp.fechaEstado = IIf(aRow("SE_FC_EST").Equals(System.DBNull.Value), "", aRow("SE_FC_EST"))
                aSubexp.autoConsumo = IIf(aRow("SE_AUTOC").Equals("S"), True, False)
                aSubexp.especie = IIf(aRow("ESPECIE").Equals(System.DBNull.Value), "", aRow("ESPECIE"))
                'aSubexp.idEspecie = IIf(aRow("SE_CO_EXPECIE").Equals(System.DBNull.Value), "", aRow("SE_CO_EXPECIE"))
                aSubexp.longitud = IIf(aRow("UB_LON").Equals(System.DBNull.Value), 0, aRow("UB_LON"))
                aSubexp.latitud = IIf(aRow("UB_LAT").Equals(System.DBNull.Value), 0, aRow("UB_LAT"))
                aSubexp.provincia = IIf(aRow("PROV_NOM").Equals(System.DBNull.Value), "", aRow("PROV_NOM"))
                aSubexp.municipio = IIf(aRow("MUN_NOM").Equals(System.DBNull.Value), "", aRow("MUN_NOM"))
                aSubexp.comarca = IIf(aRow("COMARCA_SGSA").Equals(System.DBNull.Value), "", aRow("COMARCA_SGSA"))
                colSubexplotaciones(i) = aSubexp
                i += 1
            Next
            Return colSubexplotaciones
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Chequeo de C�digos de Explotaciones", EnableSession:=True)> _
    Public Function CheckExplotacion(ByVal strCodigo As String) As Subexplotacion()
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim i As Integer
        Dim colSubexplotaciones() As Subexplotacion
        Try
            'agm 23/03/2012 Migraci�n Ganader�a REGA
            strQuery = "SELECT * from DGG.REGA_MV_UBICACION_ESPECIES WHERE SE_CE_EXPLOT = '" + strCodigo + "'"
            'strQuery = "SELECT * from DGG.VUB_MV_UBICACION_ESPECIES WHERE SE_CE_EXPLOT = '" + strCodigo + "'"
            strTableName = "VUC_UBICACION_ESPECIES_COMP"
            strDatasetName = "VUC_UBICACION_ESPECIES_COMP"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)


            'Debemos a�adir los elementos a la colecci�n de Id's de explotaciones
            ReDim colSubexplotaciones(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim aSubexp As Subexplotacion
                aSubexp = New Subexplotacion()
                aSubexp.id = IIf(aRow("SE_CO_SUBEXPLOT").Equals(System.DBNull.Value), 0, aRow("SE_CO_SUBEXPLOT"))
                aSubexp.idExplotacion = IIf(aRow("SE_CE_EXPLOT").Equals(System.DBNull.Value), "", aRow("SE_CE_EXPLOT"))
                'aSubexp.direccion = IIf(aRow("SE_DIREC").Equals(System.DBNull.Value), "", aRow("SE_DIREC"))
                'aSubexp.codPostal = IIf(aRow("SE_CP").Equals(System.DBNull.Value), "", aRow("SE_CP"))
                'aSubexp.estado = IIf(aRow("ESTADO").Equals(System.DBNull.Value), "", aRow("ESTADO"))
                'aSubexp.fechaEstado = IIf(aRow("SE_FC_EST").Equals(System.DBNull.Value), "", aRow("SE_FC_EST"))
                aSubexp.autoConsumo = IIf(aRow("SE_AUTOC").Equals("S"), True, False)
                aSubexp.especie = IIf(aRow("ESPECIE").Equals(System.DBNull.Value), "", aRow("ESPECIE"))
                'aSubexp.idEspecie = IIf(aRow("SE_CO_EXPECIE").Equals(System.DBNull.Value), "", aRow("SE_CO_EXPECIE"))
                aSubexp.longitud = IIf(aRow("UB_LON").Equals(System.DBNull.Value), 0, aRow("UB_LON"))
                aSubexp.latitud = IIf(aRow("UB_LAT").Equals(System.DBNull.Value), 0, aRow("UB_LAT"))
                aSubexp.provincia = IIf(aRow("PROV_NOM").Equals(System.DBNull.Value), "", aRow("PROV_NOM"))
                aSubexp.municipio = IIf(aRow("MUN_NOM").Equals(System.DBNull.Value), "", aRow("MUN_NOM"))
                aSubexp.comarca = IIf(aRow("COMARCA_SGSA").Equals(System.DBNull.Value), "", aRow("COMARCA_SGSA"))
                colSubexplotaciones(i) = aSubexp
                i += 1
            Next
            Return colSubexplotaciones
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios por Tema por ID", EnableSession:=True)> _
    Public Function ObtenerServiciosTemaID(ByVal idTema As Integer, ByVal codIdioma As String) As Servicio()
        Try

            Dim strRoles As String
            Dim mTema As Tema
            strRoles = ObtenerAutenticacion()

            mTema = New Tema()
            mTema.CargarTema(idTema, strRoles, codIdioma)

            Return mTema.servicios

        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    <System.Web.Services.WebMethod(Description:="Obtencion de servicio de FEGA", EnableSession:=False)> _
    Public Function ObtenerServicioFEGA(ByVal tipoconsulta As String, ByVal ejercicio As String, _
    ByVal ambito As String, ByVal sector As String, ByVal codIdioma As String) As Servicio
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet

            strTableName = "VIS_SEF_SERVICIOS_FEGA"
            strDatasetName = "VIS_SEF_SERVICIOS_FEGA"
            strQuery = "SELECT VIS_SEF_SERVICIOS_FEGA.SER_CO_ID " & _
            " FROM VIS_SEF_SERVICIOS_FEGA " & _
            " WHERE "
            Dim strAnd As String = ""

            If tipoconsulta <> "" Then
                strQuery &= "SEF_DS_TIPOCONSULTA = '" & tipoconsulta & "' "
                strAnd = " AND "
            End If

            If ejercicio <> "" Then
                strQuery &= strAnd & " SEF_DS_EJERCICIO = '" & ejercicio & "' "
            End If
            If ambito <> "" Then
                strQuery &= strAnd & " SEF_DS_AMBITO = '" & ambito & "' "
            End If
            If sector <> "" Then
                strQuery &= strAnd & " SEF_CO_SECTOR = '" & sector & "' "
            End If

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim serv As New Servicio

            'Cuidado aqu�, puede fallar
            serv.Cargar(aDS.Tables(0).Rows(0)("SER_CO_ID"), codidioma)

            Return serv

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Capas del Un Servicio", EnableSession:=True)> _
    Public Function ObtenerCapasServicio(ByVal CodigoServicio As Integer, ByVal codIdioma As String) As Capa()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim arrCapas() As Capa
            Dim i As Integer

            'strRoles = ObtenerAutenticacion()


            strTableName = "vis_cap_capas"
            strDatasetName = "vis_cap_capas"
            'strQuery = "SELECT vis_cap_capas.cap_co_id " & _
            '			" FROM vis_cap_capas,vis_ser_servicios, vis_hos_hosts" & _
            '			" WHERE ((vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id)" & _
            '			" AND (vis_hos_hosts.hos_co_id = vis_ser_servicios.hos_co_id) " & _
            '			" AND (upper(vis_hos_hosts.hos_ds_nombre)='" & UCase(host) & "') AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "'))"

            strQuery = "SELECT vis_cap_capas.cap_co_id " & _
               " FROM vis_cap_capas,vis_ser_servicios" & _
               " WHERE vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id" & _
               " AND vis_ser_servicios.ser_co_id=" & CodigoServicio & ""

            ' " AND (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
            ' & _
            '" and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles + ")"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim arrCapas(aDS.Tables(0).Rows.Count - 1)
            Dim aRow As Data.DataRow
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim cpNueva As New Capa

                cpNueva.Cargar(aRow("CAP_CO_ID"), codIdioma)
                arrCapas(i) = cpNueva
                i += 1
            Next
            Return arrCapas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Capa", EnableSession:=True)> _
    Public Function ObtenerCapa(ByVal servicio As String, ByVal CodigoServicio As Integer, _
            ByVal NombreCapa As String, ByVal codIdioma As String) As Capa()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim arrCapas() As Capa
            Dim i As Integer

            strTableName = "vis_cap_capas"
            strDatasetName = "vis_cap_capas"
            'strQuery = "SELECT vis_cap_capas.cap_co_id " & _
            '			" FROM vis_cap_capas,vis_ser_servicios, vis_hos_hosts" & _
            '			" WHERE ((vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id)" & _
            '			" AND (vis_hos_hosts.hos_co_id = vis_ser_servicios.hos_co_id) " & _
            '			" AND (upper(vis_hos_hosts.hos_ds_nombre)='" & UCase(host) & "') AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "'))"

            If CodigoServicio > 0 Then
                strQuery = "SELECT vis_cap_capas.cap_co_id " & _
                      " FROM vis_cap_capas,vis_ser_servicios" & _
                      " WHERE vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id" & _
                      " AND vis_ser_servicios.ser_co_id=" & CodigoServicio & ""
            Else
                strQuery = "SELECT vis_cap_capas.cap_co_id " & _
                   " FROM vis_cap_capas,vis_ser_servicios" & _
                   " WHERE ((vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id)" & _
                   " AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "'))"
            End If

            strQuery &= " AND vis_cap_capas.cap_ds_nombre = '" & NombreCapa & "'"

            ' " AND (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
            ' & _
            '" and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles + ")"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim arrCapas(aDS.Tables(0).Rows.Count - 1)
            Dim aRow As Data.DataRow
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim cpNueva As New Capa

                cpNueva.Cargar(aRow("CAP_CO_ID"), codIdioma)
                arrCapas(i) = cpNueva
                i += 1
            Next
            Return arrCapas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Capas", EnableSession:=True)> _
    Public Function ObtenerCapas(ByVal host As String, ByVal servicio As String, ByVal codIdioma As String) As Capa()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim arrCapas() As Capa
            Dim i As Integer

            'strRoles = ObtenerAutenticacion()


            strTableName = "vis_cap_capas"
            strDatasetName = "vis_cap_capas"
            'strQuery = "SELECT vis_cap_capas.cap_co_id " & _
            '			" FROM vis_cap_capas,vis_ser_servicios, vis_hos_hosts" & _
            '			" WHERE ((vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id)" & _
            '			" AND (vis_hos_hosts.hos_co_id = vis_ser_servicios.hos_co_id) " & _
            '			" AND (upper(vis_hos_hosts.hos_ds_nombre)='" & UCase(host) & "') AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "'))"

            strQuery = "SELECT vis_cap_capas.cap_co_id " & _
               " FROM vis_cap_capas,vis_ser_servicios" & _
               " WHERE ((vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id)" & _
               " AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "'))"

            ' " AND (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
            ' & _
            '" and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles + ")"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim arrCapas(aDS.Tables(0).Rows.Count - 1)
            Dim aRow As Data.DataRow
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim cpNueva As New Capa

                cpNueva.Cargar(aRow("CAP_CO_ID"), codIdioma)
                arrCapas(i) = cpNueva
                i += 1
            Next
            Return arrCapas

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Campos", EnableSession:=True)> _
    Public Function ObtenerCampos(ByVal host As String, ByVal servicio As String, ByVal capa As String, ByVal codIdioma As String) As Campo()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrCampos() As Campo
            strRoles = ObtenerAutenticacion()

            strTableName = "vis_cam_campos"
            strDatasetName = "vis_cam_campos"
            strQuery = "SELECT vis_cam_campos.cam_co_id " & _
                        " FROM vis_cam_campos, vis_cap_capas, vis_ser_servicios,vis_hos_hosts,vis_rol_roles_servicio " & _
                        " WHERE ((vis_cap_capas.cap_co_id = vis_cam_campos.cap_co_id) " & _
                        " AND (vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id) " & _
                        " AND (vis_hos_hosts.hos_co_id = vis_ser_servicios.hos_co_id) " & _
                        " AND (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
                        " AND (upper(vis_hos_hosts.hos_ds_nombre)='" & UCase(host) & "')" & _
                        " AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "') " & _
                        " AND (upper(vis_cap_capas.cap_co_id)='" & UCase(capa) & "')" & _
                        " and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles + ")"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim arrCampos(aDS.Tables(0).Rows.Count - 1)
            Dim aRow As Data.DataRow
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim cmpNuevo As New Campo
                cmpNuevo.Cargar(aRow("CAM_CO_ID"), codIdioma)
                arrCampos(i) = cmpNuevo
                i += 1
            Next
            Return arrCampos
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Campos", EnableSession:=True)> _
    Public Function ObtenerCamposCapa(ByVal CodigoCapa As String, ByVal codIdioma As String) As Campo()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrCampos() As Campo
            'strRoles = ObtenerAutenticacion()

            strTableName = "vis_cam_campos"
            strDatasetName = "vis_cam_campos"

            strQuery = "SELECT vis_cam_campos.cam_co_id " & _
               " FROM vis_cam_campos, vis_cap_capas " & _
               " WHERE vis_cap_capas.cap_co_id = vis_cam_campos.cap_co_id " & _
               " AND vis_cap_capas.cap_co_id = '" & CodigoCapa & "' "


            '" AND (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
            ' & _
            '" and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles + ")"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim arrCampos(aDS.Tables(0).Rows.Count - 1)
            Dim aRow As Data.DataRow
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim cmpNuevo As New Campo
                cmpNuevo.Cargar(aRow("CAM_CO_ID"), codIdioma)
                arrCampos(i) = cmpNuevo
                i += 1
            Next
            Return arrCampos
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Campos", EnableSession:=True)> _
    Public Function ObtenerCamposPorNombre(ByVal host As String, ByVal servicio As String, ByVal capa As String, ByVal codIdioma As String) As Campo()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrCampos() As Campo
            'strRoles = ObtenerAutenticacion()

            strTableName = "vis_cam_campos"
            strDatasetName = "vis_cam_campos"
            'strQuery = "SELECT vis_cam_campos.cam_co_id " & _
            '   " FROM vis_cam_campos, vis_cap_capas, vis_ser_servicios,vis_hos_hosts " & _
            '   " WHERE ((vis_cap_capas.cap_co_id = vis_cam_campos.cap_co_id) " & _
            '   " AND (vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id) " & _
            '   " AND (vis_hos_hosts.hos_co_id = vis_ser_servicios.hos_co_id) " & _
            '   " AND (upper(vis_hos_hosts.hos_ds_nombre)='" & UCase(host) & "')" & _
            '   " AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "') " & _
            '   " AND (upper(vis_cap_capas.cap_ds_nombre)='" & UCase(capa) & "'))"

            strQuery = "SELECT vis_cam_campos.cam_co_id " & _
               " FROM vis_cam_campos, vis_cap_capas, vis_ser_servicios " & _
               " WHERE ((vis_cap_capas.cap_co_id = vis_cam_campos.cap_co_id) " & _
               " AND (vis_ser_servicios.ser_co_id = vis_cap_capas.ser_co_id) " & _
               " AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "') " & _
               " AND (upper(vis_cap_capas.cap_ds_nombre)='" & UCase(capa) & "'))"


            '" AND (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
            ' & _
            '" and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles + ")"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim arrCampos(aDS.Tables(0).Rows.Count - 1)
            Dim aRow As Data.DataRow
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim cmpNuevo As New Campo
                cmpNuevo.Cargar(aRow("CAM_CO_ID"), codIdioma)
                arrCampos(i) = cmpNuevo
                i += 1
            Next
            Return arrCampos
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Funcionalidades del Servicio", EnableSession:=True)> _
    Public Function ObtenerFuncionalidadesServicio(ByVal host As String, ByVal servicio As String) As Funcionalidad()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrFuncionalidad() As Funcionalidad

            strRoles = ObtenerAutenticacion()
            strTableName = "vis_fun_funcionalidad"
            strDatasetName = "vis_fun_funcionalidad"
            strQuery = "SELECT vis_fun_funcionalidades.fun_co_id " & _
                       " FROM vis_fun_funcionalidades,vis_sfn_serv_funcionalidades,vis_ser_servicios,vis_hos_hosts,vis_rol_roles_servicio " & _
                       " WHERE ((vis_fun_funcionalidades.fun_co_id =vis_sfn_serv_funcionalidades.fun_co_id) " & _
                       " AND (vis_ser_servicios.ser_co_id =vis_sfn_serv_funcionalidades.ser_co_id) " & _
                       " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                       " AND (vis_hos_hosts.hos_co_id = vis_ser_servicios.hos_co_id) " & _
                       " AND (upper(vis_hos_hosts.hos_ds_nombre)='" & UCase(host) & "')" & _
                       " AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "')" & _
                       " and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles + ")"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim arrFuncionalidad(aDS.Tables(0).Rows.Count - 1)
            Dim aRow As Data.DataRow

            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim fnNueva As New Funcionalidad
                fnNueva.Cargar(aRow("FUN_CO_ID"))
                arrFuncionalidad(i) = fnNueva
                i += 1
            Next
            Return arrFuncionalidad

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Funcionalidades de la Capa", EnableSession:=True)> _
    Public Function ObtenerFuncionalidadesCapa(ByVal host As String, ByVal servicio As String, ByVal capa As String) As Funcionalidad()
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim i As Integer
            Dim arrFuncionalidad() As Funcionalidad

            strRoles = ObtenerAutenticacion()

            strTableName = "vis_fun_funcionalidad"
            strDatasetName = "vis_fun_funcionalidad"
            strQuery = "SELECT vis_fun_funcionalidades.fun_co_id" & _
                       " FROM vis_fun_funcionalidades,vis_sfn_serv_funcionalidades,vis_ser_servicios,vis_hos_hosts,vis_rol_roles_servicio " & _
                       " WHERE ((vis_fun_funcionalidades.fun_co_id =vis_sfn_serv_funcionalidades.fun_co_id) " & _
                       " AND (vis_ser_servicios.ser_co_id =vis_sfn_serv_funcionalidades.ser_co_id) " & _
                       " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                       " AND (vis_hos_hosts.hos_co_id = vis_ser_servicios.hos_co_id) " & _
                       " AND (upper(vis_hos_hosts.hos_ds_nombre)='" & UCase(host) & "')" & _
                       " AND (upper(vis_ser_servicios.ser_ds_nombre)='" & UCase(servicio) & "')" & _
                       " and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles + ")"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim arrFuncionalidad(aDS.Tables(0).Rows.Count - 1)
            Dim aRow As Data.DataRow

            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim fnNueva As New Funcionalidad
                fnNueva.Cargar(aRow("FUN_CO_ID"))
                arrFuncionalidad(i) = fnNueva
                i += 1
            Next
            Return arrFuncionalidad
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Autenticacion", EnableSession:=True)> _
    Public Function Autenticacion(ByVal user As String, ByVal pwd As String) As UserInfo
        Dim ldapConnStr As String

        Dim strDirectorio As String
        'Dim conLdap As LdapConnection
        'Dim strUser As String
        'Dim ident As LdapDirectoryIdentifier
        Dim dirEnt As DirectoryEntry
        Dim prefijo As String
        Dim obj As Object
        Dim RetArray As ArrayList
        'Dim usuarioSolo As String
        Dim params() As String
        Dim usrInfo As UserInfo
        Dim ldapUrls As String()

        Dim dominio As String
        Dim tokensDominio As String()

        Try
            Inicializacion()
            ldapConnStr = System.Configuration.ConfigurationManager.AppSettings("Ldap")
            ldapUrls = ldapConnStr.Split("|")

            prefijo = System.Configuration.ConfigurationManager.AppSettings("Prefijo")
            strDirectorio = System.Configuration.ConfigurationManager.AppSettings("Directorio")
            'Try
            'dirEnt = New DirectoryEntry(ldapConnStr, prefijo + user + strDirectorio, pwd, AuthenticationTypes.None)

            user = LCase(user)
            params = user.Split("@") 'Separamos usuario y dominio
            'usuarioSolo = params(0)

            ' C�digo antiguo --> tarda mucho tiempo en validar porque prueba con todas las URLs LDAP del webconfig:
            ' si la URL LDAP no corresponde con el dominio que hemos seleccionado, tarda demasiado (dirEnt.NativeObject)

            'Dim i As Integer
            'user = LCase(user)
            'For i = 0 To ldapUrls.Length - 1
            '    dirEnt = New DirectoryEntry(ldapUrls(i), user, pwd, AuthenticationTypes.None)

            '    params = user.Split("@")
            '    usuarioSolo = params(0)
            '    'cnUsuario = dirEnt.Properties("cn").Value
            '    Try
            '        obj = dirEnt.NativeObject
            '    Catch ex As Exception
            '    End Try
            'Next i


            ' PJP 08/08/2013
            ' Con esto optimizamos el tiempo de espera en la validaci�n, ya que s�lo cogemos la URLDAP
            ' correspondiente al dominio que hayamos seleccionado. Nos basamos para ello en el primer valor del
            ' dominio, que identifica un�vocamente ese dominio y su URL LDAP asociada.
            dominio = params(1)
            tokensDominio = dominio.Split(".")

            Dim i As Integer
            Dim LdapURL As String = ""
            For i = 0 To ldapUrls.Length - 1
                If ldapUrls(i).Contains(tokensDominio(0)) Then
                    LdapURL = ldapUrls(i)
                    Exit For
                End If
            Next i

            If LdapURL <> "" Then
                dirEnt = New DirectoryEntry(LdapURL, user, pwd, AuthenticationTypes.None)

                Try
                    obj = dirEnt.NativeObject
                Catch ex As Exception
                End Try

                If obj Is Nothing Then
                    RetArray = New ArrayList
                    RetArray.Add("Error")
                    Me.Session("Rol") = RetArray
                    Return Nothing
                End If
                usrInfo = New UserInfo
                usrInfo.Cargar(user)

                If usrInfo.grupos Is Nothing Then
                    usrInfo.grupos = New ArrayList
                    usrInfo.grupos.Add("NO_ROL")
                ElseIf (usrInfo.grupos.Count = 0) Then
                    usrInfo.grupos.Add("NO_ROL")
                End If

                Me.Session("Usuario") = user
                Me.Session("Rol") = usrInfo.grupos
                Return usrInfo
            End If

        Catch ex As Exception

            RetArray = New ArrayList
            RetArray.Add("Error")
            Me.Session("Rol") = RetArray
            Return Nothing

        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Obtenci�n de Grupos a los que pertenece el usuario", EnableSession:=True)> _
    Public Function GetGrupos(ByVal user As String) As ArrayList
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strQuery As String
        Dim arrValores As New ArrayList

        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos

            strTablename = "VIS_USR_USUARIOS_ROL"
            strDatasetName = "VIS_USR_USUARIOS_ROL"
            strQuery = "SELECT DISTINCT VIS_USR_USUARIOS_ROL.ROL_CO_ID, VIS_ROL_ROLES.ROL_DS_NOMBRE" & _
              " FROM VIS_USU_USUARIOS,VIS_USR_USUARIOS_ROL, VIS_ROL_ROLES " & _
              " WHERE VIS_USU_USUARIOS.USU_CO_ID = VIS_USR_USUARIOS_ROL.USU_CO_ID " & _
              " AND VIS_ROL_ROLES.ROL_CO_ID = VIS_USR_USUARIOS_ROL.ROL_CO_ID " & _
              " AND VIS_USU_USUARIOS.USU_DS_NOMBRE = '" & user & "'"

            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)

            For Each aRow In aDS.Tables(0).Rows
                arrValores.Add(aRow("ROL_DS_NOMBRE"))
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    <System.Web.Services.WebMethod(Description:="Autenticacion", EnableSession:=True)> _
    Public Function AutenticacionOld(ByVal user As String, ByVal pwd As String) As Boolean
        Dim ldapConnStr As String
        Dim ldapConnStrGrupos As String
        Dim strDirectorio As String
        'Dim conLdap As LdapConnection
        'Dim strUser As String
        'Dim ident As LdapDirectoryIdentifier
        Dim dirEnt As DirectoryEntry
        Dim prefijo As String
        Dim mCount As Integer
        Dim obj As Object
        Dim search As DirectorySearcher
        Dim searchGrupos As DirectorySearcher
        Dim oResults As SearchResultCollection
        Dim oResult As SearchResult
        'Dim result As SearchResult
        Dim RetArray As New ArrayList
        Dim mLDAPRecord As String
        Dim usuarioSolo As String
        Dim cnUsuario As String
        Dim params() As String
        Dim valido As Boolean
        Dim ldapSufijo As String
        Dim ldapServer As String

        Try
            Inicializacion()
            ldapConnStr = System.Configuration.ConfigurationManager.AppSettings("Ldap")

            prefijo = System.Configuration.ConfigurationManager.AppSettings("Prefijo")
            strDirectorio = System.Configuration.ConfigurationManager.AppSettings("Directorio")
            'Try
            'dirEnt = New DirectoryEntry(ldapConnStr, prefijo + user + strDirectorio, pwd, AuthenticationTypes.None)

            dirEnt = New DirectoryEntry(ldapConnStr, user, pwd, AuthenticationTypes.None)

            params = user.Split("@")
            usuarioSolo = params(0)
            'cnUsuario = dirEnt.Properties("cn").Value


            obj = dirEnt.NativeObject
            search = New DirectorySearcher(dirEnt)
            'search.ReferralChasing = ReferralChasingOption.All
            search.SearchScope = DirectoryServices.SearchScope.Subtree

            search.Filter = "(userPrincipalName=" & user & ")" '"(SAMAccountName=" & usuarioSolo & ")"
            search.PropertiesToLoad.Add("cn")
            oResults = search.FindAll()
            mCount = oResults.Count
            cnUsuario = usuarioSolo
            If mCount > 0 Then
                For Each oResult In oResults
                    cnUsuario = oResult.GetDirectoryEntry().Properties("cn").Value
                    'Hemos obtenido el valor del CN!!!
                    'RetArray = GetGroups(mLDAPRecord, oResult.Path)
                Next
            Else
                'Lo hacemos temporalmente para obtener los CN de los usuarios fuera de mapa.mapya.es
                cnUsuario = ObtenerCN(user)
            End If

            search.Dispose()
            search = Nothing
            dirEnt.Dispose()
            dirEnt = Nothing

            ldapConnStrGrupos = System.Configuration.ConfigurationManager.AppSettings("LdapGIS")
            dirEnt = New DirectoryEntry(ldapConnStrGrupos, user, pwd, AuthenticationTypes.None)
            'dirEnt = New DirectoryEntry(ldapConnStr, user, pwd, AuthenticationTypes.None)
            'Obtenemos los grupos del Directorio Activo
            searchGrupos = New DirectorySearcher(dirEnt)
            searchGrupos.Filter = "(objectclass=Group)"
            searchGrupos.PropertiesToLoad.Add("cn")
            oResults = searchGrupos.FindAll()
            mCount = oResults.Count
            If mCount > 0 Then
                For Each oResult In oResults
                    mLDAPRecord = oResult.GetDirectoryEntry().Properties("cn").Value
                    If (GetGroups(mLDAPRecord, oResult.Path, cnUsuario)) Then
                        RetArray.Add(mLDAPRecord)
                    End If
                Next
            Else
                RetArray.Add("NO_ROL")
            End If
            If (RetArray.Count = 0) Then
                RetArray.Add("NO_ROL")
            End If
            searchGrupos.Dispose()
            searchGrupos = Nothing
            dirEnt.Dispose()
            dirEnt = Nothing

            Me.Session("Usuario") = user
            Me.Session("Rol") = RetArray
            Return True

            ' '' '' '' '' ''oResults = search.FindAll()
            ' '' '' '' '' ''mCount = oResults.Count
            ' '' '' '' '' ''If mCount > 0 Then
            ' '' '' '' '' ''    For Each oResult In oResults
            ' '' '' '' '' ''        mLDAPRecord = oResult.GetDirectoryEntry().Properties("cn").Value
            ' '' '' '' '' ''        RetArray = GetGroups(mLDAPRecord, oResult.Path)
            ' '' '' '' '' ''    Next
            ' '' '' '' '' ''Else
            ' '' '' '' '' ''    RetArray.Add("NO_ROL")
            ' '' '' '' '' ''End If

            ' '' '' '' '' ''Me.Session("Usuario") = user
            ' '' '' '' '' ''Me.Session("Rol") = RetArray
            ' '' '' '' '' ''Return True
            'Catch ldEx As LdapException
            '    RetArray.Add("Error")
            '   Me.Session("Rol") = RetArray
            '   Return False
            'End Try

        Catch ex As Exception
            RetArray.Add("Error")
            Me.Session("Rol") = RetArray
            Return False

        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Autenticacion2", EnableSession:=True)> _
    Public Function Autenticacion2(ByVal user As String, ByVal pwd As String) As ArrayList
        Dim ldapConnStr As String
        Dim ldapConnStrGrupos As String
        Dim strDirectorio As String
        'Dim conLdap As LdapConnection
        'Dim strUser As String
        'Dim ident As LdapDirectoryIdentifier
        Dim dirEnt As DirectoryEntry
        Dim prefijo As String
        Dim mCount As Integer
        Dim obj As Object
        Dim search As DirectorySearcher
        Dim searchGrupos As DirectorySearcher
        Dim oResults As SearchResultCollection
        Dim oResult As SearchResult
        'Dim result As SearchResult
        Dim RetArray As New ArrayList
        Dim mLDAPRecord As String
        Dim usuarioSolo As String
        Dim cnUsuario As String
        Dim params() As String
        Dim valido As Boolean
        Dim ldapSufijo As String
        Dim ldapServer As String

        Try
            Inicializacion()
            ldapConnStr = System.Configuration.ConfigurationManager.AppSettings("Ldap")

            prefijo = System.Configuration.ConfigurationManager.AppSettings("Prefijo")
            strDirectorio = System.Configuration.ConfigurationManager.AppSettings("Directorio")
            'Try
            'dirEnt = New DirectoryEntry(ldapConnStr, prefijo + user + strDirectorio, pwd, AuthenticationTypes.None)

            dirEnt = New DirectoryEntry(ldapConnStr, user, pwd, AuthenticationTypes.None)

            params = user.Split("@")
            usuarioSolo = params(0)
            'cnUsuario = dirEnt.Properties("cn").Value


            obj = dirEnt.NativeObject
            search = New DirectorySearcher(dirEnt)
            'search.ReferralChasing = ReferralChasingOption.All
            search.SearchScope = DirectoryServices.SearchScope.Subtree

            search.Filter = "(userPrincipalName=" & user & ")" '"(SAMAccountName=" & usuarioSolo & ")"
            search.PropertiesToLoad.Add("cn")
            oResults = search.FindAll()
            mCount = oResults.Count
            cnUsuario = usuarioSolo
            If mCount > 0 Then
                For Each oResult In oResults
                    cnUsuario = oResult.GetDirectoryEntry().Properties("cn").Value
                    'Hemos obtenido el valor del CN!!!
                    'RetArray = GetGroups(mLDAPRecord, oResult.Path)
                Next
            Else
                'Lo hacemos temporalmente para obtener los CN de los usuarios fuera de mapa.mapya.es
                cnUsuario = ObtenerCN(user)
            End If

            search.Dispose()
            search = Nothing
            dirEnt.Dispose()
            dirEnt = Nothing

            ldapConnStrGrupos = System.Configuration.ConfigurationManager.AppSettings("LdapGIS")
            dirEnt = New DirectoryEntry(ldapConnStrGrupos, user, pwd, AuthenticationTypes.None)
            'dirEnt = New DirectoryEntry(ldapConnStr, user, pwd, AuthenticationTypes.None)
            'Obtenemos los grupos del Directorio Activo
            searchGrupos = New DirectorySearcher(dirEnt)
            searchGrupos.Filter = "(objectclass=Group)"
            searchGrupos.PropertiesToLoad.Add("cn")
            oResults = searchGrupos.FindAll()
            mCount = oResults.Count
            If mCount > 0 Then
                For Each oResult In oResults
                    mLDAPRecord = oResult.GetDirectoryEntry().Properties("cn").Value
                    If (GetGroups(mLDAPRecord, oResult.Path, cnUsuario)) Then
                        RetArray.Add(mLDAPRecord)
                    End If
                Next
            Else
                RetArray.Add("NO_ROL")
            End If
            If (RetArray.Count = 0) Then
                RetArray.Add("NO_ROL")
            End If
            searchGrupos.Dispose()
            searchGrupos = Nothing
            dirEnt.Dispose()
            dirEnt = Nothing

            Me.Session("Usuario") = user
            Me.Session("Rol") = RetArray
            Return RetArray

            ' '' '' '' '' ''oResults = search.FindAll()
            ' '' '' '' '' ''mCount = oResults.Count
            ' '' '' '' '' ''If mCount > 0 Then
            ' '' '' '' '' ''    For Each oResult In oResults
            ' '' '' '' '' ''        mLDAPRecord = oResult.GetDirectoryEntry().Properties("cn").Value
            ' '' '' '' '' ''        RetArray = GetGroups(mLDAPRecord, oResult.Path)
            ' '' '' '' '' ''    Next
            ' '' '' '' '' ''Else
            ' '' '' '' '' ''    RetArray.Add("NO_ROL")
            ' '' '' '' '' ''End If

            ' '' '' '' '' ''Me.Session("Usuario") = user
            ' '' '' '' '' ''Me.Session("Rol") = RetArray
            ' '' '' '' '' ''Return True
            'Catch ldEx As LdapException
            '    RetArray.Add("Error")
            '   Me.Session("Rol") = RetArray
            '   Return False
            'End Try

        Catch ex As Exception
            RetArray.Add("Error")
            Me.Session("Rol") = RetArray
            Return RetArray

        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Inicializar", EnableSession:=True)> _
    Public Function Inicializacion() As Boolean
        Me.Session.Clear()
        Return True
    End Function

    Public Function GetGroups(ByVal cn As String, ByVal path As String, ByVal usuario As String) As Boolean
        Dim strServer As String

        Dim search As DirectorySearcher = New DirectorySearcher(path)
        search.Filter = "(cn=" & cn & ")"
        search.PropertiesToLoad.Add("member")
        'Dim groupNames As StringBuilder = New StringBuilder()
        'Dim retArray As ArrayList
        Try
            Dim result As SearchResult = search.FindOne()
            Dim propertyCount As Integer = result.Properties("member").Count


            Dim dn As String
            Dim nombreUsuario As String
            Dim equalsIndex, commaIndex


            Dim propertyCounter As Integer
            'retArray = New ArrayList()
            For propertyCounter = 0 To propertyCount - 1
                dn = CType(result.Properties("member")(propertyCounter), String)

                equalsIndex = dn.IndexOf("=", 1)
                commaIndex = dn.IndexOf(",", 1)
                If (equalsIndex = -1) Then
                    Return Nothing
                End If
                nombreUsuario = dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1)
                If (nombreUsuario = usuario) Then
                    Return True
                    'retArray.Add(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1))
                End If
                ' '' '' ''retArray.Add(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1))

            Next

        Catch ex As Exception
            Return Nothing
        End Try

        Return False
    End Function


    Private Function ObtenerCN(ByVal usuario As String) As String
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strQuery As String
        Dim strCN As String
        Try



            strTablename = "VIS_USU_USUARIOS"
            strDatasetName = "VIS_USU_USUARIOS"
            strQuery = "SELECT * " & _
                       " FROM " & strTablename & _
                       " WHERE UPPER(usu_ds_nombre) = '" & UCase(usuario) & "'"

            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                strCN = aRow("USU_DS_DESCRIPCION")
            Next

            Return strCN
        Catch ex As Exception
            Return ""
        End Try



    End Function
    Public Shared Function ExecuteQuery(ByVal strquery As String, ByVal strTableName As String, ByVal strDatasetName As String, _
                                        Optional ByVal connectionKey As String = "bbdd") As Data.DataSet

        Dim aDataSet As Data.DataSet = New Data.DataSet(strDatasetName)

        Dim aConn As PgSqlConnection
        Try
            Dim strConn As String
            strConn = System.Configuration.ConfigurationManager.AppSettings(connectionKey)

            aConn = New PgSqlConnection(strConn)
            aConn.Open()

            'Step 2.
            'Instantiate OleDbDataAdapter to create DataSet
            Dim mAdapter As PgSqlDataAdapter = New PgSqlDataAdapter

            'Fetch Product Details
            mAdapter.SelectCommand = New PgSqlCommand(strquery, aConn)

            'In-Memory cache of data


            'Step 3.
            'Fill the dataset 
            mAdapter.Fill(aDataSet, strTableName)

            aConn.Close()
            Return aDataSet

        Catch ex As Exception
            aConn.Close()
            Throw New Exception(ex.Message, ex)
        End Try


    End Function

    Public Shared Function ExecuteQueryGISPE(ByVal strquery As String, ByVal strTableName As String, ByVal strDatasetName As String) As Data.DataSet

        Return ExecuteQuery(strquery, strTableName, strDatasetName, "bbddgispe")

        'Try
        '    Dim strConn As String
        '    strConn = System.Configuration.ConfigurationManager.AppSettings("bbddgispe")

        '    Dim aConn As PgSqlConnection = New PgSqlConnection(strConn)
        '    aConn.Open()

        '    'Step 2.
        '    'Instantiate OleDbDataAdapter to create DataSet
        '    Dim mAdapter As PgSqlDataAdapter = New PgSqlDataAdapter

        '    'Fetch Product Details
        '    mAdapter.SelectCommand = New PgSqlCommand(strquery, aConn)

        '    'In-Memory cache of data
        '    Dim aDataSet As Data.DataSet = New Data.DataSet(strDatasetName)

        '    'Step 3.
        '    'Fill the dataset 
        '    mAdapter.Fill(aDataSet, strTableName)

        '    aConn.Close()
        '    Return aDataSet

        'Catch ex As Exception
        '    Return Nothing
        'End Try




    End Function

    Public Shared Function ExecuteQueryRAG(ByVal strquery As String, ByVal strTableName As String, ByVal strDatasetName As String) As Data.DataSet

        Return ExecuteQuery(strquery, strTableName, strDatasetName, "bbddrag")

        'Try
        '    Dim strConn As String
        '    strConn = System.Configuration.ConfigurationManager.AppSettings("bbddrag")

        '    Dim aConn As PgSqlConnection = New PgSqlConnection(strConn)
        '    aConn.Open()

        '    'Step 2.
        '    'Instantiate OleDbDataAdapter to create DataSet
        '    Dim mAdapter As PgSqlDataAdapter = New PgSqlDataAdapter

        '    'Fetch Product Details
        '    mAdapter.SelectCommand = New PgSqlCommand(strquery, aConn)

        '    'In-Memory cache of data
        '    Dim aDataSet As Data.DataSet = New Data.DataSet(strDatasetName)

        '    'Step 3.
        '    'Fill the dataset 
        '    mAdapter.Fill(aDataSet, strTableName)

        '    aConn.Close()
        '    Return aDataSet

        'Catch ex As Exception
        '    Return Nothing
        'End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Tematicos", EnableSession:=True)> _
    Public Function ObtenerListaTematicos() As TematicoLista()
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strUsuario As String, strRoles As String
        Dim i As Integer
        Dim resultadoTematico() As TematicoLista, strQuery As String
        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos
            strRoles = ObtenerAutenticacion()

            strUsuario = Me.Session("Usuario")

            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT distinct(vis_tad_tematicosdemanda.tad_co_id), vis_tad_tematicosdemanda.TAD_DS_NOMBRE_ANALISIS, vis_tad_tematicosdemanda.TAD_DS_USUARIO,vis_tad_tematicosdemanda.TPR_CO_ID, vis_tad_tematicosdemanda.TIT_CO_ID, vis_tad_tematicosdemanda.TAD_DS_DESCRIPCION, vis_tad_tematicosdemanda.TAD_DS_FECHA" & _
              " FROM vis_tad_tematicosdemanda LEFT JOIN vis_tro_tematicorol  " & _
                " ON vis_tad_tematicosdemanda.tad_co_id = vis_tro_tematicorol.tad_co_id " & _
              " WHERE vis_tad_tematicosdemanda.tad_ds_usuario='" & strUsuario & "'"
            '" (vis_tad_tematicosdemanda.tpr_co_id=1 or (vis_tad_tematicosdemanda.tpr_co_id=2 AND vis_tad_tematicosdemanda.tad_ds_usuario='" & strUsuario & "')" & _
            '" OR (vis_tad_tematicosdemanda.tpr_co_id=3 AND vis_tro_tematicorol.rol_ca_id " & strRoles & "))"

            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            ReDim resultadoTematico(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim temNuevo As New TematicoLista
                temNuevo.Id = aRow("TAD_CO_ID")
                temNuevo.nombre = IIf(aRow("TAD_DS_NOMBRE_ANALISIS").Equals(System.DBNull.Value), "", aRow("TAD_DS_NOMBRE_ANALISIS"))
                temNuevo.autor = IIf(aRow("TAD_DS_USUARIO").Equals(System.DBNull.Value), "", aRow("TAD_DS_USUARIO"))
                temNuevo.privacidad = IIf(aRow("TPR_CO_ID").Equals(System.DBNull.Value), 1, aRow("TPR_CO_ID"))
                temNuevo.idLayer = IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), "1", aRow("TIT_CO_ID").ToString())
                temNuevo.descripcion = IIf(aRow("TAD_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TAD_DS_DESCRIPCION"))
                temNuevo.fecha = IIf(aRow("TAD_DS_FECHA").Equals(System.DBNull.Value), "", aRow("TAD_DS_FECHA"))

                resultadoTematico(i) = temNuevo
                i += 1
            Next
            Return resultadoTematico
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Tem�tico Avanzado", EnableSession:=True)> _
    Public Function ObtenerTematicoAvanzado(ByVal Id As Integer, ByVal userInfo As UserInfo) As TematicoAvzdo
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strUsuario As String, strRoles As String
        Dim i As Integer
        Dim resultado As TematicoAvzdo, strQuery As String
        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos

            strUsuario = userInfo.nombre

            strTablename = "VIS_TAV_TEMATICOS_AVANZADOS"
            strDatasetName = "VIS_TAV_TEMATICOS_AVANZADOS"

            strRoles = ObtenerAutenticacion()

            If strUsuario Is Nothing Then
                Return Nothing
            End If

            Dim rolAdmATematicos As String = "'ADMTEMATICOSAV'"
            If strRoles.IndexOf(rolAdmATematicos) <> -1 Then
                strQuery = "SELECT * FROM VIS_TAV_TEMATICOS_AVANZADOS TAV INNER JOIN VIS_TTA_TIPO_TEMATICO_AV TTA ON TTA.TTA_CO_ID = TAV.TTA_CO_ID  WHERE TAV_CO_ID=" & Id
            Else
                strQuery = "SELECT * FROM VIS_TAV_TEMATICOS_AVANZADOS TAV INNER JOIN VIS_TTA_TIPO_TEMATICO_AV TTA ON TTA.TTA_CO_ID = TAV.TTA_CO_ID  WHERE USU_DS_NOMBRE = '" & strUsuario & "' AND TAV_CO_ID=" & Id
            End If


            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName, "bbddComun")
            aRow = aDS.Tables(0).Rows(0)
            Dim item As New TematicoAvzdo
            item.Id = aRow("TAV_CO_ID")
            item.nombre = aRow("TAV_DS_NOMBRE")
            item.fecha = aRow("TAV_DS_FECHA")
            item.IdTipo = aRow("TTA_CO_ID")
            item.Tipo = aRow("TTA_DS_DESCRIPCION")
            item.LayerDrawingOptions = IIf(aRow("TAV_DS_LAYERDRAWINGOPTIONS").Equals(System.DBNull.Value), "", aRow("TAV_DS_LAYERDRAWINGOPTIONS"))
            item.LayerDataSource = IIf(aRow("TAV_DS_LAYERDATASOURCE").Equals(System.DBNull.Value), "", aRow("TAV_DS_LAYERDATASOURCE"))
            item.PointClustering = IIf(aRow("TAV_BOL_POINTCLUSTERING").Equals("N"), False, True)
            item.Renderer = IIf(aRow("TAV_DS_JSON_RENDERER").Equals(System.DBNull.Value), "", aRow("TAV_DS_JSON_RENDERER"))
            item.definitionExp = IIf(aRow("TAV_DS_DEFINITIONEXPRESSION").Equals(System.DBNull.Value), "", aRow("TAV_DS_DEFINITIONEXPRESSION"))
            item.Properties = IIf(aRow("TAV_DS_PROPERTIES").Equals(System.DBNull.Value), "", aRow("TAV_DS_PROPERTIES"))

            resultado = item
            Return resultado
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Obtener Tem�ticos Avanzados", EnableSession:=True)> _
    Public Function ObtenerListaTematicosAvanzadosBusqueda(ByVal fecha As String, ByVal descripcion As String, ByVal tipoOrigen As Integer, ByVal userInfo As UserInfo) As TematicoAvzdo()
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strUsuario As String, strRoles As String
        Dim i As Integer
        Dim resultado() As TematicoAvzdo, strQuery As String
        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos

            strUsuario = userInfo.nombre

            strRoles = ObtenerAutenticacion()

            If strUsuario Is Nothing Then
                Return Nothing
            End If

            Dim rolAdmATematicos As String = "'ADMTEMATICOSAV'"
            Dim strAnd As String = "WHERE"

            strTablename = "VIS_TAV_TEMATICOS_AVANZADOS"
            strDatasetName = "VIS_TAV_TEMATICOS_AVANZADOS"

            If strRoles.IndexOf(rolAdmATematicos) <> -1 Then
                strQuery = "SELECT TAV_CO_ID, TAV_DS_NOMBRE, TAV_DS_FECHA FROM VIS_TAV_TEMATICOS_AVANZADOS "
            Else
                strQuery = "SELECT TAV_CO_ID, TAV_DS_NOMBRE, TAV_DS_FECHA FROM VIS_TAV_TEMATICOS_AVANZADOS WHERE USU_DS_NOMBRE = '" & strUsuario & "'"
                strAnd = " AND "
            End If

            '" (vis_tad_tematicosdemanda.tpr_co_id=1 or (vis_tad_tematicosdemanda.tpr_co_id=2 AND vis_tad_tematicosdemanda.tad_ds_usuario='" & strUsuario & "')" & _
            '" OR (vis_tad_tematicosdemanda.tpr_co_id=3 AND vis_tro_tematicorol.rol_ca_id " & strRoles & ")) "

            If descripcion <> "" Then
                strQuery &= strAnd & "  UPPER(TAV_DS_NOMBRE) LIKE '%" & UCase(descripcion) & "%'"
                strAnd = " AND "
            End If

            If fecha <> "" Then
                strQuery &= strAnd & " TAV_DS_FECHA LIKE '%" & fecha & "%'"
                strAnd = " AND "
            End If

            If tipoOrigen <> -1 Then
                strQuery &= strAnd & " TTA_CO_ID = " & tipoOrigen & ""
                strAnd = " AND "
            End If

            strQuery &= " order by TAV_CO_ID DESC"

            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName, "bbddComun")
            ReDim resultado(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim item As New TematicoAvzdo
                item.Id = aRow("TAV_CO_ID")
                item.nombre = aRow("TAV_DS_NOMBRE")
                item.fecha = aRow("TAV_DS_FECHA")

                resultado(i) = item
                i += 1
            Next
            Return resultado
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Obtener Selecciones", EnableSession:=True)> _
    Public Function ObtenerListaSeleccionesBusqueda(ByVal fecha As String, ByVal descripcion As String, _
                                                    ByVal codCapa As String, _
                                                    ByVal userInfo As UserInfo) As SeleccionLista()
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strUsuario As String, strRoles As String
        Dim i As Integer
        Dim resultado() As SeleccionLista, strQuery As String
        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos

            strUsuario = userInfo.nombre


            strTablename = "VIS_SEL_SELECCION"
            strDatasetName = "VIS_SEL_SELECCION"
            strQuery = "SELECT distinct(VIS_SEL_SELECCION.SEL_CO_ID) SEL_CO_ID, VIS_SEL_SELECCION.SEL_DS_DESCRIPCION, VIS_SEL_SELECCION.SEL_DS_FECHA, VIS_SEL_SELECCION.CAP_CO_ID,  VIS_CAP_CAPAS.SER_CO_ID, VIS_SEL_SELECCION.SEL_DS_DEFINITIONEXP, " & _
              "VIS_SEL_SELECCION.FILTRO_CAP_CO_ID, VIS_CAP_CAPAS_FILTRO.SER_CO_ID SER_CO_ID_FILTRO, VIS_SEL_SELECCION.FILTRO_DS_DEFINITIONEXP, SEL_DS_OBJECTIDS " & _
              " FROM VIS_SEL_SELECCION INNER JOIN VIS_CAP_CAPAS ON VIS_SEL_SELECCION.CAP_CO_ID = VIS_CAP_CAPAS.CAP_CO_ID " & _
              " LEFT JOIN VIS_CAP_CAPAS VIS_CAP_CAPAS_FILTRO ON VIS_SEL_SELECCION.FILTRO_CAP_CO_ID = VIS_CAP_CAPAS_FILTRO.CAP_CO_ID  " & _
              " WHERE " & _
              "	VIS_SEL_SELECCION.USU_DS_NOMBRE='" & strUsuario & "'"
            '" (vis_tad_tematicosdemanda.tpr_co_id=1 or (vis_tad_tematicosdemanda.tpr_co_id=2 AND vis_tad_tematicosdemanda.tad_ds_usuario='" & strUsuario & "')" & _
            '" OR (vis_tad_tematicosdemanda.tpr_co_id=3 AND vis_tro_tematicorol.rol_ca_id " & strRoles & ")) "

            If descripcion <> "" Then
                strQuery &= " AND UPPER(VIS_SEL_SELECCION.SEL_DS_DESCRIPCION) LIKE '%" & UCase(descripcion) & "%'"
            End If

            If codCapa <> "" Then
                strQuery &= " AND VIS_SEL_SELECCION.CAP_CO_ID ='" & codCapa & "'"
            End If

            If fecha <> "" Then
                strQuery &= " AND VIS_SEL_SELECCION.SEL_DS_FECHA LIKE '%" & fecha & "%'"
            End If


            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            ReDim resultado(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim temNuevo As New SeleccionLista
                temNuevo.Id = aRow("SEL_CO_ID")
                temNuevo.descripcion = IIf(aRow("SEL_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("SEL_DS_DESCRIPCION"))
                temNuevo.fecha = IIf(aRow("SEL_DS_FECHA").Equals(System.DBNull.Value), "", aRow("SEL_DS_FECHA"))
                temNuevo.codCapa = aRow("CAP_CO_ID")
                temNuevo.codServicio = aRow("SER_CO_ID")
                temNuevo.objectIds = IIf(aRow("SEL_DS_OBJECTIDS").Equals(System.DBNull.Value), "", aRow("SEL_DS_OBJECTIDS"))
                temNuevo.definitionExp = IIf(aRow("SEL_DS_DEFINITIONEXP").Equals(System.DBNull.Value), "", aRow("SEL_DS_DEFINITIONEXP"))
                temNuevo.codCapaFiltro = IIf(aRow("FILTRO_CAP_CO_ID").Equals(System.DBNull.Value), "", aRow("FILTRO_CAP_CO_ID"))
                temNuevo.codServicioFiltro = IIf(aRow("SER_CO_ID_FILTRO").Equals(System.DBNull.Value), "", aRow("SER_CO_ID_FILTRO"))
                temNuevo.definitionExpFiltro = IIf(aRow("FILTRO_DS_DEFINITIONEXP").Equals(System.DBNull.Value), "", aRow("FILTRO_DS_DEFINITIONEXP"))

                resultado(i) = temNuevo
                i += 1
            Next
            Return resultado
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Tematicos filtro", EnableSession:=True)> _
    Public Function ObtenerListaTematicosBusquedaFlex(ByVal fecha As String, ByVal nombre As String, ByVal userInfo As UserInfo) As TematicoLista()
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strUsuario As String, strRoles As String
        Dim i As Integer
        Dim resultadoTematico() As TematicoLista, strQuery As String
        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos
            strRoles = ObtenerAutenticacionFlex(userInfo)

            strUsuario = userInfo.nombre


            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT distinct(vis_tad_tematicosdemanda.tad_co_id), vis_tad_tematicosdemanda.TAD_DS_NOMBRE_ANALISIS, vis_tad_tematicosdemanda.TAD_DS_USUARIO,vis_tad_tematicosdemanda.TPR_CO_ID, vis_tad_tematicosdemanda.TIT_CO_ID, vis_tad_tematicosdemanda.TAD_DS_DESCRIPCION, vis_tad_tematicosdemanda.TAD_DS_FECHA" & _
              " FROM vis_tad_tematicosdemandaLEFT JOIN vis_tro_tematicorol  " & _
                " ON vis_tad_tematicosdemanda.tad_co_id = vis_tro_tematicorol.tad_co_id " & _
              " WHERE " & _
              "	vis_tad_tematicosdemanda.tad_ds_usuario='" & strUsuario & "'"
            '" (vis_tad_tematicosdemanda.tpr_co_id=1 or (vis_tad_tematicosdemanda.tpr_co_id=2 AND vis_tad_tematicosdemanda.tad_ds_usuario='" & strUsuario & "')" & _
            '" OR (vis_tad_tematicosdemanda.tpr_co_id=3 AND vis_tro_tematicorol.rol_ca_id " & strRoles & ")) "

            If nombre <> "" Then
                strQuery &= " AND UPPER(vis_tad_tematicosdemanda.TAD_DS_NOMBRE_ANALISIS) LIKE '%" & UCase(nombre) & "%'"
            End If

            If fecha <> "" Then
                strQuery &= " AND vis_tad_tematicosdemanda.TAD_DS_FECHA LIKE '%" & fecha & "%'"
            End If


            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            ReDim resultadoTematico(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim temNuevo As New TematicoLista
                temNuevo.Id = aRow("TAD_CO_ID")
                temNuevo.nombre = IIf(aRow("TAD_DS_NOMBRE_ANALISIS").Equals(System.DBNull.Value), "", aRow("TAD_DS_NOMBRE_ANALISIS"))
                temNuevo.autor = IIf(aRow("TAD_DS_USUARIO").Equals(System.DBNull.Value), "", aRow("TAD_DS_USUARIO"))
                temNuevo.privacidad = IIf(aRow("TPR_CO_ID").Equals(System.DBNull.Value), 1, aRow("TPR_CO_ID"))
                temNuevo.idLayer = IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), "1", aRow("TIT_CO_ID").ToString())
                temNuevo.descripcion = IIf(aRow("TAD_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TAD_DS_DESCRIPCION"))
                temNuevo.fecha = IIf(aRow("TAD_DS_FECHA").Equals(System.DBNull.Value), "", aRow("TAD_DS_FECHA"))

                resultadoTematico(i) = temNuevo
                i += 1
            Next
            Return resultadoTematico
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Tematicos filtro", EnableSession:=True)> _
    Public Function ObtenerListaTematicosBusquedaGeo(ByVal fecha As String, ByVal nombre As String, ByVal userInfo As UserInfo) As TematicoLista()
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strUsuario As String, strRoles As String
        Dim i As Integer
        Dim resultadoTematico() As TematicoLista, strQuery As String
        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos
            '  strRoles = ObtenerAutenticacionFlex(userInfo)

            'strUsuario = userInfo.nombre


            strTablename = "VIS_TAD_TEMATICOS_GEOMETRIA"
            strDatasetName = "VIS_TAD_TEMATICOS_GEOMETRIA"
            strQuery = "SELECT distinct(VIS_TAD_TEMATICOS_GEOMETRIA.tad_co_id), VIS_TAD_TEMATICOS_GEOMETRIA.TAD_DS_NOMBRE_ANALISIS, VIS_TAD_TEMATICOS_GEOMETRIA.TAD_DS_USUARIO,VIS_TAD_TEMATICOS_GEOMETRIA.TPR_CO_ID, VIS_TAD_TEMATICOS_GEOMETRIA.TIT_CO_ID, VIS_TAD_TEMATICOS_GEOMETRIA.TAD_DS_DESCRIPCION, VIS_TAD_TEMATICOS_GEOMETRIA.TAD_DS_FECHA" & _
                           " FROM VIS_TAD_TEMATICOS_GEOMETRIA "

            If nombre <> "" Or fecha <> "" Then
                strQuery &= " WHERE "
            End If
            If nombre <> "" Then
                strQuery &= "  VIS_TAD_TEMATICOS_GEOMETRIA.TAD_DS_NOMBRE_ANALISIS LIKE '%" & nombre & "%'"
            End If

            If fecha <> "" Then
                If nombre <> "" Then
                    strQuery &= " AND "
                End If
                strQuery &= " VIS_TAD_TEMATICOS_GEOMETRIA.TAD_DS_FECHA LIKE '%" & fecha & "%'"
            End If


            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            ReDim resultadoTematico(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim temNuevo As New TematicoLista
                temNuevo.Id = aRow("TAD_CO_ID")
                temNuevo.nombre = IIf(aRow("TAD_DS_NOMBRE_ANALISIS").Equals(System.DBNull.Value), "", aRow("TAD_DS_NOMBRE_ANALISIS"))
                temNuevo.autor = IIf(aRow("TAD_DS_USUARIO").Equals(System.DBNull.Value), "", aRow("TAD_DS_USUARIO"))
                temNuevo.privacidad = IIf(aRow("TPR_CO_ID").Equals(System.DBNull.Value), 1, aRow("TPR_CO_ID"))
                temNuevo.idLayer = IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), "1", aRow("TIT_CO_ID").ToString())
                temNuevo.descripcion = IIf(aRow("TAD_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TAD_DS_DESCRIPCION"))
                temNuevo.fecha = IIf(aRow("TAD_DS_FECHA").Equals(System.DBNull.Value), "", aRow("TAD_DS_FECHA"))

                resultadoTematico(i) = temNuevo
                i += 1
            Next
            Return resultadoTematico
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Tematicos predefinidos", EnableSession:=True)> _
    Public Function ObtenerListaTematicosPrefef(ByVal userInfo As UserInfo) As TematicoPredefinido()
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strUsuario As String, strRoles As String
        Dim i As Integer
        Dim resultado() As TematicoPredefinido, strQuery As String
        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos
            strRoles = ObtenerAutenticacionFlex(userInfo)

            strUsuario = userInfo.nombre


            strTablename = "VIS_TPF_TEMATICOSPREDEFINIDOS"
            strDatasetName = "VIS_TPF_TEMATICOSPREDEFINIDOS"
            strQuery = "SELECT distinct(VIS_TPF_TEMATICOSPREDEFINIDOS.TPF_CO_ID), VIS_TPF_TEMATICOSPREDEFINIDOS.TPF_DS_DESCRIPCION, " & _
              " VIS_TPF_TEMATICOSPREDEFINIDOS.TIT_CO_ID,VIS_TPF_TEMATICOSPREDEFINIDOS.CEX_CO_ID, VIS_TEX_TABLASEXTERNAS.TEX_DS_NOMBRE, " & _
              " VIS_CEX_CAMPOSTABLASEXTERNAS.CEX_DS_NOMBRE" & _
              " FROM VIS_TPF_TEMATICOSPREDEFINIDOS inner join vis_tpfr_tematicopredefrol  on VIS_TPF_TEMATICOSPREDEFINIDOS.tpf_co_id = vis_tpfr_tematicopredefrol.tpf_co_id " & _
              " INNER JOIN VIS_CEX_CAMPOSTABLASEXTERNAS ON VIS_TPF_TEMATICOSPREDEFINIDOS.CEX_CO_ID = VIS_CEX_CAMPOSTABLASEXTERNAS.CEX_CO_ID " & _
              " INNER JOIN VIS_TEX_TABLASEXTERNAS ON VIS_TEX_TABLASEXTERNAS.TEX_CO_ID = VIS_CEX_CAMPOSTABLASEXTERNAS.TEX_CO_ID " & _
              " WHERE (vis_tpfr_tematicopredefrol.rol_co_id " & strRoles

            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            ReDim resultado(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim temNuevo As New TematicoPredefinido
                temNuevo.Id = aRow("TPF_CO_ID")
                temNuevo.descripcion = IIf(aRow("TPF_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TPF_DS_DESCRIPCION"))
                temNuevo.tipoTematico = IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), 0, aRow("TIT_CO_ID"))
                temNuevo.campoJoinTematico = IIf(aRow("CEX_CO_ID").Equals(System.DBNull.Value), 0, aRow("CEX_CO_ID"))
                temNuevo.nombreCampoJoinTematico = IIf(aRow("CEX_DS_NOMBRE").Equals(System.DBNull.Value), "", aRow("CEX_DS_NOMBRE"))
                temNuevo.tablaExterna = IIf(aRow("TEX_DS_NOMBRE").Equals(System.DBNull.Value), "", aRow("TEX_DS_NOMBRE"))
                resultado(i) = temNuevo
                i += 1
            Next
            Return resultado
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Tematicos filtro", EnableSession:=True)> _
    Public Function ObtenerListaTematicosBusqueda(ByVal fecha As String, ByVal nombre As String) As TematicoLista()
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strUsuario As String, strRoles As String
        Dim i As Integer
        Dim resultadoTematico() As TematicoLista, strQuery As String
        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos
            strRoles = ObtenerAutenticacion()

            strUsuario = Me.Session("Usuario")

            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT distinct(vis_tad_tematicosdemanda.tad_co_id), vis_tad_tematicosdemanda.TAD_DS_NOMBRE_ANALISIS, vis_tad_tematicosdemanda.TAD_DS_USUARIO,vis_tad_tematicosdemanda.TPR_CO_ID, vis_tad_tematicosdemanda.TIT_CO_ID, vis_tad_tematicosdemanda.TAD_DS_DESCRIPCION, vis_tad_tematicosdemanda.TAD_DS_FECHA" & _
              " FROM vis_tad_tematicosdemandaLEFT JOIN vis_tro_tematicorol  " & _
                " ON vis_tad_tematicosdemanda.tad_co_id = vis_tro_tematicorol.tad_co_id " & _
              " WHERE  " & _
              "	vis_tad_tematicosdemanda.tad_ds_usuario='" & strUsuario & "'"
            '" (vis_tad_tematicosdemanda.tpr_co_id=1 or (vis_tad_tematicosdemanda.tpr_co_id=2 AND vis_tad_tematicosdemanda.tad_ds_usuario='" & strUsuario & "')" & _
            '" OR (vis_tad_tematicosdemanda.tpr_co_id=3 AND vis_tro_tematicorol.rol_ca_id " & strRoles & ")) "

            If nombre <> "" Then
                strQuery &= " AND vis_tad_tematicosdemanda.TAD_DS_NOMBRE_ANALISIS LIKE '%" & nombre & "%'"
            End If

            If fecha <> "" Then
                strQuery &= " AND vis_tad_tematicosdemanda.TAD_DS_FECHA LIKE '%" & fecha & "%'"
            End If


            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            ReDim resultadoTematico(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim temNuevo As New TematicoLista
                temNuevo.Id = aRow("TAD_CO_ID")
                temNuevo.nombre = IIf(aRow("TAD_DS_NOMBRE_ANALISIS").Equals(System.DBNull.Value), "", aRow("TAD_DS_NOMBRE_ANALISIS"))
                temNuevo.autor = IIf(aRow("TAD_DS_USUARIO").Equals(System.DBNull.Value), "", aRow("TAD_DS_USUARIO"))
                temNuevo.privacidad = IIf(aRow("TPR_CO_ID").Equals(System.DBNull.Value), 1, aRow("TPR_CO_ID"))
                temNuevo.idLayer = IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), "1", aRow("TIT_CO_ID").ToString())
                temNuevo.descripcion = IIf(aRow("TAD_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TAD_DS_DESCRIPCION"))
                temNuevo.fecha = IIf(aRow("TAD_DS_FECHA").Equals(System.DBNull.Value), "", aRow("TAD_DS_FECHA"))

                resultadoTematico(i) = temNuevo
                i += 1
            Next
            Return resultadoTematico
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="ObtenerPlantillas", EnableSession:=True)> _
    Public Function ObtenerPlantillas(ByVal nombre As String) As Plantilla()
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strQuery As String
        Dim colPlantillas As Plantilla()
        Dim aPlantilla As Plantilla
        Try
            strTablename = "VIS_SPN_PLANTILLAS"
            strDatasetName = "VIS_SPN_PLANTILLAS"
            strQuery = "SELECT SPN_CO_ID, VIS_SER_SERVICIOS.SER_CO_ID, SPN_DS_MXD, SPN_DS_NOMBRE FROM VIS_SPN_PLANTILLAS,VIS_SER_SERVICIOS " & _
                "WHERE VIS_SER_SERVICIOS.SER_CO_ID=VIS_SPN_PLANTILLAS.SER_CO_ID AND UPPER(VIS_SER_SERVICIOS.SER_DS_NOMBRE) LIKE '%" + UCase(nombre) + "'"
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            ReDim colPlantillas(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                aPlantilla = New Plantilla()
                aPlantilla.id = IIf(aRow("SPN_CO_ID").Equals(System.DBNull.Value), "", aRow("SPN_CO_ID"))
                aPlantilla.idServicio = IIf(aRow("SER_CO_ID").Equals(System.DBNull.Value), "", aRow("SER_CO_ID"))
                aPlantilla.mxd = IIf(aRow("SPN_DS_MXD").Equals(System.DBNull.Value), "", aRow("SPN_DS_MXD"))
                aPlantilla.nombre = IIf(aRow("SPN_DS_NOMBRE").Equals(System.DBNull.Value), "", aRow("SPN_DS_NOMBRE"))
                colPlantillas(i) = aPlantilla
                i += 1
            Next
            Return colPlantillas
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="ObtenerMXD", EnableSession:=True)> _
    Public Function ObtenerMXD(ByVal idPlantilla As Long) As String
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strQuery As String
        Dim strMxd As String
        Try
            strTablename = "VIS_SPN_PLANTILLAS"
            strDatasetName = "VIS_SPN_PLANTILLAS"
            strQuery = "SELECT  SPN_DS_MXD FROM VIS_SPN_PLANTILLAS " & _
                "WHERE SPN_CO_ID ='" + idPlantilla.ToString() + "'"
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            strMxd = ""
            For Each aRow In aDS.Tables(0).Rows
                strMxd = IIf(aRow("SPN_DS_MXD").Equals(System.DBNull.Value), "", aRow("SPN_DS_MXD"))

            Next
            Return strMxd
        Catch ex As Exception
            Return ""
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Ejecuci�n de Consulta", EnableSession:=True)> _
    Public Function EjecutarConsultaPrueba() As Ficha
        Dim consultaEjecucion As New Consulta
        Dim fichaResultado As Ficha


        consultaEjecucion.Cargar("SONDEOS_LISTA", "es")
        Dim claves() As String, valores() As String, params() As String
        ReDim claves(3)
        claves(0) = "PROV"
        claves(1) = "MUNI"
        claves(2) = "CUENCA"
        claves(3) = "UNIDAD"
        ReDim valores(3)
        valores(0) = "22"
        valores(1) = "22081"
        valores(2) = "09"
        valores(3) = "0918"
        ReDim params(3)
        params(0) = "PROV"
        params(1) = "MUNI"
        params(2) = "CUENCA"
        params(3) = "UNIDAD"
        fichaResultado = consultaEjecucion.Ejecutar(claves, valores, params)



        Return fichaResultado

    End Function



    <System.Web.Services.WebMethod(Description:="Ejecuci�n de Consulta", EnableSession:=True)> _
    Public Function EjecutarConsulta(ByVal nombre As String, ByVal claves() As String, ByVal valores() As String, ByVal params() As String, ByVal codIdioma As String) As Ficha
        Dim consultaEjecucion As New Consulta
        Dim fichaResultado As Ficha


        consultaEjecucion.Cargar(nombre, codIdioma)

        fichaResultado = consultaEjecucion.Ejecutar(claves, valores, params)



        Return fichaResultado

    End Function

    <System.Web.Services.WebMethod(Description:="Ejecuci�n de Consulta", EnableSession:=True)> _
    Public Function EjecutarConsultaPost(ByVal xmlConsulta As XmlDocument, ByVal codIdioma As String) As Ficha
        Dim consultaEjecucion As New Consulta
        Dim fichaResultado As Ficha
        Dim m_nodelist As XmlNodeList
        Dim m_node As XmlNode, m_node2 As XmlNode
        Dim strNombre As String
        m_nodelist = xmlConsulta.SelectNodes("/consulta")
        'S�lo tenemos un nodo!!!
        m_node = m_nodelist(0)
        m_node2 = m_node.SelectSingleNode("name")
        strNombre = m_node2.InnerText
        'Ya tenemos el valor del nombre de consulta, ahora hay que cargarla
        consultaEjecucion.Cargar(strNombre, codIdioma)

        fichaResultado = consultaEjecucion.EjecutarPost(xmlConsulta)


        Return fichaResultado

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Tem�tico", EnableSession:=True)> _
    Public Function ObtenerTematico(ByVal idTematico As Integer) As Tematico
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim resultadoTematico As Tematico, strQuery As String
        Try
            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT TAD_CO_ID FROM VIS_TAD_TEMATICOSDEMANDA WHERE TAD_CO_ID=" & idTematico
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                resultadoTematico = New Tematico()
                resultadoTematico.Cargar(aRow("TAD_CO_ID"), Me.Session("Usuario"))
            Next
            Return resultadoTematico
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Tem�tico", EnableSession:=True)> _
    Public Function ObtenerTematicoMemoria(ByVal idTematico As Integer) As Tematico
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim resultadoTematico As Tematico, strQuery As String
        Try
            strTablename = "VIS_TAD_TEMATICOS_GEOMETRIA"
            strDatasetName = "VIS_TAD_TEMATICOS_GEOMETRIA"
            strQuery = "SELECT TAD_CO_ID FROM VIS_TAD_TEMATICOS_GEOMETRIA WHERE TAD_CO_ID=" & idTematico
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                resultadoTematico = New Tematico()
                resultadoTematico.CargarMemoria(aRow("TAD_CO_ID"), Me.Session("Usuario"))
            Next
            Return resultadoTematico
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Borrar Datos de Seleccu�n", EnableSession:=True)> _
    Public Function BorrarSeleccion(ByVal codSeleccion As Integer, ByVal userInfo As UserInfo) As Boolean
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim bolResultado As Boolean = False
        Try

            Dim strUsuario As String = userInfo.nombre

            If strUsuario Is Nothing Then
                Return False
            End If

            strQuery = "DELETE FROM VIS_SEL_SELECCION WHERE SEL_CO_ID=" + codSeleccion.ToString() + " AND USU_DS_NOMBRE='" & strUsuario & "'"
            strTableName = "T"
            strDatasetName = "T"
            bolResultado = ExecuteNonQuery(strQuery)


            Return bolResultado
        Catch ex As Exception
            Return False
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Borrar Datos de Tem�ticos Avanzados", EnableSession:=True)> _
    Public Function BorrarTematicoAv(ByVal Id As Integer, ByVal userInfo As UserInfo) As Boolean
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim bolResultado As Boolean = False
        Try


            Dim strUsuario As String = userInfo.nombre
            Dim strRoles As String = ObtenerAutenticacion()

            If strUsuario Is Nothing Then
                Return False
            End If

            Dim rolAdmATematicos As String = "'ADMTEMATICOSAV'"
            If strRoles.IndexOf(rolAdmATematicos) <> -1 Then
                strQuery = "DELETE FROM VIS_TAV_TEMATICOS_AVANZADOS WHERE TAV_CO_ID=" + Id.ToString()
            Else
                strQuery = "DELETE FROM VIS_TAV_TEMATICOS_AVANZADOS WHERE TAV_CO_ID=" + Id.ToString() + " AND USU_DS_NOMBRE='" & strUsuario & "'"
            End If

            strTableName = "T"
            strDatasetName = "T"
            bolResultado = ExecuteNonQuery(strQuery, "bbddComun")

            strQuery = "DROP TABLE TEMATICOAV_" + Id.ToString()
            strTableName = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName, "bbddComun")

            Return bolResultado
        Catch ex As Exception
            Return False
        End Try
    End Function


    <System.Web.Services.WebMethod(Description:="Borrar Datos de Tematicos", EnableSession:=True)> _
    Public Function BorrarTematico(ByVal idTematico As Integer) As Boolean
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim bolResultado As Boolean
        Try


            strQuery = "DELETE FROM VIS_TAD_TEMATICOSDEMANDA WHERE TAD_CO_ID='" + idTematico.ToString() + "'"
            strTableName = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            strQuery = "DROP TABLE TEMATICO_" + idTematico.ToString()
            strTableName = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function



    <System.Web.Services.WebMethod(Description:="Borrar Datos de Tematicos", EnableSession:=True)> _
    Public Function BorrarTematicoMemoria(ByVal idTematico As Integer) As Boolean
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim bolResultado As Boolean
        Try


            strQuery = "DELETE FROM VIS_TAD_TEMATICOS_GEOMETRIA WHERE TAD_CO_ID='" + idTematico.ToString() + "'"
            strTableName = "VIS_TAD_TEMATICOS_GEOMETRIA"
            strDatasetName = "VIS_TAD_TEMATICOS_GEOMETRIA"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Borrar Datos de Tematicos", EnableSession:=True)> _
    Public Function BorrarTablaTemporalTematicoGeometria(ByVal idTematico As Integer) As Boolean
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim bolResultado As Boolean
        Try

            strQuery = "DROP TABLE TEMATICO_" + idTematico.ToString()
            strTableName = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Private Enum eTipoServidorGIS
        ArcIMS
        ArcGISServer
    End Enum
    <System.Web.Services.WebMethod(Description:="Carga de Datos de Tematicos de ArcGIS Server", EnableSession:=True)> _
    Public Function CargarTematicoArcGISServer( _
  ByVal fichEntrada As String, ByVal tablaEntrada As String, ByVal campoEntrada As String, _
  ByVal campoJoin As String, ByVal tipoTematico As Integer, ByVal nombreTematico As String, _
  ByVal privacidadTematico As Integer, ByVal descripcion As String) As String

        Return CargarTematicoGenerico( _
         fichEntrada, tablaEntrada, campoEntrada, _
         campoJoin, tipoTematico, nombreTematico, _
         privacidadTematico, eTipoServidorGIS.ArcGISServer, descripcion)

    End Function
    Private Function CargarTematicoGenerico( _
   ByVal fichEntrada As String, ByVal tablaEntrada As String, ByVal campoEntrada As String, _
   ByVal campoJoin As String, ByVal tipoTematico As Integer, ByVal nombreTematico As String, _
   ByVal privacidadTematico As Integer, ByVal tipoServGIS As eTipoServidorGIS, ByVal descripcion As String) As String

        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim idTematico As Long
        Dim strFicheroTrabajo As String
        Dim bolResultado As Boolean

        Dim arrRoles As ArrayList
        Dim i As Integer
        Try
            'En primer lugar obtenemos el Id del tem�tico, que nos servir� para autentificarlo
            strTableName = "vis_tad_tematicosdemanda"
            strDatasetName = "vis_tad_tematicosdemanda"
            strQuery = "SELECT SEQ_TAD_CO_ID.NEXTVAL FROM DUAL"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow
            Dim dsTrabajo As Data.DataSet

            For Each aRow In aDS.Tables(0).Rows
                idTematico = aRow(0)
            Next
            If (tablaEntrada.StartsWith("'") And tablaEntrada.EndsWith("'")) Then
                tablaEntrada = tablaEntrada.Substring(1, tablaEntrada.Length - 2)
            End If
            Dim tablaTematico As String, campoTematico As String
            Dim xmlText As String, strEsquema As String
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")

            strTableName = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT TIT_DS_TABLA_TEMATICO,TIT_DS_CAMPO_TEMATICO FROM VIS_TIT_TIPO_TEMATICO WHERE TIT_CO_ID=" & tipoTematico

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                tablaTematico = aRow("TIT_DS_TABLA_TEMATICO")
                campoTematico = aRow("TIT_DS_CAMPO_TEMATICO")
            Next

            Select Case tipoServGIS
                Case eTipoServidorGIS.ArcIMS
                    xmlText = "<SPATIALQUERY where=""" & tablaTematico & "." & campoTematico & "=" & strEsquema & "TEMATICO_" & idTematico & "." & campoJoin & """ jointables=""" & strEsquema & "TEMATICO_" & idTematico & """/>"
                Case Else
                    xmlText = ""
            End Select

            strTableName = "vis_tad_tematicosdemanda"
            strDatasetName = "vis_tad_tematicosdemanda"
            strQuery = "INSERT INTO VIS_TAD_TEMATICOSDEMANDA (TAD_CO_ID,TAD_DS_FICHERO_ENTRADA, TAD_DS_TABLA_ENTRADA, TAD_DS_CAMPO_TEM_ENTRADA, TIT_CO_ID, EST_CO_ID, TAD_DS_NOMBRE_ANALISIS, TAD_DS_CAMPO_TEM_JOIN, TPR_CO_ID, TAD_DS_USUARIO, TAD_DS_RESULTADO, TAD_DS_DESCRIPCION ) " & _
              "VALUES('" & idTematico & "','" & fichEntrada & "','" & tablaEntrada & "','" & campoEntrada & "','" & tipoTematico.ToString & "','1','" & nombreTematico & "','" & UCase(campoJoin) & "','" & privacidadTematico & "','" & Me.Session("Usuario") & "','" & xmlText & "','" & descripcion & "') "
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            If (aDS Is Nothing) Then
                Return Nothing
            End If
            'Si el tem�tico es privado (tipo 2), s�lo podr� acceder la persona que lo ha generado.
            'Si es compartido (tipo 3), lo compartir� con los roles que ella posee
            'Si es p�blico (tipo 1), lo ver� todo el mundo
            If (privacidadTematico <> 1) Then
                strTableName = "vis_tro_tematicorol"
                strDatasetName = "vis_tro_tematicorol"
                If (privacidadTematico = 3) Then
                    arrRoles = Me.Session("Rol")
                    For i = 0 To arrRoles.Count - 1
                        strQuery = "INSERT INTO VIS_TRO_TEMATICOROL (TAD_CO_ID,ROL_CA_ID) " & _
                          "VALUES('" & idTematico & "','" & arrRoles(i) & "') "
                        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                    Next i
                End If
            End If
            'Copiamos el fichero excel o access al directorio de trabajo de la aplicacion

            Dim strDirOrigen As String
            strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
            strFicheroTrabajo = strDirOrigen & fichEntrada 'CopiarFichero(fichEntrada, idTematico)
            Dim sExtension As String
            sExtension = Path.GetExtension(strFicheroTrabajo)
            'A continuaci�n debemos cargar los datos del tem�tico en una tabla Oracle
            'Para ello abrimos una conexi�n con el fichero excel (utilizamos para ello ADO)
            If (sExtension = ".xls") Then
                dsTrabajo = ObtenerDatosExcel(strFicheroTrabajo, tablaEntrada)
            ElseIf (sExtension = ".mdb") Then
                dsTrabajo = ObtenerDatosAccess(strFicheroTrabajo, tablaEntrada)
            Else
                Return -1
            End If
            dsTrabajo.Tables(0).TableName = tablaTematico

            'Cargamos la tabla en Oracle. Para ello, primero debemos crearla
            bolResultado = GenerarTablaTematico(dsTrabajo, idTematico, campoJoin)
            If Not (bolResultado) Then
                Return -1
            End If
            Dim dvOrdenado As New DataView(dsTrabajo.Tables(0))
            dvOrdenado.Sort = campoEntrada

            Dim dsOrdenado As New DataSet()
            dsOrdenado.Tables.Add(dvOrdenado.ToTable())

            bolResultado = CargarDatosTematico(dsOrdenado, idTematico)
            'bolResultado = CargarDatosTematico(dsTrabajo, idTematico)
            If Not (bolResultado) Then
                Return -1
            End If


            Select Case tipoServGIS
                Case eTipoServidorGIS.ArcIMS
                    Return xmlText
                Case Else
                    Return idTematico.ToString()
            End Select


        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Carga de Datos de Tematicos de ArcGIS Server para Geometr�as", EnableSession:=True)> _
    Public Function CargarTematicoGeometria( _
  ByVal fichEntrada As String, ByVal campoEntrada As String, _
  ByVal tipoTematico As Integer, ByVal nombreTematico As String, _
  ByVal privacidadTematico As Integer, ByVal descripcion As String, ByVal userInfo As UserInfo, ByVal tipoValoresUnicos As Boolean) As String

        Dim strTableName As String
        Dim strDatasetName As String, strNumMaxExcel As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aDTAux As Data.DataTable
        Dim idTematico As Long
        Dim strFicheroTrabajo As String
        Dim bolResultado As Boolean
        Dim tablaEntrada As String
        Dim arrRoles As ArrayList
        Dim tipoServGIS As eTipoServidorGIS
        Dim i As Integer
        Try
            'En primer lugar obtenemos el Id del tem�tico, que nos servir� para autentificarlo
            strNumMaxExcel = System.Configuration.ConfigurationManager.AppSettings("NumeroMaxRegistrosExcel")
            strTableName = "vis_tad_tematicos_geometria"
            strDatasetName = "vis_tad_tematicos_geometria"
            strQuery = "SELECT SEQ_TAD_CO_ID.NEXTVAL FROM DUAL"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow
            Dim dsTrabajo As Data.DataSet

            For Each aRow In aDS.Tables(0).Rows
                idTematico = aRow(0)
            Next
            'If (tablaEntrada.StartsWith("'") And tablaEntrada.EndsWith("'")) Then
            '    tablaEntrada = tablaEntrada.Substring(1, tablaEntrada.Length - 2)
            'End If
            tablaEntrada = "GEOMETRIAS$"
            Dim tablaTematico As String, campoTematico As String
            Dim xmlText As String, strEsquema As String
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")

            'strTableName = "VIS_TAD_TEMATICOS_GEOMETRIA"
            'strDatasetName = "VIS_TAD_TEMATICOS_GEOMETRIA"
            'strQuery = "SELECT TIT_DS_TABLA_TEMATICO,TIT_DS_CAMPO_TEMATICO FROM VIS_TIT_TIPO_TEMATICO WHERE TIT_CO_ID=" & tipoTematico

            'aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            'For Each aRow In aDS.Tables(0).Rows
            '    tablaTematico = aRow("TIT_DS_TABLA_TEMATICO")
            '    campoTematico = aRow("TIT_DS_CAMPO_TEMATICO")
            'Next
            tipoServGIS = eTipoServidorGIS.ArcGISServer
            Select Case tipoServGIS
                Case eTipoServidorGIS.ArcIMS
                    '    xmlText = "<SPATIALQUERY where=""" & tablaTematico & "." & campoTematico & "=" & strEsquema & "TEMATICO_" & idTematico & "." & campoJoin & """ jointables=""" & strEsquema & "TEMATICO_" & idTematico & """/>"
                Case Else
                    xmlText = ""
            End Select

            strTableName = "VIS_TAD_TEMATICOS_GEOMETRIA"
            strDatasetName = "VIS_TAD_TEMATICOS_GEOMETRIA"
            strQuery = "INSERT INTO VIS_TAD_TEMATICOS_GEOMETRIA (TAD_CO_ID,TAD_DS_FICHERO_ENTRADA,   TIT_CO_ID,  TAD_DS_NOMBRE_ANALISIS,  TPR_CO_ID, TAD_DS_USUARIO, TAD_DS_RESULTADO, TAD_DS_DESCRIPCION ) " & _
              "VALUES('" & idTematico & "','" & fichEntrada & "','" & tipoTematico.ToString & "','" & nombreTematico & "','" & privacidadTematico & "','" & userInfo.nombre & "','" & xmlText & "','" & descripcion & "') "
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            If (aDS Is Nothing) Then
                Return Nothing
            End If
            'Si el tem�tico es privado (tipo 2), s�lo podr� acceder la persona que lo ha generado.
            'Si es compartido (tipo 3), lo compartir� con los roles que ella posee
            'Si es p�blico (tipo 1), lo ver� todo el mundo
            'If (privacidadTematico <> 1) Then
            '    strTableName = "vis_tro_tematicorol"
            '    strDatasetName = "vis_tro_tematicorol"
            '    If (privacidadTematico = 3) Then
            '        arrRoles = userInfo.grupos         'Me.Session("Rol")
            '        For i = 0 To arrRoles.Count - 1
            '            strQuery = "INSERT INTO VIS_TRO_TEMATICOROL (TAD_CO_ID,ROL_CA_ID) " & _
            '              "VALUES('" & idTematico & "','" & arrRoles(i) & "') "
            '            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            '        Next i
            '    End If
            'End If
            'Copiamos el fichero excel o access al directorio de trabajo de la aplicacion

            If fichEntrada <> "" Then
                Dim strDirOrigen As String
                strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
                strFicheroTrabajo = strDirOrigen & fichEntrada 'CopiarFichero(fichEntrada, idTematico)
                Dim sExtension As String
                sExtension = Path.GetExtension(strFicheroTrabajo)
                'A continuaci�n debemos cargar los datos del tem�tico en una tabla Oracle
                'Para ello abrimos una conexi�n con el fichero excel (utilizamos para ello ADO)
                If (sExtension = ".xls") Then
                    dsTrabajo = ObtenerDatosExcel(strFicheroTrabajo, tablaEntrada)
                ElseIf (sExtension = ".mdb") Then
                    dsTrabajo = ObtenerDatosAccess(strFicheroTrabajo, tablaEntrada)
                Else
                    Return -1
                End If
                'Fran:solo para valores �nicos
                Dim aa As DataView = dsTrabajo.Tables(0).DefaultView
                Dim arrayColumns(0) As String


                arrayColumns(0) = campoEntrada
                aDTAux = aa.ToTable(True, arrayColumns)
                dsTrabajo.Tables(0).TableName = tablaEntrada 'tablaTematico


                Dim iCount As Integer
                With aDTAux
                    iCount = CType(.Compute("COUNT(" + campoEntrada + ")", ""), Integer)
                End With
                If ((iCount > CInt(strNumMaxExcel)) And tipoValoresUnicos) Then
                    Return -1

                Else


                    'Cargamos la tabla en Oracle. Para ello, primero debemos crearla
                    bolResultado = GenerarTablaTematico(dsTrabajo, idTematico, campoEntrada)
                    If Not (bolResultado) Then
                        Return -1
                    End If
                    Dim dvOrdenado As New DataView(dsTrabajo.Tables(0))
                    dvOrdenado.Sort = campoEntrada

                    Dim dsOrdenado As New DataSet()
                    dsOrdenado.Tables.Add(dvOrdenado.ToTable())

                    bolResultado = CargarDatosTematico(dsOrdenado, idTematico)
                    'bolResultado = CargarDatosTematico(dsTrabajo, idTematico)
                    If Not (bolResultado) Then
                        Return -1
                    End If
                End If
            End If

            Select Case tipoServGIS
                Case eTipoServidorGIS.ArcIMS
                    Return xmlText
                Case Else
                    Return idTematico.ToString()
            End Select


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Carga de Datos de Tematicos de ArcGIS Server", EnableSession:=True)> _
    Public Function CargarTematicoAvanzadoExcel( _
    ByVal fichEntrada As String, ByVal tablaEntrada As String, ByVal campoEntrada As String, _
    ByVal campoJoin As String, ByVal tipoTematico As Integer, ByVal idCapaTematico As Integer, ByVal nombreTematico As String, _
    ByVal userInfo As UserInfo, ByVal tipoValoresUnicos As Boolean) As String

        Dim strTableName As String
        Dim strDatasetName As String, strNumMaxExcel As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aDTAux As Data.DataTable
        Dim idTematico As Long
        Dim strFicheroTrabajo As String
        Dim bolResultado As Boolean

        Dim arrRoles As ArrayList
        Dim i As Integer
        Try
            'En primer lugar obtenemos el Id del tem�tico, que nos servir� para autentificarlo
            strNumMaxExcel = System.Configuration.ConfigurationManager.AppSettings("NumeroMaxRegistrosExcel")
            strTableName = "VIS_TAV_TEMATICOS_AVANZADOS"
            strDatasetName = "VIS_TAV_TEMATICOS_AVANZADOS"
            strQuery = "SELECT SEQ_TAV_CO_ID.NEXTVAL FROM DUAL"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName, "bbddComun")
            Dim aRow As Data.DataRow
            Dim dsTrabajo As Data.DataSet

            For Each aRow In aDS.Tables(0).Rows
                idTematico = aRow(0)
            Next

            Dim tablaTematico As String, campoTematico As String

            If idCapaTematico > 0 Then
                strTableName = "VIS_CTA_CAPAS_TEMATICO_AV"
                strDatasetName = "VIS_CTA_CAPAS_TEMATICO_AV"
                strQuery = "SELECT CTA_DS_DATASOURCENAME,CTA_DS_CAMPOCLAVE FROM VIS_CTA_CAPAS_TEMATICO_AV WHERE CTA_CO_ID=" & idCapaTematico

                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName, "bbddComun")
                For Each aRow In aDS.Tables(0).Rows
                    tablaTematico = aRow("CTA_DS_DATASOURCENAME")
                    campoTematico = aRow("CTA_DS_CAMPOCLAVE")
                Next

                strTableName = "VIS_TAV_TEMATICOS_AVANZADOS"
                strDatasetName = "VIS_TAV_TEMATICOS_AVANZADOS"
                strQuery = "INSERT INTO VIS_TAV_TEMATICOS_AVANZADOS (TAV_CO_ID,USU_DS_NOMBRE,TAV_DS_NOMBRE, TTA_CO_ID, TAV_DS_LAYERDATASOURCE) " & _
                  "VALUES(" & idTematico & ",'" & userInfo.nombre & "','" & nombreTematico & "'," & tipoTematico & ",'" & tablaTematico & "') "
            Else
                strTableName = "VIS_TAV_TEMATICOS_AVANZADOS"
                strDatasetName = "VIS_TAV_TEMATICOS_AVANZADOS"
                strQuery = "INSERT INTO VIS_TAV_TEMATICOS_AVANZADOS (TAV_CO_ID,USU_DS_NOMBRE,TAV_DS_NOMBRE, TTA_CO_ID) " & _
                  "VALUES(" & idTematico & ",'" & userInfo.nombre & "','" & nombreTematico & "'," & tipoTematico & ") "
            End If

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName, "bbddComun")
            If (aDS Is Nothing) Then
                Return Nothing
            End If
            'Si el tem�tico es privado (tipo 2), s�lo podr� acceder la persona que lo ha generado.
            'Si es compartido (tipo 3), lo compartir� con los roles que ella posee
            'Si es p�blico (tipo 1), lo ver� todo el mundo
            'If (privacidadTematico <> 1) Then
            ' strTableName = "vis_tro_tematicorol"
            'strDatasetName = "vis_tro_tematicorol"
            'If (privacidadTematico = 3) Then
            'arrRoles = userInfo.grupos         'Me.Session("Rol")
            'For i = 0 To arrRoles.Count - 1
            'strQuery = "INSERT INTO VIS_TRO_TEMATICOROL (TAD_CO_ID,ROL_CA_ID) " & _
            '  "VALUES('" & idTematico & "','" & arrRoles(i) & "') "
            'aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            'Next i
            'End If
            'End If
            'Copiamos el fichero excel o access al directorio de trabajo de la aplicacion

            If fichEntrada <> "" Then
                Dim strDirOrigen As String
                strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
                strFicheroTrabajo = strDirOrigen & fichEntrada 'CopiarFichero(fichEntrada, idTematico)
                Dim sExtension As String
                sExtension = Path.GetExtension(strFicheroTrabajo)
                'A continuaci�n debemos cargar los datos del tem�tico en una tabla Oracle
                'Para ello abrimos una conexi�n con el fichero excel (utilizamos para ello ADO)
                If (sExtension = ".xls") Then
                    dsTrabajo = ObtenerDatosExcel(strFicheroTrabajo, tablaEntrada)
                ElseIf (sExtension = ".mdb") Then
                    dsTrabajo = ObtenerDatosAccess(strFicheroTrabajo, tablaEntrada)
                Else
                    BorrarTematico(Convert.ToInt32(idTematico))
                    Return -1
                End If
                'Fran:solo para valores �nicos
                Dim aa As DataView = dsTrabajo.Tables(0).DefaultView
                Dim arrayColumns(0) As String

                ' ReDim arrayColumns(1)
                arrayColumns(0) = campoEntrada
                aDTAux = aa.ToTable(True, arrayColumns)
                dsTrabajo.Tables(0).TableName = tablaTematico


                Dim iCount As Integer
                With aDTAux
                    iCount = CType(.Compute("COUNT(" + campoEntrada + ")", ""), Integer)
                End With
                If ((iCount > CInt(strNumMaxExcel)) And tipoValoresUnicos) Then
                    BorrarTematicoAv(Convert.ToInt32(idTematico), userInfo)
                    Return -1
                Else
                    'Cargamos la tabla en Oracle. Para ello, primero debemos crearla
                    bolResultado = GenerarTablaTematicoAv(dsTrabajo, idTematico, campoJoin)
                    If Not (bolResultado) Then
                        BorrarTematico(Convert.ToInt32(idTematico))
                        Return -1
                    End If
                    Dim dvOrdenado As New DataView(dsTrabajo.Tables(0))
                    dvOrdenado.Sort = campoEntrada

                    Dim dsOrdenado As New DataSet()
                    dsOrdenado.Tables.Add(dvOrdenado.ToTable())

                    bolResultado = CargarDatosTematicoAv(dsOrdenado, idTematico)
                    'bolResultado = CargarDatosTematico(dsTrabajo, idTematico)
                    If Not (bolResultado) Then
                        BorrarTematicoAv(Convert.ToInt32(idTematico), userInfo)
                        Return -1
                    End If
                End If
            End If

            Return idTematico.ToString()

        Catch ex As Exception
            BorrarTematicoAv(Convert.ToInt32(idTematico), userInfo)
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Carga de Datos de Tematicos de ArcGIS Server", EnableSession:=True)> _
    Public Function CargarTematicoArcGISServerFlex( _
    ByVal fichEntrada As String, ByVal tablaEntrada As String, ByVal campoEntrada As String, _
    ByVal campoJoin As String, ByVal tipoTematico As Integer, ByVal nombreTematico As String, _
    ByVal privacidadTematico As Integer, ByVal descripcion As String, ByVal userInfo As UserInfo, ByVal tipoValoresUnicos As Boolean) As String

        Return CargarTematicoGenericoFlex( _
         fichEntrada, tablaEntrada, campoEntrada, _
         campoJoin, tipoTematico, nombreTematico, _
         privacidadTematico, eTipoServidorGIS.ArcGISServer, descripcion, userInfo, tipoValoresUnicos)

    End Function
    Private Function CargarTematicoGenericoFlex( _
   ByVal fichEntrada As String, ByVal tablaEntrada As String, ByVal campoEntrada As String, _
   ByVal campoJoin As String, ByVal tipoTematico As Integer, ByVal nombreTematico As String, _
   ByVal privacidadTematico As Integer, ByVal tipoServGIS As eTipoServidorGIS, ByVal descripcion As String, ByVal userInfo As UserInfo, ByVal tipoValoresUnicos As Boolean) As String

        Dim strTableName As String
        Dim strDatasetName As String, strNumMaxExcel As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aDTAux As Data.DataTable
        Dim idTematico As Long
        Dim strFicheroTrabajo As String
        Dim bolResultado As Boolean

        Dim arrRoles As ArrayList
        Dim i As Integer
        Try
            'En primer lugar obtenemos el Id del tem�tico, que nos servir� para autentificarlo
            strNumMaxExcel = System.Configuration.ConfigurationManager.AppSettings("NumeroMaxRegistrosExcel")
            strTableName = "vis_tad_tematicosdemanda"
            strDatasetName = "vis_tad_tematicosdemanda"
            strQuery = "SELECT SEQ_TAD_CO_ID.NEXTVAL FROM DUAL"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow
            Dim dsTrabajo As Data.DataSet

            For Each aRow In aDS.Tables(0).Rows
                idTematico = aRow(0)
            Next
            If (tablaEntrada.StartsWith("'") And tablaEntrada.EndsWith("'")) Then
                tablaEntrada = tablaEntrada.Substring(1, tablaEntrada.Length - 2)
            End If
            Dim tablaTematico As String, campoTematico As String
            Dim xmlText As String, strEsquema As String
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")

            strTableName = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT TIT_DS_TABLA_TEMATICO,TIT_DS_CAMPO_TEMATICO FROM VIS_TIT_TIPO_TEMATICO WHERE TIT_CO_ID=" & tipoTematico

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                tablaTematico = aRow("TIT_DS_TABLA_TEMATICO")
                campoTematico = aRow("TIT_DS_CAMPO_TEMATICO")
            Next

            Select Case tipoServGIS
                Case eTipoServidorGIS.ArcIMS
                    xmlText = "<SPATIALQUERY where=""" & tablaTematico & "." & campoTematico & "=" & strEsquema & "TEMATICO_" & idTematico & "." & campoJoin & """ jointables=""" & strEsquema & "TEMATICO_" & idTematico & """/>"
                Case Else
                    xmlText = ""
            End Select

            strTableName = "vis_tad_tematicosdemanda"
            strDatasetName = "vis_tad_tematicosdemanda"
            strQuery = "INSERT INTO VIS_TAD_TEMATICOSDEMANDA (TAD_CO_ID,TAD_DS_FICHERO_ENTRADA, TAD_DS_TABLA_ENTRADA, TAD_DS_CAMPO_TEM_ENTRADA, TIT_CO_ID, EST_CO_ID, TAD_DS_NOMBRE_ANALISIS, TAD_DS_CAMPO_TEM_JOIN, TPR_CO_ID, TAD_DS_USUARIO, TAD_DS_RESULTADO, TAD_DS_DESCRIPCION ) " & _
              "VALUES('" & idTematico & "','" & fichEntrada & "','" & tablaEntrada & "','" & campoEntrada & "','" & tipoTematico.ToString & "','1','" & nombreTematico & "','" & UCase(campoJoin) & "','" & privacidadTematico & "','" & userInfo.nombre & "','" & xmlText & "','" & descripcion & "') "
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            If (aDS Is Nothing) Then
                Return Nothing
            End If
            'Si el tem�tico es privado (tipo 2), s�lo podr� acceder la persona que lo ha generado.
            'Si es compartido (tipo 3), lo compartir� con los roles que ella posee
            'Si es p�blico (tipo 1), lo ver� todo el mundo
            If (privacidadTematico <> 1) Then
                strTableName = "vis_tro_tematicorol"
                strDatasetName = "vis_tro_tematicorol"
                If (privacidadTematico = 3) Then
                    arrRoles = userInfo.grupos         'Me.Session("Rol")
                    For i = 0 To arrRoles.Count - 1
                        strQuery = "INSERT INTO VIS_TRO_TEMATICOROL (TAD_CO_ID,ROL_CA_ID) " & _
                          "VALUES('" & idTematico & "','" & arrRoles(i) & "') "
                        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                    Next i
                End If
            End If
            'Copiamos el fichero excel o access al directorio de trabajo de la aplicacion

            If fichEntrada <> "" Then
                Dim strDirOrigen As String
                strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
                strFicheroTrabajo = strDirOrigen & fichEntrada 'CopiarFichero(fichEntrada, idTematico)
                Dim sExtension As String
                sExtension = Path.GetExtension(strFicheroTrabajo)
                'A continuaci�n debemos cargar los datos del tem�tico en una tabla Oracle
                'Para ello abrimos una conexi�n con el fichero excel (utilizamos para ello ADO)
                If (sExtension = ".xls") Then
                    dsTrabajo = ObtenerDatosExcel(strFicheroTrabajo, tablaEntrada)
                ElseIf (sExtension = ".mdb") Then
                    dsTrabajo = ObtenerDatosAccess(strFicheroTrabajo, tablaEntrada)
                Else
                    BorrarTematico(Convert.ToInt32(idTematico))
                    Return -1
                End If
                'Fran:solo para valores �nicos
                Dim aa As DataView = dsTrabajo.Tables(0).DefaultView
                Dim arrayColumns(0) As String

                ' ReDim arrayColumns(1)
                arrayColumns(0) = campoEntrada
                aDTAux = aa.ToTable(True, arrayColumns)
                dsTrabajo.Tables(0).TableName = tablaTematico


                Dim iCount As Integer
                With aDTAux
                    iCount = CType(.Compute("COUNT(" + campoEntrada + ")", ""), Integer)
                End With
                If ((iCount > CInt(strNumMaxExcel)) And tipoValoresUnicos) Then
                    BorrarTematico(Convert.ToInt32(idTematico))
                    Return -1
                Else
                    'Cargamos la tabla en Oracle. Para ello, primero debemos crearla
                    bolResultado = GenerarTablaTematico(dsTrabajo, idTematico, campoJoin)
                    If Not (bolResultado) Then
                        BorrarTematico(Convert.ToInt32(idTematico))
                        Return -1
                    End If
                    Dim dvOrdenado As New DataView(dsTrabajo.Tables(0))
                    dvOrdenado.Sort = campoEntrada

                    Dim dsOrdenado As New DataSet()
                    dsOrdenado.Tables.Add(dvOrdenado.ToTable())

                    bolResultado = CargarDatosTematico(dsOrdenado, idTematico)
                    'bolResultado = CargarDatosTematico(dsTrabajo, idTematico)
                    If Not (bolResultado) Then
                        BorrarTematico(Convert.ToInt32(idTematico))
                        Return -1
                    End If
                End If
            End If

            Select Case tipoServGIS
                Case eTipoServidorGIS.ArcIMS
                    Return xmlText
                Case Else
                    Return idTematico.ToString()
            End Select


        Catch ex As Exception
            BorrarTematico(Convert.ToInt32(idTematico))
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios de un Tipo determinado", EnableSession:=True)> _
    Public Function ObtenerServiciosTipo(ByVal idTipoServicio As Integer, ByVal codIdioma As String, ByVal idVisor As Integer) As TipoServicio
        Try

            Dim mTipoServicio As New TipoServicio
            mTipoServicio.Cargar(idTipoServicio, codIdioma, idVisor)
            'Dim arrServicios() As Servicio
            'arrServicios = mTipoServicio.servicios

            'Return arrServicios
            Return mTipoServicio

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Carga de Datos de Tematicos", EnableSession:=True)> _
    Public Function CargarTematico(ByVal fichEntrada As String, ByVal tablaEntrada As String, ByVal campoEntrada As String, ByVal campoJoin As String, ByVal tipoTematico As Integer, ByVal nombreTematico As String, ByVal privacidadTematico As Integer) As String

        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim idTematico As Long
        Dim strFicheroTrabajo As String
        Dim bolResultado As Boolean

        Dim arrRoles As ArrayList
        Dim i As Integer
        Try
            'En primer lugar obtenemos el Id del tem�tico, que nos servir� para autentificarlo
            strTableName = "vis_tad_tematicosdemanda"
            strDatasetName = "vis_tad_tematicosdemanda"
            strQuery = "SELECT SEQ_TAD_CO_ID.NEXTVAL FROM DUAL"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow
            Dim dsTrabajo As Data.DataSet

            For Each aRow In aDS.Tables(0).Rows
                idTematico = aRow(0)
            Next
            If (tablaEntrada.StartsWith("'") And tablaEntrada.EndsWith("'")) Then
                tablaEntrada = tablaEntrada.Substring(1, tablaEntrada.Length - 2)
            End If
            Dim tablaTematico As String, campoTematico As String
            Dim xmlText As String, strEsquema As String
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")

            strTableName = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT TIT_DS_TABLA_TEMATICO,TIT_DS_CAMPO_TEMATICO FROM VIS_TIT_TIPO_TEMATICO WHERE TIT_CO_ID=" & tipoTematico

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                tablaTematico = aRow("TIT_DS_TABLA_TEMATICO")
                campoTematico = aRow("TIT_DS_CAMPO_TEMATICO")
            Next

            xmlText = "<SPATIALQUERY where=""" & tablaTematico & "." & campoTematico & "=" & strEsquema & "TEMATICO_" & idTematico & "." & campoJoin & """ jointables=""" & strEsquema & "TEMATICO_" & idTematico & """/>"

            strTableName = "vis_tad_tematicosdemanda"
            strDatasetName = "vis_tad_tematicosdemanda"
            strQuery = "INSERT INTO VIS_TAD_TEMATICOSDEMANDA (TAD_CO_ID,TAD_DS_FICHERO_ENTRADA, TAD_DS_TABLA_ENTRADA, TAD_DS_CAMPO_TEM_ENTRADA, TIT_CO_ID, EST_CO_ID, TAD_DS_NOMBRE_ANALISIS, TAD_DS_CAMPO_TEM_JOIN, TPR_CO_ID, TAD_DS_USUARIO, TAD_DS_RESULTADO ) " & _
                       "VALUES('" & idTematico & "','" & fichEntrada & "','" & tablaEntrada & "','" & campoEntrada & "','" & tipoTematico.ToString & "','1','" & nombreTematico & "','" & campoJoin & "','" & privacidadTematico & "','" & Me.Session("Usuario") & "','" & xmlText & "') "
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            If (aDS Is Nothing) Then
                Return Nothing
            End If
            'Si el tem�tico es privado (tipo 2), s�lo podr� acceder la persona que lo ha generado.
            'Si es compartido (tipo 3), lo compartir� con los roles que ella posee
            'Si es p�blico (tipo 1), lo ver� todo el mundo
            If (privacidadTematico <> 1) Then
                strTableName = "vis_tro_tematicorol"
                strDatasetName = "vis_tro_tematicorol"
                If (privacidadTematico = 3) Then
                    arrRoles = Me.Session("Rol")
                    For i = 0 To arrRoles.Count - 1
                        strQuery = "INSERT INTO VIS_TRO_TEMATICOROL (TAD_CO_ID,ROL_CA_ID) " & _
                                   "VALUES('" & idTematico & "','" & arrRoles(i) & "') "
                        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                    Next i
                End If
            End If
            'Copiamos el fichero excel o access al directorio de trabajo de la aplicacion

            Dim strDirOrigen As String
            strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
            strFicheroTrabajo = strDirOrigen & fichEntrada 'CopiarFichero(fichEntrada, idTematico)
            Dim sExtension As String
            sExtension = Path.GetExtension(strFicheroTrabajo)
            'A continuaci�n debemos cargar los datos del tem�tico en una tabla Oracle
            'Para ello abrimos una conexi�n con el fichero excel (utilizamos para ello ADO)
            If (sExtension = ".xls") Then
                dsTrabajo = ObtenerDatosExcel(strFicheroTrabajo, tablaEntrada)
            ElseIf (sExtension = ".mdb") Then
                dsTrabajo = ObtenerDatosAccess(strFicheroTrabajo, tablaEntrada)
            Else
                Return -1
            End If

            'Cargamos la tabla en Oracle. Para ello, primero debemos crearla
            bolResultado = GenerarTablaTematico(dsTrabajo, idTematico)
            If Not (bolResultado) Then
                Return -1
            End If
            bolResultado = CargarDatosTematico(dsTrabajo, idTematico)
            If Not (bolResultado) Then
                Return -1
            End If



            Return xmlText

        Catch ex As Exception
            Return Nothing
        End Try



    End Function
    Public Function CopiarFichero(ByVal strFicheroEntrada As String, ByVal idTematico As Long) As String
        Dim strDirTrabajo As String
        Dim strDirOrigen As String
        'strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
        strDirTrabajo = System.Configuration.ConfigurationManager.AppSettings("DirTematico")
        Dim sFile As String
        Dim sExtension As String

        Try
            sFile = Path.GetFileName(strFicheroEntrada)
            sExtension = Path.GetExtension(strFicheroEntrada)

            System.IO.File.Copy(strFicheroEntrada, strDirTrabajo & "\" & sFile, True)
            System.IO.File.Move(strDirTrabajo & "\" & sFile, strDirTrabajo & "\Tematico" & idTematico & sExtension)
            Return strDirTrabajo & "\Tematico" & idTematico & sExtension
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function ObtenerDatosExcel(ByVal strFicheroTrabajo As String, ByVal strHojaTrabajo As String) As Data.DataSet

        Dim oConn As New OleDbConnection()
        Dim oCmd As New OleDbCommand()
        Dim oDa As New OleDbDataAdapter()
        Dim oDs As New DataSet()
        Try
            'oConn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
            'strFicheroTrabajo & ";Extended Properties=Excel 8.0;"

            oConn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strFicheroTrabajo & ";" & _
                "Extended Properties=""Excel 12.0 Xml;HDR=YES"";"

            oConn.Open()
            oCmd.CommandText = "SELECT * FROM [" & strHojaTrabajo & "]"
            oCmd.Connection = oConn
            oDa.SelectCommand = oCmd
            oDa.FillSchema(oDs, SchemaType.Source)
            oDa.Fill(oDs)
            oConn.Close()
            Return oDs
        Catch ex As Exception
            If oConn.State <> ConnectionState.Closed Then oConn.Close()
            Return Nothing
        End Try

    End Function
    Public Function ObtenerDatosAccess(ByVal strFicheroTrabajo As String, ByVal strTablaTrabajo As String) As Data.DataSet

        Dim oConn As New OleDbConnection()
        Dim oCmd As New OleDbCommand()
        Dim oDa As New OleDbDataAdapter()
        Dim oDs As New DataSet()

        oConn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & strFicheroTrabajo
        'oConn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strFicheroTrabajo & ";" & _
        '       "Extended Properties=""Excel 12.0 Xml;HDR=YES"";"

        oConn.Open()
        oCmd.CommandText = "SELECT * FROM " & strTablaTrabajo
        oCmd.Connection = oConn
        oDa.SelectCommand = oCmd
        oDa.FillSchema(oDs, SchemaType.Source)
        oDa.Fill(oDs)
        oConn.Close()
        Return oDs
    End Function

    Public Function GenerarTablaTematicoAv(ByVal dsEntrada As Data.DataSet, ByVal idTematico As Integer, Optional ByVal campoTematico As String = "") As Boolean
        Dim strQuery As String, strEsquema As String
        Dim clmn As Data.DataColumn
        Dim i As Integer
        Try
            'Debemos en primer lugar generar la consulta de creaci�n
            'de las tablas de tem�ticos
            strQuery = "CREATE TABLE TEMATICOAV_" & idTematico & "("
            i = 0
            For Each clmn In dsEntrada.Tables(0).Columns
                'If dsEntrada.Tables(0).TableName = "COMUN.MUNICIPIOS_25_MMA_ET" And clmn.ColumnName = campoTematico Then
                '	strQuery &= clmn.ColumnName & " NUMBER(5,0)"
                'Else
                Select Case clmn.DataType.FullName
                    Case "System.Boolean"
                        strQuery &= clmn.ColumnName & " NUMBER(5,0)"
                    Case "System.Byte", "System.Int16", "System.Int32", "System.Int64", "System.SByte", "System.UInt16", "System.UInt32", "System.UInt64", "System.Decimal"
                        strQuery &= clmn.ColumnName & " NUMBER(10,0)"
                    Case "System.Single", "System.Double"
                        strQuery &= clmn.ColumnName & " FLOAT "
                    Case "System.Char"
                        strQuery &= clmn.ColumnName & " CHAR(" & clmn.MaxLength & ")"
                    Case "System.DateTime", "System.TimeSpan"
                        strQuery &= clmn.ColumnName & " DATE"
                    Case "System.String"
                        strQuery &= clmn.ColumnName & " VARCHAR2(" & clmn.MaxLength & ")"
                End Select
                'End If
                If (i = dsEntrada.Tables(0).Columns.Count - 1) Then
                    strQuery &= ")"
                Else
                    strQuery &= ","
                End If
                i += 1
            Next clmn
            Dim strTableName As String
            Dim strDatasetName As String
            Dim aDS As Data.DataSet

            strTableName = "TEMATICOAV_" & idTematico
            strDatasetName = "TEMATICOAV_" & idTematico

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName, "bbddComun")
            Dim sqlIndice As String = "CREATE INDEX VISOR.IX_TEMATICOAV_" & idTematico & "  ON " + strEsquema + "TEMATICOAV_" & idTematico & "(" & campoTematico & ")"

            Dim strConn As String
            strConn = System.Configuration.ConfigurationManager.AppSettings("bbddComun")

            Dim aConn As PgSqlConnection = New PgSqlConnection(strConn)
            aConn.Open()
            Dim cmd As PgSqlCommand
            aConn = New PgSqlConnection(strConn)
            cmd = New PgSqlCommand(sqlIndice, aConn)
            aConn.Open()
            cmd.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function GenerarTablaTematico(ByVal dsEntrada As Data.DataSet, ByVal idTematico As Integer, Optional ByVal campoTematico As String = "") As Boolean
        Dim strQuery As String, strEsquema As String
        Dim clmn As Data.DataColumn
        Dim i As Integer
        Try
            'Debemos en primer lugar generar la consulta de creaci�n
            'de las tablas de tem�ticos
            strQuery = "CREATE TABLE TEMATICO_" & idTematico & "("
            i = 0
            For Each clmn In dsEntrada.Tables(0).Columns
                'If dsEntrada.Tables(0).TableName = "COMUN.MUNICIPIOS_25_MMA_ET" And clmn.ColumnName = campoTematico Then
                '	strQuery &= clmn.ColumnName & " NUMBER(5,0)"
                'Else
                Select Case clmn.DataType.FullName
                    Case "System.Boolean"
                        strQuery &= clmn.ColumnName & " NUMBER(5,0)"
                    Case "System.Byte", "System.Int16", "System.Int32", "System.Int64", "System.SByte", "System.UInt16", "System.UInt32", "System.UInt64", "System.Decimal"
                        strQuery &= clmn.ColumnName & " NUMBER(10,0)"
                    Case "System.Single", "System.Double"
                        strQuery &= clmn.ColumnName & " FLOAT "
                    Case "System.Char"
                        strQuery &= clmn.ColumnName & " CHAR(" & clmn.MaxLength & ")"
                    Case "System.DateTime", "System.TimeSpan"
                        strQuery &= clmn.ColumnName & " DATE"
                    Case "System.String"
                        strQuery &= clmn.ColumnName & " VARCHAR2(" & clmn.MaxLength & ")"
                End Select
                'End If
                If (i = dsEntrada.Tables(0).Columns.Count - 1) Then
                    strQuery &= ")"
                Else
                    strQuery &= ","
                End If
                i += 1
            Next clmn
            Dim strTableName As String
            Dim strDatasetName As String
            Dim aDS As Data.DataSet

            strTableName = "TEMATICO_" & idTematico
            strDatasetName = "TEMATICO_" & idTematico

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")
            Dim sqlIndice As String = "CREATE INDEX " + strEsquema + "IX_TEMATICO_" & idTematico & "  ON " + strEsquema + "TEMATICO_" & idTematico & "(" & campoTematico & ")"

            Dim strConn As String
            strConn = System.Configuration.ConfigurationManager.AppSettings("bbdd")

            Dim aConn As PgSqlConnection = New PgSqlConnection(strConn)
            aConn.Open()
            Dim cmd As PgSqlCommand
            aConn = New PgSqlConnection(strConn)
            cmd = New PgSqlCommand(sqlIndice, aConn)
            aConn.Open()
            cmd.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function CargarDatosTematicoAv(ByVal dsEntrada As Data.DataSet, ByVal idTematico As Integer) As Boolean
        'Debemos leer los datos de entrada y meterlos dentro de Oracle
        'Abrimos en primer lugar el dataset de Oracle para luego ir llen�ndolo
        Dim strQuery As String, strTableName As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim i As Integer
        Try
            strTableName = "TEMATICOAV_" & idTematico
            strDatasetName = "TEMATICOAV_" & idTematico
            strQuery = "SELECT * FROM " & strTableName
            Dim strConn As String
            strConn = System.Configuration.ConfigurationManager.AppSettings("bbddComun")

            Dim aConn As PgSqlConnection = New PgSqlConnection(strConn)
            aConn.Open()
            'Step 2.
            'Instantiate OleDbDataAdapter to create DataSet
            Dim mAdapter As PgSqlDataAdapter = New PgSqlDataAdapter
            Dim cmdBuilder As PgSqlCommandBuilder = New PgSqlCommandBuilder(mAdapter)
            mAdapter.SelectCommand = New PgSqlCommand(strQuery, aConn)
            aDS = New Data.DataSet()

            mAdapter.Fill(aDS, strTableName)
            Dim cmd As PgSqlCommand
            cmd = cmdBuilder.GetInsertCommand()
            For Each aRow In dsEntrada.Tables(0).Rows
                For i = 0 To dsEntrada.Tables(0).Columns.Count - 1
                    cmd.Parameters(i).Value = aRow(i)
                Next
                cmd.ExecuteNonQuery()
            Next

            aConn.Close()
            'Fetch Product Details


            'aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            '    aRowNueva = aDS.Tables(0).NewRow()
            '    For Each clmn In dsEntrada.Tables(0).Columns
            '        aRowNueva(clmn.ColumnName) = aRow(clmn.ColumnName)
            '    Next
            '    aDS.Tables(0).Rows.Add(aRowNueva)
            '    aRowNueva.AcceptChanges()
            'Next

            'aDS.AcceptChanges()
            'aDS.Tables(0).AcceptChanges()


            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function CargarDatosTematico(ByVal dsEntrada As Data.DataSet, ByVal idTematico As Integer) As Boolean
        'Debemos leer los datos de entrada y meterlos dentro de Oracle
        'Abrimos en primer lugar el dataset de Oracle para luego ir llen�ndolo
        Dim strQuery As String, strTableName As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim i As Integer
        Try
            strTableName = "TEMATICO_" & idTematico
            strDatasetName = "TEMATICO_" & idTematico
            strQuery = "SELECT * FROM " & strTableName
            Dim strConn As String
            strConn = System.Configuration.ConfigurationManager.AppSettings("bbdd")

            Dim aConn As PgSqlConnection = New PgSqlConnection(strConn)
            aConn.Open()
            'Step 2.
            'Instantiate OleDbDataAdapter to create DataSet
            Dim mAdapter As PgSqlDataAdapter = New PgSqlDataAdapter
            Dim cmdBuilder As PgSqlCommandBuilder = New PgSqlCommandBuilder(mAdapter)
            mAdapter.SelectCommand = New PgSqlCommand(strQuery, aConn)
            aDS = New Data.DataSet()

            mAdapter.Fill(aDS, strTableName)
            Dim cmd As PgSqlCommand
            cmd = cmdBuilder.GetInsertCommand()
            For Each aRow In dsEntrada.Tables(0).Rows
                For i = 0 To dsEntrada.Tables(0).Columns.Count - 1
                    cmd.Parameters(i).Value = aRow(i)
                Next
                cmd.ExecuteNonQuery()
            Next

            aConn.Close()
            'Fetch Product Details


            'aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            '    aRowNueva = aDS.Tables(0).NewRow()
            '    For Each clmn In dsEntrada.Tables(0).Columns
            '        aRowNueva(clmn.ColumnName) = aRow(clmn.ColumnName)
            '    Next
            '    aDS.Tables(0).Rows.Add(aRowNueva)
            '    aRowNueva.AcceptChanges()
            'Next

            'aDS.AcceptChanges()
            'aDS.Tables(0).AcceptChanges()


            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Modificar Datos Alerta", EnableSession:=True)> _
    Public Function GuardarExcepciones(ByVal colRadios As RadioEntrada()) As Boolean

        Dim aRadio As RadioEntrada

        For Each aRadio In colRadios
            If Not (aRadio.modificarRadio()) Then
                Return False
            End If
        Next
        Return True
    End Function
    <System.Web.Services.WebMethod(Description:="Carga de Datos de Tematicos", EnableSession:=True)> _
    Public Function ObtenerValoresCampos(ByVal idTematico As Integer) As ValoresCampo()
        Dim aValorCampo As ValoresCampo
        Dim valores() As ValoresCampo
        Dim colMinimos As String()
        Dim strEsquema As String, strTableName As String, strQuery As String, strDatasetName As String
        Dim aDS As Data.DataSet

        Try
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")
            'Obtenemos los nombres de los campos

            strTableName = strEsquema + "TEMATICO_" & idTematico
            strDatasetName = "TEMATICO_" & idTematico
            strQuery = "SELECT * FROM " + strTableName
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aCol As Data.DataColumn
            Dim i As Integer
            ReDim valores(aDS.Tables(0).Columns.Count - 1)
            i = 0
            For Each aCol In aDS.Tables(0).Columns
                aValorCampo = New ValoresCampo()

                '' '' ''Select Case aCol.DataType.Name
                '' '' ''    Case "Decimal"
                '' '' ''        aValorCampo.tipo = "Number"
                '' '' ''    Case "String"
                '' '' ''        aValorCampo.tipo = "String"
                '' '' ''    Case "DateTime"
                '' '' ''        aValorCampo.tipo = "Date"
                '' '' ''    Case Else
                '' '' ''        aValorCampo.tipo = "String"
                '' '' ''End Select
                aValorCampo.nombre = strTableName + "." + aCol.ColumnName
                aValorCampo.aliasCampo = aCol.ColumnName
                colMinimos = ObtenerMaxMin(strTableName, aCol.ColumnName)
                If colMinimos Is Nothing Then
                    aValorCampo.minimo = ""
                    aValorCampo.maximo = ""
                Else
                    aValorCampo.minimo = colMinimos(0)
                    aValorCampo.maximo = colMinimos(1)
                End If

                aValorCampo.valores = ObtenerValoresUnicos(strTableName, aCol.ColumnName)
                valores.SetValue(aValorCampo, i)
                i += 1
            Next
            Return valores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Campos de tablas externas", EnableSession:=True)> _
    Public Function ObtenerCamposTematicoPredefinido(ByVal idTematico As Integer) As CampoTablaExterna()
        Dim aCampo As CampoTablaExterna
        Dim aRow As Data.DataRow
        Dim campos() As CampoTablaExterna
        Dim strEsquema As String, strTableName As String, strQuery As String, strDatasetName As String
        Dim aDS As Data.DataSet

        Try
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")
            'Obtenemos los nombres de los campos

            strTableName = "VIS_CEX_CAMPOSTABLASEXTERNAS"
            strDatasetName = "VIS_CEX_CAMPOSTABLASEXTERNAS"
            strQuery = "SELECT CAMPOS2.CEX_CO_ID, CAMPOS2.CEX_DS_NOMBRE, CAMPOS2.CEX_DS_DESCRIPCION, CAMPOS2.CEX_BOL_CLAVE, CAMPOS2.CEX_BOL_NUMERICO" & _
            " FROM VIS_CEX_CAMPOSTABLASEXTERNAS INNER JOIN VIS_TPF_TEMATICOSPREDEFINIDOS " & _
            " ON VIS_CEX_CAMPOSTABLASEXTERNAS.CEX_CO_ID = VIS_TPF_TEMATICOSPREDEFINIDOS.CEX_CO_ID " & _
            " INNER JOIN VIS_TEX_TABLASEXTERNAS ON VIS_CEX_CAMPOSTABLASEXTERNAS.TEX_CO_ID = VIS_TEX_TABLASEXTERNAS.TEX_CO_ID  " & _
            " INNER JOIN VIS_CEX_CAMPOSTABLASEXTERNAS CAMPOS2 ON VIS_TEX_TABLASEXTERNAS.TEX_CO_ID = CAMPOS2.TEX_CO_ID  " & _
            " WHERE VIS_TPF_TEMATICOSPREDEFINIDOS.TPF_CO_ID = " + idTematico.ToString()
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim i As Integer
            ReDim campos(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                aCampo = New CampoTablaExterna()
                aCampo.Id = aRow("CEX_CO_ID")
                aCampo.nombre = aRow("CEX_DS_NOMBRE")
                aCampo.descripcion = aRow("CEX_DS_DESCRIPCION")
                aCampo.clave = aRow("CEX_BOL_CLAVE")
                aCampo.numerico = aRow("CEX_BOL_NUMERICO")
                campos.SetValue(aCampo, i)
                i += 1
            Next
            Return campos
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Carga de Datos de Tematicos", EnableSession:=True)> _
    Public Function ObtenerValoresCampos2(ByVal strNombreTabla As String, ByVal strNombreCampo As String) As ValoresCampo()
        Dim aValorCampo As ValoresCampo
        Dim valores() As ValoresCampo
        Dim colMinimos As String()
        Dim strEsquema As String, strTableName As String, strQuery As String, strDatasetName As String
        Dim aDS As Data.DataSet

        Try
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")
            'Obtenemos los nombres de los campos

            strTableName = strNombreTabla
            strDatasetName = strNombreTabla
            strQuery = "SELECT * FROM " + strTableName
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aCol As Data.DataColumn
            Dim i As Integer
            ReDim valores(aDS.Tables(0).Columns.Count - 1)
            i = 0
            For Each aCol In aDS.Tables(0).Columns
                aValorCampo = New ValoresCampo()

                aValorCampo.nombre = strEsquema + strTableName + "." + aCol.ColumnName
                aValorCampo.aliasCampo = aCol.ColumnName
                colMinimos = ObtenerMaxMin(strNombreTabla, aCol.ColumnName)
                aValorCampo.minimo = colMinimos(0)
                aValorCampo.maximo = colMinimos(1)
                aValorCampo.valores = ObtenerValoresUnicos(strNombreTabla, aCol.ColumnName)
                valores.SetValue(aValorCampo, i)
                i += 1
            Next
            Return valores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Private Function ObtenerValoresUnicos(ByVal strNombreTabla As String, ByVal campoEntrada As String) As String()
        Dim strQuery As String, strTableName As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim arrValores() As String
        Dim strEsquema As String
        Try
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")

            strTableName = strNombreTabla
            strDatasetName = strNombreTabla
            strQuery = "SELECT DISTINCT(" & campoEntrada & ") FROM " & strNombreTabla & " WHERE " + campoEntrada + " IS NOT NULL ORDER BY " & campoEntrada
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)



            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                arrValores(i) = aRow(0)
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Private Function ObtenerMaxMin(ByVal strNombreTabla As String, ByVal campo As String) As String()
        Dim strQuery As String, strTableName As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim arrValores(1) As String
        Dim campoEntrada As String
        Dim valorMinimo As Object, valorMaximo As Object
        Dim valorDiferencia As Double, strEsquema As String
        Try
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")
            strTableName = strNombreTabla
            strDatasetName = strNombreTabla
            strQuery = "select min(" + campo + ") as MINIMO,max(" + campo + ") AS MAXIMO FROM " + strNombreTabla + " WHERE " + campo + " IS NOT NULL"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                If (aRow("MINIMO").Equals(System.DBNull.Value) Or aRow("MAXIMO").Equals(System.DBNull.Value)) Then
                    Return Nothing
                End If
                valorMaximo = IIf(IsNumeric(aRow("MAXIMO")), Replace(CStr(aRow("MAXIMO")), ",", "."), aRow("MAXIMO"))
                valorMinimo = IIf(IsNumeric(aRow("MINIMO")), Replace(CStr(aRow("MINIMO")), ",", "."), aRow("MINIMO"))
                arrValores(0) = valorMinimo 'aRow("MINIMO")
                arrValores(1) = valorMaximo 'aRow("MAXIMO")
            Next

            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Generacion de Tematicos", EnableSession:=True)> _
    Public Function GenerarTematico(ByVal idTematico As Integer, ByVal tipoLeyenda As String, ByVal tipoValores As Integer, ByVal lstValores() As String, ByVal lstColores() As String, ByVal lstEtiquetas() As String) As String
        'Debemos generar el AXL correspondiente a la leyenda
        'que nos servir� para representar el tem�tico
        Dim strQuery As String
        Dim xmlText As String, xmlProperties As String
        Dim strEsquema As String
        Dim i As Integer, j As Integer
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim campoEntrada As String, tablaTematico As String, campoTematico As String, tipoTematico As Integer, campoJoin As String, nombreTematico As String
        Try
            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT TAD_DS_CAMPO_TEM_ENTRADA,TAD_DS_CAMPO_TEM_JOIN,TIT_DS_TABLA_TEMATICO,TIT_DS_CAMPO_TEMATICO,TAD_DS_NOMBRE_ANALISIS,VIS_TAD_TEMATICOSDEMANDA.TIT_CO_ID FROM VIS_TAD_TEMATICOSDEMANDA,VIS_TIT_TIPO_TEMATICO WHERE VIS_TAD_TEMATICOSDEMANDA.TIT_CO_ID=VIS_TIT_TIPO_TEMATICO.TIT_CO_ID AND TAD_CO_ID=" & idTematico
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                campoEntrada = aRow("TAD_DS_CAMPO_TEM_ENTRADA")
                campoJoin = aRow("TAD_DS_CAMPO_TEM_JOIN")
                tablaTematico = aRow("TIT_DS_TABLA_TEMATICO")
                campoTematico = aRow("TIT_DS_CAMPO_TEMATICO")
                tipoTematico = aRow("TIT_CO_ID")
                nombreTematico = aRow("TAD_DS_NOMBRE_ANALISIS")
            Next
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")
            'Debemos empezar a escribir todos los layers
            xmlProperties = "<LEGEND title=""" & nombreTematico & """ font=""Arial"" autoextend=""true"" columns=""1"">"
            xmlProperties += "<LAYERS>"
            xmlText = "<LAYERLIST>"
            For i = 1 To 5
                If (tipoTematico <> i) Then
                    xmlText += "<LAYERDEF id=""" & i & """ visible=""false"" />"
                    xmlProperties += "<LAYER id=""" & i & """/>"
                Else
                    xmlProperties += ""
                    xmlText += "<LAYERDEF id=""" & i & """ visible=""true"" >"
                    xmlText += "<SPATIALQUERY where=""" & tablaTematico & "." & campoTematico & "=" & strEsquema & "TEMATICO_" & idTematico & "." & campoJoin & """ jointables=""" & strEsquema & "TEMATICO_" & idTematico & """/>"
                    xmlText += "<VALUEMAPRENDERER lookupfield=""" & strEsquema & "TEMATICO_" & idTematico & "." & campoEntrada & """ > "

                    Select Case tipoValores
                        Case 1 'Valores Exactos
                            For j = 0 To lstValores.Length - 1
                                xmlText += "<EXACT value=""" & lstValores(j) & """ label=""" & lstEtiquetas(j) & """>"
                                xmlText += "<SIMPLEPOLYGONSYMBOL filltype=""" & tipoLeyenda & """ fillcolor=""" & lstColores(j) & """ />"
                                'xmlText += "<SIMPLEPOLYGONSYMBOL filltype=""solid"" fillcolor=""" & lstColores(j) & """ />"
                                'filltype ="solid | bdiagonal | fdiagonal | cross | diagcross | horizontal | vertical | gray | lightgray | darkgray"  [solid]  
                                xmlText += "</EXACT>"
                            Next j
                        Case 2 'Rangos
                            Dim k As Integer
                            k = 0
                            For j = 0 To lstValores.Length - 1 Step 2

                                If (j < (lstValores.Length - 1)) Then
                                    xmlText += "<RANGE lower=""" & lstValores(j) & """ upper=""" & lstValores(j + 1) & """ equality=""lower"" label=""" & lstEtiquetas(k) & """>"
                                    xmlText += "<SIMPLEPOLYGONSYMBOL filltype=""" & tipoLeyenda & """ fillcolor=""" & lstColores(k) & """ />"
                                    'xmlText += "<SIMPLEPOLYGONSYMBOL filltype=""solid"" fillcolor=""" & lstColores(j) & """ />"
                                    'filltype ="solid | bdiagonal | fdiagonal | cross | diagcross | horizontal | vertical | gray | lightgray | darkgray"  [solid]  
                                    xmlText += "</RANGE>"
                                End If
                                k += 1
                            Next
                    End Select
                    Select Case tipoLeyenda
                        Case 1
                            'Valores

                        Case 2
                        Case 3
                    End Select
                    xmlText += "</VALUEMAPRENDERER>"
                    xmlText += "</LAYERDEF>"

                End If
            Next
            xmlProperties += "</LAYERS>"
            xmlProperties += "</LEGEND>"
            xmlText += "</LAYERLIST>"

            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "UPDATE VIS_TAD_TEMATICOSDEMANDA SET EST_CO_ID=2, TAD_DS_RESULTADO='" + xmlProperties + xmlText + "' WHERE TAD_CO_ID=" & idTematico
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            Return xmlProperties + xmlText
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Lista de Tipos de Tem�ticos Disponibles", EnableSession:=True)> _
    Public Function ListaTematicos() As TipoTematico()
        Dim strQuery As String, strConn As String
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim lstTematicos As TipoTematico()
        Dim aRow As System.Data.DataRow
        Try

            strConn = System.Configuration.ConfigurationManager.AppSettings("bbdd")
            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT TIT_CO_ID, TIT_DS_DESCRIPCION, TIT_DS_TABLA_TEMATICO, TIT_DS_CAMPO_TEMATICO FROM VIS_TIT_TIPO_TEMATICO ORDER BY TIT_CO_ID"
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            ReDim lstTematicos(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim temat As New TipoTematico()
                temat.Id = aRow("TIT_CO_ID")
                temat.nombre = aRow("TIT_DS_DESCRIPCION")
                temat.tabla = aRow("TIT_DS_TABLA_TEMATICO")
                temat.campo = aRow("TIT_DS_CAMPO_TEMATICO")
                lstTematicos(i) = temat
                i += 1
            Next
            aDS.Clear()
            aDS = Nothing
            Return lstTematicos
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Lista de Archivos Disponibles", EnableSession:=True)> _
    Public Function GuardarSimbologia(ByVal idTematico As Integer, ByVal axlRender As String) As Boolean
        Dim strQuery As String
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim tablaTematico As String, campoTematico As String
        Dim xmlText As String, strEsquema As String
        Dim tipoTematico As String, campoJoin As String
        Dim aRow As System.Data.DataRow
        'Step 2
        Dim strConn As String
        Dim strResultado As String
        Dim aConn As PgSqlConnection

        Try
            strConn = System.Configuration.ConfigurationManager.AppSettings("bbdd")
            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT TAD_DS_RESULTADO FROM VIS_TAD_TEMATICOSDEMANDA WHERE TAD_CO_ID=" & idTematico

            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                strResultado = aRow("TAD_DS_RESULTADO")
            Next
            strResultado += axlRender
            'xmlText = "<SPATIALQUEReY where=""" & tablaTematico & "." & campoTematico & "=" & strEsquema & "TEMATICO_" & idTematico & "." & campoJoin & """ jointables=""" & strEsquema & "TEMATICO_" & idTematico & """/>"
            'xmlText += vbNewLine + axlRender
            Dim cmdParam As OracleParameter
            Dim cmd As PgSqlCommand
            aConn = New PgSqlConnection(strConn)
            cmd = New PgSqlCommand("UPDATE VIS_TAD_TEMATICOSDEMANDA SET TAD_DS_RESULTADO = :Resultado, EST_CO_ID=2 WHERE TAD_CO_ID = " & idTematico, aConn)
            cmdParam = New OracleParameter(":Resultado", System.Data.OracleClient.OracleType.Clob, strResultado.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, strResultado)
            cmd.Parameters.Add(cmdParam)
            aConn.Open()
            cmd.ExecuteNonQuery()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Generaci�n de Alertas Sanitarias", EnableSession:=True)> _
    Public Function BusquedaUbicaciones(ByVal strCodigoRega As String) As Subexplotacion


    End Function
    <System.Web.Services.WebMethod(Description:="Generaci�n de Alertas Sanitarias", EnableSession:=True)> _
    Public Function GenerarAlertaFlex(ByVal alertaEntrada As Alerta) As Alerta
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim idAlerta As Long
        Dim nuevaAlerta As Alerta
        Dim i As Integer
        Dim arrCoordenadas() As String
        Dim strCodigo As String
        Dim aRow As Data.DataRow
        Try
            'Si es una edici�n de alerta, llegar� con id, si no, llegar� con el -1
            If (alertaEntrada.id <> -1) Then
                BorrarAlertaSanitaria(alertaEntrada.id)
                idAlerta = alertaEntrada.id
            Else
                'En primer lugar obtenemos el Id de la alerta, que nos servir� para autentificarla
                strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
                strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
                strQuery = "SELECT SEQ_ALE_CO_ID.NEXTVAL FROM DUAL"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)



                For Each aRow In aDS.Tables(0).Rows
                    idAlerta = aRow(0)
                Next
            End If
            strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
            strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
            'Una vez que tenemos el id, hacemos la inserci�n de los registros
            strQuery = "INSERT INTO SAN_ALE_ALERTAS_SANITARIAS ( ALE_CO_ID, ALE_DS_DESCRIPCION, ALE_DT_FECHA, ALE_BO_COMARCAS, ALE_DS_USUARIO, ENF_CO_ID,ALE_DS_FOCO) " & _
                       "VALUES ('" & idAlerta & "','" & alertaEntrada.descripcion & "','" & alertaEntrada.fecha & "','" & IIf(alertaEntrada.comarcas, "S", "N") & "','" & alertaEntrada.usuario & "','" & alertaEntrada.enferm.Id & "','" & alertaEntrada.identificador & "')"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            'Debemos introducir tambi�n los puntos, las especies y los radios
            'Empezamos por los puntos de afecci�n
            For i = 0 To alertaEntrada.puntos.Length - 1
                'Obtenemos la descripci�n del foco en cada punto
                Dim strCodigoFoco As String
                Dim strDescripcionFoco As String
                strCodigoFoco = alertaEntrada.puntos(i).idExplotacion
                strDescripcionFoco = alertaEntrada.puntos(i).descripcion
                ReDim arrCoordenadas(1)
                If ((alertaEntrada.puntos(i).latitud <> 0) And (alertaEntrada.puntos(i).longitud <> 0)) Then
                    arrCoordenadas(0) = alertaEntrada.puntos(i).latitud
                    arrCoordenadas(1) = alertaEntrada.puntos(i).longitud
                Else
                    'Tenemos un c�digo de explotaci�n
                    strCodigo = strCodigoFoco

                    'Buscamos las coordenadas del punto en la base de datos
                    strTableName = "T_UBICACION_GIS_REGA"
                    strDatasetName = "T_UBICACION_GIS_REGA"
                    'agm 23/03/2012 Migraci�n Ganader�a REGA
                    'strQuery = "SELECT UB_LAT,UB_LON FROM DGG.T_UBICACION_GIS_REGA WHERE UB_CE_SUBE='" & strCodigo & "'"
                    strQuery = "SELECT UB_LAT,UB_LON FROM DGG.SA_REGA_UBICACION WHERE UB_CO_SUBEXPLOT='" & strCodigo & "'"

                    aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                    For Each aRow In aDS.Tables(0).Rows
                        arrCoordenadas(0) = IIf(aRow("UB_LAT").Equals(System.DBNull.Value), "", aRow("UB_LAT"))
                        arrCoordenadas(1) = IIf(aRow("UB_LON").Equals(System.DBNull.Value), "", aRow("UB_LON"))
                    Next
                End If
                arrCoordenadas(0) = Replace(arrCoordenadas(0), ".", ",")
                arrCoordenadas(1) = Replace(arrCoordenadas(1), ".", ",")
                strQuery = "INSERT INTO SAN_PTO_PUNTOS_ALERTAS (PTO_CO_ID, ALE_CO_ID, PTO_CO_SUBEXPLOTACION, PTO_NM_LATITUD, PTO_NM_LONGITUD,PTO_DS_IDENTIFICACION) " & _
                           " VALUES (SEQ_PTO_CO_ID.NEXTVAL,'" & idAlerta & "','" & strCodigo & "','" & arrCoordenadas(0) & "','" & arrCoordenadas(1) & "','" & strDescripcionFoco & "')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next
            'Especies
            For i = 0 To alertaEntrada.especies.Length - 1
                strQuery = "INSERT INTO SAN_AES_ALERTAS_ESPECIES (ALE_CO_ID, ESP_CO_ID) " & _
                           " VALUES('" & idAlerta & "','" & alertaEntrada.especies(i).Id & "')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next
            'Radios
            For i = 0 To alertaEntrada.radios.Length - 1
                strQuery = "INSERT INTO SAN_RAD_RADIOS_AFECCION (ALE_CO_ID, RAD_CO_ID, RAD_NM_LONGITUD) " & _
                           "VALUES('" & idAlerta & "',SEQ_RAD_CO_ID.NEXTVAL,'" & alertaEntrada.radios(i).longitud & "')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next
            nuevaAlerta = ObtenerAlerta(idAlerta)
            Return nuevaAlerta
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Generaci�n de Alertas Sanitarias", EnableSession:=True)> _
    Public Function GenerarAlerta(ByVal alertaEntrada As Alerta) As Alerta
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim idAlerta As Long
        Dim nuevaAlerta As Alerta
        Dim i As Integer
        Dim arrCoordenadas() As String
        Dim strCodigo As String
        Dim aRow As Data.DataRow
        Try
            'Si es una edici�n de alerta, llegar� con id, si no, llegar� con el -1
            If (alertaEntrada.id <> -1) Then
                BorrarAlertaSanitaria(alertaEntrada.id)
                idAlerta = alertaEntrada.id
            Else
                'En primer lugar obtenemos el Id de la alerta, que nos servir� para autentificarla
                strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
                strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
                strQuery = "SELECT SEQ_ALE_CO_ID.NEXTVAL FROM DUAL"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)



                For Each aRow In aDS.Tables(0).Rows
                    idAlerta = aRow(0)
                Next
            End If
            strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
            strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
            'Una vez que tenemos el id, hacemos la inserci�n de los registros
            strQuery = "INSERT INTO SAN_ALE_ALERTAS_SANITARIAS ( ALE_CO_ID, ALE_DS_DESCRIPCION, ALE_DT_FECHA, ALE_BO_COMARCAS, ALE_DS_USUARIO, ENF_CO_ID,ALE_DS_FOCO) " & _
                       "VALUES ('" & idAlerta & "','" & alertaEntrada.descripcion & "','" & alertaEntrada.fecha & "','" & IIf(alertaEntrada.comarcas, "S", "N") & "','" & Me.Session("Usuario") & "','" & alertaEntrada.enferm.Id & "','" & alertaEntrada.identificador & "')"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            'Debemos introducir tambi�n los puntos, las especies y los radios
            'Empezamos por los puntos de afecci�n
            For i = 0 To alertaEntrada.puntos.Length - 1
                'Obtenemos la descripci�n del foco en cada punto
                Dim strCodigoFoco As String
                Dim strDescripcionFoco As String
                strCodigoFoco = alertaEntrada.puntos(i).idExplotacion
                strDescripcionFoco = alertaEntrada.puntos(i).descripcion
                ReDim arrCoordenadas(1)
                If ((alertaEntrada.puntos(i).latitud <> 0) And (alertaEntrada.puntos(i).longitud <> 0)) Then
                    arrCoordenadas(0) = alertaEntrada.puntos(i).latitud
                    arrCoordenadas(1) = alertaEntrada.puntos(i).longitud
                Else
                    'Tenemos un c�digo de explotaci�n
                    strCodigo = strCodigoFoco

                    'Buscamos las coordenadas del punto en la base de datos
                    strTableName = "T_UBICACION_GIS_REGA"
                    strDatasetName = "T_UBICACION_GIS_REGA"
                    'agm 23/03/2012 Migraci�n Ganader�a REGA
                    strQuery = "SELECT UB_LAT,UB_LON FROM DGG.SA_REGA_UBICACION WHERE SE_CO_SUBEXPLOT='" & strCodigo & "'"
                    'strQuery = "SELECT UB_LAT,UB_LON FROM DGG.T_UBICACION_GIS_REGA WHERE UB_CE_SUBE='" & strCodigo & "'"

                    aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                    For Each aRow In aDS.Tables(0).Rows
                        arrCoordenadas(0) = IIf(aRow("UB_LAT").Equals(System.DBNull.Value), "", aRow("UB_LAT"))
                        arrCoordenadas(1) = IIf(aRow("UB_LON").Equals(System.DBNull.Value), "", aRow("UB_LON"))
                    Next
                End If
                arrCoordenadas(0) = Replace(arrCoordenadas(0), ".", ",")
                arrCoordenadas(1) = Replace(arrCoordenadas(1), ".", ",")
                strQuery = "INSERT INTO SAN_PTO_PUNTOS_ALERTAS (PTO_CO_ID, ALE_CO_ID, PTO_CO_SUBEXPLOTACION, PTO_NM_LATITUD, PTO_NM_LONGITUD,PTO_DS_IDENTIFICACION) " & _
                           " VALUES (SEQ_PTO_CO_ID.NEXTVAL,'" & idAlerta & "','" & strCodigo & "','" & arrCoordenadas(0) & "','" & arrCoordenadas(1) & "','" & strDescripcionFoco & "')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next
            'Especies
            For i = 0 To alertaEntrada.especies.Length - 1
                strQuery = "INSERT INTO SAN_AES_ALERTAS_ESPECIES (ALE_CO_ID, ESP_CO_ID) " & _
                           " VALUES('" & idAlerta & "','" & alertaEntrada.especies(i).Id & "')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next
            'Radios
            For i = 0 To alertaEntrada.radios.Length - 1
                strQuery = "INSERT INTO SAN_RAD_RADIOS_AFECCION (ALE_CO_ID, RAD_CO_ID, RAD_NM_LONGITUD) " & _
                           "VALUES('" & idAlerta & "',SEQ_RAD_CO_ID.NEXTVAL,'" & alertaEntrada.radios(i).longitud & "')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next
            nuevaAlerta = ObtenerAlerta(idAlerta)
            Return nuevaAlerta
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Generaci�n de Alertas Sanitarias", EnableSession:=True)> _
    Public Function GenerarAlertaOld(ByVal strDescripcion As String, ByVal dtFecha As String, ByVal bolComarcas As Boolean, ByVal idEnfermedad As Integer, ByVal lstPuntos() As String, ByVal lstEspecies() As String, ByVal lstRadios() As Long, ByVal identificador As String) As Alerta
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim idAlerta As Long
        Dim nuevaAlerta As Alerta
        Dim i As Integer
        Dim arrCoordenadas() As String
        Dim strCodigo As String
        Try
            'En primer lugar obtenemos el Id de la alerta, que nos servir� para autentificarla
            strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
            strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
            strQuery = "SELECT SEQ_ALE_CO_ID.NEXTVAL FROM DUAL"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow


            For Each aRow In aDS.Tables(0).Rows
                idAlerta = aRow(0)
            Next

            'Una vez que tenemos el id, hacemos la inserci�n de los registros
            strQuery = "INSERT INTO SAN_ALE_ALERTAS_SANITARIAS ( ALE_CO_ID, ALE_DS_DESCRIPCION, ALE_DT_FECHA, ALE_BO_COMARCAS, ALE_DS_USUARIO, ENF_CO_ID,ALE_DS_FOCO) " & _
                       "VALUES ('" & idAlerta & "','" & strDescripcion & "','" & dtFecha & "','" & IIf(bolComarcas, "S", "N") & "','" & Me.Session("Usuario") & "','" & idEnfermedad & "','" & identificador & "')"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            'Debemos introducir tambi�n los puntos, las especies y los radios
            'Empezamos por los puntos de afecci�n
            For i = 0 To lstPuntos.Length - 1
                'Obtenemos la descripci�n del foco en cada punto
                Dim strCodigoFoco As String
                Dim strDescripcionFoco As String
                strCodigoFoco = lstPuntos(i).Split("|")(0)
                strDescripcionFoco = lstPuntos(i).Split("|")(1)
                If (strCodigoFoco.Contains("@")) Then
                    'Tenemos unas coordenadas
                    arrCoordenadas = strCodigoFoco.Split("@")
                    strCodigo = ""
                Else
                    'Tenemos un c�digo de explotaci�n
                    strCodigo = strCodigoFoco
                    ReDim arrCoordenadas(1)
                    'Buscamos las coordenadas del punto en la base de datos
                    strTableName = "T_UBICACION_GIS_REGA"
                    strDatasetName = "T_UBICACION_GIS_REGA"
                    'agm 23/03/2012 Migraci�n Ganader�a REGA
                    strQuery = "SELECT UB_LAT,UB_LON FROM DGG.SA_REGA_UBICACION WHERE SE_CO_SUBEXPLOT='" & strCodigo & "'"
                    'strQuery = "SELECT UB_LAT,UB_LON FROM DGG.T_UBICACION_GIS_REGA WHERE UB_CE_SUBE='" & strCodigo & "'"
                    aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                    For Each aRow In aDS.Tables(0).Rows
                        arrCoordenadas(0) = IIf(aRow("UB_LAT").Equals(System.DBNull.Value), "", aRow("UB_LAT"))
                        arrCoordenadas(1) = IIf(aRow("UB_LON").Equals(System.DBNull.Value), "", aRow("UB_LON"))
                    Next
                End If
                arrCoordenadas(0) = Replace(arrCoordenadas(0), ".", ",")
                arrCoordenadas(1) = Replace(arrCoordenadas(1), ".", ",")
                strQuery = "INSERT INTO SAN_PTO_PUNTOS_ALERTAS (PTO_CO_ID, ALE_CO_ID, PTO_CO_SUBEXPLOTACION, PTO_NM_LATITUD, PTO_NM_LONGITUD,PTO_DS_IDENTIFICACION) " & _
                           " VALUES (SEQ_PTO_CO_ID.NEXTVAL,'" & idAlerta & "','" & strCodigo & "','" & arrCoordenadas(0) & "','" & arrCoordenadas(1) & "','" & strDescripcionFoco & "')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next
            'Especies
            For i = 0 To lstEspecies.Length - 1
                strQuery = "INSERT INTO SAN_AES_ALERTAS_ESPECIES (ALE_CO_ID, ESP_CO_ID) " & _
                           " VALUES('" & idAlerta & "','" & lstEspecies(i) & "')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next
            'Radios
            For i = 0 To lstRadios.Length - 1
                strQuery = "INSERT INTO SAN_RAD_RADIOS_AFECCION (ALE_CO_ID, RAD_CO_ID, RAD_NM_LONGITUD) " & _
                           "VALUES('" & idAlerta & "',SEQ_RAD_CO_ID.NEXTVAL,'" & lstRadios(i) & "')"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next
            nuevaAlerta = ObtenerAlerta(idAlerta)
            Return nuevaAlerta
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Obtenci�n de Alertas Sanitarias", EnableSession:=True)> _
    Public Function ObtenerAlerta(ByVal idAlerta As Long) As Alerta
        Dim aAlert As Alerta
        aAlert = New Alerta()
        aAlert.Cargar(idAlerta)
        Return aAlert
    End Function
    <System.Web.Services.WebMethod(Description:="B�squeda de Alertas Sanitarias", EnableSession:=True)> _
    Public Function BuscarAlerta(ByVal tipoBusqueda As Integer, ByVal valorCampo As String) As Alerta()
        Dim aAlerts As Alerta()
        Dim i As Integer
        Dim idAlerta As Long
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow

        strQuery = "SELECT ALE_CO_ID FROM SAN_ALE_ALERTAS_SANITARIAS WHERE "
        strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
        strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
        If (tipoBusqueda = 1) Then
            strQuery += "ENF_CO_ID='" + valorCampo + "'"
        ElseIf (tipoBusqueda = 2) Then
            strQuery += "TO_CHAR(ALE_DT_FECHA,'dd/MM/yyyy')='" + valorCampo + "'"
        ElseIf (tipoBusqueda = 3) Then
            strQuery += "TO_CHAR(ALE_DT_GENERACION,'dd/MM/yyyy')='" + valorCampo + "'"
        ElseIf (tipoBusqueda = 4) Then
            strQuery += "UPPER(ALE_DS_FOCO) LIKE '%" + UCase(valorCampo) + "%'"
        ElseIf (tipoBusqueda = 5) Then
            strQuery += "UPPER(ALE_DS_DESCRIPCION) LIKE '%" + UCase(valorCampo) + "%'"
        End If
        aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
        i = 0
        ReDim aAlerts(aDS.Tables(0).Rows.Count - 1)
        For Each aRow In aDS.Tables(0).Rows
            aAlerts(i) = New Alerta()
            idAlerta = aRow("ALE_CO_ID")
            aAlerts(i).Cargar(idAlerta)
            i += 1
        Next
        Return aAlerts
    End Function



    <System.Web.Services.WebMethod(Description:="Informaci�n Alerta Sanitaria", EnableSession:=True)> _
    Public Function ObtenerInfoAlerta(ByVal strNombre As String) As Alerta()
        Dim aAlerts As Alerta()
        Dim i As Integer
        Dim idAlerta As Long
        Dim aDS As Data.DataSet
        Dim strQuery As String, strQuery2 As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow

        strQuery = "SELECT ALE_CO_ID FROM SAN_ALE_ALERTAS_SANITARIAS WHERE "
        strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
        strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
        'strQuery2 = " UPPER(ALE_CO_ID) LIKE '%" + UCase(strNombre) + "%'"
        strQuery2 = " UPPER(ALE_CO_ID) =" + UCase(strNombre)

        aDS = ExecuteQuery(strQuery + strQuery2, strTableName, strDatasetName)
        i = 0
        ReDim aAlerts(aDS.Tables(0).Rows.Count - 1)
        For Each aRow In aDS.Tables(0).Rows
            aAlerts(i) = New Alerta()
            idAlerta = aRow("ALE_CO_ID")
            aAlerts(i).Cargar(idAlerta)
            i += 1
        Next
        Return aAlerts
    End Function




    <System.Web.Services.WebMethod(Description:="B�squeda de Alertas Sanitarias", EnableSession:=True)> _
    Public Function BuscarAlertaNew(ByVal codEnfermedad As String, ByVal strFecha As String, ByVal strNombre As String, ByVal strDescripcion As String) As Alerta()
        Dim aAlerts As Alerta()
        Dim i As Integer
        Dim k As Integer
        'Dim idAlerta As Long
        Dim aDS As Data.DataSet
        Dim aDSEnf As Data.DataSet
        Dim strQuery As String, strQuery2 As String, strQueryEnf As String
        Dim strTableName As String, strDatasetName As String
        Dim strTableNameEnf As String, strDatasetNameEnf As String
        Dim aRow As Data.DataRow
        Dim aRowEnf As Data.DataRow

        strQuery = "SELECT ALE_CO_ID, ENF_CO_ID, ALE_DT_FECHA, ALE_DS_DESCRIPCION FROM SAN_ALE_ALERTAS_SANITARIAS WHERE "
        strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
        strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
        strQuery2 = ""
        If codEnfermedad <> "" Then
            strQuery2 += " ENF_CO_ID='" + codEnfermedad + "'"
        End If
        If strFecha <> "" Then
            If (strQuery2.Length > 0) Then
                strQuery2 += " AND "
            End If
            strQuery2 += " TO_CHAR(ALE_DT_FECHA,'dd/MM/yyyy')='" + strFecha + "'"
        End If
        If strNombre <> "" Then
            If (strQuery2.Length > 0) Then
                strQuery2 += " AND "
            End If
            strQuery2 += " UPPER(ALE_CO_ID) LIKE '%" + UCase(strNombre) + "%'"
            'strQuery2 += " UPPER(ALE_CO_ID) =" + UCase(strNombre)
        End If
        If strDescripcion <> "" Then
            If (strQuery2.Length > 0) Then
                strQuery2 += " AND "
            End If
            strQuery2 += " UPPER(ALE_DS_DESCRIPCION) LIKE '%" + UCase(strDescripcion) + "%'"
        End If
        strQuery2 += " ORDER BY ALE_CO_ID DESC"
        aDS = ExecuteQuery(strQuery + strQuery2, strTableName, strDatasetName)
        i = 0
        ReDim aAlerts(aDS.Tables(0).Rows.Count - 1)
        For Each aRow In aDS.Tables(0).Rows
            aAlerts(i) = New Alerta()
            'idAlerta = aRow("ALE_CO_ID")
            'aAlerts(i).Cargar(idAlerta)
            aAlerts(i).enferm = New Enfermedad()
            aAlerts(i).enferm.Id = aRow("ENF_CO_ID")

            strTableNameEnf = "SAN_ENF_ENFERMEDADES"
            strDatasetNameEnf = "SAN_ENF_ENFERMEDADES"
            strQueryEnf = "SELECT ENF_DS_DESCRIPCION FROM " & _
                           strTableNameEnf & " WHERE ENF_CO_ID='" & aAlerts(i).enferm.Id & "'"
            aDSEnf = ExecuteQuery(strQueryEnf, strTableNameEnf, strDatasetNameEnf)

            For Each aRowEnf In aDSEnf.Tables(0).Rows
                aAlerts(i).enferm.nombre = aRowEnf("ENF_DS_DESCRIPCION")
            Next

            'aAlerts(i).fecha = aRow("ALE_DT_FECHA")
            aAlerts(i).fecha = IIf(DBNull.Value.Equals(aRow("ALE_DT_FECHA")), "", aRow("ALE_DT_FECHA"))
            aAlerts(i).id = aRow("ALE_CO_ID")   ' Todas las alertas tiene un ID
            'aAlerts(i).descripcion = aRow("ALE_DS_DESCRIPCION")
            aAlerts(i).descripcion = IIf(DBNull.Value.Equals(aRow("ALE_DS_DESCRIPCION")), "", aRow("ALE_DS_DESCRIPCION"))
            i += 1
        Next
        Return aAlerts
    End Function



    '<System.Web.Services.WebMethod(Description:="B�squeda de Alertas Sanitarias", EnableSession:=True)> _
    'Public Function BuscarAlertaNew(ByVal codEnfermedad As String, ByVal strFecha As String, ByVal strNombre As String, ByVal strDescripcion As String) As Alerta()
    'Dim aAlerts As Alerta()
    'Dim i As Integer
    'Dim idAlerta As Long
    'Dim aDS As Data.DataSet
    'Dim strQuery As String, strQuery2 As String
    'Dim strTableName As String, strDatasetName As String
    'Dim aRow As Data.DataRow
    '
    'strQuery = "SELECT ALE_CO_ID FROM SAN_ALE_ALERTAS_SANITARIAS WHERE "
    'strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
    'strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
    'strQuery2 = ""
    'If codEnfermedad <> "" Then
    'strQuery2 += " ENF_CO_ID='" + codEnfermedad + "'"
    'End If
    'If strFecha <> "" Then
    'If (strQuery2.Length > 0) Then
    'strQuery2 += " AND "
    'End If
    'strQuery2 += " TO_CHAR(ALE_DT_FECHA,'dd/MM/yyyy')='" + strFecha + "'"
    'End If
    'If strNombre <> "" Then
    'If (strQuery2.Length > 0) Then
    'strQuery2 += " AND "
    'End If
    'strQuery2 += " UPPER(ALE_CO_ID) LIKE '%" + UCase(strNombre) + "%'"
    'End If
    'If strDescripcion <> "" Then
    'If (strQuery2.Length > 0) Then
    'strQuery2 += " AND "
    'End If
    'strQuery2 += " UPPER(ALE_DS_DESCRIPCION) LIKE '%" + UCase(strDescripcion) + "%'"
    'End If
    'strQuery2 += " ORDER BY ALE_CO_ID DESC"
    'aDS = ExecuteQuery(strQuery + strQuery2, strTableName, strDatasetName)
    'i = 0
    'ReDim aAlerts(aDS.Tables(0).Rows.Count - 1)
    'For Each aRow In aDS.Tables(0).Rows
    'aAlerts(i) = New Alerta()
    'idAlerta = aRow("ALE_CO_ID")
    'aAlerts(i).Cargar(idAlerta)
    'i += 1
    'Next
    'Return aAlerts
    'End Function

    <System.Web.Services.WebMethod(Description:="Obtenci�n de Comarcas Afectadas por una alerta sanitaria", EnableSession:=True)> _
    Public Function BorrarAlertaSanitaria(ByVal idAlerta As Integer) As Boolean
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Try
            'Borramos Especies Afectadas
            strQuery = "DELETE FROM SAN_AES_ALERTAS_ESPECIES WHERE ALE_CO_ID='" + idAlerta.ToString() + "'"
            strTableName = "SAN_AES_ALERTAS_ESPECIES"
            strDatasetName = "SAN_AES_ALERTAS_ESPECIES"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            strQuery = "DELETE FROM SAN_PTO_PUNTOS_ALERTAS WHERE ALE_CO_ID='" + idAlerta.ToString() + "'"
            strTableName = "SAN_PTO_PUNTOS_ALERTAS"
            strDatasetName = "SAN_PTO_PUNTOS_ALERTAS"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            strQuery = "DELETE  FROM SAN_REX_RADIOS_EXCEPCIONES_NUE WHERE SAN_REX_RADIOS_EXCEPCIONES_NUE.RAD_CO_ID in (SELECT RAD_CO_ID FROM SAN_RAD_RADIOS_AFECCION WHERE ALE_CO_ID='" + idAlerta.ToString() + "')"
            strTableName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
            strDatasetName = "SAN_REX_RADIOS_EXCEPCIONES_NUE"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            strQuery = "DELETE FROM SAN_RAD_RADIOS_AFECCION WHERE ALE_CO_ID='" + idAlerta.ToString() + "'"
            strTableName = "SAN_RAD_RADIOS_AFECCION"
            strDatasetName = "SAN_RAD_RADIOS_AFECCION"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            strQuery = "DELETE FROM SAN_ALE_ALERTAS_SANITARIAS WHERE ALE_CO_ID='" + idAlerta.ToString() + "'"
            strTableName = "SAN_ALE_ALERTAS_SANITARIAS"
            strDatasetName = "SAN_ALE_ALERTAS_SANITARIAS"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Return True
        Catch
            Return False
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Guardar Colores Anillos Sanitarios", EnableSession:=True)> _
    Public Function GuardarColores(ByVal lstRadios() As Long, ByVal lstColores() As String) As Boolean
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim i As Integer
        Try
            'Borramos Especies Afectadas
            For i = 0 To lstRadios.Length - 1

                strQuery = "UPDATE SAN_RAD_RADIOS_AFECCION SET RAD_DS_COLOR='" + lstColores(i) + "' WHERE RAD_CO_ID=" + CStr(lstRadios(i))
                strTableName = "SAN_RAD_RADIOS_AFECCION"
                strDatasetName = "SAN_RAD_RADIOS_AFECCION"
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Next i
        Catch ex As Exception

        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Consulta de Alertas Sanitarias", EnableSession:=True)> _
    Public Function ConsultaAlerta(ByVal idAlerta As Long, ByVal colIdFocos As Long(), ByVal xmlAlerta As XmlDocument) As FichaAlerta
        Dim aAlert As Alerta
        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim h As Integer
        Dim jj As Integer
        Dim aDS As Data.DataSet
        Dim strQuery As String, strQuery1 As String, strQuery2 As String, strQuery3 As String, strQuery4 As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        'Dim colIdExplotaciones() As Collection
        Dim colIdComarcas() As Collection
        Dim strEspecies As String
        Dim strIdComarcas As String
        Dim addId As Boolean
        Dim seguir As Boolean
        Dim idEspecie As String
        Dim addEspecie As Boolean
        Dim strQueryExcepciones As String, strQueryExcepciones2 As String
        Dim ficAlSanitaria As FichaAlerta
        Dim m_nodelist As XmlNodeList, m_nodelist2 As XmlNodeList, m_nodelist3 As XmlNodeList
        Dim m_node As XmlNode, m_node2 As XmlNode, m_node3 As XmlNode
        Dim idPunto As Long
        Dim primera As Boolean
        aAlert = New Alerta()
        aAlert.Cargar(idAlerta)

        ficAlSanitaria = New FichaAlerta()
        ficAlSanitaria.descripcion = aAlert.descripcion
        ficAlSanitaria.identificador = aAlert.identificador
        If (colIdFocos Is Nothing) Then
            ficAlSanitaria.focos = aAlert.puntos
        Else
            ReDim ficAlSanitaria.focos(colIdFocos.Length - 1)
            For i = 0 To colIdFocos.Length - 1
                ficAlSanitaria.focos(i) = New PuntoInfeccion()
                ficAlSanitaria.focos(i).Cargar(colIdFocos(i))
            Next i
        End If
        ReDim ficAlSanitaria.anillos(aAlert.radios.Length - 1)
        If (aAlert.comarcas) Then
            strQueryExcepciones = ""
            strQueryExcepciones2 = ""
            'Si la alerta incluye las comarcas, la selecci�n no se realiza desde la base de datos, sino gr�ficamente.
            'Ahora debemos seleccionar los atributos de las ubicaciones
            m_nodelist = xmlAlerta.SelectNodes("/Alerta/anillo")
            'Iniciamos el ciclo de lectura
            strEspecies = " INSTR( "
            For i = 0 To aAlert.especies.Length - 1
                strEspecies += "'" & aAlert.especies(i).Id & "'"
                If (i = aAlert.especies.Length - 1) Then
                    'agm 23/03/2012 Migraci�n Ganader�a REGA
                    strEspecies += ",SE_CO_ESPECIE)>0"
                    'strEspecies += ",SE_CO_EXPECIE)>0"
                Else
                    strEspecies += " || '-' || "
                End If
            Next
            'En este caso lo vamos a utilizar para las comarcas (mucho m�s sencillo)
            ReDim colIdComarcas(m_nodelist.Count - 1)
            For i = 0 To m_nodelist.Count - 1
                Dim queryExcepciones As String
                m_node = m_nodelist(i)
                ficAlSanitaria.anillos(i) = New Anillo()
                ReDim ficAlSanitaria.anillos(i).subexplotaciones(0)
                Dim strId As String
                Dim strFocos As String()
                colIdComarcas(i) = New Collection()
                m_node2 = m_node.SelectSingleNode("id")
                strId = m_node2.InnerText
                strFocos = strId.Split("_")
                ficAlSanitaria.anillos(i).longitud = strFocos(strFocos.Length - 1)
                m_nodelist2 = m_node.SelectNodes("comarca")
                ' m_nodelist3 = m_node.SelectNodes("subexplotaciones")
                If (m_nodelist2.Count > 0) Then
                    strIdComarcas = " INSTR( "
                    For j = 0 To m_nodelist2.Count - 1
                        m_node2 = m_nodelist2(j)
                        addId = True
                        For k = 0 To i
                            'System.Array.Find(ficAlSanitaria.anillos(i).subexplotaciones, Subexplotacion.Comparar())
                            If colIdComarcas(k).Contains(m_node2.InnerText) Then
                                addId = False
                                Exit For
                            End If
                        Next
                        If addId Then
                            colIdComarcas(i).Add(m_node2.InnerText, m_node2.InnerText)
                            If (colIdComarcas(i).Count > 1) Then
                                strIdComarcas += " || '-' || "
                            End If
                            strIdComarcas += "'" + m_node2.InnerText + "'"

                        End If
                    Next j


                    strIdComarcas += ",COMARCA_SGSA)>0"

                    'agm 23/03/2012 Migraci�n Ganader�a REGA
                    'strQuery = "SELECT * from DGG.REGA_MV_UBICACION_ESPECIE_COMP WHERE " + strIdComarcas + " AND " + strEspecies


                    strQuery = "SELECT SE_CE_EXPLOT,TIPO_EXPLOTACION,SE_CO_ESPECIE,SE_CO_SUBEXPLOT,ES_CO_FAMILIA,ESPECIE,ID_ESTADO,ESTADO,SE_FC_EST,SE_AUTOC,SE_DIREC,SE_CP,SE_PROV,SE_MUN,SE_MUNPROV,MUNICIPIO,PROVINCIA,COMARCA_SGSA,COMARCA_SGSA_NOM " & _
                        ", SUM(COALESCE(CNV.CN_CENSO,0) )  + SUM(COALESCE(CVAC.CN_MACHOS_12M,0)) + SUM(COALESCE(CVAC.CN_HEMBRAS_12M,0)) + SUM(COALESCE(CVAC.CN_MACHOS_12_24M,0)) + SUM(COALESCE(CVAC.CN_HEMBRAS_12_24M,0)) + SUM(COALESCE(CVAC.CN_MACHOS_24M,0)) + SUM(COALESCE(CVAC.CN_HEMBRAS_24M,0)) " & _
                        "CENSO from DGG.REGA_MV_UBICACION_ESPECIE_COMP " & _
                        "LEFT JOIN DGG.REGA_MV_T_CENSONOVAC CNV ON CNV.CN_CE_SUBEXP =  DGG.REGA_MV_UBICACION_ESPECIE_COMP.SE_CE_EXPLOT AND CNV.CN_CO_ESPECIE = DGG.REGA_MV_UBICACION_ESPECIE_COMP.SE_CO_ESPECIE " & _
                        "LEFT JOIN DGG.REGA_MV_T_CENSOVAC CVAC ON CVAC.CN_CE_SUBEXP =  DGG.REGA_MV_UBICACION_ESPECIE_COMP.SE_CE_EXPLOT AND CVAC.CN_CO_ESPECIE = DGG.REGA_MV_UBICACION_ESPECIE_COMP.SE_CO_ESPECIE " & _
                        " WHERE " + strIdComarcas + " AND " + strEspecies

                    strQuery4 = " GROUP BY SE_CE_EXPLOT,TIPO_EXPLOTACION,SE_CO_ESPECIE,SE_CO_SUBEXPLOT,ES_CO_FAMILIA,ESPECIE,ID_ESTADO,ESTADO,SE_FC_EST,SE_AUTOC,SE_DIREC,SE_CP,SE_PROV,SE_MUN,SE_MUNPROV,MUNICIPIO,PROVINCIA,COMARCA_SGSA,COMARCA_SGSA_NOM"

                    'strQuery = "SELECT * from DGG.VUC_MV_UBICACION_ESPECIES_COMP WHERE " + strIdComarcas + " AND " + strEspecies
                    Dim z As Integer
                    If Not (aAlert.radios(i).excepciones.municipios Is Nothing) Then
                        If (aAlert.radios(i).excepciones.municipios.Length) Then
                            strQueryExcepciones += " AND SE_MUNPROV NOT IN("
                            For z = 0 To aAlert.radios(i).excepciones.municipios.Length - 1
                                strQueryExcepciones += "'" + aAlert.radios(i).excepciones.municipios(z).Id.ToString + "'"
                                If (z = aAlert.radios(i).excepciones.municipios.Length - 1) Then
                                    strQueryExcepciones += ") "
                                Else
                                    strQueryExcepciones += ","
                                End If
                            Next
                        End If
                    End If
                    If Not (aAlert.radios(i).excepciones.subexplotaciones Is Nothing) Then
                        If (aAlert.radios(i).excepciones.subexplotaciones.Length) Then
                            'agm 23/03/2012 Migraci�n Ganader�a REGA
                            strQueryExcepciones += " AND SE_CO_SUBEXPLOT NOT IN("
                            'strQueryExcepciones += " AND SE_CLAVE NOT IN("
                            For z = 0 To aAlert.radios(i).excepciones.subexplotaciones.Length - 1
                                strQueryExcepciones += "'" + aAlert.radios(i).excepciones.subexplotaciones(z).id.ToString + "'"
                                If (z = aAlert.radios(i).excepciones.subexplotaciones.Length - 1) Then
                                    strQueryExcepciones += ") "
                                Else
                                    strQueryExcepciones += ","
                                End If
                            Next
                        End If
                    End If
                    If (i > 0) Then
                        If Not (aAlert.radios(i).excepciones.municipios Is Nothing) Then
                            If (aAlert.radios(i - 1).excepciones.municipios.Length) Then
                                strQueryExcepciones2 += " OR (SE_MUNPROV IN("
                                For z = 0 To aAlert.radios(i - 1).excepciones.municipios.Length - 1
                                    strQueryExcepciones2 += "'" + aAlert.radios(i - 1).excepciones.municipios(z).Id.ToString + "'"
                                    If (z = aAlert.radios(i - 1).excepciones.municipios.Length - 1) Then
                                        strQueryExcepciones2 += ") "
                                    Else
                                        strQueryExcepciones2 += ","
                                    End If
                                Next
                            End If
                        End If
                        If Not (aAlert.radios(i).excepciones.subexplotaciones Is Nothing) Then
                            If (aAlert.radios(i - 1).excepciones.subexplotaciones.Length) Then
                                'agm 23/03/2012 Migraci�n Ganader�a REGA
                                strQueryExcepciones2 += " OR (SE_CO_SUBEXPLOT IN("
                                'strQueryExcepciones2 += " OR (SE_CLAVE IN("
                                For z = 0 To aAlert.radios(i - 1).excepciones.subexplotaciones.Length - 1
                                    strQueryExcepciones2 += "'" + aAlert.radios(i - 1).excepciones.subexplotaciones(z).id.ToString + "'"
                                    If (z = aAlert.radios(i - 1).excepciones.subexplotaciones.Length - 1) Then
                                        strQueryExcepciones2 += ") "
                                    Else
                                        strQueryExcepciones2 += ","
                                    End If
                                Next
                            End If
                        End If
                    End If

                    'agm 23/03/2012 Migraci�n Ganader�a REGA
                    strTableName = "REGA_MV_UBICACION_ESPECIES_COMP"
                    strDatasetName = "REGA_MV_UBICACION_ESPECIES_COMP"
                    'strTableName = "VUC_MV_UBICACION_ESPECIES_COMP"
                    'strDatasetName = "VUC_MV_UBICACION_ESPECIES_COMP"
                    strQuery += strQueryExcepciones + strQueryExcepciones2 + strQuery4

                    aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)


                    'Debemos a�adir los elementos a la colecci�n de Id's de explotaciones
                    ReDim Preserve ficAlSanitaria.anillos(i).subexplotaciones(aDS.Tables(0).Rows.Count - 1)


                    k = 0
                    For Each aRow In aDS.Tables(0).Rows

                        ficAlSanitaria.anillos(i).subexplotaciones(k) = New Subexplotacion()
                        'agm 23/03/2012 Migraci�n Ganader�a REGA
                        ficAlSanitaria.anillos(i).subexplotaciones(k).id = IIf(aRow("SE_CO_SUBEXPLOT").Equals(System.DBNull.Value), 0, aRow("SE_CO_SUBEXPLOT"))
                        ficAlSanitaria.anillos(i).subexplotaciones(k).idExplotacion = IIf(aRow("SE_CE_EXPLOT").Equals(System.DBNull.Value), "", aRow("SE_CE_EXPLOT"))
                        'ficAlSanitaria.anillos(i).subexplotaciones(k).id = IIf(aRow("SE_CLAVE").Equals(System.DBNull.Value), 0, aRow("SE_CLAVE"))
                        'ficAlSanitaria.anillos(i).subexplotaciones(k).idExplotacion = IIf(aRow("SE_CE_EXPLOT").Equals(System.DBNull.Value), "", aRow("SE_CE_EXPLOT"))


                        'ficAlSanitaria.anillos(i).subexplotaciones(k).direccion = IIf(aRow("SE_DIREC").Equals(System.DBNull.Value), "", aRow("SE_DIREC"))
                        'ficAlSanitaria.anillos(i).subexplotaciones(k).codPostal = IIf(aRow("SE_CP").Equals(System.DBNull.Value), "", aRow("SE_CP"))
                        'ficAlSanitaria.anillos(i).subexplotaciones(k).estado = IIf(aRow("ESTADO").Equals(System.DBNull.Value), "", aRow("ESTADO"))
                        'ficAlSanitaria.anillos(i).subexplotaciones(k).fechaEstado = IIf(aRow("SE_FC_EST").Equals(System.DBNull.Value), "", aRow("SE_FC_EST"))
                        ficAlSanitaria.anillos(i).subexplotaciones(k).autoConsumo = IIf(aRow("SE_AUTOC").Equals("S"), True, False)
                        ficAlSanitaria.anillos(i).subexplotaciones(k).especie = IIf(aRow("ESPECIE").Equals(System.DBNull.Value), "", aRow("ESPECIE"))
                        ficAlSanitaria.anillos(i).subexplotaciones(k).idEspecie = IIf(aRow("SE_CO_ESPECIE").Equals(System.DBNull.Value), "", aRow("SE_CO_ESPECIE"))
                        'ficAlSanitaria.anillos(i).subexplotaciones(k).longitud = IIf(aRow("UB_LON").Equals(System.DBNull.Value), 0, aRow("UB_LON"))
                        'ficAlSanitaria.anillos(i).subexplotaciones(k).latitud = IIf(aRow("UB_LAT").Equals(System.DBNull.Value), 0, aRow("UB_LAT"))
                        ficAlSanitaria.anillos(i).subexplotaciones(k).provincia = IIf(aRow("PROVINCIA").Equals(System.DBNull.Value), "", aRow("PROVINCIA"))
                        ficAlSanitaria.anillos(i).subexplotaciones(k).municipio = IIf(aRow("MUNICIPIO").Equals(System.DBNull.Value), "", aRow("MUNICIPIO"))
                        ficAlSanitaria.anillos(i).subexplotaciones(k).comarca = IIf(aRow("COMARCA_SGSA_NOM").Equals(System.DBNull.Value), "", aRow("COMARCA_SGSA_NOM"))
                        ficAlSanitaria.anillos(i).subexplotaciones(k).censo = IIf(aRow("CENSO").Equals(System.DBNull.Value), 0, aRow("CENSO"))

                        k += 1
                    Next
                End If
            Next

            Return ficAlSanitaria


        Else
            'La selecci�n de elementos la realizamos desde la base de datos
            'Obtenemos los radios y hacemos los c�rculos conc�ntricos de las afecciones
            'Generamos la sentencia con las especies
            strEspecies = " INSTR( "
            For i = 0 To aAlert.especies.Length - 1
                strEspecies += "'" & aAlert.especies(i).Id & "'"
                If (i = aAlert.especies.Length - 1) Then
                    'agm 23/03/2012 Migraci�n Ganader�a REGA
                    strEspecies += ",SE_CO_ESPECIE)>0"
                    'strEspecies += ",SE_CO_EXPECIE)>0"
                Else
                    strEspecies += " || '-' || "
                End If
            Next

            'agm 23/03/2012 Migraci�n Ganader�a REGA
            'strQuery1 = "SELECT DISTINCT(SE_CO_SUBEXPLOT),SE_CE_EXPLOT,SE_DIREC,SE_CP,ESTADO,SE_FC_EST,SE_AUTOC,ESPECIE,SE_CO_ESPECIE,UB_LON,UB_LAT,PROV_NOM,MUN_NOM,COM_SGSA_NOM" & _
            '                       " from DGG.REGA_MV_UBICACION_ESPECIES WHERE " & strEspecies

            'agm 04/06/2012 Inclusi�n CENSO
            strQuery1 = "SELECT DISTINCT(SE_CO_SUBEXPLOT),SE_CE_EXPLOT,SE_DIREC,SE_CP,ESTADO,SE_FC_EST,SE_AUTOC,ESPECIE,SE_CO_ESPECIE,UB_LON,UB_LAT,PROV_NOM,MUN_NOM,COM_SGSA_NOM" & _
                " , SUM(COALESCE(CNV.CN_CENSO,0) )  + SUM(COALESCE(CVAC.CN_MACHOS_12M,0)) + SUM(COALESCE(CVAC.CN_HEMBRAS_12M,0)) + " & _
                " SUM(COALESCE(CVAC.CN_MACHOS_12_24M,0)) + SUM(COALESCE(CVAC.CN_HEMBRAS_12_24M,0)) + SUM(COALESCE(CVAC.CN_MACHOS_24M,0)) + SUM(COALESCE(CVAC.CN_HEMBRAS_24M,0)) CENSO " & _
                " from DGG.REGA_MV_UBICACION_ESPECIES LEFT JOIN DGG.REGA_MV_T_CENSONOVAC CNV ON CNV.CN_CE_SUBEXP =  DGG.REGA_MV_UBICACION_ESPECIES.SE_CE_EXPLOT AND CNV.CN_CO_ESPECIE = DGG.REGA_MV_UBICACION_ESPECIES.SE_CO_ESPECIE " & _
                " LEFT JOIN DGG.REGA_MV_T_CENSOVAC CVAC ON CVAC.CN_CE_SUBEXP =  DGG.REGA_MV_UBICACION_ESPECIES.SE_CE_EXPLOT AND CVAC.CN_CO_ESPECIE = DGG.REGA_MV_UBICACION_ESPECIES.SE_CO_ESPECIE " & _
                "WHERE(" & strEspecies & ")"

            strQuery4 = " GROUP BY SE_CO_SUBEXPLOT,SE_CE_EXPLOT, SE_DIREC,SE_CP,ESTADO,SE_FC_EST,SE_AUTOC,ESPECIE,SE_CO_ESPECIE,UB_LON,UB_LAT,PROV_NOM,MUN_NOM,COM_SGSA_NOM"

            'strQuery1 = "SELECT DISTINCT(UB_CE_SUBE),SE_CE_EXPLOT,SE_DIREC,SE_CP,ESTADO,SE_FC_EST,SE_AUTOC,ESPECIE,SE_CO_EXPECIE,UB_LON,UB_LAT,PROV_NOM,MUN_NOM,COM_SGSA_NOM" & _
            '                        " from DGG.VUB_MV_UBICACION_ESPECIES WHERE " & strEspecies
            'ReDim colIdExplotaciones(aAlert.radios.Length - 1)
            For i = 0 To aAlert.radios.Length - 1
                ficAlSanitaria.anillos(i) = New Anillo()
                ficAlSanitaria.anillos(i).longitud = aAlert.radios(i).longitud
                ReDim ficAlSanitaria.anillos(i).subexplotaciones(0)
                Dim z As Integer
                Dim strDistancia As String = ""

                strQueryExcepciones = ""
                strQueryExcepciones2 = ""
                If Not (aAlert.radios(i).excepciones.comarcas Is Nothing) Then
                    If (aAlert.radios(i).excepciones.comarcas.Length) Then
                        strQueryExcepciones += " AND COMARCA_SGSA NOT IN("
                        For z = 0 To aAlert.radios(i).excepciones.comarcas.Length - 1
                            strQueryExcepciones += "'" + aAlert.radios(i).excepciones.comarcas(z).Id.ToString + "'"
                            If (z = aAlert.radios(i).excepciones.comarcas.Length - 1) Then
                                strQueryExcepciones += ") "
                            Else
                                strQueryExcepciones += ","
                            End If
                        Next
                    End If
                End If
                If Not (aAlert.radios(i).excepciones.municipios Is Nothing) Then
                    If (aAlert.radios(i).excepciones.municipios.Length) Then
                        strQueryExcepciones += " AND SE_MUNPROV NOT IN("
                        For z = 0 To aAlert.radios(i).excepciones.municipios.Length - 1
                            strQueryExcepciones += "'" + aAlert.radios(i).excepciones.municipios(z).Id.ToString + "'"
                            If (z = aAlert.radios(i).excepciones.municipios.Length - 1) Then
                                strQueryExcepciones += ") "
                            Else
                                strQueryExcepciones += ","
                            End If
                        Next
                    End If
                End If
                If Not (aAlert.radios(i).excepciones.subexplotaciones Is Nothing) Then
                    If (aAlert.radios(i).excepciones.subexplotaciones.Length) Then
                        'agm 23/03/2012 Migraci�n Ganader�a REGA
                        strQueryExcepciones += " AND SE_CO_SUBEXPLOT NOT IN("
                        'strQueryExcepciones += " AND SE_CLAVE NOT IN("
                        For z = 0 To aAlert.radios(i).excepciones.subexplotaciones.Length - 1
                            strQueryExcepciones += "'" + aAlert.radios(i).excepciones.subexplotaciones(z).id.ToString + "'"
                            If (z = aAlert.radios(i).excepciones.subexplotaciones.Length - 1) Then
                                strQueryExcepciones += ") "
                            Else
                                strQueryExcepciones += ","
                            End If
                        Next
                    End If
                End If
                If (i > 0) Then
                    If Not (aAlert.radios(i - 1).excepciones.comarcas Is Nothing) Then
                        If (aAlert.radios(i - 1).excepciones.comarcas.Length) Then
                            strQueryExcepciones2 += " OR (COMARCA_SGSA IN("
                            For z = 0 To aAlert.radios(i - 1).excepciones.comarcas.Length - 1
                                strQueryExcepciones2 += "'" + aAlert.radios(i - 1).excepciones.comarcas(z).Id.ToString + "'"
                                If (z = aAlert.radios(i - 1).excepciones.comarcas.Length - 1) Then
                                    strQueryExcepciones2 += ") "
                                Else
                                    strQueryExcepciones2 += ","
                                End If
                            Next
                        End If
                    End If
                    If Not (aAlert.radios(i - 1).excepciones.municipios Is Nothing) Then
                        If (aAlert.radios(i - 1).excepciones.municipios.Length) Then
                            strQueryExcepciones2 += " OR (SE_MUNPROV IN("
                            For z = 0 To aAlert.radios(i - 1).excepciones.municipios.Length - 1
                                strQueryExcepciones2 += "'" + aAlert.radios(i - 1).excepciones.municipios(z).Id.ToString + "'"
                                If (z = aAlert.radios(i - 1).excepciones.municipios.Length - 1) Then
                                    strQueryExcepciones2 += ") "
                                Else
                                    strQueryExcepciones2 += ","
                                End If
                            Next
                        End If
                    End If
                    If Not (aAlert.radios(i - 1).excepciones.subexplotaciones Is Nothing) Then
                        If (aAlert.radios(i - 1).excepciones.subexplotaciones.Length) Then
                            'agm 23/03/2012 Migraci�n Ganader�a REGA
                            strQueryExcepciones2 += " OR (SE_CO_SUBEXPLOT IN("
                            'strQueryExcepciones2 += " OR (SE_CLAVE IN("
                            For z = 0 To aAlert.radios(i - 1).excepciones.subexplotaciones.Length - 1
                                strQueryExcepciones2 += "'" + aAlert.radios(i - 1).excepciones.subexplotaciones(z).id.ToString + "'"
                                If (z = aAlert.radios(i - 1).excepciones.subexplotaciones.Length - 1) Then
                                    strQueryExcepciones2 += ") "
                                Else
                                    strQueryExcepciones2 += ","
                                End If
                            Next
                        End If
                    End If
                End If
                If (strQueryExcepciones2 <> "") Then
                    strQuery3 = strQueryExcepciones2 + " AND ( "
                Else
                    strQuery3 = ""
                End If
                strQuery2 = strQueryExcepciones + " AND ( "
                primera = True

                Dim strExplotaciones As String
                strExplotaciones = ""

                For j = 0 To aAlert.puntos.Length - 1
                    If Not (colIdFocos Is Nothing) Then
                        Dim l As Integer

                        seguir = False
                        For l = 0 To colIdFocos.Length - 1
                            If aAlert.puntos(j).id = colIdFocos(l) Then
                                seguir = True
                            End If
                        Next
                    Else
                        seguir = True
                    End If
                    If seguir Then
                        'Obtenemos si es un punto con latitud y longitud o es una subexplotacion
                        'Tenemos que eliminar de la consulta nuestra subexplotaci�n(es)

                        If (aAlert.puntos(j).idExplotacion <> "") Then
                            'agm 23/03/2012 Migraci�n Ganader�a REGA
                            strExplotaciones += " AND (SE_CO_SUBEXPLOT<>'" & aAlert.puntos(j).idExplotacion & "')"
                            'strExplotaciones += " AND (UB_CE_SUBE<>'" & aAlert.puntos(j).idExplotacion & "')"
                            strTableName = "SA_REGA_UBICACION"
                            strDatasetName = "SA_REGA_UBICACION"
                            'agm 23/03/2012 Migraci�n Ganader�a REGA
                            strQuery = "SELECT UB_LAT,UB_LON FROM DGG.SA_REGA_UBICACION WHERE UB_CO_SUBEXPLOT='" & aAlert.puntos(j).idExplotacion & "'"
                            'strQuery = "SELECT UB_LAT,UB_LON FROM DGG.T_UBICACION_GIS_REGA WHERE UB_CE_SUBE='" & aAlert.puntos(j).idExplotacion & "'"
                            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                            For Each aRow In aDS.Tables(0).Rows
                                aAlert.puntos(j).latitud = IIf(aRow("UB_LAT").Equals(System.DBNull.Value), "", aRow("UB_LAT"))
                                aAlert.puntos(j).longitud = IIf(aRow("UB_LON").Equals(System.DBNull.Value), "", aRow("UB_LON"))
                            Next

                        End If
                        'Seleccionamos, para el punto determinado, por el �rea de seguridad

                        If (Not (primera)) Then
                            strQuery2 += " OR "
                            If (strQuery3 <> "") Then
                                strQuery3 += " OR "
                            End If
                        Else
                            primera = False
                        End If
                        strQuery2 += "( UB_LON BETWEEN ('" & aAlert.puntos(j).longitud & "' - ('" & aAlert.radios(i).longitud & "'/84.00022221)) and ('" & aAlert.puntos(j).longitud & "' + ('" & aAlert.radios(i).longitud & "'/84.00022221)) " & _
                                    " AND UB_LAT BETWEEN ('" & aAlert.puntos(j).latitud & "' - ('" & aAlert.radios(i).longitud & "'/111.302)) and ('" & aAlert.puntos(j).latitud & "' + ('" & aAlert.radios(i).longitud & "'/111.302)) " & _
                                    " and calc_distancia('" & aAlert.puntos(j).latitud & "','" & aAlert.puntos(j).longitud & "',UB_LAT,UB_LON) <='" & aAlert.radios(i).longitud & "' "
                        If (strQuery3 <> "") Then
                            strQuery3 += "( UB_LON BETWEEN ('" & aAlert.puntos(j).longitud & "' - ('" & aAlert.radios(i).longitud & "'/84.00022221)) and ('" & aAlert.puntos(j).longitud & "' + ('" & aAlert.radios(i).longitud & "'/84.00022221)) " & _
                                        " AND UB_LAT BETWEEN ('" & aAlert.puntos(j).latitud & "' - ('" & aAlert.radios(i).longitud & "'/111.302)) and ('" & aAlert.puntos(j).latitud & "' + ('" & aAlert.radios(i).longitud & "'/111.302)) " '& _
                            strDistancia += " and calc_distancia('" & aAlert.puntos(j).latitud & "','" & aAlert.puntos(j).longitud & "',UB_LAT,UB_LON) <='" & aAlert.radios(i).longitud & "' ) "
                        End If
                        If (i > 0) Then
                            'strQuery2 += " AND calc_distancia('" & aAlert.puntos(j).latitud & "','" & aAlert.puntos(j).longitud & "',UB_LAT,UB_LON) >'" & aAlert.radios(i - 1).longitud & "'"
                            strDistancia += " AND calc_distancia('" & aAlert.puntos(j).latitud & "','" & aAlert.puntos(j).longitud & "',UB_LAT,UB_LON) >'" & aAlert.radios(i - 1).longitud & "'"
                        End If

                        strQuery2 += ")"
                    End If
                Next j
                strTableName = "VUB_UBICACION_ESPECIES"
                strDatasetName = "VUB_UBICACION_ESPECIES"
                If (strQuery3 <> "") Then
                    strQuery3 += ")))"
                Else
                    strQuery2 += ")"
                End If
                Dim strTotal As String
                strTotal = strQuery1 + strQuery2 + strExplotaciones + strQuery3 + strDistancia + strQuery4
                aDS = ExecuteQuery(strTotal, strTableName, strDatasetName)

                'Debemos a�adir los elementos a la colecci�n de Id's de explotaciones
                'colIdExplotaciones(i) = New Collection()
                For Each aRow In aDS.Tables(0).Rows

                    'addId = True
                    'For k = 0 To i

                    ' If colIdExplotaciones(k).Contains(aRow("UB_CE_SUBE")) Then
                    'addId = False
                    'Exit For
                    'End If
                    'Next
                    'If (addId) Then
                    If (ficAlSanitaria.anillos(i).subexplotaciones Is Nothing) Then
                        ReDim Preserve ficAlSanitaria.anillos(i).subexplotaciones(0)
                    Else
                        ReDim Preserve ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length)
                    End If
                    'colIdExplotaciones(i).Add(aRow("UB_CE_SUBE"), aRow("UB_CE_SUBE"))
                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1) = New Subexplotacion()

                    'agm 23/03/2012 Migraci�n Ganader�a REGA
                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).id = IIf(aRow("SE_CO_SUBEXPLOT").Equals(System.DBNull.Value), "", aRow("SE_CO_SUBEXPLOT"))
                    'ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).id = IIf(aRow("UB_CE_SUBE").Equals(System.DBNull.Value), "", aRow("UB_CE_SUBE"))

                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).idExplotacion = IIf(aRow("SE_CE_EXPLOT").Equals(System.DBNull.Value), "", aRow("SE_CE_EXPLOT"))
                    'ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).direccion = IIf(aRow("SE_DIREC").Equals(System.DBNull.Value), "", aRow("SE_DIREC"))
                    'ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).codPostal = IIf(aRow("SE_CP").Equals(System.DBNull.Value), "", aRow("SE_CP"))
                    'ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).estado = IIf(aRow("ESTADO").Equals(System.DBNull.Value), "", aRow("ESTADO"))
                    'ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).fechaEstado = IIf(aRow("SE_FC_EST").Equals(System.DBNull.Value), "", aRow("SE_FC_EST"))
                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).autoConsumo = IIf(aRow("SE_AUTOC").Equals("S"), True, False)
                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).especie = IIf(aRow("ESPECIE").Equals(System.DBNull.Value), "", aRow("ESPECIE"))
                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).idEspecie = IIf(aRow("SE_CO_ESPECIE").Equals(System.DBNull.Value), "", aRow("SE_CO_ESPECIE"))
                    'ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).longitud = IIf(aRow("UB_LON").Equals(System.DBNull.Value), "", aRow("UB_LON"))
                    'ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).latitud = IIf(aRow("UB_LAT").Equals(System.DBNull.Value), "", aRow("UB_LAT"))
                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).provincia = IIf(aRow("PROV_NOM").Equals(System.DBNull.Value), "", aRow("PROV_NOM"))
                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).municipio = IIf(aRow("MUN_NOM").Equals(System.DBNull.Value), "", aRow("MUN_NOM"))
                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).comarca = IIf(aRow("COM_SGSA_NOM").Equals(System.DBNull.Value), "", aRow("COM_SGSA_NOM"))
                    ficAlSanitaria.anillos(i).subexplotaciones(ficAlSanitaria.anillos(i).subexplotaciones.Length - 1).censo = IIf(aRow("CENSO").Equals(System.DBNull.Value), 0, aRow("CENSO"))

                    'End If
                Next
            Next i

            Return ficAlSanitaria
        End If

    End Function


    <System.Web.Services.WebMethod(Description:="Edici�n de Tem�ticos existentes", EnableSession:=True)> _
    Public Function EdicionTematico(ByVal idTematico As Integer, ByVal nombre As String, ByVal privacidad As Integer, ByVal axlRender As String) As Boolean
        Dim strQuery As String
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim tablaTematico As String, campoTematico As String
        Dim xmlText As String, strEsquema As String
        Dim tipoTematico As String, campoJoin As String
        Dim aRow As System.Data.DataRow
        Try

            'strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")

            'strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            'strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            'strQuery = "SELECT TIT_DS_TABLA_TEMATICO,TIT_DS_CAMPO_TEMATICO FROM VIS_TIT_TIPO_TEMATICO WHERE TIT_CO_ID=" & tipoTematico

            'aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            'For Each aRow In aDS.Tables(0).Rows
            '    tablaTematico = aRow("TIT_DS_TABLA_TEMATICO")
            '    campoTematico = aRow("TIT_DS_CAMPO_TEMATICO")
            'Next
            'xmlText = "<SPATIALQUERY where=""" & tablaTematico & "." & campoTematico & "=" & strEsquema & "TEMATICO_" & idTematico & "." & campoJoin & """ jointables=""" & strEsquema & "TEMATICO_" & idTematico & """/>"
            'xmlText += vbNewLine + axlRender
            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "UPDATE VIS_TAD_TEMATICOSDEMANDA SET TAD_DS_NOMBRE_ANALISIS='" + nombre + "', TAD_DS_RESULTADO='" + axlRender + "',TPR_CO_ID=" + privacidad.ToString() + " WHERE TAD_CO_ID=" & idTematico
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            'Si el tem�tico es privado (tipo 2), s�lo podr� acceder la persona que lo ha generado.
            'Si es compartido (tipo 3), lo compartir� con los roles que ella posee
            'Si es p�blico (tipo 1), lo ver� todo el mundo
            Dim arrRoles As ArrayList
            Dim i As Integer
            If (privacidad <> 1) Then
                strTablename = "vis_tro_tematicorol"
                strDatasetName = "vis_tro_tematicorol"
                If (privacidad = 3) Then
                    arrRoles = Me.Session("Rol")
                    For i = 0 To arrRoles.Count - 1
                        strQuery = "INSERT INTO VIS_TRO_TEMATICOROL (TAD_CO_ID,ROL_CA_ID) " & _
                                   "VALUES('" & idTematico & "','" & arrRoles(i) & "') "
                        aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
                    Next i
                End If
            End If

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Lista de Archivos Disponibles", EnableSession:=True)> _
    Public Function ListaArchivos() As String()
        Dim strDirOrigen As String
        Dim arrRol As ArrayList
        Dim strRol As String
        Dim files() As String

        'ReDim files(0)
        Try
            If Me.Session("Rol") Is Nothing Then
                arrRol = New ArrayList()
                arrRol.Add("NO_ROL")
            Else
                arrRol = Me.Session("Rol")
            End If
            For Each strRol In arrRol
                If strRol = "Error" Then
                    'No est� autentificado
                    Return Nothing
                ElseIf strRol = "NO_ROL" Then
                    strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
                Else
                    strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen") & "\" & strRol
                End If

                'Dim sFile As String()
                Dim strFile As String
                Dim i As Integer
                Dim filTemp As String()

                filTemp = GetPatternedFiles(strDirOrigen, New String() {"*.mdb", "*.xls"})
                If Not filTemp Is Nothing Then

                    'sFile = System.IO.Directory.GetFiles(strDirOrigen, "*.mdb")
                    ''For i = 0 To filTemp.Length - 1
                    ''    strFile = Path.GetFileName(filTemp(i))
                    ''    If strFile <> Nothing Then
                    ''        filTemp(i) = (strFile)
                    ''    End If
                    ''Next
                    Dim longitud
                    If files Is Nothing Then
                        longitud = 0
                        ReDim Preserve files(filTemp.Length - 1)
                    Else
                        longitud = files.Length
                        ReDim Preserve files(files.Length + filTemp.Length - 1)
                    End If
                    filTemp.CopyTo(files, longitud)

                End If
            Next
            Return files
        Catch ex As Exception
            Dim arr(0) As String
            arr(0) = ex.Message
            Return arr
        End Try
    End Function
    <System.Web.Services.WebMethod(Description:="Lista de Tablas Disponibles con Campos", EnableSession:=True)> _
    Public Function ListaTablasNueva(ByVal fichEntrada As String) As String()
        Dim schemaTable As New DataTable
        Dim workAdapter As New OleDbDataAdapter
        Dim workSet As New DataSet
        Dim conn As New OleDbConnection
        Dim i As Integer
        Dim x As Integer
        Dim charArray As Char() = {",", " "}
        Dim charArray2 As Char() = {"$"}
        Dim cmdString As String
        Dim cmdString2 As String
        Dim cmd As New OleDbCommand
        Dim tableName As String

        workSet.DataSetName = "excelData"


        Try
            Dim connString As String
            Dim sExtension As String
            Dim strDirOrigen As String
            strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
            sExtension = Path.GetExtension(fichEntrada)

            'A continuaci�n debemos cargar los datos del tem�tico en una tabla Oracle
            'Para ello abrimos una conexi�n con el fichero excel (utilizamos para ello ADO)
            'If (sExtension = ".xls") Then
            '    connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
            '                     strDirOrigen & fichEntrada & ";Extended Properties=Excel 8.0;"

            If sExtension <> ".mdb" Then
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strDirOrigen & fichEntrada & ";" & _
                "Extended Properties=""Excel 12.0 Xml;HDR=YES"";"
            ElseIf (sExtension = ".mdb") Then
                connString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & strDirOrigen & fichEntrada
            Else
                Return Nothing
            End If
            ' Set the connection string.
            ' Open the connection.
            conn.ConnectionString = connString
            conn.Open()

            ' Populate the DataTable with schema
            ' information on the data source tables.
            schemaTable = _
                conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, _
                New Object() {Nothing, Nothing, Nothing, "TABLE"})

            ' Populate an array with the table names.
            i = schemaTable.Rows.Count - 1
            Dim tablesArray(i) As String
            For i = 0 To schemaTable.Rows.Count - 1

                tablesArray(i) = schemaTable.Rows(i).Item("Table_Name")

            Next

            ' Clear the DataTable
            schemaTable.Clear()

            conn.Close()
            Return tablesArray

        Catch ex As Exception
            'Error handling
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Lista de Tablas Disponibles", EnableSession:=True)> _
    Public Function ListaTablas(ByVal fichEntrada As String) As TablaTematico()

        Dim schemaTable As New DataTable, schemaTable2 As DataTable
        Dim workAdapter As New OleDbDataAdapter
        Dim workSet As New DataSet
        Dim conn As New OleDbConnection
        Dim i As Integer, j As Integer
        Dim x As Integer
        Dim charArray As Char() = {",", " "}
        Dim charArray2 As Char() = {"$"}
        Dim cmdString As String
        Dim cmdString2 As String
        Dim cmd As New OleDbCommand
        Dim tableName As String

        workSet.DataSetName = "excelData"


        Try
            Dim connString As String
            Dim sExtension As String
            Dim strDirOrigen As String
            strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
            sExtension = Path.GetExtension(fichEntrada)

            'A continuaci�n debemos cargar los datos del tem�tico en una tabla Oracle
            'Para ello abrimos una conexi�n con el fichero excel (utilizamos para ello ADO)
            'If (sExtension = ".xls") Then
            '    connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
            '                     strDirOrigen & fichEntrada & ";Extended Properties=Excel 8.0;"
            If sExtension <> ".mdb" Then
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strDirOrigen & fichEntrada & ";" & _
                "Extended Properties=""Excel 12.0 Xml;HDR=YES"";"

                'If sExtension <> ".mdb" Then
                '    connString = "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DBQ=" & strDirOrigen & fichEntrada & ";" & _
                '    "Extended Properties=""Excel 12.0 Xml;HDR=YES"";"
            ElseIf (sExtension = ".mdb") Then
                connString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & strDirOrigen & fichEntrada
            Else
                Return Nothing
            End If
            ' Set the connection string.
            ' Open the connection.
            conn.ConnectionString = connString
            conn.Open()

            ' Populate the DataTable with schema
            ' information on the data source tables.
            schemaTable = _
                conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, _
                New Object() {Nothing, Nothing, Nothing, "TABLE"})

            ' Populate an array with the table names.
            i = schemaTable.Rows.Count - 1
            Dim tablas(i) As TablaTematico
            For i = 0 To schemaTable.Rows.Count - 1
                tablas(i) = New TablaTematico
                tablas(i).nombre = schemaTable.Rows(i).Item("Table_Name")
                schemaTable2 = _
                                    conn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, _
                                    New Object() {Nothing, Nothing, _
                                    tablas(i).nombre, Nothing})
                j = schemaTable2.Rows.Count - 1
                ReDim tablas(i).campos(j)

                For x = 0 To schemaTable2.Rows.Count - 1
                    tablas(i).campos(x) = New CampoTematico
                    tablas(i).campos(x).nombre = schemaTable2.Rows(x).Item("Column_Name")


                    Select Case schemaTable2.Rows(x).Item("DATA_TYPE")
                        Case System.Data.OleDb.OleDbType.BigInt, System.Data.OleDb.OleDbType.Integer, System.Data.OleDb.OleDbType.Numeric, _
                             System.Data.OleDb.OleDbType.Single, System.Data.OleDb.OleDbType.SmallInt, System.Data.OleDb.OleDbType.TinyInt, System.Data.OleDb.OleDbType.UnsignedBigInt, _
                             System.Data.OleDb.OleDbType.UnsignedInt, System.Data.OleDb.OleDbType.UnsignedSmallInt, System.Data.OleDb.OleDbType.UnsignedTinyInt, System.Data.OleDb.OleDbType.VarNumeric, _
                             System.Data.OleDb.OleDbType.Decimal, System.Data.OleDb.OleDbType.Double
                            tablas(i).campos(x).tipo = "Numerico" 'CampoTematico.tipoCampo.Numerico

                        Case System.Data.OleDb.OleDbType.BSTR, System.Data.OleDb.OleDbType.Char, System.Data.OleDb.OleDbType.LongVarChar, System.Data.OleDb.OleDbType.LongVarWChar, _
                             System.Data.OleDb.OleDbType.VarChar, System.Data.OleDb.OleDbType.VarWChar, System.Data.OleDb.OleDbType.WChar
                            tablas(i).campos(x).tipo = "Caracter" 'CampoTematico.tipoCampo.Caracter

                        Case System.Data.OleDb.OleDbType.Date, System.Data.OleDb.OleDbType.DBDate, System.Data.OleDb.OleDbType.DBTime, System.Data.OleDb.OleDbType.DBTimeStamp
                            tablas(i).campos(x).tipo = "Fecha" 'CampoTematico.tipoCampo.Fecha
                        Case System.Data.OleDb.OleDbType.Boolean
                            tablas(i).campos(x).tipo = "Booleano" 'CampoTematico.tipoCampo.Booleano
                        Case Else
                            tablas(i).campos(x).tipo = "Otros" 'CampoTematico.tipoCampo.Otros
                    End Select
                Next

                ' Clear the DataTable
                schemaTable2.Clear()

            Next

            ' Clear the DataTable
            schemaTable.Clear()

            conn.Close()
            Return tablas

        Catch ex As Exception
            'Error handling
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Lista de Campos Disponibles", EnableSession:=True)> _
    Public Function ListaCampos(ByVal fichEntrada As String, ByVal nombreTabla As String) As String()

        Dim schemaTable As New DataTable
        Dim workAdapter As New OleDbDataAdapter
        Dim workSet As New DataSet
        Dim conn As New OleDbConnection
        Dim i As Integer
        Dim x As Integer
        Dim charArray As Char() = {",", " "}
        Dim charArray2 As Char() = {"$"}
        Dim cmd As New OleDbCommand


        workSet.DataSetName = "excelData"


        Try
            Dim connString As String
            Dim sExtension As String
            Dim strDirOrigen As String
            strDirOrigen = System.Configuration.ConfigurationManager.AppSettings("DirOrigen")
            sExtension = Path.GetExtension(fichEntrada)
            'A continuaci�n debemos cargar los datos del tem�tico en una tabla Oracle
            'Para ello abrimos una conexi�n con el fichero excel (utilizamos para ello ADO)
            'If (sExtension = ".xls") Then
            '    connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
            '                    strDirOrigen & fichEntrada & ";Extended Properties=Excel 8.0;"
            If sExtension <> ".mdb" Then
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strDirOrigen & fichEntrada & ";" & _
                "Extended Properties=""Excel 12.0 Xml;HDR=YES"";"
            ElseIf (sExtension = ".mdb") Then
                connString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & strDirOrigen & fichEntrada
            Else
                Return Nothing
            End If
            ' Set the connection string.
            ' Open the connection.
            conn.ConnectionString = connString
            conn.Open()

            schemaTable = _
                    conn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, _
                    New Object() {Nothing, Nothing, _
                    nombreTabla, Nothing})

            ' Step through the column names and append
            ' them into a SELECT statement
            i = schemaTable.Rows.Count - 1
            Dim columnArray(i) As String
            For x = 0 To schemaTable.Rows.Count - 1
                columnArray(x) = schemaTable.Rows(x).Item("Column_Name")
            Next

            ' Clear the DataTable
            schemaTable.Clear()

            conn.Close()
            Return columnArray

        Catch ex As Exception
            'Error handling
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener CCAA", EnableSession:=True)> _
    Public Function DameCCAA_ET() As CCAA()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CCAA
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaComunidades_ET")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdComunidades_ET")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreComunidades_ET")
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New CCAA()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Sectores Caminos Naturales", EnableSession:=True)> _
    Public Function DameSectoresCN() As Sector()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Sector
        Try

            ReDim arrValores(6)
            Dim i As Integer

            Dim aValor As New Sector()
            aValor.Id = i + 1
            aValor.nombre = "Noroeste"
            arrValores(i) = aValor
            i += 1
            aValor = New Sector()
            aValor.Id = i + 1
            aValor.nombre = "Noreste"
            arrValores(i) = aValor
            i += 1
            aValor = New Sector()
            aValor.Id = i + 1
            aValor.nombre = "Este"
            arrValores(i) = aValor
            i += 1
            aValor = New Sector()
            aValor.Id = i + 1
            aValor.nombre = "Balear"
            arrValores(i) = aValor
            i += 1
            aValor = New Sector()
            aValor.Id = i + 1
            aValor.nombre = "Sur"
            arrValores(i) = aValor
            i += 1
            aValor = New Sector()
            aValor.Id = i + 1
            aValor.nombre = "Centro"
            arrValores(i) = aValor
            i += 1
            aValor = New Sector()
            aValor.Id = i + 1
            aValor.nombre = "Canario"
            arrValores(i) = aValor

            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener CCAA", EnableSession:=True)> _
    Public Function DameCCAA_CN() As CCAA()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CCAA
        Try
            
            strTabla = "DGDR.CN_TBMUNICIPIOS_AUX"
            strCampoId = "ccaa_cod"
            strCampoNombre = "ccaa_nom"
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New CCAA()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Tipos de Tem�tico Avanzado", EnableSession:=True)> _
    Public Function DameTiposTematicosAvanzados() As TipoTematicoAvanzado()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As TipoTematicoAvanzado
        Try

            strTabla = "VISOR.VIS_TTA_TIPO_TEMATICO_AV"
            strCampoId = "TTA_CO_ID"
            strCampoNombre = "TTA_DS_DESCRIPCION"
            strDatasetName = strTabla
            strQuery = "SELECT " + strCampoId + "," + strCampoNombre + " FROM " + strTabla + " ORDER BY(" + strCampoId + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName, "bbddComun")
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim elem As New TipoTematicoAvanzado()
                elem.Id = aRow(strCampoId)
                elem.nombre = aRow(strCampoNombre)
                arrValores(i) = elem
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Capas base para tem�ticos avanzados", EnableSession:=True)> _
    Public Function DameCapasTematicosAvanzados() As CapaTematicoAvanzado()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CapaTematicoAvanzado
        Try

            strTabla = "VISOR.VIS_CTA_CAPAS_TEMATICO_AV"
            strDatasetName = strTabla
            strQuery = "SELECT * FROM VIS_CTA_CAPAS_TEMATICO_AV CTA INNER JOIN VIS_WTA_WORKSPACES_TEMATICO_AV WTA ON WTA.WTA_CO_ID = CTA.WTA_CO_ID ORDER BY CTA_CO_ID"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName, "bbddComun")
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim elem As New CapaTematicoAvanzado()
                elem.Id = aRow("CTA_CO_ID")
                elem.descripcion = aRow("CTA_DS_DESCRIPCION")
                elem.WorkspaceName = aRow("WTA_DS_NOMBRE")
                elem.dataSource = aRow("CTA_DS_DATASOURCENAME")
                elem.campoClave = aRow("CTA_DS_CAMPOCLAVE")
                'elem.camposVisibles = IIf(IsDBNull(aRow("CTA_DS_VISIBLEFIELDS")), "", aRow("CTA_DS_VISIBLEFIELDS"))
                Dim permEtiq As String = IIf(IsDBNull(aRow("CTA_BOL_ALLOWLABELS")), "N", aRow("CTA_BOL_ALLOWLABELS"))
                elem.permitirEtiquetado = IIf(permEtiq = "N", False, True)
                arrValores(i) = elem
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Capas base para tem�ticos avanzados", EnableSession:=True)> _
    Public Function DameCamposCapaTematicosAvanzados(ByVal CodCapa As Integer) As Campo()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Campo
        Try

            strTabla = "VISOR.VIS_CPT_CAMPOS_TEMATICO_AV"
            strDatasetName = strTabla
            strQuery = "SELECT * FROM VIS_CPT_CAMPOS_TEMATICO_AV WHERE CTA_CO_ID = " & CodCapa & " ORDER BY CPT_CO_ID"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName, "bbddComun")
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim elem As New Campo()
                elem.nombre = aRow("CPT_DS_NOMBRE")
                elem.descripcion = aRow("CPT_DS_DESCRIPCION")
                Dim esClave As String = IIf(IsDBNull(aRow("CPT_BO_CLAVE")), "N", aRow("CPT_BO_CLAVE"))
                elem.clave = IIf(esClave = "N", False, True)
                arrValores(i) = elem
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener CCAA", EnableSession:=True)> _
    Public Function DameCCAA() As CCAA()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CCAA
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaComunidades")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdComunidades")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreComunidades")
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New CCAA()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function



    <System.Web.Services.WebMethod(Description:="Obtener CCAA", EnableSession:=True)> _
    Public Function DameCCAAGISPE() As CCAA()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CCAA
        Try
            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaCCAAGISPE")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdCCAAGISPE")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreCCAAGISPE")
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQueryGISPE(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New CCAA()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function



    <System.Web.Services.WebMethod(Description:="Obtener CCAA", EnableSession:=True)> _
    Public Function DameCCAASnczi() As CCAA()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CCAA
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaComunidadesSnczi")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdComunidadesSnczi")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreComunidadesSnczi")
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New CCAA()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener CCAA", EnableSession:=True)> _
    Public Function DameCCAARegadios() As CCAA()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CCAA
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaComunidadesRegadios")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdComunidadesRegadios")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreComunidadesRegadios")
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New CCAA()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener CCAA", EnableSession:=True)> _
    Public Function DameCCAAZonasInteresRiego() As CCAA()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CCAA
        Try

            strTabla = "DGDR.ZIN_CCA_CCAA"
            strCampoId = "COD_CCAA"
            strCampoNombre = "NOMBRE_CCAA"
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New CCAA()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener CCAA", EnableSession:=True)> _
    Public Function DameCCAAColectivosRiego() As CCAA()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CCAA
        Try

            strTabla = "DGDR.CR_ACA_AUX_CCAA"
            strCampoId = "COD_CCAA"
            strCampoNombre = "NOMBRE_CCAA"
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New CCAA()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener CCAA Gu�a Playas", EnableSession:=True)> _
    Public Function DameCCAAGPL() As CCAA()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As CCAA
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaComunidadesGPL")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdComunidadesGPL")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreComunidadesGPL")
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New CCAA()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener CCAA", EnableSession:=True)> _
    Public Function DameTipologiasPresas() As Tipologia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Tipologia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaTiposPresas")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdTiposPresas")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreTiposPresas")
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCCAA As New Tipologia()
                aCCAA.Id = aRow(strCampoId)
                aCCAA.nombre = aRow(strCampoNombre)
                arrValores(i) = aCCAA
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="DameTipologiasInfraestructurasGISPE", EnableSession:=True)> _
    Public Function DameTipologiasInfraestructurasGISPE() As Tipologia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Tipologia
        Try
            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaTipologiaInfraGISPE")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdTipologiaInfra")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("DescripcionTipologiaInfra")
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQueryGISPE(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aTipologia As New Tipologia()
                aTipologia.Id = aRow(strCampoId)
                aTipologia.nombre = aRow(strCampoNombre)
                arrValores(i) = aTipologia
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Obtener Familias Alimentacion", EnableSession:=False)> _
    Public Function DameFamiliasAlimentacion() As FamiliaAlimentacion()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As FamiliaAlimentacion
        Dim familiasValidas() As String = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "13", "14", "15", "16", "23", "17", "22"}
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaFamiliasAlimentacion")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdFamiliasAlimentacion")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreFamiliasAlimentacion")
            strDatasetName = strTabla
            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            'ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            ReDim arrValores(familiasValidas.Length - 1)
            Dim i As Integer
            Dim j As Integer

            For Each aRow In aDS.Tables(0).Rows
                For j = 0 To familiasValidas.Length - 1
                    Dim aFamilia As New FamiliaAlimentacion()
                    Dim familiaOk As Boolean = False
                    If aRow(strCampoId).ToString.Equals(familiasValidas(j)) Then
                        familiaOk = True
                    End If
                    If familiaOk Then
                        aFamilia.Id = aRow(strCampoId)
                        aFamilia.nombre = aRow(strCampoNombre)
                        arrValores(i) = aFamilia
                        i += 1
                    End If
                Next

            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias", EnableSession:=True)> _
    Public Function DameProvincias_CN(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = "DGDR.CN_TBMUNICIPIOS_AUX"
            strCampoIdCCAA = "ccaa_cod"
            strCampoId = "prov_cod"
            strCampoNombre = "prov_nom"
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + idCCAA + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias", EnableSession:=True)> _
    Public Function DameProvincias_Sector_CN(ByVal idSector As String) As Provincia()

        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia

        Try

            strTabla = "DGDR.CN_TBMUNICIPIOS_AUX"
            strCampoId = "prov_cod"
            strCampoNombre = "prov_nom"
            strDatasetName = strTabla

            strQuery = "select distinct muni.prov_cod prov_cod, aux.prov_nom prov_nom from " & _
                        "DGDR.CN_TBMUNICIPIOS_AUX aux " & _
                        "inner Join DGDR.CN_TBMUNICIPIOS muni on " & _
                        "AUX.INE_CODC = MUNI.IDINE_CODC " & _
                        "WHERE SUBSTRING(MUNI.IDENLACE FROM 1 FOR 2) = '0" & idSector & "' ORDER BY prov_nom ASC"


            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias", EnableSession:=True)> _
    Public Function DameProvincias_ET(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaProvincias_ET")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdProvincias_ET")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreProvincias_ET")
            strCampoIdCCAA = System.Configuration.ConfigurationManager.AppSettings("IdComunidades_ET")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + idCCAA + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Provincias", EnableSession:=True)> _
    Public Function DameProvincias(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaProvincias")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdProvincias")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreProvincias")
            strCampoIdCCAA = System.Configuration.ConfigurationManager.AppSettings("IdComunidades")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + idCCAA + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias", EnableSession:=True)> _
    Public Function DameProvinciasSnczi(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaProvinciasSnczi")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasSnczi")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreProvinciasSnczi")
            strCampoIdCCAA = System.Configuration.ConfigurationManager.AppSettings("IdCCAAProvSnczi")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + idCCAA + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias", EnableSession:=True)> _
    Public Function DameProvinciasRegadios(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaProvinciasRegadios")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasRegadios")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreProvinciasRegadios")
            strCampoIdCCAA = System.Configuration.ConfigurationManager.AppSettings("IdCCAAProvRegadios")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + Right("00" + idCCAA, 2) + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias", EnableSession:=True)> _
    Public Function DameProvinciasZonasInteresRiego(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = "DGDR.ZIN_PRV_PROVINCIA"
            strCampoId = "COD_PROV"
            strCampoNombre = "NOMBRE_PROV"
            strCampoIdCCAA = "COD_CCAA"
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + Right("00" + idCCAA, 2) + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias", EnableSession:=True)> _
    Public Function DameProvinciasColectivosRiego(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = "DGDR.CR_APR_AUX_PROVINCIA"
            strCampoId = "COD_PROVINCIA"
            strCampoNombre = "NOMBRE_PROVINCIA"
            strCampoIdCCAA = "COD_CCAA"
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + Right("00" + idCCAA, 2) + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias", EnableSession:=True)> _
    Public Function DameProvinciasGISPE(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaProvinciasGISPE")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasGISPE")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreProvinciasGISPE")
            strCampoIdCCAA = System.Configuration.ConfigurationManager.AppSettings("IdCCAAProvGISPE")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + idCCAA + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQueryGISPE(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias Gu�a de Playas", EnableSession:=True)> _
    Public Function DameProvinciasGPL(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaProvinciasGPL")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasGPL")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreProvinciasGPL")
            strCampoIdCCAA = System.Configuration.ConfigurationManager.AppSettings("IdCCAAProvGPL")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + idCCAA + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Provincias Piezometr�a", EnableSession:=True)> _
    Public Function DameProvinciasCCAA(ByVal idCCAA As String) As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoIdString As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaProvincias")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdProvincias")
            strCampoIdString = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasString")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreProvincias")
            strCampoIdCCAA = System.Configuration.ConfigurationManager.AppSettings("IdComunidades")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + "," + strCampoIdString + " FROM " + strTabla + " WHERE " + strCampoIdCCAA + "='" + idCCAA + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                aProv.IdString = aRow(strCampoIdString)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Provincias Piezometr�a", EnableSession:=True)> _
    Public Function DameProvinciasPMetria() As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoIdString As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaProvincias")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdProvincias")
            strCampoIdString = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasString")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreProvincias")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoIdString + "," + strCampoNombre + " FROM " + strTabla + " ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                aProv.IdString = aRow(strCampoIdString)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Provincias Sondeos", EnableSession:=True)> _
    Public Function DameProvinciasSondeos() As Provincia()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdCCAA As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Provincia
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaProvSondeos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdProvSondeos")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreProvSondeos")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aProv As New Provincia()
                aProv.Id = aRow(strCampoId)
                aProv.nombre = aRow(strCampoNombre)
                arrValores(i) = aProv
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Municipios", EnableSession:=True)> _
    Public Function DameMunicipios_ET(ByVal idProvincia As String) As Municipio()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Municipio
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaMunicipios_ET")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdMunicipios_ET")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipios_ET")
            strCampoIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvincias_ET")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdProvincia + "='" + idProvincia + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMun As New Municipio()
                aMun.Id = aRow(strCampoId)
                aMun.IdProv = idProvincia
                aMun.nombre = aRow(strCampoNombre)
                arrValores(i) = aMun
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Municipios", EnableSession:=True)> _
    Public Function DameMunicipios_CN(ByVal idProvincia As String) As Municipio()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Municipio
        Try

            strTabla = "DGDR.CN_TBMUNICIPIOS_AUX"
            strCampoId = "INE_CODC"
            strCampoNombre = "MUNI_NOM"
            strCampoIdProvincia = "PROV_COD"
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdProvincia + "='" + idProvincia + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMun As New Municipio()
                aMun.Id = aRow(strCampoId)
                aMun.IdProv = idProvincia
                aMun.nombre = aRow(strCampoNombre)
                arrValores(i) = aMun
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Municipios Gu�a de Playas", EnableSession:=True)> _
    Public Function DameMunicipiosGPL(ByVal idProvincia As String) As Municipio()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Municipio
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaMunicipiosGPL")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdMunicipiosGPL")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipiosGPL")
            strCampoIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasGPL")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdProvincia + "='" + idProvincia + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMun As New Municipio()
                aMun.Id = aRow(strCampoId)
                aMun.IdProv = idProvincia
                aMun.nombre = aRow(strCampoNombre)
                arrValores(i) = aMun
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Municipios", EnableSession:=True)> _
    Public Function DameMunicipios(ByVal idProvincia As String) As Municipio()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Municipio
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaMunicipios")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdMunicipios")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipios")
            strCampoIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvincias")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdProvincia + "='" + idProvincia + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMun As New Municipio()
                aMun.Id = aRow(strCampoId)
                aMun.IdProv = idProvincia
                aMun.nombre = aRow(strCampoNombre)
                arrValores(i) = aMun
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Municipios", EnableSession:=True)> _
    Public Function DameMunicipiosRegadios(ByVal idProvincia As String) As Municipio()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Municipio
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaMunicipiosRegadios")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdMunicipiosRegadios")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipiosRegadios")
            strCampoIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasRegadios")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdProvincia + "='" + Right("00" + idProvincia, 2) + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMun As New Municipio()
                aMun.Id = aRow(strCampoId)
                aMun.IdProv = idProvincia
                aMun.nombre = aRow(strCampoNombre)
                arrValores(i) = aMun
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Municipios", EnableSession:=True)> _
    Public Function DameMunicipiosZonasInteresRiego(ByVal idProvincia As String) As Municipio()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Municipio
        Try

            strTabla = "DGDR.ZIN_MUN_MUNICIPIO"
            strCampoId = "COD_INE"
            strCampoNombre = "NOMBRE_MUN"
            strCampoIdProvincia = "COD_PROV"
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdProvincia + "='" + Right("00" + idProvincia, 2) + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMun As New Municipio()
                aMun.Id = aRow(strCampoId)
                aMun.IdProv = idProvincia
                aMun.nombre = aRow(strCampoNombre)
                arrValores(i) = aMun
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Municipios", EnableSession:=True)> _
    Public Function DameMunicipiosColectivosRiego(ByVal idProvincia As String) As Municipio()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Municipio
        Try

            strTabla = "DGDR.CR_AMU_AUX_MUNICIPIO"
            strCampoId = "COD_INE"
            strCampoNombre = "NOMBRE_MUNICIPIO"
            strCampoIdProvincia = "COD_PROVINCIA"
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdProvincia + "='" + Right("00" + idProvincia, 2) + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMun As New Municipio()
                aMun.Id = aRow(strCampoId)
                aMun.IdProv = idProvincia
                aMun.nombre = aRow(strCampoNombre)
                arrValores(i) = aMun
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Municipios", EnableSession:=True)> _
    Public Function DameMunicipiosPMetria(ByVal idProvincia As String) As Municipio()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoIdString As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Municipio
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaMunicipios")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdMunicipios")
            strCampoIdString = System.Configuration.ConfigurationManager.AppSettings("IdMunicipiosString")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipios")
            strCampoIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvincias")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoIdString + "," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdProvincia + "='" + idProvincia + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMun As New Municipio()
                aMun.Id = aRow(strCampoId)
                aMun.IdProv = idProvincia
                aMun.IdString = aRow(strCampoIdString)
                ' aMun.IdProvString = idProvincia.
                aMun.nombre = aRow(strCampoNombre)
                arrValores(i) = aMun
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Municipios", EnableSession:=True)> _
    Public Function DameMunicipiosSondeos(ByVal idProvincia As Integer) As Municipio()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Municipio
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaMunicSondeos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdMunicSondeos")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreMunicSondeos")
            strCampoIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProviMunicSondeos")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE TO_NUMBER(" + strCampoIdProvincia + ",'99')='" + idProvincia.ToString() + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMun As New Municipio()
                aMun.Id = aRow(strCampoId)
                aMun.IdProv = idProvincia
                aMun.nombre = aRow(strCampoNombre)
                arrValores(i) = aMun
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Comarcas", EnableSession:=True)> _
    Public Function DameComarcas(ByVal idProvincia As String) As Comarca()
        Dim strQuery As String
        Dim strTabla As String, strTabla2 As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdProvincia As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Comarca
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaComarcas")
            'strTabla2 = System.Configuration.ConfigurationManager.AppSettings("TablaComarcas2")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdComarcas")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreComarcas")
            strCampoIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvComarcas")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdProvincia + "='" + Format(CInt(idProvincia), "0#") + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"
            '" UNION SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla2 + " WHERE " + strCampoIdProvincia + "='" + Format(CInt(idProvincia), "0#") + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aComarca As New Comarca()
                aComarca.Id = aRow(strCampoId)
                aComarca.IdProv = idProvincia
                aComarca.nombre = aRow(strCampoNombre)
                arrValores(i) = aComarca
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Comarcas", EnableSession:=True)> _
    Public Function DameComarcasVeterinarias(ByVal idProvincia As String) As Comarca()
        Dim strQuery As String
        Dim strTabla As String, strTabla2 As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Comarca
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaComarcasSanitarias")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdComarcasSanitarias")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreComarcasSanitarias")
            strPrefijo = System.Configuration.ConfigurationManager.AppSettings("PrefijoIDComarcasSanitarias")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoId + " LIKE '" + strPrefijo + Format(CInt(idProvincia), "0#") + "%' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0  ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aComarca As New Comarca()
                aComarca.Id = aRow(strCampoId)
                aComarca.IdProv = idProvincia
                aComarca.nombre = aRow(strCampoNombre)
                arrValores(i) = aComarca
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Cuencas Hidrogr�ficas ", EnableSession:=True)> _
    Public Function DameCuencas() As Cuenca()
        Dim strQuery As String
        Dim strTabla As String, strTabla2 As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Cuenca
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaCuencas")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdCuencas")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreCuencas")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCuenca As New Cuenca()
                aCuenca.Id = aRow(strCampoId)
                aCuenca.nombre = aRow(strCampoNombre)
                arrValores(i) = aCuenca
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Cuencas Hidrogr�ficas ", EnableSession:=True)> _
    Public Function DameCuencasSaih() As Cuenca()
        Dim strQuery As String
        Dim strTabla As String, strTablaEmb As String, strTablaPluv As String, strTablaCaud As String, strCampoNomDemar As String, strCampoDemar As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Cuenca
        Dim arrValoresAux As String

        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strTablaEmb = System.Configuration.ConfigurationManager.AppSettings("TablaEmbalsesSaih")
            strTablaPluv = System.Configuration.ConfigurationManager.AppSettings("TablaPluviometros")
            strTablaCaud = System.Configuration.ConfigurationManager.AppSettings("TablaCaudales")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdCuencasSaih")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreCuencasSaih")
            strCampoDemar = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strCampoNomDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strDatasetName = strTabla
            strQuery = "select DISTINCT " + strCampoDemar + " from (select DISTINCT (" + strCampoDemar + " ) from " + strTablaEmb + " a UNION SELECT DISTINCT (" + strCampoDemar + ")  from " + strTablaPluv + " b UNION SELECT DISTINCT (" + strCampoDemar + ")  from " + strTablaCaud + " c) d"
            'strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)

            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                ' Dim aCuenca As New Cuenca()
                ' aCuenca.Id = aRow(strCampoId)
                ' aCuenca.nombre = aRow(strCampoNombre)
                If (i > 0) Then
                    arrValoresAux += ","
                End If
                arrValoresAux += aRow(strCampoDemar).ToString
                i += 1
            Next

            strQuery = "select " + strCampoNomDemar + " , " + strCampoDemar + " from " + strTabla + " where " + strCampoDemar + " in (" + arrValoresAux + ") order by " + strCampoNomDemar
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim j As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCuenca As New Cuenca()
                aCuenca.Id = aRow(strCampoDemar)
                aCuenca.nombre = aRow(strCampoNomDemar)
                arrValores(j) = aCuenca
                j += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Cuencas Hidrogr�ficas Piez�metros", EnableSession:=True)> _
    Public Function DameCuencasPMetria() As Cuenca()
        Dim strQuery As String
        Dim strTabla As String, strTabla2 As String, strDatasetName As String, strCampoNombreDemar As String, strCampoIdDemar As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Cuenca
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaPiezometrosSolo")
            strTabla2 = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("idDemarPiezometros")
            ' strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreCuencasPMetria")
            strCampoIdDemar = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strCampoNombreDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")

            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoIdDemar + ")," + strCampoNombreDemar + " FROM " + strTabla2 + "," + strTabla + " WHERE " + strTabla + "." + strCampoId + " = " + strTabla2 + "." + strCampoIdDemar + " ORDER BY(" + strCampoNombreDemar + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCuenca As New Cuenca()
                aCuenca.Id = aRow(strCampoIdDemar)
                aCuenca.nombre = aRow(strCampoNombreDemar)
                arrValores(i) = aCuenca
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function




    <System.Web.Services.WebMethod(Description:="Obtener Cuencas Hidrogr�ficas Sondeos", EnableSession:=True)> _
    Public Function DameCuencasSondeos() As Cuenca()
        Dim strQuery As String
        Dim strTabla As String, strTabla2 As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Cuenca
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaCuencasSondeos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdCuencasSondeos")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreCuencasSondeos")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCuenca As New Cuenca()
                aCuenca.Id = aRow(strCampoId)
                aCuenca.nombre = aRow(strCampoNombre)
                arrValores(i) = aCuenca
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Cuencas Hidrogr�ficas Piez�metros", EnableSession:=True)> _
    Public Function DameUHidrogeologicas(ByVal codCuenca As String) As UHidrogeologica()
        Dim strQuery As String, strCampoCuenca As String, strCampoCodUH As String
        Dim strTabla As String, strTabla2 As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As UHidrogeologica
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaUHidrogeologicas")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdUHidrogeologicas")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreUHidrogeologicas")
            strCampoCuenca = System.Configuration.ConfigurationManager.AppSettings("IdCuencaUHidrogeologicas")
            strCampoCodUH = System.Configuration.ConfigurationManager.AppSettings("CodigoUnificadoUH")

            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + ", " + strCampoCuenca + " , " + strCampoCodUH + "  FROM " + strTabla + " WHERE " + strCampoCuenca + " = '" + codCuenca + "'"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aUH As New UHidrogeologica()
                aUH.Id = aRow(strCampoId)
                aUH.nombre = aRow(strCampoNombre)
                aUH.IdCuenca = aRow(strCampoCuenca)
                aUH.UH = aRow(strCampoCodUH)
                arrValores(i) = aUH
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Masas de Agua Piez�metros", EnableSession:=True)> _
    Public Function DameMasasAgua(ByVal codDemar As String) As MasaAgua()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCodDemar As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As MasaAgua
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaMasaAgua")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("codMasaAgua")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreMasaAgua")
            strCodDemar = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")


            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + "  FROM " + strTabla + " WHERE " + strCodDemar + " = '" + codDemar + "'" + " ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aMasa As New MasaAgua()
                aMasa.CodMSBTDM = aRow(strCampoId)
                aMasa.nombre = aRow(strCampoNombre)
                arrValores(i) = aMasa
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Cuencas Hidrogr�ficas Sondeos", EnableSession:=True)> _
    Public Function DameUHidrogeologicasSondeos(ByVal codCuenca As String) As UHidrogeologica()
        Dim strQuery As String, strCampoCuenca As String
        Dim strTabla As String, strTabla2 As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As UHidrogeologica
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaUHidrogeologicasSondeos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("CodUHidrogeologicasSondeos")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreUHidrogeologicasSondeos")
            strCampoCuenca = System.Configuration.ConfigurationManager.AppSettings("IdCuencaUHidrogeologicasSondeos")

            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + ", " + strCampoCuenca + "  FROM " + strTabla + " WHERE " + strCampoCuenca + " = '" + codCuenca + "'" + " ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aUH As New UHidrogeologica()
                aUH.Id = aRow(strCampoId)
                aUH.nombre = aRow(strCampoNombre)
                aUH.IdCuenca = aRow(strCampoCuenca)
                aUH.UH = aUH.Id
                arrValores(i) = aUH
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    ' <System.Web.Services.WebMethod(Description:="Obtencion de Servicios por descripci�n", EnableSession:=True)> _
    'Public Function obtenerDescripcionServicio(ByVal codIdioma As String, ByVal strDesc As String) As BusquedaAvanzadaServicios()

    '     Dim strTablename As String, strDatasetName As String
    '     Dim aDS As Data.DataSet
    '     Dim aRow As Data.DataRow
    '     Dim strQuery As String
    '     Dim arrValores() As BusquedaAvanzadaServicios

    '     Try

    '         strTablename = "VIS_SER_SERVICIOS"
    '         strDatasetName = "VIS_SER_SERVICIOS"

    '         'strQuery = "SELECT * FROM ( "
    '         'strQuery = strQuery + "SELECT DISTINCT "
    '         'strQuery = strQuery + "vss.SER_CO_ID, vss.SER_DS_ALIAS "
    '         'strQuery = strQuery + "FROM (SELECT DISTINCT TEM_CO_ID, "
    '         'strQuery = strQuery + "SYS_CONNECT_BY_PATH(TEM_DS_DESCRIPCION, '\') || '\' || SYS_CONNECT_BY_PATH(TEM_DS_DESCCOMPLETA, '\') descripcion "
    '         'strQuery = strQuery + "FROM VIS_TEM_TEMAS "
    '         'strQuery = strQuery + "START WITH TEM_CO_IDPADRE IS NULL "
    '         'strQuery = strQuery + "CONNECT BY TEM_CO_IDPADRE = PRIOR TEM_CO_ID ) vtt "
    '         'strQuery = strQuery + "INNER JOIN (VIS_SER_SERVICIOS vss left join VIS_SDS_DESC_SERVICIOS on vss.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '         'strQuery = strQuery + "and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') ON vtt.TEM_CO_ID = vss.TEM_CO_ID WHERE UPPER(descripcion) LIKE upper('%" + strDesc + "%') "
    '         'strQuery = strQuery + "UNION "
    '         'strQuery = strQuery + "SELECT DISTINCT "
    '         'strQuery = strQuery + "vss1.SER_CO_ID, vss1.SER_DS_ALIAS "
    '         'strQuery = strQuery + "FROM (VIS_SER_SERVICIOS vss1 left join VIS_SDS_DESC_SERVICIOS on vss1.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '         'strQuery = strQuery + "and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='es') "
    '         'strQuery = strQuery + "WHERE UPPER(vss1.SER_DS_DESCRIPCION) LIKE upper('%" + strDesc + "%') OR UPPER(vss1.SER_DS_ALIAS) LIKE upper('%" + strDesc + "%') "
    '         'strQuery = strQuery + ")"

    '         strQuery = "SELECT * FROM "
    '         strQuery = strQuery + " (SELECT DISTINCT "
    '         strQuery = strQuery + " vss.SER_CO_ID, vss.SER_DS_ALIAS "
    '         strQuery = strQuery + " FROM"
    '         strQuery = strQuery + " (SELECT * "
    '         strQuery = strQuery + " FROM"
    '         strQuery = strQuery + " (SELECT DISTINCT vistt.TEM_CO_ID, "
    '         strQuery = strQuery + " SYS_CONNECT_BY_PATH(vtdt.TEM_DS_DESCRIPCION, '\') descripcion "
    '         strQuery = strQuery + " FROM "
    '         strQuery = strQuery + " (VIS_TEM_TEMAS vistt LEFT JOIN VIS_TDS_DESC_TEMAS vtdt ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID AND vtdt.idi_ds_codigo='" + codIdioma + "')"
    '         strQuery = strQuery + " START WITH vistt.TEM_CO_IDPADRE IS NULL "
    '         strQuery = strQuery + " CONNECT BY vistt.TEM_CO_IDPADRE = PRIOR vistt.TEM_CO_ID)"
    '         strQuery = strQuery + " WHERE UPPER(descripcion) LIKE upper('%" + strDesc + "%')) vtt "
    '         strQuery = strQuery + " INNER JOIN (VIS_SER_SERVICIOS vss left join VIS_SDS_DESC_SERVICIOS on vss.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '         strQuery = strQuery + " and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') ON vtt.TEM_CO_ID = vss.TEM_CO_ID WHERE UPPER(descripcion) LIKE upper('%" + strDesc + "%') "
    '         strQuery = strQuery + " UNION "
    '         strQuery = strQuery + " SELECT DISTINCT "
    '         strQuery = strQuery + " vss1.SER_CO_ID, vss1.SER_DS_ALIAS "
    '         strQuery = strQuery + " FROM (VIS_SER_SERVICIOS vss1 left join VIS_SDS_DESC_SERVICIOS on vss1.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '         strQuery = strQuery + " and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') "
    '         strQuery = strQuery + " WHERE UPPER(vss1.SER_DS_DESCRIPCION) LIKE upper('%" + strDesc + "%') OR UPPER(vss1.SER_DS_ALIAS) LIKE upper('%" + strDesc + "%'))"



    '         aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)


    '         ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
    '         Dim i As Integer
    '         i = 0
    '         Dim bas As BusquedaAvanzadaServicios
    '         For Each aRow In aDS.Tables(0).Rows
    '             bas = New BusquedaAvanzadaServicios
    '             bas.codId = aRow("SER_CO_ID")
    '             bas.descAlias = aRow("SER_DS_ALIAS")
    '             arrValores(i) = bas
    '             i += 1
    '         Next
    '         Return arrValores



    '     Catch ex As Exception
    '         Return Nothing
    '     End Try
    ' End Function

    '<System.Web.Services.WebMethod(Description:="Obtencion de Servicios por descripci�n, tema y rol", EnableSession:=True)> _
    'Public Function obtenerDescripcionServicio(ByVal codIdioma As String, ByVal strDesc As String, ByVal strRolList As String _
    ', ByVal strTemas As String) As BusquedaAvanzadaServicios()

    '    Dim strTablename As String, strDatasetName As String
    '    Dim aDS As Data.DataSet
    '    Dim aRow As Data.DataRow
    '    Dim strQuery As String
    '    Dim arrValores() As BusquedaAvanzadaServicios
    '    Dim strSER_DS_DESCRIPCION As String = strDesc
    '    Dim strSER_DS_ALIAS As String = strDesc

    '    Try

    '        strTablename = "VIS_SER_SERVICIOS"
    '        strDatasetName = "VIS_SER_SERVICIOS"

    '        ''strQuery = "SELECT * FROM ( "
    '        ''strQuery = strQuery + "SELECT DISTINCT "
    '        ''strQuery = strQuery + "vss.SER_CO_ID, vss.SER_DS_ALIAS "
    '        ''strQuery = strQuery + "FROM (SELECT DISTINCT TEM_CO_ID, "
    '        ''strQuery = strQuery + "SYS_CONNECT_BY_PATH(TEM_DS_DESCRIPCION, '\') || '\' || SYS_CONNECT_BY_PATH(TEM_DS_DESCCOMPLETA, '\') descripcion "
    '        ''strQuery = strQuery + "FROM VIS_TEM_TEMAS "
    '        ''strQuery = strQuery + "START WITH TEM_CO_IDPADRE IS NULL "
    '        ''strQuery = strQuery + "CONNECT BY TEM_CO_IDPADRE = PRIOR TEM_CO_ID ) vtt "
    '        ''strQuery = strQuery + "INNER JOIN (VIS_SER_SERVICIOS vss left join VIS_SDS_DESC_SERVICIOS on vss.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '        ''strQuery = strQuery + "and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') ON vtt.TEM_CO_ID = vss.TEM_CO_ID WHERE UPPER(descripcion) LIKE upper('%" + strDesc + "%') "
    '        ''strQuery = strQuery + "UNION "
    '        ''strQuery = strQuery + "SELECT DISTINCT "
    '        ''strQuery = strQuery + "vss1.SER_CO_ID, vss1.SER_DS_ALIAS "
    '        ''strQuery = strQuery + "FROM (VIS_SER_SERVICIOS vss1 left join VIS_SDS_DESC_SERVICIOS on vss1.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '        ''strQuery = strQuery + "and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='es') "
    '        ''strQuery = strQuery + "WHERE UPPER(vss1.SER_DS_DESCRIPCION) LIKE upper('%" + strDesc + "%') OR UPPER(vss1.SER_DS_ALIAS) LIKE upper('%" + strDesc + "%') "
    '        ''strQuery = strQuery + ")"

    '        'strQuery = "SELECT DISTINCT SCA.SER_CO_ID, SCA.SER_DS_ALIAS FROM "
    '        'strQuery = strQuery + " (SELECT DISTINCT "
    '        'strQuery = strQuery + " vss.SER_CO_ID, vss.SER_DS_ALIAS "
    '        'strQuery = strQuery + " FROM"
    '        'strQuery = strQuery + " (SELECT * "
    '        'strQuery = strQuery + " FROM"
    '        'strQuery = strQuery + " (SELECT DISTINCT vistt.TEM_CO_ID, "
    '        'strQuery = strQuery + " SYS_CONNECT_BY_PATH(vtdt.TEM_DS_DESCRIPCION, '\') descripcion "
    '        'strQuery = strQuery + " FROM "
    '        'strQuery = strQuery + " (VIS_TEM_TEMAS vistt LEFT JOIN VIS_TDS_DESC_TEMAS vtdt ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID AND vtdt.idi_ds_codigo='" + codIdioma + "')"
    '        'strQuery = strQuery + " START WITH vistt.TEM_CO_IDPADRE IS NULL "
    '        'strQuery = strQuery + " CONNECT BY vistt.TEM_CO_IDPADRE = PRIOR vistt.TEM_CO_ID)"
    '        'strQuery = strQuery + " WHERE UPPER(descripcion) LIKE upper('%" + strDesc + "%')) vtt "
    '        'strQuery = strQuery + " INNER JOIN (VIS_SER_SERVICIOS vss left join VIS_SDS_DESC_SERVICIOS on vss.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '        'strQuery = strQuery + " and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') ON vtt.TEM_CO_ID = vss.TEM_CO_ID WHERE UPPER(descripcion) LIKE upper('%" + strDesc + "%') "
    '        'strQuery = strQuery + " UNION "
    '        'strQuery = strQuery + " SELECT DISTINCT "
    '        'strQuery = strQuery + " vss1.SER_CO_ID, vss1.SER_DS_ALIAS "
    '        'strQuery = strQuery + " FROM (VIS_SER_SERVICIOS vss1 left join VIS_SDS_DESC_SERVICIOS on vss1.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '        'strQuery = strQuery + " and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') "
    '        'strQuery = strQuery + " WHERE UPPER(vss1.SER_DS_DESCRIPCION) LIKE upper('%" + strDesc + "%') "
    '        'strQuery = strQuery + " OR UPPER(vss1.SER_DS_ALIAS) LIKE upper('%" + strDesc + "%')) SCA"


    '        'strQuery = strQuery + " INNER JOIN VIS_ROL_ROLES_SERVICIO vrrs"
    '        'strQuery = strQuery + " ON SCA.SER_CO_ID = vrrs.SER_CO_ID"

    '        'strQuery = strQuery + " INNER JOIN VIS_USR_USUARIOS_ROL vuur"
    '        'strQuery = strQuery + " ON vuur.ROL_CO_ID = vrrs.ROL_CO_ID "

    '        'strQuery = strQuery + " WHERE "
    '        'strQuery = strQuery + " (vuur.ROL_CO_ID=1 "
    '        'If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
    '        ' strQuery = strQuery + " OR vuur.ROL_CO_ID =" + strRolList
    '        'End If
    '        'strQuery = strQuery + ")"

    '        strQuery = "SELECT DISTINCT SCA.SER_CO_ID, SCA.SER_DS_ALIAS "
    '        strQuery = strQuery + " FROM "
    '        strQuery = strQuery + " ("
    '        strQuery = strQuery + " SELECT DISTINCT vss.SER_CO_ID, vss.SER_DS_ALIAS,vtt.TEM_CO_ID FROM "
    '        strQuery = strQuery + " ("
    '        strQuery = strQuery + " SELECT * FROM "
    '        strQuery = strQuery + " ("
    '        strQuery = strQuery + " SELECT DISTINCT vistt.TEM_CO_ID, SYS_CONNECT_BY_PATH(vtdt.TEM_DS_DESCRIPCION, '\') descripcion "
    '        strQuery = strQuery + " FROM ("
    '        strQuery = strQuery + " VIS_TEM_TEMAS vistt LEFT JOIN VIS_TDS_DESC_TEMAS vtdt "
    '        strQuery = strQuery + " ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID "
    '        strQuery = strQuery + " AND UPPER(vtdt.idi_ds_codigo)=UPPER('" + codIdioma + "')) "
    '        strQuery = strQuery + " START WITH vistt.TEM_CO_IDPADRE "
    '        If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
    '            strQuery = strQuery + " IN ( " + strTemas + ")"
    '        Else
    '            strQuery = strQuery + " IS NULL "
    '        End If

    '        strQuery = strQuery + " CONNECT BY vistt.TEM_CO_IDPADRE = PRIOR vistt.TEM_CO_ID"
    '        strQuery = strQuery + " ) "
    '        strQuery = strQuery + " WHERE " + strDesc
    '        strQuery = strQuery + " ) vtt "
    '        strQuery = strQuery + " INNER JOIN (VIS_SER_SERVICIOS vss LEFT JOIN VIS_SDS_DESC_SERVICIOS "
    '        strQuery = strQuery + " ON vss.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '        strQuery = strQuery + " AND UPPER(VIS_SDS_DESC_SERVICIOS.idi_ds_codigo)=UPPER('" + codIdioma + "')) "
    '        strQuery = strQuery + " ON vtt.TEM_CO_ID = vss.TEM_CO_ID "
    '        strQuery = strQuery + " WHERE " + strDesc
    '        If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
    '            strQuery = strQuery + " AND vtt.TEM_CO_ID IN (" + strTemas + ")"
    '        End If
    '        strQuery = strQuery + " UNION "

    '        strQuery = strQuery + " SELECT DISTINCT vss1.SER_CO_ID, vss1.SER_DS_ALIAS,vtt1.TEM_CO_ID FROM "
    '        strQuery = strQuery + " ("
    '        strQuery = strQuery + " VIS_SER_SERVICIOS vss1 LEFT JOIN VIS_SDS_DESC_SERVICIOS "
    '        strQuery = strQuery + " ON vss1.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
    '        strQuery = strQuery + " And UPPER(VIS_SDS_DESC_SERVICIOS.idi_ds_codigo)=UPPER('" + codIdioma + "')"
    '        strQuery = strQuery + " INNER JOIN VIS_TEM_TEMAS vtt1 ON vtt1.TEM_CO_ID = vss1.TEM_CO_ID "
    '        strQuery = strQuery + " ) "
    '        strQuery = strQuery + " WHERE (" + strSER_DS_DESCRIPCION.Replace("descripcion", "vss1.SER_DS_DESCRIPCION")
    '        strQuery = strQuery + " OR " + strSER_DS_ALIAS.Replace("descripcion", "vss1.SER_DS_ALIAS") + ") "
    '        'strQuery = strQuery + " UPPER(vss1.SER_DS_DESCRIPCION) LIKE upper('%" + strDesc + "%') "
    '        'strQuery = strQuery + " OR UPPER(vss1.SER_DS_ALIAS) LIKE UPPER('%" + strDesc + "%')"
    '        If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
    '            strQuery = strQuery + " AND vtt1.TEM_CO_ID IN (" + strTemas + ")"
    '        End If

    '        strQuery = strQuery + " ) SCA "

    '        strQuery = strQuery + " INNER JOIN VIS_ROL_ROLES_SERVICIO vrrs"
    '        strQuery = strQuery + " ON SCA.SER_CO_ID = vrrs.SER_CO_ID"
    '        strQuery = strQuery + " INNER JOIN VIS_ROL_ROLES vrr "
    '        strQuery = strQuery + " ON vrrs.ROL_CO_ID = vrr.ROL_CO_ID"
    '        strQuery = strQuery + " WHERE UPPER(vrr.ROL_DS_NOMBRE) IN ("
    '        If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
    '            strQuery = strQuery + "UPPER('" + strRolList.Replace(",", "'),UPPER('") + "')"
    '        Else
    '            strQuery = strQuery + "'ALL'"
    '        End If
    '        strQuery = strQuery + ")"
    '        'strQuery = strQuery + " INNER JOIN VIS_ROL_ROLES_SERVICIO vrrs"
    '        'strQuery = strQuery + " ON SCA.SER_CO_ID = vrrs.SER_CO_ID"

    '        'strQuery = strQuery + " INNER JOIN VIS_USR_USUARIOS_ROL vuur"
    '        'strQuery = strQuery + " ON vuur.ROL_CO_ID = vrrs.ROL_CO_ID "

    '        'strQuery = strQuery + " WHERE "
    '        'strQuery = strQuery + " (vuur.ROL_CO_ID=1 "
    '        'If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
    '        ' strQuery = strQuery + " OR vuur.ROL_CO_ID =" + strRolList
    '        'End If
    '        'strQuery = strQuery + ")"

    '        aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)


    '        ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
    '        Dim i As Integer
    '        i = 0
    '        Dim bas As BusquedaAvanzadaServicios
    '        For Each aRow In aDS.Tables(0).Rows
    '            bas = New BusquedaAvanzadaServicios
    '            bas.codId = aRow("SER_CO_ID")
    '            bas.descAlias = aRow("SER_DS_ALIAS")
    '            arrValores(i) = bas
    '            i += 1
    '        Next
    '        Return arrValores



    '    Catch ex As Exception
    '        Return Nothing
    '    End Try
    'End Function



    '<System.Web.Services.WebMethod(Description:="Obtencion de Servicios por descripci�n, tema y rol", EnableSession:=True)> _
    'Public Function obtenerDescripcionServicio(ByVal codIdioma As String, ByVal strDesc As String, ByVal strRolList As String _
    ', ByVal strTemas As String, ByVal codVisor As String) As BusquedaAvanzadaServicios()

    '    Dim strTablename As String, strDatasetName As String
    '    Dim aDS As Data.DataSet
    '    Dim aRow As Data.DataRow
    '    Dim strQuery As String
    '    Dim arrValores() As BusquedaAvanzadaServicios
    '    Dim strSER_DS_DESCRIPCION As String = strDesc
    '    Dim strSER_DS_ALIAS As String = strDesc

    '    Try

    '        strTablename = "VIS_SER_SERVICIOS"
    '        strDatasetName = "VIS_SER_SERVICIOS"

    '        strQuery = "	SELECT DISTINCT ServiciosVisor.SER_CO_ID, ServiciosVisor.SER_DS_DESCRIPCION SER_DS_ALIAS 	FROM "
    '        strQuery = strQuery + " 	( "
    '        strQuery = strQuery + " 	  	SELECT TEM_CO_ID, descripcion,TEM_CO_IDPADRE FROM  "
    '        strQuery = strQuery + " 		( "
    '        strQuery = strQuery + " 		  SELECT DISTINCT vistt.TEM_CO_ID, SYS_CONNECT_BY_PATH( "
    '        strQuery = strQuery + " 		   ("
    '        strQuery = strQuery + " 		   CASE WHEN vtdt.TEM_DS_DESCRIPCION is NULL or TRIM(vistt.TEM_DS_DESCRIPCION) = '' "
    '        strQuery = strQuery + " 		   		THEN  vistt.TEM_DS_DESCRIPCION "
    '        strQuery = strQuery + " 		   ELSE vtdt.TEM_DS_DESCRIPCION END	 "
    '        strQuery = strQuery + " 		   ), '\') descripcion, vistt.TEM_CO_IDPADRE"
    '        strQuery = strQuery + " 		  FROM "
    '        strQuery = strQuery + " 		  ( "
    '        strQuery = strQuery + " 		  	VIS_TEM_TEMAS vistt LEFT JOIN VIS_TDS_DESC_TEMAS vtdt  ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID  "
    '        strQuery = strQuery + " 		       AND UPPER(vtdt.idi_ds_codigo)=UPPER('" + codIdioma + "')"
    '        strQuery = strQuery + " 		  ) "
    '        strQuery = strQuery + " 		  START WITH vistt.TEM_CO_IDPADRE IS NULL "
    '        strQuery = strQuery + " 		  CONNECT BY vistt.TEM_CO_IDPADRE = PRIOR vistt.TEM_CO_ID "
    '        strQuery = strQuery + " 	    )  "
    '        strQuery = strQuery + " 		WHERE " + strDesc
    '        strQuery = strQuery + " 	) TemasVisor "

    '        strQuery = strQuery + " 	INNER JOIN "

    '        strQuery = strQuery + " 	( "
    '        strQuery = strQuery + " 		SELECT DISTINCT vss.SER_CO_ID , "
    '        strQuery = strQuery + " 			   CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION "
    '        strQuery = strQuery + " 			      ELSE vsds.SER_DS_DESCRIPCION END SER_DS_DESCRIPCION, "
    '        strQuery = strQuery + " 			   vtts.TEMA_CO_ID  "

    '        strQuery = strQuery + " 		FROM VIS_ROL_ROLES_SERVICIO vrrs "
    '        strQuery = strQuery + " 			 INNER JOIN VIS_SER_SERVICIOS vss ON vrrs.SER_CO_ID = vss.SER_CO_ID "
    '        strQuery = strQuery + " 			 INNER JOIN VIS_TEMS_TEMAS_SERVICIO vtts ON vss.SER_CO_ID = vtts.SER_CO_ID "
    '        strQuery = strQuery + " 			 INNER JOIN VIS_VSR_VISORES_SERVICIO vvvs ON vss.SER_CO_ID = vvvs.SER_CO_ID  "
    '        strQuery = strQuery + " 			 LEFT JOIN VIS_SDS_DESC_SERVICIOS vsds ON vss.SER_CO_ID = vsds.SER_CO_ID "
    '        strQuery = strQuery + " 			     AND vsds.IDI_DS_CODIGO='" + codIdioma + "' "

    '        strQuery = strQuery + " 		WHERE (vvvs.VIS_CO_ID = '" + codVisor + "') "
    '        If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
    '            strQuery = strQuery + " AND vtts.TEMA_CO_ID IN (" + strTemas + ")"
    '        End If
    '        strQuery = strQuery + " AND UPPER(vrrs.ROL_DS_ROL_SERVICIO) IN ("
    '        If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
    '            strQuery = strQuery + "UPPER('" + strRolList.Replace(",", "'),UPPER('") + "')"
    '        Else
    '            strQuery = strQuery + "'ALL'"
    '        End If
    '        strQuery = strQuery + ") "
    '        strQuery = strQuery + " 	) ServiciosVisor "

    '        strQuery = strQuery + " 	ON ServiciosVisor.TEMA_CO_ID = TemasVisor.TEM_CO_ID "


    '        strQuery = strQuery + " 	UNION "

    '        strQuery = strQuery + " 		SELECT DISTINCT vss.SER_CO_ID , "
    '        strQuery = strQuery + " 			   CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION ELSE vsds.SER_DS_DESCRIPCION END SER_DS_DESCRIPCION"
    '        'strQuery = strQuery + " 			   ,vtts.TEMA_CO_ID  "

    '        strQuery = strQuery + " 		FROM VIS_ROL_ROLES_SERVICIO vrrs  "
    '        strQuery = strQuery + " 			 INNER JOIN VIS_SER_SERVICIOS vss ON vrrs.SER_CO_ID = vss.SER_CO_ID "
    '        strQuery = strQuery + " 			 INNER JOIN VIS_TEMS_TEMAS_SERVICIO vtts ON vss.SER_CO_ID = vtts.SER_CO_ID "
    '        strQuery = strQuery + " 			 INNER JOIN VIS_VSR_VISORES_SERVICIO vvvs ON vss.SER_CO_ID = vvvs.SER_CO_ID "
    '        strQuery = strQuery + " 			 LEFT JOIN VIS_SDS_DESC_SERVICIOS vsds ON vss.SER_CO_ID = vsds.SER_CO_ID AND vsds.IDI_DS_CODIGO='ES' "

    '        strQuery = strQuery + " 		WHERE (vvvs.VIS_CO_ID = '" + codVisor + "') "
    '        If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
    '            strQuery = strQuery + " AND vtts.TEMA_CO_ID IN (" + strTemas + ")"
    '        End If
    '        strQuery = strQuery + " AND UPPER(vrrs.ROL_DS_ROL_SERVICIO) IN ("
    '        If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
    '            strQuery = strQuery + "UPPER('" + strRolList.Replace(",", "'),UPPER('") + "')"
    '        Else
    '            strQuery = strQuery + "'ALL'"
    '        End If
    '        strQuery = strQuery + ") "

    '        'strQuery = strQuery + " 			  AND UPPER(CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION ELSE vsds.SER_DS_DESCRIPCION END) LIKE UPPER('%2006%') "                                                                       
    '        strQuery = strQuery + " 			  AND " + strSER_DS_DESCRIPCION.Replace("descripcion", "CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION ELSE vsds.SER_DS_DESCRIPCION END")


    '        aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)


    '        ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
    '        Dim i As Integer
    '        i = 0
    '        Dim bas As BusquedaAvanzadaServicios
    '        For Each aRow In aDS.Tables(0).Rows
    '            bas = New BusquedaAvanzadaServicios
    '            bas.codId = aRow("SER_CO_ID")
    '            bas.descAlias = aRow("SER_DS_ALIAS")
    '            arrValores(i) = bas
    '            i += 1
    '        Next
    '        Return arrValores



    '    Catch ex As Exception
    '        Return Nothing
    '    End Try
    'End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios por descripci�n, tema y rol", EnableSession:=True)> _
    Public Function obtenerDescripcionServicio(ByVal codIdioma As String, ByVal strDesc As String, ByVal strRolList As String _
    , ByVal strTemas As String, ByVal strLevels As String, ByVal codVisor As String) As BusquedaAvanzadaServicios()

        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim myElemento As BusquedaAvanzadaServicios
        Dim strQuery As String = ""
        Dim arrValores(0) As BusquedaAvanzadaServicios
        Dim strSER_DS_DESCRIPCION As String = strDesc
        Dim strSER_DS_ALIAS As String = strDesc
        Dim arrLevels As String() = strLevels.Split(",")

        Try

            strTablename = "VIS_SER_SERVICIOS"
            strDatasetName = "VIS_SER_SERVICIOS"

            '' ''strQuery = "SELECT * FROM ( "
            '' ''strQuery = strQuery + "SELECT DISTINCT "
            '' ''strQuery = strQuery + "vss.SER_CO_ID, vss.SER_DS_ALIAS "
            '' ''strQuery = strQuery + "FROM (SELECT DISTINCT TEM_CO_ID, "
            '' ''strQuery = strQuery + "SYS_CONNECT_BY_PATH(TEM_DS_DESCRIPCION, '\') || '\' || SYS_CONNECT_BY_PATH(TEM_DS_DESCCOMPLETA, '\') descripcion "
            '' ''strQuery = strQuery + "FROM VIS_TEM_TEMAS "
            '' ''strQuery = strQuery + "START WITH TEM_CO_IDPADRE IS NULL "
            '' ''strQuery = strQuery + "CONNECT BY TEM_CO_IDPADRE = PRIOR TEM_CO_ID ) vtt "
            '' ''strQuery = strQuery + "INNER JOIN (VIS_SER_SERVICIOS vss left join VIS_SDS_DESC_SERVICIOS on vss.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
            '' ''strQuery = strQuery + "and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') ON vtt.TEM_CO_ID = vss.TEM_CO_ID WHERE UPPER(descripcion) LIKE upper('%" + strDesc + "%') "
            '' ''strQuery = strQuery + "UNION "
            '' ''strQuery = strQuery + "SELECT DISTINCT "
            '' ''strQuery = strQuery + "vss1.SER_CO_ID, vss1.SER_DS_ALIAS "
            '' ''strQuery = strQuery + "FROM (VIS_SER_SERVICIOS vss1 left join VIS_SDS_DESC_SERVICIOS on vss1.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
            '' ''strQuery = strQuery + "and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='es') "
            '' ''strQuery = strQuery + "WHERE UPPER(vss1.SER_DS_DESCRIPCION) LIKE upper('%" + strDesc + "%') OR UPPER(vss1.SER_DS_ALIAS) LIKE upper('%" + strDesc + "%') "
            '' ''strQuery = strQuery + ")"

            ' ''strQuery = "SELECT DISTINCT SCA.SER_CO_ID, SCA.SER_DS_ALIAS FROM "
            ' ''strQuery = strQuery + " (SELECT DISTINCT "
            ' ''strQuery = strQuery + " vss.SER_CO_ID, vss.SER_DS_ALIAS "
            ' ''strQuery = strQuery + " FROM"
            ' ''strQuery = strQuery + " (SELECT * "
            ' ''strQuery = strQuery + " FROM"
            ' ''strQuery = strQuery + " (SELECT DISTINCT vistt.TEM_CO_ID, "
            ' ''strQuery = strQuery + " SYS_CONNECT_BY_PATH(vtdt.TEM_DS_DESCRIPCION, '\') descripcion "
            ' ''strQuery = strQuery + " FROM "
            ' ''strQuery = strQuery + " (VIS_TEM_TEMAS vistt LEFT JOIN VIS_TDS_DESC_TEMAS vtdt ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID AND vtdt.idi_ds_codigo='" + codIdioma + "')"
            ' ''strQuery = strQuery + " START WITH vistt.TEM_CO_IDPADRE IS NULL "
            ' ''strQuery = strQuery + " CONNECT BY vistt.TEM_CO_IDPADRE = PRIOR vistt.TEM_CO_ID)"
            ' ''strQuery = strQuery + " WHERE UPPER(descripcion) LIKE upper('%" + strDesc + "%')) vtt "
            ' ''strQuery = strQuery + " INNER JOIN (VIS_SER_SERVICIOS vss left join VIS_SDS_DESC_SERVICIOS on vss.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
            ' ''strQuery = strQuery + " and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') ON vtt.TEM_CO_ID = vss.TEM_CO_ID WHERE UPPER(descripcion) LIKE upper('%" + strDesc + "%') "
            ' ''strQuery = strQuery + " UNION "
            ' ''strQuery = strQuery + " SELECT DISTINCT "
            ' ''strQuery = strQuery + " vss1.SER_CO_ID, vss1.SER_DS_ALIAS "
            ' ''strQuery = strQuery + " FROM (VIS_SER_SERVICIOS vss1 left join VIS_SDS_DESC_SERVICIOS on vss1.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
            ' ''strQuery = strQuery + " and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') "
            ' ''strQuery = strQuery + " WHERE UPPER(vss1.SER_DS_DESCRIPCION) LIKE upper('%" + strDesc + "%') "
            ' ''strQuery = strQuery + " OR UPPER(vss1.SER_DS_ALIAS) LIKE upper('%" + strDesc + "%')) SCA"


            ' ''strQuery = strQuery + " INNER JOIN VIS_ROL_ROLES_SERVICIO vrrs"
            ' ''strQuery = strQuery + " ON SCA.SER_CO_ID = vrrs.SER_CO_ID"

            ' ''strQuery = strQuery + " INNER JOIN VIS_USR_USUARIOS_ROL vuur"
            ' ''strQuery = strQuery + " ON vuur.ROL_CO_ID = vrrs.ROL_CO_ID "

            ' ''strQuery = strQuery + " WHERE "
            ' ''strQuery = strQuery + " (vuur.ROL_CO_ID=1 "
            ' ''If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
            ' '' strQuery = strQuery + " OR vuur.ROL_CO_ID =" + strRolList
            ' ''End If
            ' ''strQuery = strQuery + ")"

            ''strQuery = "SELECT DISTINCT SCA.SER_CO_ID, SCA.SER_DS_ALIAS "
            ''strQuery = strQuery + " FROM "
            ''strQuery = strQuery + " ("
            ''strQuery = strQuery + " SELECT DISTINCT vss.SER_CO_ID, vss.SER_DS_ALIAS,vtt.TEM_CO_ID FROM "
            ''strQuery = strQuery + " ("
            ''strQuery = strQuery + " SELECT * FROM "
            ''strQuery = strQuery + " ("
            ''strQuery = strQuery + " SELECT DISTINCT vistt.TEM_CO_ID, SYS_CONNECT_BY_PATH(vtdt.TEM_DS_DESCRIPCION, '\') descripcion "
            ''strQuery = strQuery + " FROM ("
            ''strQuery = strQuery + " VIS_TEM_TEMAS vistt LEFT JOIN VIS_TDS_DESC_TEMAS vtdt "
            ''strQuery = strQuery + " ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID "
            ''strQuery = strQuery + " AND UPPER(vtdt.idi_ds_codigo)=UPPER('" + codIdioma + "')) "
            ''strQuery = strQuery + " START WITH vistt.TEM_CO_IDPADRE "
            ''If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
            ''    strQuery = strQuery + " IN ( " + strTemas + ")"
            ''Else
            ''    strQuery = strQuery + " IS NULL "
            ''End If

            ''strQuery = strQuery + " CONNECT BY vistt.TEM_CO_IDPADRE = PRIOR vistt.TEM_CO_ID"
            ''strQuery = strQuery + " ) "
            ''strQuery = strQuery + " WHERE " + strDesc
            ''strQuery = strQuery + " ) vtt "
            ''strQuery = strQuery + " INNER JOIN (VIS_SER_SERVICIOS vss LEFT JOIN VIS_SDS_DESC_SERVICIOS "
            ''strQuery = strQuery + " ON vss.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
            ''strQuery = strQuery + " AND UPPER(VIS_SDS_DESC_SERVICIOS.idi_ds_codigo)=UPPER('" + codIdioma + "')) "
            ''strQuery = strQuery + " ON vtt.TEM_CO_ID = vss.TEM_CO_ID "
            ''strQuery = strQuery + " WHERE " + strDesc
            ''If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
            ''    strQuery = strQuery + " AND vtt.TEM_CO_ID IN (" + strTemas + ")"
            ''End If
            ''strQuery = strQuery + " UNION "

            ''strQuery = strQuery + " SELECT DISTINCT vss1.SER_CO_ID, vss1.SER_DS_ALIAS,vtt1.TEM_CO_ID FROM "
            ''strQuery = strQuery + " ("
            ''strQuery = strQuery + " VIS_SER_SERVICIOS vss1 LEFT JOIN VIS_SDS_DESC_SERVICIOS "
            ''strQuery = strQuery + " ON vss1.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id "
            ''strQuery = strQuery + " And UPPER(VIS_SDS_DESC_SERVICIOS.idi_ds_codigo)=UPPER('" + codIdioma + "')"
            ''strQuery = strQuery + " INNER JOIN VIS_TEM_TEMAS vtt1 ON vtt1.TEM_CO_ID = vss1.TEM_CO_ID "
            ''strQuery = strQuery + " ) "
            ''strQuery = strQuery + " WHERE (" + strSER_DS_DESCRIPCION.Replace("descripcion", "vss1.SER_DS_DESCRIPCION")
            ''strQuery = strQuery + " OR " + strSER_DS_ALIAS.Replace("descripcion", "vss1.SER_DS_ALIAS") + ") "
            ' ''strQuery = strQuery + " UPPER(vss1.SER_DS_DESCRIPCION) LIKE upper('%" + strDesc + "%') "
            ' ''strQuery = strQuery + " OR UPPER(vss1.SER_DS_ALIAS) LIKE UPPER('%" + strDesc + "%')"
            ''If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
            ''    strQuery = strQuery + " AND vtt1.TEM_CO_ID IN (" + strTemas + ")"
            ''End If

            ''strQuery = strQuery + " ) SCA "

            ''strQuery = strQuery + " INNER JOIN VIS_ROL_ROLES_SERVICIO vrrs"
            ''strQuery = strQuery + " ON SCA.SER_CO_ID = vrrs.SER_CO_ID"
            ''strQuery = strQuery + " INNER JOIN VIS_ROL_ROLES vrr "
            ''strQuery = strQuery + " ON vrrs.ROL_CO_ID = vrr.ROL_CO_ID"
            ''strQuery = strQuery + " WHERE UPPER(vrr.ROL_DS_NOMBRE) IN ("
            ''If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
            ''    strQuery = strQuery + "UPPER('" + strRolList.Replace(",", "'),UPPER('") + "')"
            ''Else
            ''    strQuery = strQuery + "'ALL'"
            ''End If
            ''strQuery = strQuery + ")"
            ' ''strQuery = strQuery + " INNER JOIN VIS_ROL_ROLES_SERVICIO vrrs"
            ' ''strQuery = strQuery + " ON SCA.SER_CO_ID = vrrs.SER_CO_ID"

            ' ''strQuery = strQuery + " INNER JOIN VIS_USR_USUARIOS_ROL vuur"
            ' ''strQuery = strQuery + " ON vuur.ROL_CO_ID = vrrs.ROL_CO_ID "

            ' ''strQuery = strQuery + " WHERE "
            ' ''strQuery = strQuery + " (vuur.ROL_CO_ID=1 "
            ' ''If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
            ' '' strQuery = strQuery + " OR vuur.ROL_CO_ID =" + strRolList
            ' ''End If
            ' ''strQuery = strQuery + ")"

            'strQuery = "	SELECT DISTINCT ServiciosVisor.SER_CO_ID, ServiciosVisor.SER_DS_DESCRIPCION SER_DS_ALIAS 	FROM "
            'strQuery = strQuery + " 	( "
            'strQuery = strQuery + " 	  	SELECT TEM_CO_ID, descripcion,TEM_CO_IDPADRE FROM  "
            'strQuery = strQuery + " 		( "
            'strQuery = strQuery + " 		  SELECT DISTINCT vistt.TEM_CO_ID, SYS_CONNECT_BY_PATH( "
            'strQuery = strQuery + " 		   ("
            'strQuery = strQuery + " 		   CASE WHEN vtdt.TEM_DS_DESCRIPCION is NULL or TRIM(vistt.TEM_DS_DESCRIPCION) = '' "
            'strQuery = strQuery + " 		   		THEN  vistt.TEM_DS_DESCRIPCION "
            'strQuery = strQuery + " 		   ELSE vtdt.TEM_DS_DESCRIPCION END	 "
            'strQuery = strQuery + " 		   ), '\') descripcion, vistt.TEM_CO_IDPADRE"
            'strQuery = strQuery + " 		  FROM "
            'strQuery = strQuery + " 		  ( "
            'strQuery = strQuery + " 		  	VIS_TEM_TEMAS vistt LEFT JOIN VIS_TDS_DESC_TEMAS vtdt  ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID  "
            'strQuery = strQuery + " 		       AND UPPER(vtdt.idi_ds_codigo)=UPPER('" + codIdioma + "')"
            'strQuery = strQuery + " 		  ) "
            'strQuery = strQuery + " 		  START WITH vistt.TEM_CO_IDPADRE IS NULL "
            'strQuery = strQuery + " 		  CONNECT BY vistt.TEM_CO_IDPADRE = PRIOR vistt.TEM_CO_ID "
            'strQuery = strQuery + " 	    )  "
            'strQuery = strQuery + " 		WHERE " + strDesc
            'strQuery = strQuery + " 	) TemasVisor "

            'strQuery = strQuery + " 	INNER JOIN "

            'strQuery = strQuery + " 	( "
            'strQuery = strQuery + " 		SELECT DISTINCT vss.SER_CO_ID , "
            'strQuery = strQuery + " 			   CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION "
            'strQuery = strQuery + " 			      ELSE vsds.SER_DS_DESCRIPCION END SER_DS_DESCRIPCION, "
            'strQuery = strQuery + " 			   vtts.TEMA_CO_ID  "

            'strQuery = strQuery + " 		FROM VIS_ROL_ROLES_SERVICIO vrrs "
            'strQuery = strQuery + " 			 INNER JOIN VIS_SER_SERVICIOS vss ON vrrs.SER_CO_ID = vss.SER_CO_ID "
            'strQuery = strQuery + " 			 INNER JOIN VIS_TEMS_TEMAS_SERVICIO vtts ON vss.SER_CO_ID = vtts.SER_CO_ID "
            'strQuery = strQuery + " 			 INNER JOIN VIS_VSR_VISORES_SERVICIO vvvs ON vss.SER_CO_ID = vvvs.SER_CO_ID  "
            'strQuery = strQuery + " 			 LEFT JOIN VIS_SDS_DESC_SERVICIOS vsds ON vss.SER_CO_ID = vsds.SER_CO_ID "
            'strQuery = strQuery + " 			     AND vsds.IDI_DS_CODIGO='" + codIdioma + "' "

            'strQuery = strQuery + " 		WHERE (vvvs.VIS_CO_ID = '" + codVisor + "') "
            'If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
            '    strQuery = strQuery + " AND vtts.TEMA_CO_ID IN (" + strTemas + ")"
            'End If

            'strQuery = strQuery + " AND UPPER(vrrs.ROL_DS_ROL_SERVICIO) IN ("
            'If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
            '    strQuery = strQuery + "UPPER('" + strRolList.Replace(",", "'),UPPER('") + "')"
            'Else
            '    strQuery = strQuery + "'ALL'"
            'End If
            'strQuery = strQuery + ") "
            'strQuery = strQuery + " 	) ServiciosVisor "

            'strQuery = strQuery + " 	ON ServiciosVisor.TEMA_CO_ID = TemasVisor.TEM_CO_ID "


            'strQuery = strQuery + " 	UNION "

            'strQuery = strQuery + " 		SELECT DISTINCT vss.SER_CO_ID , "
            'strQuery = strQuery + " 			   CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION ELSE vsds.SER_DS_DESCRIPCION END SER_DS_DESCRIPCION"
            ''strQuery = strQuery + " 			   ,vtts.TEMA_CO_ID  "

            'strQuery = strQuery + " 		FROM VIS_ROL_ROLES_SERVICIO vrrs  "
            'strQuery = strQuery + " 			 INNER JOIN VIS_SER_SERVICIOS vss ON vrrs.SER_CO_ID = vss.SER_CO_ID "
            'strQuery = strQuery + " 			 INNER JOIN VIS_TEMS_TEMAS_SERVICIO vtts ON vss.SER_CO_ID = vtts.SER_CO_ID "
            'strQuery = strQuery + " 			 INNER JOIN VIS_VSR_VISORES_SERVICIO vvvs ON vss.SER_CO_ID = vvvs.SER_CO_ID "
            'strQuery = strQuery + " 			 LEFT JOIN VIS_SDS_DESC_SERVICIOS vsds ON vss.SER_CO_ID = vsds.SER_CO_ID AND vsds.IDI_DS_CODIGO='ES' "

            'strQuery = strQuery + " 		WHERE (vvvs.VIS_CO_ID = '" + codVisor + "') "
            'If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
            '    strQuery = strQuery + " AND vtts.TEMA_CO_ID IN (" + strTemas + ")"
            'End If
            'strQuery = strQuery + " AND UPPER(vrrs.ROL_DS_ROL_SERVICIO) IN ("
            'If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
            '    strQuery = strQuery + "UPPER('" + strRolList.Replace(",", "'),UPPER('") + "')"
            'Else
            '    strQuery = strQuery + "'ALL'"
            'End If
            'strQuery = strQuery + ") "

            ''strQuery = strQuery + " 			  AND UPPER(CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION ELSE vsds.SER_DS_DESCRIPCION END) LIKE UPPER('%2006%') "                                                                       
            'strQuery = strQuery + " 			  AND " + strSER_DS_DESCRIPCION.Replace("descripcion", "CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION ELSE vsds.SER_DS_DESCRIPCION END")


            'agm 17/09/2012: toma casta�a
            For j As Integer = 0 To arrLevels.Length - 1

                If j > 0 Then
                    strQuery = strQuery + " UNION "
                End If

                strQuery = strQuery + "SELECT DISTINCT ServiciosVisor.SER_CO_ID, ServiciosVisor.SER_DS_DESCRIPCION SER_DS_ALIAS , ServiciosVisor.SER_DS_INFO, ServiciosVisor.SEW_CO_UID "
                strQuery = strQuery + "FROM  	"
                strQuery = strQuery + "(  	  	"
                strQuery = strQuery + "		SELECT TEM_CO_ID, descripcion,TEM_CO_IDPADRE "
                strQuery = strQuery + "		FROM   		"
                strQuery = strQuery + "		(  		  "

                strQuery = strQuery + "With Recursive X (RootTema,TEM_CO_ID, TEM_CO_IDPADRE, descripcion, level) as "
                strQuery = strQuery + "( "
                strQuery = strQuery + "		select vistt.TEM_CO_ID, vistt.TEM_CO_ID, vistt.TEM_CO_IDPADRE, "
                strQuery = strQuery + "		CAST( "
                strQuery = strQuery + "		CASE WHEN vtdt.TEM_DS_DESCRIPCION is NULL or TRIM(vistt.TEM_DS_DESCRIPCION) = '' "
                strQuery = strQuery + "		THEN  vistt.TEM_DS_DESCRIPCION ELSE vtdt.TEM_DS_DESCRIPCION END "
                strQuery = strQuery + "		AS varchar(256)),"
                strQuery = strQuery + "		1 "
                strQuery = strQuery + "		from VIS_TEM_TEMAS "
                strQuery = strQuery + "		vistt "
                strQuery = strQuery + "		LEFT JOIN VIS_TDS_DESC_TEMAS vtdt  "
                strQuery = strQuery + "		ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID "
                strQuery = strQuery + "		AND UPPER(vtdt.idi_ds_codigo)=UPPER('es') "
                strQuery = strQuery + "		where vistt.TEM_CO_IDPADRE Is null "
                strQuery = strQuery + "		union all "
                strQuery = strQuery + "		select X.TEM_CO_ID, vistt.TEM_CO_ID, vistt.TEM_CO_IDPADRE, "
                strQuery = strQuery + "		CAST(X.descripcion || '\' || "
                strQuery = strQuery + "		CASE WHEN vistt.TEM_DS_DESCRIPCION is NULL or TRIM(vistt.TEM_DS_DESCRIPCION) = '' "
                strQuery = strQuery + "		THEN  vistt.TEM_DS_DESCRIPCION ELSE vistt.TEM_DS_DESCRIPCION END "
                strQuery = strQuery + "		AS varchar(256)), "
                strQuery = strQuery + "		level+1 "
                strQuery = strQuery + "		from X INNER JOIN VIS_TEM_TEMAS vistt "
                strQuery = strQuery + "		LEFT JOIN VIS_TDS_DESC_TEMAS vtdt  "
                strQuery = strQuery + "		ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID "
                strQuery = strQuery + "		AND UPPER(vtdt.idi_ds_codigo)=UPPER('es') "
                strQuery = strQuery + "		ON X.TEM_CO_ID = vistt.TEM_CO_IDPADRE "
                strQuery = strQuery + ") "
                strQuery = strQuery + "SELECT DISTINCT RootTema,TEM_CO_ID,descripcion,TEM_CO_IDPADRE "
                strQuery = strQuery + "FROM X "

                'strQuery = strQuery + "				  SELECT DISTINCT "
                'strQuery = strQuery & "				        REPLACE (SYS_CONNECT_BY_PATH (CASE LEVEL WHEN " & arrLevels(j) & "THEN vistt.TEM_CO_ID END, '~'),'~') AS RootTema,"
                'strQuery = strQuery + "				        vistt.TEM_CO_ID, SYS_CONNECT_BY_PATH((CASE WHEN vtdt.TEM_DS_DESCRIPCION is NULL or TRIM(vistt.TEM_DS_DESCRIPCION) = '' THEN  vistt.TEM_DS_DESCRIPCION ELSE vtdt.TEM_DS_DESCRIPCION END), '\') descripcion "
                'strQuery = strQuery + "				  , vistt.TEM_CO_IDPADRE "
                'strQuery = strQuery + "				  FROM "
                'strQuery = strQuery + "				  (  		"
                'strQuery = strQuery + "				  		VIS_TEM_TEMAS vistt "
                'strQuery = strQuery + "						LEFT JOIN VIS_TDS_DESC_TEMAS vtdt  ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID AND UPPER(vtdt.idi_ds_codigo)=UPPER('es') 		  "
                'strQuery = strQuery + "				  )"
                'strQuery = strQuery + "				  START WITH vistt.TEM_CO_IDPADRE IS NULL  		  "
                'strQuery = strQuery + "				  CONNECT BY vistt.TEM_CO_IDPADRE = PRIOR vistt.TEM_CO_ID  	    "
                strQuery = strQuery + "        ) T1"
                strQuery = strQuery + "		WHERE " + strDesc 'upper(descripcion) like upper('%arroce%') 	"
                If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
                    strQuery = strQuery + " 			  AND RootTema IN (" + strTemas + ") "
                End If
                strQuery = strQuery + ") TemasVisor  	"
                strQuery = strQuery + "INNER JOIN  	"
                strQuery = strQuery + "("
                strQuery = strQuery + "	SELECT DISTINCT vss.SER_CO_ID,vswms.SEW_CO_UID,vss.SER_DS_INFO, CASE WHEN CAST(vsds.SER_DS_ALIAS AS VARCHAR) IS NULL THEN CAST(vss.SER_DS_ALIAS AS VARCHAR) ELSE CAST(vsds.SER_DS_ALIAS AS VARCHAR) END SER_DS_DESCRIPCION "
                strQuery = strQuery + "	,vtts.TEMA_CO_ID   		"
                strQuery = strQuery + "	FROM VIS_ROL_ROLES_SERVICIO vrrs  			 "
                strQuery = strQuery + "	INNER JOIN VIS_SER_SERVICIOS vss ON vrrs.SER_CO_ID = vss.SER_CO_ID  			 "
                strQuery = strQuery + "	INNER JOIN VIS_TEMS_TEMAS_SERVICIO vtts ON vss.SER_CO_ID = vtts.SER_CO_ID  "
                strQuery = strQuery + "	LEFT JOIN VIS_SEW_SERVICIOS_WMS vswms ON vss.SER_CO_ID = vswms.SER_CO_ID "
                strQuery = strQuery + "	INNER JOIN VIS_VSR_VISORES_SERVICIO vvvs ON vss.SER_CO_ID = vvvs.SER_CO_ID  "
                strQuery = strQuery + "	LEFT JOIN VIS_SDS_DESC_SERVICIOS vsds ON vss.SER_CO_ID = vsds.SER_CO_ID "
                strQuery = strQuery + "	AND vsds.IDI_DS_CODIGO='" + codIdioma + "'  "
                strQuery = strQuery + "	WHERE (vvvs.VIS_CO_ID = '" + codVisor + "') "
                'If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
                '    strQuery = strQuery + " AND vtts.TEMA_CO_ID IN (" + strTemas + ")"
                'End If

                strQuery = strQuery + " AND UPPER(vrrs.ROL_DS_ROL_SERVICIO) IN ("
                If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
                    strQuery = strQuery + "UPPER('" + strRolList.Replace(",", "'),UPPER('") + "')"
                Else
                    strQuery = strQuery + "'ALL'"
                End If
                strQuery = strQuery + ") "
                strQuery = strQuery + ") ServiciosVisor  	"
                strQuery = strQuery + "ON ServiciosVisor.TEMA_CO_ID = TemasVisor.TEM_CO_ID  	"

                strQuery = strQuery + "UNION  "

                strQuery = strQuery + "SELECT DISTINCT vss.SER_CO_ID ,COALESCE(vsds.SER_DS_ALIAS,vss.SER_DS_ALIAS) SER_DS_DESCRIPCION, vss.SER_DS_INFO ,vswms.SEW_CO_UID "
                strQuery = strQuery + "FROM VIS_ROL_ROLES_SERVICIO vrrs "
                strQuery = strQuery + "	 INNER JOIN VIS_SER_SERVICIOS vss ON vrrs.SER_CO_ID = vss.SER_CO_ID "
                strQuery = strQuery + "	 INNER JOIN VIS_TEMS_TEMAS_SERVICIO vtts ON vss.SER_CO_ID = vtts.SER_CO_ID "
                strQuery = strQuery + "	 LEFT JOIN VIS_SEW_SERVICIOS_WMS vswms ON vss.SER_CO_ID = vswms.SER_CO_ID  "
                strQuery = strQuery + "	 INNER JOIN VIS_VSR_VISORES_SERVICIO vvvs ON vss.SER_CO_ID = vvvs.SER_CO_ID "
                strQuery = strQuery + "	 INNER JOIN "
                strQuery = strQuery + "	 ( "

                
                'strQuery = strQuery + "		SELECT DISTINCT "
                'strQuery = strQuery & "				        REPLACE (SYS_CONNECT_BY_PATH (CASE LEVEL WHEN " & arrLevels(j) & "THEN vistt.TEM_CO_ID END, '~'),'~') AS RootTema,"
                'strQuery = strQuery + "				        vistt.TEM_CO_ID, SYS_CONNECT_BY_PATH((CASE WHEN vtdt.TEM_DS_DESCRIPCION is NULL or TRIM(vistt.TEM_DS_DESCRIPCION) = '' THEN  vistt.TEM_DS_DESCRIPCION ELSE vtdt.TEM_DS_DESCRIPCION END), '\') descripcion "
                'strQuery = strQuery + "		, vistt.TEM_CO_IDPADRE "
                'strQuery = strQuery + "		FROM "
                'strQuery = strQuery + "		( "
                'strQuery = strQuery + "				VIS_TEM_TEMAS vistt "
                'strQuery = strQuery + "		        LEFT JOIN VIS_TDS_DESC_TEMAS vtdt  ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID "
                'strQuery = strQuery + "				AND UPPER(vtdt.idi_ds_codigo)=UPPER('" + codIdioma + "') "
                'strQuery = strQuery + "		) "
                'strQuery = strQuery + " START WITH vistt.TEM_CO_IDPADRE "
                'strQuery = strQuery + " IS NULL "
                'strQuery = strQuery + "		CONNECT BY vistt.TEM_CO_IDPADRE = PRIOR vistt.TEM_CO_ID "

                strQuery = strQuery + "With Recursive X (RootTema,TEM_CO_ID, TEM_CO_IDPADRE, descripcion, level) as "
                strQuery = strQuery + "( "
                strQuery = strQuery + "		select vistt.TEM_CO_ID, vistt.TEM_CO_ID, vistt.TEM_CO_IDPADRE, "
                strQuery = strQuery + "		CAST( "
                strQuery = strQuery + "		CASE WHEN vtdt.TEM_DS_DESCRIPCION is NULL or TRIM(vistt.TEM_DS_DESCRIPCION) = '' "
                strQuery = strQuery + "		THEN  vistt.TEM_DS_DESCRIPCION ELSE vtdt.TEM_DS_DESCRIPCION END "
                strQuery = strQuery + "		AS varchar(256)),"
                strQuery = strQuery + "		1 "
                strQuery = strQuery + "		from VIS_TEM_TEMAS "
                strQuery = strQuery + "		vistt "
                strQuery = strQuery + "		LEFT JOIN VIS_TDS_DESC_TEMAS vtdt  "
                strQuery = strQuery + "		ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID "
                strQuery = strQuery + "		AND UPPER(vtdt.idi_ds_codigo)=UPPER('es') "
                strQuery = strQuery + "		where vistt.TEM_CO_IDPADRE Is null "
                strQuery = strQuery + "		union all "
                strQuery = strQuery + "		select X.TEM_CO_ID, vistt.TEM_CO_ID, vistt.TEM_CO_IDPADRE, "
                strQuery = strQuery + "		CAST(X.descripcion || '\' || "
                strQuery = strQuery + "		CASE WHEN vistt.TEM_DS_DESCRIPCION is NULL or TRIM(vistt.TEM_DS_DESCRIPCION) = '' "
                strQuery = strQuery + "		THEN  vistt.TEM_DS_DESCRIPCION ELSE vistt.TEM_DS_DESCRIPCION END "
                strQuery = strQuery + "		AS varchar(256)), "
                strQuery = strQuery + "		level+1 "
                strQuery = strQuery + "		from X INNER JOIN VIS_TEM_TEMAS vistt "
                strQuery = strQuery + "		LEFT JOIN VIS_TDS_DESC_TEMAS vtdt  "
                strQuery = strQuery + "		ON vistt.TEM_CO_ID = vtdt.TEM_CO_ID "
                strQuery = strQuery + "		AND UPPER(vtdt.idi_ds_codigo)=UPPER('es') "
                strQuery = strQuery + "		ON X.TEM_CO_ID = vistt.TEM_CO_IDPADRE "
                strQuery = strQuery + ") "
                strQuery = strQuery + "SELECT DISTINCT RootTema,TEM_CO_ID,descripcion,TEM_CO_IDPADRE "
                strQuery = strQuery + "FROM X "

                strQuery = strQuery + "	 ) tTemas ON tTemas.TEM_CO_ID = vtts.TEMA_CO_ID "
                strQuery = strQuery + "	 LEFT JOIN VIS_SDS_DESC_SERVICIOS vsds ON vss.SER_CO_ID = vsds.SER_CO_ID "
                strQuery = strQuery + "				AND vsds.IDI_DS_CODIGO='" + codIdioma + "' "
                strQuery = strQuery + "WHERE (vvvs.VIS_CO_ID = '" + codVisor + "') "
                'strQuery = strQuery + "	  AND UPPER(vrrs.ROL_DS_ROL_SERVICIO) IN ('ALL',UPPER('alimentaci�n')) "
                'strQuery = strQuery + "	  AND UPPER(CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION ELSE vsds.SER_DS_DESCRIPCION END) LIKE UPPER('%arroce%') "
                strQuery = strQuery + " AND UPPER(vrrs.ROL_DS_ROL_SERVICIO) IN ("
                If (strRolList.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strRolList)) Then
                    strQuery = strQuery + "UPPER('" + strRolList.Replace(",", "'),UPPER('") + "')"
                Else
                    strQuery = strQuery + "'ALL'"
                End If
                strQuery = strQuery + ") "

                'strQuery = strQuery + " 			  AND UPPER(CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION ELSE vsds.SER_DS_DESCRIPCION END) LIKE UPPER('%2006%') "                                                                       
                strQuery = strQuery + " 			  AND " + strSER_DS_DESCRIPCION.Replace("descripcion", "CASE WHEN vsds.SER_DS_DESCRIPCION IS NULL THEN vss.SER_DS_DESCRIPCION ELSE vsds.SER_DS_DESCRIPCION END")
                If (strTemas.ToString().Trim() <> "" And Not String.IsNullOrEmpty(strTemas)) Then
                    strQuery = strQuery + " 			  AND RootTema IN (" + strTemas + ") "
                End If

            Next

            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)

            Dim i As Integer
            Dim serCoIdMultiple As Boolean = False
            i = 0
            Dim bas As BusquedaAvanzadaServicios
            For Each aRow In aDS.Tables(0).Rows
                bas = New BusquedaAvanzadaServicios
                Dim cont As Integer
                For Each myElemento In arrValores
                    If Not (myElemento Is Nothing) Then
                        If (aRow("SER_CO_ID").ToString.Equals(myElemento.codId)) Then
                            ReDim Preserve myElemento.uuid(0 To myElemento.uuid.Length)
                            cont = myElemento.uuid.Length - 1
                            myElemento.uuid(cont) = IIf(aRow("SEW_CO_UID").Equals(System.DBNull.Value), "", aRow("SEW_CO_UID"))
                            serCoIdMultiple = True
                            Exit For
                        End If
                    End If
                Next
                If serCoIdMultiple = False Then
                    bas.codId = aRow("SER_CO_ID")
                    bas.descAlias = aRow("SER_DS_ALIAS")
                    bas.infoPdf = IIf(aRow("SER_DS_INFO").Equals(System.DBNull.Value), "", aRow("SER_DS_INFO"))
                    bas.uuid(0) = IIf(aRow("SEW_CO_UID").Equals(System.DBNull.Value), "", aRow("SEW_CO_UID"))
                    If (Not arrValores(0) Is Nothing) Then
                        ReDim Preserve arrValores(0 To arrValores.Length)
                    End If
                    arrValores(arrValores.Length - 1) = bas
                End If
                i += 1
                serCoIdMultiple = False
            Next
            Return arrValores



        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    <System.Web.Services.WebMethod(Description:="Obtener DPHs ", EnableSession:=True)> _
    Public Function DameDPHs(ByVal strWhere As String) As Dph()
        Dim strQuery As String
        Dim strTabla As String, strCampoIdTramo As String, strCampoCauce As String, strCampoDisponible As String, strCampoProvTramo As String, strDatasetName As String, strCampoEstudio As String, strCampoIdEstudio As String, strCampoOrganismo As String, strCampoTipoEstudio As String
        Dim strCampoCodProvTramo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Dph
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaDPHZI")
            strCampoIdTramo = System.Configuration.ConfigurationManager.AppSettings("IdTramo")
            strCampoIdEstudio = System.Configuration.ConfigurationManager.AppSettings("NombreIdEstudio")
            strCampoEstudio = System.Configuration.ConfigurationManager.AppSettings("NombreEstudio")
            strCampoTipoEstudio = System.Configuration.ConfigurationManager.AppSettings("NombreTipoEstudio")
            strCampoOrganismo = System.Configuration.ConfigurationManager.AppSettings("NombreOrganismo")
            strCampoDisponible = System.Configuration.ConfigurationManager.AppSettings("NombreDisponible")
            strCampoProvTramo = System.Configuration.ConfigurationManager.AppSettings("NombreProvTramo")
            strCampoCodProvTramo = System.Configuration.ConfigurationManager.AppSettings("NombreCodProvTramo")
            strCampoCauce = System.Configuration.ConfigurationManager.AppSettings("NombreCauce")
            strDatasetName = strTabla




            strQuery = "SELECT DISTINCT(" + strCampoIdTramo + ")," + strCampoCauce + "," + strCampoIdEstudio + "," + strCampoEstudio + "," + strCampoTipoEstudio + "," + strCampoOrganismo + "," + strCampoDisponible + "," + strCampoProvTramo + "," + strCampoCodProvTramo + " FROM " + strTabla

            '  " from (SELECT DISTINCT TRANSLATE(" + strCampoCauce + ",'����������������������������������������������'," & _
            '  "'aeiouaeiouaoaeiooaeioucAEIOUAEIOUAOAEIOOAEIOUC')" + strCampoCauce + "," + strCampoId + "," + strCampoTipoEstudio + "," + strCampoCCAA & _
            '  " FROM " + strTabla2 + " UNION SELECT distinct TRANSLATE(" + strCampoCauce + ",'����������������������������������������������'," & _
            ' "'aeiouaeiouaoaeiooaeioucAEIOUAEIOUAOAEIOOAEIOUC')" + strCampoCauce + "," + strCampoId + "," + strCampoTipoEstudio + "," + strCampoCCAA & _
            ' " FROM " + strTabla + " ) "

            If (strWhere <> String.Empty) Then
                strQuery += " WHERE " + strWhere
            End If
            strQuery += " ORDER BY " + strCampoEstudio + "," + strCampoCauce + ""
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aDph As New Dph()
                aDph.IdTramo = aRow(strCampoIdTramo)
                aDph.IdEstudio = aRow(strCampoIdEstudio)
                aDph.Organismo = aRow(strCampoOrganismo)
                aDph.TipoEstudio = aRow(strCampoTipoEstudio)
                aDph.Estudio = aRow(strCampoEstudio)
                aDph.Cauce = aRow(strCampoCauce)
                aDph.Disponible = aRow(strCampoDisponible)
                aDph.ProvTramo = aRow(strCampoProvTramo)
                aDph.CodProvTramo = aRow(strCampoCodProvTramo)
                arrValores(i) = aDph
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener ARPSIs ", EnableSession:=True)> _
    Public Function DameARPSIS(ByVal strWhere As String) As ARPSIS()
        Dim strQuery As String, strCampoCodDemar As String, strCampoCodSubTramo As String
        Dim strTabla As String, strCampoNombreARPSIS As String, strCampoNombreAspf As String, strCampoNumInun As String, strCampoFecUltInun As String
        Dim strDatasetName As String, strCampoObjectId As String, strCampoCriterio As String, strCampoEstado As String, strCampoDemar As String, strCampoOrigen As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As ARPSIS
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaARPSIS")
            strCampoObjectId = System.Configuration.ConfigurationManager.AppSettings("IdCaudales") 'Es el campo OBJECTID
            strCampoNombreARPSIS = System.Configuration.ConfigurationManager.AppSettings("NombreARPSIS")
            strCampoNombreAspf = System.Configuration.ConfigurationManager.AppSettings("NombreAPSF")
            strCampoNumInun = System.Configuration.ConfigurationManager.AppSettings("NumeroInund")
            strCampoFecUltInun = System.Configuration.ConfigurationManager.AppSettings("FechaUltInun")
            strCampoOrigen = System.Configuration.ConfigurationManager.AppSettings("OrigenARPSIS")
            strCampoDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarARPSIS")
            strCampoCriterio = System.Configuration.ConfigurationManager.AppSettings("CriterioARPSIS")
            strCampoEstado = System.Configuration.ConfigurationManager.AppSettings("EstadoARPSIS")
            strCampoCodDemar = System.Configuration.ConfigurationManager.AppSettings("SondeosIdDemar") 'Es el cod_demar
            strCampoCodSubTramo = System.Configuration.ConfigurationManager.AppSettings("CodSubTramo")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoObjectId + ")," + strCampoNombreARPSIS + "," + strCampoFecUltInun + "," + strCampoNombreAspf + "," + strCampoNumInun + "," + strCampoOrigen + "," + strCampoCodSubTramo + "," + strCampoCodDemar + "," + strCampoDemar + "," + strCampoCriterio + "," + strCampoEstado + " FROM " + strTabla


            If (strWhere <> String.Empty) Then
                strQuery += " WHERE " + strWhere
            End If
            strQuery += " ORDER BY(" + strCampoNombreAspf + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aARPSIS As New ARPSIS()
                aARPSIS.ObjectId = IIf(aRow(strCampoObjectId).Equals(System.DBNull.Value), "", aRow(strCampoObjectId))
                aARPSIS.NomSubTramo = IIf(aRow(strCampoNombreARPSIS).Equals(System.DBNull.Value), "", aRow(strCampoNombreARPSIS))
                aARPSIS.FechaUltInun = IIf(aRow(strCampoFecUltInun).Equals(System.DBNull.Value), "", aRow(strCampoFecUltInun))
                aARPSIS.NomASPF = IIf(aRow(strCampoNombreAspf).Equals(System.DBNull.Value), "", aRow(strCampoNombreAspf))
                aARPSIS.NumInun = IIf(aRow(strCampoNumInun).Equals(System.DBNull.Value), "", aRow(strCampoNumInun))
                aARPSIS.Origen = IIf(aRow(strCampoOrigen).Equals(System.DBNull.Value), "", aRow(strCampoOrigen))
                aARPSIS.Demarcacion = IIf(aRow(strCampoDemar).Equals(System.DBNull.Value), "", aRow(strCampoDemar))
                aARPSIS.CodDemarcacion = IIf(aRow(strCampoCodDemar).Equals(System.DBNull.Value), "", aRow(strCampoCodDemar))
                aARPSIS.Criterio = IIf(aRow(strCampoCriterio).Equals(System.DBNull.Value), "", aRow(strCampoCriterio))
                aARPSIS.Estado = IIf(aRow(strCampoEstado).Equals(System.DBNull.Value), "", aRow(strCampoEstado))
                aARPSIS.CodSubTramo = IIf(aRow(strCampoCodSubTramo).Equals(System.DBNull.Value), "", aRow(strCampoCodSubTramo))
                arrValores(i) = aARPSIS
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Demarcaciones Hidrogr�ficas ", EnableSession:=True)> _
    Public Function DameDemarcaciones() As Demarcacion()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Demarcacion
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aDemarcacion As New Demarcacion()
                aDemarcacion.Id = aRow(strCampoId)
                aDemarcacion.nombre = aRow(strCampoNombre)
                arrValores(i) = aDemarcacion
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Demarcaciones Hidrogr�ficas ARPSIS", EnableSession:=True)> _
    Public Function DameDemarcacionesARPSIS() As Demarcacion()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Demarcacion
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaARPSIS")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreDemarARPSIS")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aDemarcacion As New Demarcacion()
                aDemarcacion.Id = aRow(strCampoId)
                aDemarcacion.nombre = aRow(strCampoNombre)
                arrValores(i) = aDemarcacion
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Demarcaciones Hidrogr�ficas Regadios", EnableSession:=True)> _
    Public Function DameDemarcacionesRegadios() As Demarcacion()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Demarcacion
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacionesRegadios")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdDemarcacionesRegadios")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreDemarRegadios")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aDemarcacion As New Demarcacion()
                aDemarcacion.Id = aRow(strCampoId)
                aDemarcacion.nombre = aRow(strCampoNombre)
                arrValores(i) = aDemarcacion
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Demarcaciones Hidrogr�ficas Regadios", EnableSession:=True)> _
    Public Function DameDemarcacionesZonasInteresRiego() As Demarcacion()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Demarcacion
        Try

            strTabla = "DGDR.ZIN_DHI_DEMARCACION_HIDRO"
            strCampoId = "COD_DEMARHIDRO"
            strCampoNombre = "NOMBRE_DEMARHIDRO"
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aDemarcacion As New Demarcacion()
                aDemarcacion.Id = aRow(strCampoId)
                aDemarcacion.nombre = aRow(strCampoNombre)
                arrValores(i) = aDemarcacion
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Obtener Demarcaciones Hidrogr�ficas Regadios", EnableSession:=True)> _
    Public Function DameDemarcacionesColectivosRiego() As Demarcacion()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Demarcacion
        Try

            strTabla = "DGDR.CR_ACH_AUX_CUENCAS_HIDRO"
            strCampoId = "COD_CH"
            strCampoNombre = "NOMBRE_CH"
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " ORDER BY(" + strCampoNombre + ")"
            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aDemarcacion As New Demarcacion()
                aDemarcacion.Id = aRow(strCampoId)
                aDemarcacion.nombre = aRow(strCampoNombre)
                arrValores(i) = aDemarcacion
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener lista de capas para validaci�n topol�gica", EnableSession:=True)> _
    Public Function ObtenerCapasValidacionComun(ByVal capas() As String) As DataTable
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strBuilder As New StringBuilder()
        ' Dim capas() As Integer = {"1", "2", "3"}

        ' Nombre de la tabla que contiene las entidades participantes
        Dim tablaCapas As String = "VIS_CAE_CATALOGOENTIDADES"
        ' Nombre del campo clave en la tabla citada
        Dim campoClave As String = "ENT_CO_ID"

        Try
            strBuilder.Append("SELECT ")
            strBuilder.Append("* ") 'Campos a devolver
            strBuilder.Append("FROM ")
            strBuilder.Append(tablaCapas)

            ' Construcci�n de la cl�usula WHERE
            If capas.Length > 0 Then

                strBuilder.Append(" ")
                strBuilder.Append("WHERE ")

                Dim contador As Integer = 0

                While contador < capas.Length
                    strBuilder.Append(campoClave)
                    strBuilder.Append(" = ")
                    strBuilder.Append(capas(contador))

                    If contador < capas.Length - 1 Then
                        strBuilder.Append(" OR ")
                    End If

                    contador += 1
                End While

            End If

            strBuilder.Append(" ORDER BY " + campoClave + " ASC ")

            strQuery = strBuilder.ToString()

            aDS = ExecuteQuery(strQuery, "CapasValidacion", "CapasValidacion", "bbddComun")

            Return aDS.Tables(0)

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtener lista de capas para validaci�n topol�gica", EnableSession:=True)> _
    Public Function ObtenerCapasValidacion(ByVal capas() As String) As DataTable
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strBuilder As New StringBuilder()
        ' Dim capas() As Integer = {"1", "2", "3"}

        ' Nombre de la tabla que contiene las entidades participantes
        Dim tablaCapas As String = "RAG_ENT_ENTIDADES"
        ' Nombre del campo clave en la tabla citada
        Dim campoClave As String = "ENT_CO_ID"

        Try
            strBuilder.Append("SELECT ")
            strBuilder.Append("* ") 'Campos a devolver
            strBuilder.Append("FROM ")
            strBuilder.Append(tablaCapas)

            ' Construcci�n de la cl�usula WHERE
            If capas.Length > 0 Then

                strBuilder.Append(" ")
                strBuilder.Append("WHERE ")

                Dim contador As Integer = 0

                While contador < capas.Length
                    strBuilder.Append(campoClave)
                    strBuilder.Append(" = ")
                    strBuilder.Append(capas(contador))

                    If contador < capas.Length - 1 Then
                        strBuilder.Append(" OR ")
                    End If

                    contador += 1
                End While

            End If

            strBuilder.Append(" ORDER BY " + campoClave + " ASC ")

            strQuery = strBuilder.ToString()

            aDS = ExecuteQueryRAG(strQuery, "CapasValidacion", "CapasValidacion")

            Return aDS.Tables(0)

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="ObtenerReglasValidacion", EnableSession:=True)> _
    Public Function ObtenerReglasValidacion(ByVal IdOperacion As String) As DataTable
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strBuilder As New StringBuilder()

        Try
            strBuilder.Append("SELECT ")
            strBuilder.Append("EV.EV_CO_ID, ")
            strBuilder.Append("EV.EV_CO_ID_ENTIDAD, ")
            strBuilder.Append("ENT_BASE.ENT_DS_DESCRIPCION ENTBASE_DESC, ")
            strBuilder.Append("ENT_BASE.ENT_DS_XMLENTIDAD ENTBASE_XML, ")
            strBuilder.Append("RV.RV_CO_ID, ")
            strBuilder.Append("RV.CV_CO_ID, ")
            strBuilder.Append("CVAL.CV_CO_ID_ENTIDAD, ")
            strBuilder.Append("ENT_VALIDACION.ENT_DS_DESCRIPCION ENTVAL_DESC, ")
            strBuilder.Append("ENT_VALIDACION.ENT_DS_XMLENTIDAD ENTVAL_XML ")
            strBuilder.Append("FROM ")
            strBuilder.Append("RAG_EV_EVENTOS EV ")
            strBuilder.Append("INNER JOIN ")
            strBuilder.Append("RAG_ENT_ENTIDADES ENT_BASE ")
            strBuilder.Append("ON EV.EV_CO_ID_ENTIDAD = ENT_BASE.ENT_CO_ID ")
            strBuilder.Append("INNER JOIN ")
            strBuilder.Append("RAG_RV_REGLAS_VAL RV ")
            strBuilder.Append("ON EV.EV_CO_ID = RV.EV_CO_ID ")
            strBuilder.Append("INNER JOIN ")
            strBuilder.Append("RAG_CV_CONDICIONES_VALIDACION CVAL ")
            strBuilder.Append("ON RV.CV_CO_ID = CVAL.CV_CO_ID ")
            strBuilder.Append("INNER JOIN ")
            strBuilder.Append("RAG_ENT_ENTIDADES ENT_VALIDACION ")
            strBuilder.Append("ON CVAL.CV_CO_ID_ENTIDAD = ENT_VALIDACION.ENT_CO_ID ")
            strBuilder.Append("WHERE ")
            strBuilder.Append("EV.EV_CO_ID = ")
            strBuilder.Append(IdOperacion)
            strBuilder.Append(" ORDER BY RV.RV_CO_ID ASC, RV.RV_NM_ORDEN ASC ")

            strQuery = strBuilder.ToString()

            aDS = ExecuteQueryRAG(strQuery, "ReglasValidacion", "ReglasValidacion")

            Return aDS.Tables(0)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="DameInfraestructurasGISPE ", EnableSession:=True)> _
    Public Function DameInfraestructurasGISPE(ByVal whereQuery As String, ByVal presas As Boolean, ByVal diques As Boolean) As RetornoInfraestructura
        Dim strQuery As String
        Dim strDatasetName As String, strCampoId As String, strNombrePresa As String, strNombrePresas As String, strNombreTitular As String, strNombreFase As String, strIdDemar As String
        Dim strNombreTipo As String, strNombreCorriente As String, strIdProvSnczi As String, strIdProvGeo As String, strIdTiposPre As String, strIdTiposPrePre As String, strNombreDemar As String, strNombreProv As String, strNombrePresasPre As String, strNombrePresasGeo As String, strNombreProvGeo As String
        Dim strTablaAdmitvos As String, strTablaPresas As String, strTablaTipologias As String, strTablaPresasPre As String, strTablaProv As String, strTablaGeografi As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim res As New RetornoInfraestructura()
        Dim arrValores() As InfraestructuraGISPE

        Try

            strTablaAdmitvos = System.Configuration.ConfigurationManager.AppSettings("TablaPresasAdmtivos")
            strTablaPresas = System.Configuration.ConfigurationManager.AppSettings("TablaPresas")
            'strNombrePresa = System.Configuration.ConfigurationManager.AppSettings("NombrePresas")
            'strTablaTipologias = System.Configuration.ConfigurationManager.AppSettings("TablaPresasTipos")
            'strTablaPresasPre = System.Configuration.ConfigurationManager.AppSettings("TablaPresasPresas")
            'strTablaGeografi = System.Configuration.ConfigurationManager.AppSettings("TablaPresasGeo")
            'strTablaProv = System.Configuration.ConfigurationManager.AppSettings("TablaProvinciasSnczi")
            'strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdPresas2")
            'strNombrePresas = System.Configuration.ConfigurationManager.AppSettings("NombrePresas2")
            'strNombreTitular = System.Configuration.ConfigurationManager.AppSettings("NombreTitular")
            'strNombreFase = System.Configuration.ConfigurationManager.AppSettings("NombreFases")
            'strNombreTipo = System.Configuration.ConfigurationManager.AppSettings("NombreTipologia")
            'strNombreDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            'strNombreProv = System.Configuration.ConfigurationManager.AppSettings("NombreProvinciasSnczi")
            'strNombrePresasPre = System.Configuration.ConfigurationManager.AppSettings("NombrePresasPre")
            'strNombrePresasGeo = System.Configuration.ConfigurationManager.AppSettings("NombrePresasGeo")
            'strNombreProvGeo = System.Configuration.ConfigurationManager.AppSettings("NombreProvGeo")
            'strIdProvGeo = System.Configuration.ConfigurationManager.AppSettings("IdProvPMetria")
            'strIdDemar = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            'strIdTiposPrePre = System.Configuration.ConfigurationManager.AppSettings("IdTiposPresasPre")
            'strIdTiposPre = System.Configuration.ConfigurationManager.AppSettings("IdTiposPresas")
            'strNombreCorriente = System.Configuration.ConfigurationManager.AppSettings("NombreCorriente")
            strDatasetName = strTablaAdmitvos

            strQuery = "SELECT * FROM " + _
                "( "

            If presas Then

                Dim strJoin As String
                strJoin = "    LEFT JOIN EGISPE_GEO.EGIS_REL_INF_DEMARTER " + _
                    "        ON EGISPE_GEO.VEGIS_INF_PRESA.ID_INFRAESTRUCTURA = EGISPE_GEO.EGIS_REL_INF_DEMARTER.ID_INFRAESTRUCTURA " + _
                    "    LEFT JOIN EGISPE_GEO.EGIS_REL_INF_MUNICIPIO  " + _
                    "        ON EGISPE_GEO.VEGIS_INF_PRESA.ID_INFRAESTRUCTURA = EGISPE_GEO.EGIS_REL_INF_MUNICIPIO.ID_INFRAESTRUCTURA "

                res.queryPresas = "    SELECT " + _
                    "    DISTINCT  " + _
                    "        EGISPE_GEO.VEGIS_INF_PRESA.ID_INFRAESTRUCTURA " + _
                    "    FROM  " + _
                    "    EGISPE_GEO.VEGIS_INF_PRESA " + strJoin

                If (whereQuery <> String.Empty) Then
                    res.queryPresas += " WHERE " + whereQuery
                End If

                strQuery += "    SELECT " + _
                    "    DISTINCT  " + _
                    "        EGISPE_GEO.VEGIS_INF_PRESA.COD_INFRAESTRUCTURA C�digo, " + _
                    "        EGISPE_GEO.VEGIS_INF_PRESA.ID_INFRAESTRUCTURA Id, " + _
                    "        EGISPE_GEO.VEGIS_INF_PRESA.NOMBRE Nombre, " + _
                    "        EGISPE_GEO.VEGIS_INF_PRESA.TITULAR Titular, " + _
                    "        EGISPE_GEO.VEGIS_INF_PRESA.CAUCE Cauce, " + _
                    "        'Presa' TipoInf " + _
                    "    FROM  " + _
                    "    EGISPE_GEO.VEGIS_INF_PRESA"

                strQuery = strQuery + strJoin

                If (whereQuery <> String.Empty) Then
                    strQuery += " WHERE " + whereQuery
                End If

                If diques Then
                    strQuery += "    UNION "
                End If
            End If

            If diques Then

                Dim strJoin As String
                strJoin = "   LEFT JOIN EGISPE_GEO.EGIS_REL_INF_DEMARTER  " + _
                    "        ON EGISPE_GEO.VEGIS_INF_BALSA.ID_INFRAESTRUCTURA = EGISPE_GEO.EGIS_REL_INF_DEMARTER.ID_INFRAESTRUCTURA " + _
                    "    LEFT JOIN EGISPE_GEO.EGIS_REL_INF_MUNICIPIO  " + _
                    "        ON EGISPE_GEO.VEGIS_INF_BALSA.ID_INFRAESTRUCTURA = EGISPE_GEO.EGIS_REL_INF_MUNICIPIO.ID_INFRAESTRUCTURA "

                res.queryDiques = "    SELECT " + _
                    "    DISTINCT  " + _
                    "        EGISPE_GEO.VEGIS_INF_BALSA.ID_INFRAESTRUCTURA " + _
                    "    FROM  " + _
                    "    EGISPE_GEO.VEGIS_INF_BALSA " + strJoin

                If (whereQuery <> String.Empty) Then
                    res.queryDiques += " WHERE " + whereQuery
                End If

                strQuery = strQuery + _
                    "    SELECT " + _
                    "    DISTINCT  " + _
                    "        EGISPE_GEO.VEGIS_INF_BALSA.COD_INFRAESTRUCTURA C�digo, " + _
                    "        EGISPE_GEO.VEGIS_INF_BALSA.ID_INFRAESTRUCTURA Id, " + _
                    "        EGISPE_GEO.VEGIS_INF_BALSA.NOMBRE Nombre, " + _
                    "        EGISPE_GEO.VEGIS_INF_BALSA.TITULAR Titular, " + _
                    "        EGISPE_GEO.VEGIS_INF_BALSA.CAUCE Cauce, " + _
                    "        'Balsa' TipoInf " + _
                    "    FROM  " + _
                    "    EGISPE_GEO.VEGIS_INF_BALSA " + strJoin
                If (whereQuery <> String.Empty) Then
                    strQuery += " WHERE " + whereQuery
                End If

            End If

            strQuery += " ) "
            strQuery += " ORDER BY Nombre"

            aDS = ExecuteQueryGISPE(strQuery, strTablaAdmitvos, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aValor As New InfraestructuraGISPE()

                aValor.Cod = aRow("C�digo")
                aValor.Id = aRow("Id")
                aValor.Nombre = aRow("Nombre")
                'aValor.Demarcacion = IIf(aRow("Demarcaci�n") Is DBNull.Value, "", aRow("Demarcaci�n"))
                'aValor.Provincia = IIf(aRow("Provincia") Is DBNull.Value, "", aRow("Provincia"))
                'aValor.Municipio = IIf(aRow("Municipio") Is DBNull.Value, "", aRow("Municipio"))
                aValor.Titular = IIf(aRow("Titular") Is DBNull.Value, "", aRow("Titular"))
                aValor.Cauce = IIf(aRow("Cauce") Is DBNull.Value, "", aRow("Cauce"))
                aValor.TipoInf = aRow("TipoInf")

                arrValores(i) = aValor
                i += 1
            Next

            res.valores = arrValores

            Return res
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="DameEmbalsesGISPE ", EnableSession:=True)> _
    Public Function DameEmbalsesGISPE(ByVal whereQuery As String, ByVal presas As Boolean, ByVal balsas As Boolean) As RetornoEmbalse
        Dim strQuery As String
        Dim strDatasetName As String, strCampoId As String, strNombrePresa As String, strNombrePresas As String, strNombreTitular As String, strNombreFase As String, strIdDemar As String
        Dim strNombreTipo As String, strNombreCorriente As String, strIdProvSnczi As String, strIdProvGeo As String, strIdTiposPre As String, strIdTiposPrePre As String, strNombreDemar As String, strNombreProv As String, strNombrePresasPre As String, strNombrePresasGeo As String, strNombreProvGeo As String
        Dim strTablaAdmitvos As String, strTablaPresas As String, strTablaTipologias As String, strTablaPresasPre As String, strTablaProv As String, strTablaGeografi As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim res As New RetornoEmbalse()
        Dim arrValores() As EmbalseGISPE

        Try

            strTablaAdmitvos = System.Configuration.ConfigurationManager.AppSettings("TablaPresasAdmtivos")
            strTablaPresas = System.Configuration.ConfigurationManager.AppSettings("TablaPresas")
            'strNombrePresa = System.Configuration.ConfigurationManager.AppSettings("NombrePresas")
            'strTablaTipologias = System.Configuration.ConfigurationManager.AppSettings("TablaPresasTipos")
            'strTablaPresasPre = System.Configuration.ConfigurationManager.AppSettings("TablaPresasPresas")
            'strTablaGeografi = System.Configuration.ConfigurationManager.AppSettings("TablaPresasGeo")
            'strTablaProv = System.Configuration.ConfigurationManager.AppSettings("TablaProvinciasSnczi")
            'strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdPresas2")
            'strNombrePresas = System.Configuration.ConfigurationManager.AppSettings("NombrePresas2")
            'strNombreTitular = System.Configuration.ConfigurationManager.AppSettings("NombreTitular")
            'strNombreFase = System.Configuration.ConfigurationManager.AppSettings("NombreFases")
            'strNombreTipo = System.Configuration.ConfigurationManager.AppSettings("NombreTipologia")
            'strNombreDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            'strNombreProv = System.Configuration.ConfigurationManager.AppSettings("NombreProvinciasSnczi")
            'strNombrePresasPre = System.Configuration.ConfigurationManager.AppSettings("NombrePresasPre")
            'strNombrePresasGeo = System.Configuration.ConfigurationManager.AppSettings("NombrePresasGeo")
            'strNombreProvGeo = System.Configuration.ConfigurationManager.AppSettings("NombreProvGeo")
            'strIdProvGeo = System.Configuration.ConfigurationManager.AppSettings("IdProvPMetria")
            'strIdDemar = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            'strIdTiposPrePre = System.Configuration.ConfigurationManager.AppSettings("IdTiposPresasPre")
            'strIdTiposPre = System.Configuration.ConfigurationManager.AppSettings("IdTiposPresas")
            'strNombreCorriente = System.Configuration.ConfigurationManager.AppSettings("NombreCorriente")
            strDatasetName = strTablaAdmitvos

            strQuery = "SELECT * FROM " + _
                "( "

            If presas Then

                Dim strJoin As String
                strJoin = "    LEFT JOIN EGISPE_GEO.EGIS_REL_EMB_DEMARTER  " + _
                    "        ON EGISPE_GEO.VEGIS_EMB_PRESA.ID_EMBALSE = EGISPE_GEO.EGIS_REL_EMB_DEMARTER.ID_EMBALSE " + _
                    "    LEFT JOIN EGISPE_GEO.EGIS_REL_EMB_MUNICIPIO  " + _
                    "        ON EGISPE_GEO.VEGIS_EMB_PRESA.ID_EMBALSE = EGISPE_GEO.EGIS_REL_EMB_MUNICIPIO.ID_EMBALSE "

                res.queryPresas = "    SELECT " + _
                    "    DISTINCT  " + _
                    "        EGISPE_GEO.VEGIS_EMB_PRESA.ID_EMBALSE " + _
                    "    FROM  " + _
                    "    EGISPE_GEO.VEGIS_EMB_PRESA " + strJoin

                If (whereQuery <> String.Empty) Then
                    res.queryPresas += " WHERE " + whereQuery
                End If

                strQuery += "    SELECT " + _
                    "    DISTINCT  " + _
                    "        EGISPE_GEO.VEGIS_EMB_PRESA.COD_EMBALSE C�digo, " + _
                    "        EGISPE_GEO.VEGIS_EMB_PRESA.ID_EMBALSE Id, " + _
                    "        EGISPE_GEO.VEGIS_EMB_PRESA.NOMBRE Nombre, " + _
                    "        EGISPE_GEO.VEGIS_EMB_PRESA.CAUCE Cauce, " + _
                    "        EGISPE_GEO.VEGIS_EMB_PRESA.TITULAR Titular, " + _
                    "        'Presa' TipoInf " + _
                    "    FROM  " + _
                    "    EGISPE_GEO.VEGIS_EMB_PRESA " + strJoin


                If (whereQuery <> String.Empty) Then
                    strQuery += " WHERE " + whereQuery
                End If

                If balsas Then
                    strQuery += "    UNION "
                End If
            End If

            If balsas Then

                Dim strJoin As String
                strJoin = "    LEFT JOIN EGISPE_GEO.EGIS_REL_EMB_DEMARTER  " + _
                    "        ON EGISPE_GEO.VEGIS_EMB_BALSA.ID_EMBALSE = EGISPE_GEO.EGIS_REL_EMB_DEMARTER.ID_EMBALSE " + _
                    "    LEFT JOIN EGISPE_GEO.EGIS_REL_EMB_MUNICIPIO  " + _
                    "        ON EGISPE_GEO.VEGIS_EMB_BALSA.ID_EMBALSE = EGISPE_GEO.EGIS_REL_EMB_MUNICIPIO.ID_EMBALSE "

                res.queryBalsas = "    SELECT " + _
                    "    DISTINCT  " + _
                    "        EGISPE_GEO.VEGIS_EMB_BALSA.ID_EMBALSE " + _
                    "    FROM  " + _
                    "    EGISPE_GEO.VEGIS_EMB_BALSA " + strJoin

                If (whereQuery <> String.Empty) Then
                    res.queryBalsas += " WHERE " + whereQuery
                End If

                strQuery = strQuery + _
                    "    SELECT " + _
                    "    DISTINCT  " + _
                    "        EGISPE_GEO.VEGIS_EMB_BALSA.COD_EMBALSE C�digo, " + _
                    "        EGISPE_GEO.VEGIS_EMB_BALSA.ID_EMBALSE Id, " + _
                    "	     EGISPE_GEO.VEGIS_EMB_BALSA.NOMBRE Nombre, " + _
                    "        EGISPE_GEO.VEGIS_EMB_BALSA.CAUCE Cauce, " + _
                    "        EGISPE_GEO.VEGIS_EMB_BALSA.TITULAR Titular, " + _
                    "        'Balsa' TipoInf " + _
                    "    FROM  " + _
                    "    EGISPE_GEO.VEGIS_EMB_BALSA " + strJoin


                If (whereQuery <> String.Empty) Then
                    strQuery += " WHERE " + whereQuery
                End If

            End If

            strQuery += " ) "
            strQuery += " ORDER BY Nombre"

            aDS = ExecuteQueryGISPE(strQuery, strTablaAdmitvos, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aValor As New EmbalseGISPE()

                aValor.Cod = aRow("C�digo")
                aValor.Id = aRow("Id")
                aValor.Nombre = aRow("Nombre")
                'aValor.Demarcacion = IIf(aRow("Demarcaci�n") Is DBNull.Value, "", aRow("Demarcaci�n"))
                'aValor.Provincia = IIf(aRow("Provincia") Is DBNull.Value, "", aRow("Provincia"))
                'aValor.Municipio = IIf(aRow("Municipio") Is DBNull.Value, "", aRow("Municipio"))
                aValor.Cauce = IIf(aRow("Cauce") Is DBNull.Value, "", aRow("Cauce"))
                aValor.Titular = IIf(aRow("Titular") Is DBNull.Value, "", aRow("Titular"))
                aValor.TipoInf = aRow("TipoInf")

                arrValores(i) = aValor
                i += 1
            Next

            res.valores = arrValores

            Return res
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Presas Snczi ", EnableSession:=True)> _
    Public Function DamePresasSnczi(ByVal whereQuery As String) As PresaAdmtivo()
        Dim strQuery As String
        Dim strDatasetName As String, strCampoId As String, strNombrePresa As String, strNombrePresas As String, strNombreTitular As String, strNombreFase As String, strIdDemar As String
        Dim strNombreTipo As String, strNombreCorriente As String, strIdProvSnczi As String, strIdProvGeo As String, strIdTiposPre As String, strIdTiposPrePre As String, strNombreDemar As String, strNombreProv As String, strNombrePresasPre As String, strNombrePresasGeo As String, strNombreProvGeo As String
        Dim strTablaAdmitvos As String, strTablaPresas As String, strTablaTipologias As String, strTablaPresasPre As String, strTablaProv As String, strTablaGeografi As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As PresaAdmtivo
        Try

            strTablaAdmitvos = System.Configuration.ConfigurationManager.AppSettings("TablaPresasAdmtivos")
            strTablaPresas = System.Configuration.ConfigurationManager.AppSettings("TablaPresas")
            strNombrePresa = System.Configuration.ConfigurationManager.AppSettings("NombrePresas")
            strTablaTipologias = System.Configuration.ConfigurationManager.AppSettings("TablaPresasTipos")
            strTablaPresasPre = System.Configuration.ConfigurationManager.AppSettings("TablaPresasPresas")
            strTablaGeografi = System.Configuration.ConfigurationManager.AppSettings("TablaPresasGeo")
            strTablaProv = System.Configuration.ConfigurationManager.AppSettings("TablaProvinciasSnczi")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdPresas2")
            strNombrePresas = System.Configuration.ConfigurationManager.AppSettings("NombrePresas2")
            strNombreTitular = System.Configuration.ConfigurationManager.AppSettings("NombreTitular")
            strNombreFase = System.Configuration.ConfigurationManager.AppSettings("NombreFases")
            strNombreTipo = System.Configuration.ConfigurationManager.AppSettings("NombreTipologia")
            strNombreDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strNombreProv = System.Configuration.ConfigurationManager.AppSettings("NombreProvinciasSnczi")
            strNombrePresasPre = System.Configuration.ConfigurationManager.AppSettings("NombrePresasPre")
            strNombrePresasGeo = System.Configuration.ConfigurationManager.AppSettings("NombrePresasGeo")
            strNombreProvGeo = System.Configuration.ConfigurationManager.AppSettings("NombreProvGeo")
            strIdProvGeo = System.Configuration.ConfigurationManager.AppSettings("IdProvPMetria")
            strIdDemar = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strIdTiposPrePre = System.Configuration.ConfigurationManager.AppSettings("IdTiposPresasPre")
            strIdTiposPre = System.Configuration.ConfigurationManager.AppSettings("IdTiposPresas")
            strNombreCorriente = System.Configuration.ConfigurationManager.AppSettings("NombreCorriente")
            strDatasetName = strTablaAdmitvos

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strNombrePresas + "," + strNombreTitular + "," + strIdProvGeo + "," + strIdDemar + "," + strNombreFase + "," + strNombreTipo + "," + strNombreDemar + "," + strNombreCorriente + "," + strNombreProv & _
            " FROM " + strTablaAdmitvos + " INNER JOIN " + strTablaGeografi + " ON " + strNombrePresas + "=" + strNombrePresasGeo & _
            " INNER JOIN " + strTablaPresas + " ON " + strNombrePresa + "=" + strNombrePresas & _
            " INNER JOIN " + strTablaPresasPre + " ON " + strNombrePresas + "=" + strNombrePresasPre & _
            " INNER JOIN " + strTablaProv + " ON " + strNombreProvGeo + "=" + strIdProvGeo & _
            " LEFT JOIN " + strTablaTipologias + " ON " + strIdTiposPrePre + "=" + strIdTiposPre

            If (whereQuery <> String.Empty) Then
                strQuery += " WHERE " + whereQuery
            End If
            strQuery += " ORDER BY(" + strNombrePresas + ")"

            aDS = ExecuteQuery(strQuery, strTablaAdmitvos, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aPresa As New PresaAdmtivo()
                aPresa.Id = aRow(strCampoId)
                aPresa.nombre = aRow(strNombrePresas)
                aPresa.Titular = aRow(strNombreTitular)
                aPresa.Fase = aRow(strNombreFase)
                aPresa.Demarcacion = aRow(strNombreDemar)
                aPresa.IdDemarcacion = aRow(strIdDemar)
                If aRow(strNombreTipo) Is DBNull.Value Then
                    aPresa.Tipologia = ""
                Else
                    aPresa.Tipologia = aRow(strNombreTipo)
                End If

                aPresa.Provincia = aRow(strNombreProv)
                aPresa.IdProvincia = aRow(strIdProvGeo)
                If aRow(strNombreCorriente) Is DBNull.Value Then
                    aPresa.cauce = ""
                Else
                    aPresa.cauce = aRow(strNombreCorriente)
                End If
                arrValores(i) = aPresa
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener �reas de Riego", EnableSession:=True)> _
    Public Function dameAreasRiego(ByVal Nombre As String, ByVal Demarcacion As String) As AreaRiego()
        Dim strQuery As String
        Dim strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strWhere As String = " WHERE "
        Dim arrValores() As AreaRiego
        Try

            strDatasetName = "AREAS_RIEGO_2013"

            strQuery = "SELECT DISTINCT(DGDR.AREAS_RIEGO_2013.COD_AREADH),DGDR.AREAS_RIEGO_2013.NOMBRE"
            strQuery += " FROM DGDR.AREAS_RIEGO_2013 "

            If (Nombre <> String.Empty) Then
                strQuery += strWhere + " (UPPER(DGDR.AREAS_RIEGO_2013.NOMBRE) LIKE '%" + Nombre.ToUpper() + "%')"
                strWhere = " AND "
            End If
            If (Demarcacion <> String.Empty) Then
                strQuery += strWhere + " DGDR.AREAS_RIEGO_2013.COD_CUENCADH ='" + Right("000" + Demarcacion, 3) + "'"
                strWhere = " AND "
            End If
            strQuery += " ORDER BY(DGDR.AREAS_RIEGO_2013.NOMBRE)"

            aDS = ExecuteQuery(strQuery, "DGDR.AREAS_RIEGO_2013", strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim fila As New AreaRiego()
                fila.Id = aRow("COD_AREADH")
                fila.nombre = IIf(IsDBNull(aRow("NOMBRE")), "", aRow("NOMBRE"))
                arrValores(i) = fila
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Zonas regables de Inter�s", EnableSession:=True)> _
    Public Function dameZonasRegablesInteres(ByVal Nombre As String, _
        ByVal Demarcacion As String, ByVal CCAA As String, ByVal Provincia As String, ByVal Municipio As String) As ZonaInteresRiego()
        Dim strQuery As String
        Dim strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strWhere As String = " WHERE "
        Dim arrValores() As ZonaInteresRiego
        Try

            strDatasetName = "ZONASREG_ZIN"

            strQuery = "SELECT DISTINCT(DGDR.ZONASREG_ZIN.COD_ZIN), DGDR.ZONASREG_ZIN.NOMBRE_ZIN, DGDR.ZONASREG_ZIN.DOCUMENTO"
            strQuery += " FROM DGDR.ZONASREG_ZIN " + _
                " INNER JOIN DGDR.ZIN_ZDH_ZONA_DEMARHIDRO" + _
                "     ON DGDR.ZONASREG_ZIN.COD_ZIN = CAST(DGDR.ZIN_ZDH_ZONA_DEMARHIDRO.COD_ZIN AS SMALLINT)" + _
                " INNER JOIN DGDR.ZIN_ZMU_ZONA_MUNICIPIO" + _
                "     ON DGDR.ZONASREG_ZIN.COD_ZIN = CAST(DGDR.ZIN_ZMU_ZONA_MUNICIPIO.COD_ZIN AS SMALLINT)" + _
                " INNER JOIN DGDR.ZIN_ZPR_ZONA_PROVINCIA" + _
                "     ON DGDR.ZONASREG_ZIN.COD_ZIN = CAST(DGDR.ZIN_ZPR_ZONA_PROVINCIA.COD_ZIN AS SMALLINT)" + _
                " INNER JOIN DGDR.ZIN_PRV_PROVINCIA" + _
                "     ON DGDR.ZIN_ZPR_ZONA_PROVINCIA.COD_PROV = DGDR.ZIN_PRV_PROVINCIA.COD_PROV"

            If (Nombre <> String.Empty) Then
                strQuery += strWhere + " (UPPER(DGDR.ZONASREG_ZIN.NOMBRE_ZIN) LIKE '%" + Nombre.ToUpper() + "%')"
                strWhere = " AND "
            End If
            If (Demarcacion <> String.Empty) Then
                strQuery += strWhere + " DGDR.ZIN_ZDH_ZONA_DEMARHIDRO.COD_DEMARHIDRO ='" + Demarcacion + "'"
                strWhere = " AND "
            End If
            If (CCAA <> String.Empty) Then
                strQuery += strWhere + " DGDR.ZIN_PRV_PROVINCIA.COD_CCAA ='" + Right("00" + CCAA, 2) + "'"
                strWhere = " AND "
            End If
            If (Provincia <> String.Empty) Then
                strQuery += strWhere + " DGDR.ZIN_PRV_PROVINCIA.COD_PROV ='" + Right("00" + Provincia, 2) + "'"
                strWhere = " AND "
            End If
            If (Municipio <> String.Empty) Then
                strQuery += strWhere + " DGDR.ZIN_ZMU_ZONA_MUNICIPIO.COD_INE ='" + Right("0000" + Municipio, 5) + "'"
                strWhere = " AND "
            End If

            strQuery += " ORDER BY(DGDR.ZONASREG_ZIN.NOMBRE_ZIN)"

            aDS = ExecuteQuery(strQuery, "DGDR.ZONASREG_ZIN", strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim fila As New ZonaInteresRiego
                fila.Id = aRow("COD_ZIN")
                fila.nombre = IIf(IsDBNull(aRow("NOMBRE_ZIN")), "", aRow("NOMBRE_ZIN"))
                fila.documento = IIf(IsDBNull(aRow("DOCUMENTO")), "", aRow("DOCUMENTO"))
                arrValores(i) = fila
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Obras de Mejora de Regad�o ", EnableSession:=True)> _
    Public Function dameObrasMejoraRegadio(ByVal Nombre As String, ByVal Codigo As String, ByVal NombreColectivo As String, ByVal CodigoColectivo As String, _
        ByVal Demarcacion As String, ByVal CCAA As String, ByVal Provincia As String, ByVal Municipio As String) As ObraModernRegadio()
        Dim strQuery As String
        Dim strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strWhere As String = " WHERE "
        Dim arrValores() As ObraModernRegadio
        Try

            strDatasetName = "OBRAS_SITUACION"

            strQuery = "SELECT DISTINCT(DGDR.OBRAS_SITUACION.COD_OBRA),DGDR.OBRAS_SITUACION.NOMBRE_OBRA_CORTO," _
                + "DGDR.OBRAS_SITUACION.NOMBRE_OBRA_LARGO"
            'DGDR.OBR_TCR_TRABAJO_CR.COD_CR, DGDR.OBR_TCR_TRABAJO_CR.NOMBRE_CR, DGDR.OBR_TRB_TRABAJO.COD_DH, DGDR.OBR_DHI_DEMARCACION_HIDRO.NOMBRE_DH, " _"
            '    + "DGDR.OBR_PRV_PROVINCIA.COD_CCAA,DGDR.OBR_CCA_CCAA.NOMBRE_CCAA," _
            '    + "DGDR.OBR_TMU_TRABAJO_MUNICIPIO.COD_PROV, DGDR.OBR_PRV_PROVINCIA.NOMBRE_PROV," _
            '    + "DGDR.OBR_TMU_TRABAJO_MUNICIPIO.COD_INE, DGDR.OBR_MUN_MUNICIPIO.NOMBRE_MUN" _
            strQuery += " FROM DGDR.OBRAS_SITUACION" + _
                " INNER JOIN DGDR.OBR_TRB_TRABAJO ON DGDR.OBRAS_SITUACION.COD_OBRA = DGDR.OBR_TRB_TRABAJO.COD_OBRA" + _
                " INNER JOIN DGDR.OBR_TCR_TRABAJO_CR" + _
                "     ON DGDR.OBRAS_SITUACION.COD_OBRA = DGDR.OBR_TCR_TRABAJO_CR.COD_OBRA" + _
                " INNER JOIN DGDR.OBR_DHI_DEMARCACION_HIDRO" + _
                "     ON DGDR.OBRAS_SITUACION.COD_DH = CAST(DGDR.OBR_DHI_DEMARCACION_HIDRO.COD_DH AS SMALLINT)" + _
                " INNER JOIN DGDR.OBR_TMU_TRABAJO_MUNICIPIO" + _
                "     ON DGDR.OBRAS_SITUACION.COD_OBRA = DGDR.OBR_TMU_TRABAJO_MUNICIPIO.COD_OBRA" + _
                " INNER JOIN DGDR.OBR_PRV_PROVINCIA" + _
                "     ON DGDR.OBR_TMU_TRABAJO_MUNICIPIO.COD_PROV = DGDR.OBR_PRV_PROVINCIA.COD_PROV" + _
                " INNER JOIN DGDR.OBR_CCA_CCAA" + _
                "     ON DGDR.OBR_PRV_PROVINCIA.COD_CCAA = DGDR.OBR_CCA_CCAA.COD_CCAA" + _
                " INNER JOIN DGDR.OBR_MUN_MUNICIPIO" + _
                "     ON DGDR.OBR_TMU_TRABAJO_MUNICIPIO.COD_INE = DGDR.OBR_MUN_MUNICIPIO.COD_INE"

            If (Nombre <> String.Empty) Then
                strQuery += strWhere + " (UPPER(DGDR.OBRAS_SITUACION.NOMBRE_OBRA_CORTO) LIKE '%" + Nombre.ToUpper() + "%' OR UPPER(DGDR.OBRAS_SITUACION.NOMBRE_OBRA_LARGO) LIKE '%" + Nombre.ToUpper() + "%')"
                strWhere = " AND "
            End If
            If (Codigo <> String.Empty) Then
                strQuery += strWhere + " DGDR.OBRAS_SITUACION.COD_OBRA ='" + Codigo + "'"
                strWhere = " AND "
            End If
            If (NombreColectivo <> String.Empty) Then
                strQuery += strWhere + " UPPER(DGDR.OBR_TCR_TRABAJO_CR.NOMBRE_CR) LIKE '% " + NombreColectivo.ToUpper() + "%'"
                strWhere = " AND "
            End If
            If (CodigoColectivo <> String.Empty) Then
                strQuery += strWhere + " DGDR.OBR_TCR_TRABAJO_CR.COD_CR ='" + CodigoColectivo + "'"
                strWhere = " AND "
            End If
            If (Demarcacion <> String.Empty) Then
                strQuery += strWhere + " DGDR.OBRAS_SITUACION.COD_DH ='" + Demarcacion + "'"
                strWhere = " AND "
            End If
            If (CCAA <> String.Empty) Then
                strQuery += strWhere + " DGDR.OBR_PRV_PROVINCIA.COD_CCAA ='" + Right("00" + CCAA, 2) + "'"
                strWhere = " AND "
            End If
            If (Provincia <> String.Empty) Then
                strQuery += strWhere + " DGDR.OBR_TMU_TRABAJO_MUNICIPIO.COD_PROV ='" + Right("00" + Provincia, 2) + "'"
                strWhere = " AND "
            End If
            If (Municipio <> String.Empty) Then
                strQuery += strWhere + " DGDR.OBR_TMU_TRABAJO_MUNICIPIO.COD_INE ='" + Right("0000" + Municipio, 5) + "'"
                strWhere = " AND "
            End If
            strQuery += " ORDER BY(DGDR.OBRAS_SITUACION.NOMBRE_OBRA_CORTO)"

            aDS = ExecuteQuery(strQuery, "DGDR.OBRAS_SITUACION", strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aObra As New ObraModernRegadio()
                aObra.Id = aRow("COD_OBRA")
                aObra.nombre = IIf(IsDBNull(aRow("NOMBRE_OBRA_CORTO")), "", aRow("NOMBRE_OBRA_CORTO"))
                aObra.nombreLargo = IIf(IsDBNull(aRow("NOMBRE_OBRA_LARGO")), "", aRow("NOMBRE_OBRA_LARGO"))
                arrValores(i) = aObra
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Informaci�n General de Regad�o ", EnableSession:=True)> _
    Public Function dameInformacionGeneralRegadio(ByVal NombreArea As String, _
        ByVal Demarcacion As String, ByVal CCAA As String, ByVal Provincia As String, ByVal Municipio As String) As Regadio()
        Dim strQuery As String
        Dim strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strWhere As String = " WHERE "
        Dim arrValores() As Regadio
        Try

            strDatasetName = "REGADIOS_H2020"

            strQuery = "SELECT DISTINCT(DGDR.REGADIOS_H2020.COD_CR) COD_CR, DGDR.REGADIOS_H2020.NOMBRE_CR NOMBRE_CR, DGDR.REGADIOS_H2020.DOCUMENTO DOC"
            strQuery += " FROM DGDR.REGADIOS_H2020" + _
                " INNER JOIN DGDR.CR_CDP_DATOS_PRINCIPALES ON DGDR.CR_CDP_DATOS_PRINCIPALES.COD_CR = DGDR.REGADIOS_H2020.COD_CR" + _
                " INNER JOIN DGDR.CR_CDT_DATOS_TERRITORIALES ON DGDR.CR_CDT_DATOS_TERRITORIALES.COD_CR = DGDR.REGADIOS_H2020.COD_CR" + _
                " INNER JOIN DGDR.CR_APR_AUX_PROVINCIA ON DGDR.CR_APR_AUX_PROVINCIA.COD_PROVINCIA = DGDR.CR_CDP_DATOS_PRINCIPALES.COD_PROVINCIA"

            If (NombreArea <> String.Empty) Then
                strQuery += strWhere + " UPPER(DGDR.REGADIOS_H2020.NOMBRE_CR) LIKE '%" + NombreArea.ToUpper() + "%'"
                strWhere = " AND "
            End If

            If (Demarcacion <> String.Empty) Then
                strQuery += strWhere + " DGDR.CR_CDP_DATOS_PRINCIPALES.COD_CH ='" + Demarcacion + "'"
                strWhere = " AND "
            End If
            If (CCAA <> String.Empty) Then
                strQuery += strWhere + " DGDR.CR_APR_AUX_PROVINCIA.COD_CCAA ='" + Right("00" + CCAA, 2) + "'"
                strWhere = " AND "
            End If
            If (Provincia <> String.Empty) Then
                strQuery += strWhere + " DGDR.CR_CDP_DATOS_PRINCIPALES.COD_PROVINCIA ='" + Right("00" + Provincia, 2) + "'"
                strWhere = " AND "
            End If
            If (Municipio <> String.Empty) Then
                strQuery += strWhere + " DGDR.CR_CDT_DATOS_TERRITORIALES.COD_INE ='" + Right("0000" + Municipio, 5) + "'"
                strWhere = " AND "
            End If
            strQuery += " ORDER BY(DGDR.REGADIOS_H2020.NOMBRE_CR)"

            aDS = ExecuteQuery(strQuery, "DGDR.REGADIOS_H2020", strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aData As New Regadio()
                aData.Id = aRow("COD_CR")
                aData.nombre = IIf(IsDBNull(aRow("NOMBRE_CR")), "", aRow("NOMBRE_CR"))
                aData.documento = IIf(IsDBNull(aRow("DOC")), "", aRow("DOC"))
                arrValores(i) = aData
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Informaci�n Regad�o 2020", EnableSession:=True)> _
    Public Function dameInformacionCRRegadio2020(ByVal NombreColectivo As String, ByVal CodigoColectivo As String, _
        ByVal Demarcacion As String, ByVal CCAA As String, ByVal Provincia As String, ByVal Municipio As String) As Regadio()
        Dim strQuery As String
        Dim strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strWhere As String = " WHERE "
        Dim arrValores() As Regadio
        Try

            strDatasetName = "REGADIOS_H2020"

            strQuery = "SELECT DISTINCT(DGDR.REGADIOS_H2020.COD_CR) COD_CR, DGDR.REGADIOS_H2020.NOMBRE_CR NOMBRE_CR, DGDR.REGADIOS_H2020.DOCUMENTO DOC"
            strQuery += " FROM DGDR.REGADIOS_H2020" + _
                " INNER JOIN DGDR.CR_CDP_DATOS_PRINCIPALES ON DGDR.CR_CDP_DATOS_PRINCIPALES.COD_CR = DGDR.REGADIOS_H2020.COD_CR" + _
                " INNER JOIN DGDR.CR_CDT_DATOS_TERRITORIALES ON DGDR.CR_CDT_DATOS_TERRITORIALES.COD_CR = DGDR.REGADIOS_H2020.COD_CR" + _
                " INNER JOIN DGDR.CR_APR_AUX_PROVINCIA ON DGDR.CR_APR_AUX_PROVINCIA.COD_PROVINCIA = DGDR.CR_CDP_DATOS_PRINCIPALES.COD_PROVINCIA" + _
                " WHERE DGDR.CR_CDP_DATOS_PRINCIPALES.BO_CR='1'"
            strWhere = " AND "

            If (CodigoColectivo <> String.Empty) Then
                strQuery += strWhere + " DGDR.REGADIOS_H2020.COD_CR ='" + CodigoColectivo + "'"
                strWhere = " AND "
            End If

            If (NombreColectivo <> String.Empty) Then
                strQuery += strWhere + " UPPER(DGDR.REGADIOS_H2020.NOMBRE_CR) LIKE '%" + NombreColectivo.ToUpper() + "%'"
                strWhere = " AND "
            End If

            If (Demarcacion <> String.Empty) Then
                strQuery += strWhere + " DGDR.CR_CDP_DATOS_PRINCIPALES.COD_CH ='" + Demarcacion + "'"
                strWhere = " AND "
            End If
            If (CCAA <> String.Empty) Then
                strQuery += strWhere + " DGDR.CR_APR_AUX_PROVINCIA.COD_CCAA ='" + Right("00" + CCAA, 2) + "'"
                strWhere = " AND "
            End If
            If (Provincia <> String.Empty) Then
                strQuery += strWhere + " DGDR.CR_CDP_DATOS_PRINCIPALES.COD_PROVINCIA ='" + Right("00" + Provincia, 2) + "'"
                strWhere = " AND "
            End If
            If (Municipio <> String.Empty) Then
                strQuery += strWhere + " DGDR.CR_CDT_DATOS_TERRITORIALES.COD_INE ='" + Right("0000" + Municipio, 5) + "'"
                strWhere = " AND "
            End If
            strQuery += " ORDER BY(DGDR.REGADIOS_H2020.NOMBRE_CR)"

            aDS = ExecuteQuery(strQuery, "DGDR.REGADIOS_H2020", strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aData As New Regadio()
                aData.Id = aRow("COD_CR")
                aData.nombre = IIf(IsDBNull(aRow("NOMBRE_CR")), "", aRow("NOMBRE_CR"))
                aData.documento = IIf(IsDBNull(aRow("DOC")), "", aRow("DOC"))
                arrValores(i) = aData
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Etpas Camino Natural", EnableSession:=True)> _
    Public Function dameEtapasCamino(ByVal IdEnlace As String) As Segmento()

        Dim strQuery As String
        Dim strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strWhere As String = " WHERE "
        Dim arrValores() As Segmento

        Try

            strDatasetName = "CN_ETAPAS_ET"

            strQuery = "select DISTINCT IDETAPA1, NOMBRE_ETA "
            strQuery += " from DGDR.CN_ETAPAS_ET WHERE substring(idetapa1  FROM 1 FOR 4) = '" & IdEnlace & "'"
            strQuery += "ORDER BY idetapa1 ASC"
            aDS = ExecuteQuery(strQuery, "CN_ETAPAS_ET", strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aData As New Segmento()
                aData.Id = aRow("IDETAPA1")
                aData.nombre = IIf(IsDBNull(aRow("NOMBRE_ETA")), "", aRow("NOMBRE_ETA"))
                arrValores(i) = aData
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Caminos Naturales", EnableSession:=True)> _
    Public Function dameCaminosNaturales(ByVal CCAA As String, ByVal CodigoProvincia As String, ByVal CodigoMunicipio As String, _
        ByVal Sector As String, ByVal Nombre As String) As CaminoNatural()

        Dim strQuery As String
        Dim strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim strWhere As String = " WHERE "
        Dim arrValores() As CaminoNatural

        Try

            strDatasetName = "CN_CAMINOS"

            strQuery = "select MUNI.IDENLACE IDENLACE, max(CAMINOS.NOMBRE) NOMBRE "
            strQuery += " from DGDR.CN_CAMINOS CAMINOS" + _
                " inner join DGDR.CN_TBMUNICIPIOS muni ON CAMINOS.IDCAMINO = MUNI.IDENLACE inner Join DGDR.CN_TBMUNICIPIOS_AUX aux on" + _
                " TO_NUMBER(AUX.INE_CODC,'99999') = TO_NUMBER(MUNI.IDINE_CODC,'99999') "
            '" AUX.INE_CODC = MUNI.IDINE_CODC "
            strWhere = " WHERE "

            If (CCAA <> String.Empty) Then
                strQuery += strWhere + " AUX.CCAA_COD =" + CCAA
                strWhere = " AND "
            End If

            If (CodigoProvincia <> String.Empty) Then
                strQuery += strWhere + " AUX.PROV_COD = " + CodigoProvincia
                strWhere = " AND "
            End If

            If (CodigoMunicipio <> String.Empty) Then
                strQuery += strWhere + " AUX.INE_CODC = '" + CodigoMunicipio + "'"
                strWhere = " AND "
            End If
            
            If (Sector <> String.Empty) Then
                strQuery += strWhere + " SUBSTRING(MUNI.IDENLACE FROM 1 FOR 2) ='" + Right("00" + Sector, 2) + "'"
                strWhere = " AND "
            End If
            If (Nombre <> String.Empty) Then
                strQuery += strWhere + " lower(CAMINOS.NOMBRE) like '%" + Nombre.ToLower() + "%'"
                strWhere = " AND "
            End If
            strQuery += " group by MUNI.IDENLACE ORDER BY max(CAMINOS.NOMBRE) "

            aDS = ExecuteQuery(strQuery, "CN_CAMINOS", strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aData As New CaminoNatural()
                aData.Id = aRow("IDENLACE")
                aData.nombre = IIf(IsDBNull(aRow("NOMBRE")), "", aRow("NOMBRE"))
                arrValores(i) = aData
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Aprovechamiento", EnableSession:=True)> _
    Public Function DameAprovechamientos(ByVal strWhere As String) As Aprovechamiento()
        Dim strQuery As String
        Dim strTabla As String
        Dim strDatasetName As String
        Dim strAprbID As String
        Dim strAprbNombreCent As String
        Dim strAprbCorriente As String
        Dim strAprbSBrutoM As String
        Dim strAprbPotenciaT As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Aprovechamiento
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaAprovechamiento")
            strDatasetName = strTabla

            strAprbID = System.Configuration.ConfigurationManager.AppSettings("aprbID")
            strAprbNombreCent = System.Configuration.ConfigurationManager.AppSettings("aprbNombreCent")
            strAprbCorriente = System.Configuration.ConfigurationManager.AppSettings("aprbCorriente")
            strAprbSBrutoM = System.Configuration.ConfigurationManager.AppSettings("aprbSBrutoM")
            strAprbPotenciaT = System.Configuration.ConfigurationManager.AppSettings("aprbPotenciaT")

            strQuery = "SELECT "
            'strQuery = strQuery + " ID, NOMBRECENT , CORRIENTE, SBRUTO_M, POTENCIA_T"
            strQuery = strQuery + strAprbID + ","
            strQuery = strQuery + strAprbNombreCent + ","
            strQuery = strQuery + strAprbCorriente + ","
            strQuery = strQuery + strAprbSBrutoM + ","
            strQuery = strQuery + strAprbPotenciaT
            strQuery = strQuery + " FROM " + strTabla + " "
            If (strWhere <> String.Empty) Then
                ' strQuery += " WHERE TRANSLATE(UPPER( " + strAprbNombreCent + "), '��������������������', 'AEIOUAEIOUAEIOUAEIOU') "
                ' strQuery += " LIKE TRANSLATE(UPPER('%" + strWhere + "), '��������������������', 'AEIOUAEIOUAEIOUAEIOU') "
                strQuery += " WHERE " + strWhere
            End If
            strQuery += " ORDER BY(" + strAprbNombreCent + ")"


            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)

            Dim i As Integer
            Dim aAprov As Aprovechamiento

            For Each aRow In aDS.Tables(0).Rows
                aAprov = New Aprovechamiento
                aAprov.ID = aRow("ID")
                aAprov.NOMBRECENT = aRow("NOMBRECENT")
                aAprov.Salto = aRow("SBRUTO_M")
                aAprov.Potencia = aRow("POTENCIA_T")
                aAprov.Corriente = aRow("CORRIENTE")
                arrValores(i) = aAprov
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Cauces", EnableSession:=True)> _
    Public Function DameCauces(ByVal idDemarcacion As String) As Cauce()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdDem As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Cauce
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaCauces")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdCauces")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreCauces")
            strCampoIdDem = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdDem + "='" + idDemarcacion + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCauce As New Cauce()
                aCauce.Id = aRow(strCampoId)
                aCauce.nombre = aRow(strCampoNombre)
                arrValores(i) = aCauce
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Presas", EnableSession:=True)> _
    Public Function DamePresas(ByVal idDemarcacion As String) As Presa()
        Dim strQuery As String
        Dim strTabla As String, strTablaAdmtivos As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoNombre2 As String, strCampoIdDem As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Presa
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaPresas")
            strTablaAdmtivos = System.Configuration.ConfigurationManager.AppSettings("TablaPresasAdmtivos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdPresas")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombrePresas")
            strCampoNombre2 = System.Configuration.ConfigurationManager.AppSettings("NombrePresas2")
            strCampoIdDem = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + "," + strTablaAdmtivos + " WHERE " + strCampoIdDem + "='" + idDemarcacion + "' AND " + strCampoNombre + " = " + strCampoNombre2 + " AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"


            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aPresa As New Presa()
                aPresa.Id = aRow(strCampoId)
                aPresa.nombre = aRow(strCampoNombre)
                arrValores(i) = aPresa
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Servicios Elementos Biol�gicos", EnableSession:=True)> _
    Public Function ObtenerServiciosElemBiol(ByVal IdTaxon As Integer, ByVal IdElemBio As Integer) As Data.DataTable()
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aDT As Data.DataTable
        Dim res(0) As Data.DataTable
        Dim AnioMax As Integer
        Try

            'strQuery = "SELECT MAX(ANO_MUESTRA) ANO_MAX_MUESTRA FROM DGA_DPH.CEB_MV_TAX_MUESTREOS_TAXONES WHERE ID_TAXON = " & IdTaxon
            strQuery = "SELECT SER_CO_ID, T.ANO_MUESTRA, T.ID_TAXON FROM " & _
                "(SELECT A.ANO_MUESTRA, B.ID_TAXON, " & IdElemBio & " ID_ELEM_BIO " & _
                " FROM (select DISTINCT ANO_MUESTRA from DGA_DPH.CEB_VPMSUP_COMPLETO ) A Left Join (select DISTINCT ANO_MUESTRA, ID_TAXON from DGA_DPH.CEB_VPMSUP_COMPLETO WHERE " & _
                " ID_TAXON = " & IdTaxon & " AND ID_ELEM_BIO = " & IdElemBio & ") B ON A.ANO_MUESTRA = B.ANO_MUESTRA ) T LEFT JOIN VIS_SEB_SERVICIOS_ELEMENTOSBIO S ON S.ANO_MUESTRA = T.ANO_MUESTRA AND S.ID_ELEM_BIO = T.ID_ELEM_BIO ORDER BY T.ANO_MUESTRA Asc"

            aDS = ExecuteQuery(strQuery, "ServiciosTaxon", "ServiciosTaxon")
            res(0) = aDS.Tables(0).Copy()
            datatableFieldsToUpper(res(0))

            Return res
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Private Sub datatableFieldsToUpper(ByRef dt As DataTable)
        For i As Integer = 0 To dt.Columns.Count - 1
            dt.Columns(i).ColumnName = dt.Columns(i).ColumnName.ToUpper()
        Next
    End Sub

    <System.Web.Services.WebMethod(Description:="Obtener Fases Presas", EnableSession:=True)> _
    Public Function DameFasesTitularesYTipos() As Data.DataTable()
        Dim strQuery As String
        Dim strTablaAdmtivos As String, strIdDemar As String, strCampoDemar As String, strDatasetName As String, strCampoFase As String, strCampoTitu As String, strTablaDemar As String, strFasesPosiblesSNCZI As String
        Dim aDS As Data.DataSet
        Dim aDT As Data.DataTable
        Dim arrValores() As Data.DataTable
        Try

            strTablaAdmtivos = System.Configuration.ConfigurationManager.AppSettings("TablaPresasAdmtivos")
            strIdDemar = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strTablaDemar = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strCampoFase = System.Configuration.ConfigurationManager.AppSettings("NombreFases")
            strFasesPosiblesSNCZI = System.Configuration.ConfigurationManager.AppSettings("NombreFasesPosiblesSNCZI")
            strCampoDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strCampoTitu = System.Configuration.ConfigurationManager.AppSettings("NombreTitular")
            strDatasetName = strTablaAdmtivos
            '  aDSTotal = New Data.DataSet(strDatasetName)
            ReDim arrValores(3)
            strQuery = "SELECT DISTINCT(" + strCampoFase + ") FROM " + strTablaAdmtivos + " WHERE " + strCampoFase + " IN (" + strFasesPosiblesSNCZI + ") ORDER BY(" + strCampoFase + ")"
            aDS = ExecuteQuery(strQuery, "Fases", strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(0) = aDT
            ' aDSTotal.Tables.Add(aDS.Tables(0).Copy())

            strQuery = "SELECT DISTINCT(" + strCampoTitu + ") FROM " + strTablaAdmtivos + " ORDER BY(" + strCampoTitu + ")"
            aDS = ExecuteQuery(strQuery, "Titulares", strDatasetName)
            ' aDSTotal.Tables.Add(aDS.Tables(0).Copy())
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(1) = aDT

            strQuery = "SELECT DISTINCT(" + strIdDemar + ") , " + strCampoDemar + " FROM " + strTablaDemar + " ORDER BY(" + strCampoDemar + ")"
            aDS = ExecuteQuery(strQuery, "Demarcaciones", strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(2) = aDT
            ' aDSTotal.Tables.Add(aDS.Tables(0).Copy())

            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Fases Presas", EnableSession:=True)> _
    Public Function DameFasesTitularesYTiposGISPE() As Data.DataTable()
        Dim strQuery As String
        Dim strTablaAdmtivos As String, strIdDemar As String, strCampoDemar As String, strDatasetName As String, strCampoFase As String, strCampoTitu As String, strTablaDemar As String, strFasesPosiblesSNCZI As String
        Dim aDS As Data.DataSet
        Dim aDT As Data.DataTable
        Dim arrValores() As Data.DataTable
        Try

            strTablaAdmtivos = System.Configuration.ConfigurationManager.AppSettings("TablaPresasAdmtivos")
            strIdDemar = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strTablaDemar = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            'strCampoFase = System.Configuration.ConfigurationManager.AppSettings("NombreFases")
            'strFasesPosiblesSNCZI = System.Configuration.ConfigurationManager.AppSettings("NombreFasesPosiblesSNCZI")
            strCampoDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strCampoTitu = System.Configuration.ConfigurationManager.AppSettings("NombreTitular")
            strDatasetName = "Tabla" 'strTablaAdmtivos

            ReDim arrValores(3)
            strQuery = "SELECT DISTINCT(FASE) FROM " + _
                "(" + _
                "SELECT DISTINCT (FASE) FASE FROM " + _
                "EGISPE_GEO.VEGIS_INF_BALSA " + _
                "UNION " + _
                "SELECT DISTINCT (FASE) FASE FROM " + _
                "EGISPE_GEO.VEGIS_INF_PRESA " + _
                "UNION " + _
                "SELECT DISTINCT (FASE) FASE FROM " + _
                "EGISPE_GEO.VEGIS_EMB_PRESA " + _
                "UNION " + _
                "SELECT DISTINCT (FASE) FASE FROM " + _
                "EGISPE_GEO.VEGIS_EMB_BALSA " + _
                ") WHERE FASE IS NOT NULL ORDER BY FASE"
            aDS = ExecuteQueryGISPE(strQuery, "Fases", strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(0) = aDT


            'strQuery = "SELECT DISTINCT(TITULAR) FROM " + _
            '    "(" + _
            '    "SELECT DISTINCT (TITULAR) TITULAR FROM " + _
            '    "EGISPE_GEO.VEGIS_EMB_PRESA " + _
            '    "UNION " + _
            '    "SELECT DISTINCT (TITULAR) TITULAR FROM " + _
            '    "EGISPE_GEO.VEGIS_EMB_BALSA " + _
            '    ")"

            ' aDSTotal.Tables.Add(aDS.Tables(0).Copy())

            strQuery = "SELECT DISTINCT(" + strCampoTitu + ") FROM " + strTablaAdmtivos + " ORDER BY(" + strCampoTitu + ")"
            aDS = ExecuteQuery(strQuery, "Titulares", strDatasetName)

            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(1) = aDT

            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(1) = aDT

            strQuery = "SELECT DISTINCT(" + strIdDemar + ") , " + strCampoDemar + " FROM " + strTablaDemar + " ORDER BY(" + strCampoDemar + ")"
            aDS = ExecuteQuery(strQuery, "Demarcaciones", strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(2) = aDT
            ' aDSTotal.Tables.Add(aDS.Tables(0).Copy())

            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Criterio y Origen ARPSIS", EnableSession:=True)> _
    Public Function DameOrigenYCriteriosARPSIS() As Data.DataTable()
        Dim strQuery As String
        Dim strDatasetName As String, strCampoOrigen As String, strCampoCriterio As String, strCampoEstado As String, strTabla As String
        Dim aDS As Data.DataSet
        Dim aDT As Data.DataTable
        Dim arrValores() As Data.DataTable
        Try


            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaARPSIS")
            strCampoOrigen = System.Configuration.ConfigurationManager.AppSettings("OrigenARPSIS")
            strCampoCriterio = System.Configuration.ConfigurationManager.AppSettings("CriterioARPSIS")
            strCampoEstado = System.Configuration.ConfigurationManager.AppSettings("EstadoARPSIS")
            strDatasetName = strTabla

            ReDim arrValores(3)
            strQuery = "SELECT DISTINCT(" + strCampoOrigen + ") FROM " + strTabla + " ORDER BY(" + strCampoOrigen + ")"
            aDS = ExecuteQuery(strQuery, "Origen", strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(0) = aDT

            strQuery = "SELECT DISTINCT(" + strCampoCriterio + ") FROM " + strTabla + " ORDER BY(" + strCampoCriterio + ")"
            aDS = ExecuteQuery(strQuery, "Criterios", strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(1) = aDT

            strQuery = "SELECT DISTINCT(" + strCampoEstado + ") FROM " + strTabla + " ORDER BY(" + strCampoEstado + ")"
            aDS = ExecuteQuery(strQuery, "Estados", strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(2) = aDT

            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Embalses", EnableSession:=True)> _
    Public Function DameEmbalses(ByVal idDemarcacion As String) As Embalse()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoIdDem As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Embalse
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaEmbalses")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdEmbalses")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreEmbalses")
            strCampoIdDem = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + " FROM " + strTabla + " WHERE " + strCampoIdDem + "='" + idDemarcacion + "' AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aEmbalse As New Embalse()
                aEmbalse.Id = aRow(strCampoId)
                aEmbalse.nombre = aRow(strCampoNombre)
                arrValores(i) = aEmbalse
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Caudales", EnableSession:=True)> _
    Public Function DameListadoSaih(ByVal codDemar As String, ByVal filtro As String) As Caudal()
        Dim strQuery As String, strQueryAux As String, strWhereAux As String
        Dim strTablaDem As String, strTablaC As String, strTablaE As String, strTablaP As String, strCampoNomDemar As String, strCampoCodDemar As String, strDatasetName As String, strCampoIdPunto As String, strCampoIdTipo As String, strCampoTipo As String, strCampoId As String, strCampoNombre As String, strCampoIdSaih As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Caudal
        Try
            strWhereAux = ""
            strTablaDem = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strCampoCodDemar = System.Configuration.ConfigurationManager.AppSettings("idDemarcaciones")
            strCampoNomDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strTablaC = System.Configuration.ConfigurationManager.AppSettings("TablaCaudales")
            strTablaE = System.Configuration.ConfigurationManager.AppSettings("TablaEmbalsesSaih")
            strTablaP = System.Configuration.ConfigurationManager.AppSettings("TablaPluviometros")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdCaudales")
            strCampoIdTipo = System.Configuration.ConfigurationManager.AppSettings("IdTipo")
            strCampoTipo = System.Configuration.ConfigurationManager.AppSettings("Tipo")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreCaudales")
            strCampoIdSaih = System.Configuration.ConfigurationManager.AppSettings("IdSaih")
            strCampoIdPunto = System.Configuration.ConfigurationManager.AppSettings("IdPunto")
            strCampoXUtm30 = "UTM_X_ET"
            strCampoYUtm30 = "UTM_Y_ET"
            strDatasetName = strTablaC
            If Not (codDemar.Equals("-1")) Then
                strQueryAux = "='" + codDemar + "'"
            Else
                strQueryAux = " IS NOT NULL "
            End If
            If Not (filtro.Equals("")) Then
                strWhereAux = " WHERE " + filtro
            End If


            'strQuery = "SELECT * FROM(SELECT DISTINCT(" + strTablaC + "." + strCampoId + ")," + strCampoNomDemar + "," + strCampoNombre + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strCampoIdPunto + "," + strCampoIdTipo + "," + strCampoTipo + "," + strTablaC + "." + strCampoCodDemar + "," + strCampoIdSaih + " FROM " + strTablaC & _
            ' " INNER JOIN " + strTablaDem + " ON " + strTablaC + "." + strCampoCodDemar + " = " + strTablaDem + "." + strCampoCodDemar + " AND " + strTablaC + "." + strCampoCodDemar + strQueryAux & _
            '" UNION SELECT DISTINCT(" + strTablaE + "." + strCampoId + ")," + strCampoNomDemar + "," + strCampoNombre + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strCampoIdPunto + "," + strCampoIdTipo + "," + strCampoTipo + "," + strTablaE + "." + strCampoCodDemar + "," + strCampoIdSaih + " FROM " + strTablaE & _
            '" INNER JOIN " + strTablaDem + " ON " + strTablaE + "." + strCampoCodDemar + " = " + strTablaDem + "." + strCampoCodDemar + " AND " + strTablaE + "." + strCampoCodDemar + strQueryAux & _
            '" UNION SELECT DISTINCT(" + strTablaP + "." + strCampoId + ")," + strCampoNomDemar + "," + strCampoNombre + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strCampoIdPunto + "," + strCampoIdTipo + "," + strCampoTipo + "," + strTablaP + "." + strCampoCodDemar + "," + strCampoIdSaih + " FROM " + strTablaP & _
            '" INNER JOIN " + strTablaDem + " ON " + strTablaP + "." + strCampoCodDemar + " = " + strTablaDem + "." + strCampoCodDemar + " AND " + strTablaP + "." + strCampoCodDemar + strQueryAux


            'strQuery += " AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + "))"

            strQuery = "SELECT * FROM(SELECT DISTINCT(" + strTablaC + "." + strCampoId + ")," + strCampoNomDemar + "," + strCampoNombre + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strCampoIdPunto + "," + strCampoIdTipo + "," + strCampoTipo + "," + strTablaC + "." + strCampoCodDemar + "," + strCampoIdSaih + " FROM " + strTablaC & _
            " INNER JOIN " + strTablaDem + " ON " + strTablaC + "." + strCampoCodDemar + " = " + strTablaDem + "." + strCampoCodDemar + " AND " + strTablaC + "." + strCampoCodDemar + strQueryAux & _
            " UNION SELECT DISTINCT(" + strTablaE + "." + strCampoId + ")," + strCampoNomDemar + "," + strCampoNombre + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strCampoIdPunto + "," + strCampoIdTipo + "," + strCampoTipo + "," + strTablaE + "." + strCampoCodDemar + "," + strCampoIdSaih + " FROM " + strTablaE & _
            " INNER JOIN " + strTablaDem + " ON " + strTablaE + "." + strCampoCodDemar + " = " + strTablaDem + "." + strCampoCodDemar + " AND " + strTablaE + "." + strCampoCodDemar + strQueryAux & _
            " UNION SELECT DISTINCT(" + strTablaP + "." + strCampoId + ")," + strCampoNomDemar + "," + strCampoNombre + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strCampoIdPunto + "," + strCampoIdTipo + "," + strCampoTipo + "," + strTablaP + "." + strCampoCodDemar + "," + strCampoIdSaih + " FROM " + strTablaP & _
            " INNER JOIN " + strTablaDem + " ON " + strTablaP + "." + strCampoCodDemar + " = " + strTablaDem + "." + strCampoCodDemar + " AND " + strTablaP + "." + strCampoCodDemar + strQueryAux


            strQuery += " AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0  "
            strQuery += " ORDER BY(" + strCampoNombre + ") ) a " + strWhereAux


            aDS = ExecuteQuery(strQuery, strTablaC, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCaudal As New Caudal()
                aCaudal.Id = aRow(strCampoId)
                aCaudal.nombre = aRow(strCampoNombre)
                aCaudal.Cuenca = aRow(strCampoIdSaih)
                aCaudal.Tipo = aRow(strCampoTipo)
                aCaudal.IdTipo = aRow(strCampoIdTipo)
                aCaudal.IdPunto = aRow(strCampoIdPunto)
                aCaudal.XUtm30 = aRow(strCampoXUtm30)
                aCaudal.YUtm30 = aRow(strCampoYUtm30)
                aCaudal.CodDemar = aRow(strCampoCodDemar)
                aCaudal.NomDemar = aRow(strCampoNomDemar)
                arrValores(i) = aCaudal
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Obtener Caudales", EnableSession:=True)> _
    Public Function DameCaudales(ByVal codDemar As String, ByVal filtro As String) As Caudal()
        Dim strQuery As String, strWhereAux As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTabla As String, strTabla2 As String, strCampoNomDemar As String, strCampoCodDemar As String, strDatasetName As String, strCampoIdPunto As String, strCampoIdTipo As String, strCampoTipo As String, strCampoId As String, strCampoNombre As String, strCampoIdSaih As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Caudal
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaCaudales")
            strTabla2 = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdCaudales")
            strCampoIdTipo = System.Configuration.ConfigurationManager.AppSettings("IdTipo")
            strCampoTipo = System.Configuration.ConfigurationManager.AppSettings("Tipo")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreCaudales")
            strCampoIdSaih = System.Configuration.ConfigurationManager.AppSettings("IdSaih")
            strCampoCodDemar = System.Configuration.ConfigurationManager.AppSettings("idDemarcaciones")
            strCampoNomDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strCampoIdPunto = System.Configuration.ConfigurationManager.AppSettings("IdPunto")
            strCampoXUtm30 = "UTM_X_ET"
            strCampoYUtm30 = "UTM_Y_ET"
            strDatasetName = strTabla

            strQuery = "SELECT * FROM ("
            strQuery += "SELECT DISTINCT(" + strTabla + "." + strCampoId + ")," + strCampoNomDemar + "," + strCampoNombre + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strCampoIdPunto + "," + strCampoIdTipo + "," + strCampoTipo + "," + strTabla + "." + strCampoCodDemar + "," + strCampoIdSaih + " FROM " + strTabla & _
            " INNER JOIN " + strTabla2 + " ON " + strTabla + "." + strCampoCodDemar + " = " + strTabla2 + "." + strCampoCodDemar + " AND " + strTabla + "." + strCampoCodDemar
            If Not (codDemar.Equals("-1")) Then
                strQuery += "='" + codDemar + "'"
            Else
                strQuery += " IS NOT NULL "
            End If
            'If Not (filtro.Equals("")) Then
            '    strQuery += " AND " + filtro
            'End If
            'strQuery += " AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ORDER BY(" + strCampoNombre + ")"

            strQuery += " AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ) a"
            If Not (filtro.Equals("")) Then
                strQuery += " WHERE " + filtro
            End If
            strQuery += " ORDER BY(" + strCampoNombre + ")"


            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCaudal As New Caudal()
                aCaudal.Id = aRow(strCampoId)
                aCaudal.nombre = aRow(strCampoNombre)
                aCaudal.Cuenca = aRow(strCampoIdSaih)
                aCaudal.Tipo = aRow(strCampoTipo)
                aCaudal.IdTipo = aRow(strCampoIdTipo)
                aCaudal.IdPunto = aRow(strCampoIdPunto)
                aCaudal.XUtm30 = aRow(strCampoXUtm30)
                aCaudal.YUtm30 = aRow(strCampoYUtm30)
                aCaudal.CodDemar = aRow(strCampoCodDemar)
                aCaudal.NomDemar = aRow(strCampoNomDemar)
                arrValores(i) = aCaudal
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Embalses Saih", EnableSession:=True)> _
    Public Function DameEmbalsesSaih(ByVal codDemar As String, ByVal filtro As String) As Embalse()
        Dim strQuery As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTabla As String, strTabla2 As String, strCampoNomDemar As String, strCampoCodDemar As String, strDatasetName As String, strCampoIdPunto As String, strCampoIdTipo As String, strCampoTipo As String, strCampoId As String, strCampoNombre As String, strCampoIdSaih As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Embalse

        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaEmbalsesSaih")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdEmbalsesSaih")
            strCampoIdTipo = System.Configuration.ConfigurationManager.AppSettings("IdTipo")
            strCampoTipo = System.Configuration.ConfigurationManager.AppSettings("Tipo")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreEmbalsesSaih")
            strCampoIdSaih = System.Configuration.ConfigurationManager.AppSettings("IdSaih")
            strCampoIdPunto = System.Configuration.ConfigurationManager.AppSettings("IdPunto")
            strTabla2 = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strCampoCodDemar = System.Configuration.ConfigurationManager.AppSettings("idDemarcaciones")
            strCampoNomDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")

            strCampoXUtm30 = "UTM_X_ET"
            strCampoYUtm30 = "UTM_Y_ET"
            strDatasetName = strTabla

            strQuery = "SELECT * FROM ("
            strQuery += "SELECT DISTINCT(" + strTabla + "." + strCampoId + ")," + strCampoNomDemar + "," + strCampoNombre + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strCampoIdPunto + "," + strCampoIdTipo + "," + strCampoTipo + "," + strTabla + "." + strCampoCodDemar + "," + strCampoIdSaih + " FROM " + strTabla & _
            " INNER JOIN " + strTabla2 + " ON " + strTabla + "." + strCampoCodDemar + " = " + strTabla2 + "." + strCampoCodDemar + " AND " + strTabla + "." + strCampoCodDemar

            If Not (codDemar.Equals("-1")) Then
                strQuery += "='" + codDemar + "'"
            Else
                strQuery += " IS NOT NULL "
            End If

            strQuery += " AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0 ) a "

            If Not (filtro.Equals("")) Then
                strQuery += " WHERE " + filtro
            End If

            strQuery += " ORDER BY(" + strCampoNombre + ")"


            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aEmbalse As New Embalse()
                aEmbalse.Id = aRow(strCampoId)
                aEmbalse.nombre = aRow(strCampoNombre)
                aEmbalse.Tipo = aRow(strCampoTipo)
                aEmbalse.IdTipo = aRow(strCampoIdTipo)
                aEmbalse.IdPunto = aRow(strCampoIdPunto)
                aEmbalse.Cuenca = aRow(strCampoIdSaih)
                aEmbalse.XUtm30 = aRow(strCampoXUtm30)
                aEmbalse.YUtm30 = aRow(strCampoYUtm30)
                aEmbalse.CodDemar = aRow(strCampoCodDemar)
                aEmbalse.NomDemar = aRow(strCampoNomDemar)
                arrValores(i) = aEmbalse
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Piez�metros por UH", EnableSession:=True)> _
    Public Function DamePiezometrosUH(ByVal codDemar As String, ByVal idMasa As String, ByVal strFecha As String) As Piezometro()
        Dim strQuery As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTabla As String, strDatasetName As String, strCampoCota As String, strCampoId As String, strCampoNombre As String, strCampoXUtmET As String, strCampoYUtmET As String
        Dim strCampoDescripcion As String, strCampoProvincia As String, strCampoMunicipio As String, strCampoDemar As String, strCampoMasa As String, strCampoUH As String
        Dim strCampoFecha As String, strCampoMedidas As String, strIdDemarPiezom As String, strIdDemar As String, strIdMasa As String, strIdProvincia As String, strIdMunicipio As String
        Dim strWhereMasa As String, strProf As String, strIdUH As String, strUrlBaseFicha As String, strCampoNivel As String, strCampoFechaNivel As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Piezometro

        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaPiezometros")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdPiezometros")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombrePiezometros")
            strCampoDescripcion = System.Configuration.ConfigurationManager.AppSettings("DescPiezometros")
            strCampoProvincia = System.Configuration.ConfigurationManager.AppSettings("NombreProvincias")
            strCampoMunicipio = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipios")
            strCampoDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strCampoUH = System.Configuration.ConfigurationManager.AppSettings("NombreUHidrogeologicas")
            strIdUH = System.Configuration.ConfigurationManager.AppSettings("IdUHPiezometros")
            strCampoXUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordXPiezometros")
            strCampoYUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordYPiezometros")
            strCampoXUtmET = System.Configuration.ConfigurationManager.AppSettings("CoordXPiezometrosET")
            strCampoYUtmET = System.Configuration.ConfigurationManager.AppSettings("CoordYPiezometrosET")
            strCampoMasa = System.Configuration.ConfigurationManager.AppSettings("NombreMasaAgua")
            strCampoFecha = System.Configuration.ConfigurationManager.AppSettings("FechaPiezometros")
            strCampoMedidas = System.Configuration.ConfigurationManager.AppSettings("NMedidasPiezometros")
            strIdDemarPiezom = System.Configuration.ConfigurationManager.AppSettings("idDemarPiezometros")
            strIdMasa = System.Configuration.ConfigurationManager.AppSettings("IdMasaPiezometros")
            strIdDemar = System.Configuration.ConfigurationManager.AppSettings("idDemarcaciones")
            strCampoCota = System.Configuration.ConfigurationManager.AppSettings("CotaPiezometros")
            strIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvinciaPiezometros")
            strIdMunicipio = System.Configuration.ConfigurationManager.AppSettings("IdMunicipioPiezometros")
            strCampoNivel = System.Configuration.ConfigurationManager.AppSettings("NivelPiezometros")
            strCampoFechaNivel = System.Configuration.ConfigurationManager.AppSettings("FechaNivelPiezometros")
            strUrlBaseFicha = System.Configuration.ConfigurationManager.AppSettings("URLBasePiezometros")
            strProf = System.Configuration.ConfigurationManager.AppSettings("ProfundidadPiezom")
            strDatasetName = strTabla

            If idMasa <> "" Then
                strWhereMasa = " AND " + strIdMasa + " ='" + idMasa + "'"
            Else
                strWhereMasa = ""
            End If
            If (strFecha = "") Then



                strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + "," + strCampoDescripcion + "," + strCampoCota + "," + strIdUH + ",PRO." + strCampoProvincia + "," + strCampoMunicipio + "," + strCampoUH + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strIdProvincia + "," + strProf + "," + "DEM." + strIdDemar + "," + strIdMasa + "," + "DEM." + strCampoDemar + "," + "MAS." + strCampoMasa + "," + strIdMunicipio + "," + strCampoXUtmET + "," + strCampoYUtmET + "," + strCampoFecha + "," + strCampoMedidas + ", 0 AS " + strCampoNivel + ", '' AS " + strCampoFechaNivel + " FROM " + strTabla & _
                           " WHERE " + strIdDemarPiezom + "='" + codDemar + "' " + strWhereMasa & _
                           "AND PIE.PIE_NUMPIE = PME.NUMPIE (+) " & _
                           "AND PIE.PIE_CPROVI = PRO.PROVC (+) " & _
                           "AND PIE.PIE_CPROVI = PRO.PROVC (+) " & _
                           "AND PIE.PIE_CPROVMUNI  = MUN.INE (+) " & _
                           "AND PIE.PIE_CUNIDH = UNI.UNI_CUNIDH (+) " & _
                           "AND PIE.PIE_CDEMH  = DEM.COD_DEMAR (+) " & _
                           "AND PIE.PIE_CMASA = MAS.COD_MSBTDM (+) " & _
                           "AND PIE.PIE_NUMPIE = NIV.NIV_NUMPIE (+) " & _
                           "GROUP BY PIE_NUMPIE,PIE_NOMBRE,PIE_DESCRI,PRO.PROV_NOM,MUN_NOM,DEM.COD_DEMAR,DEM.NOM_DEMAR,PIE_CPROVMUNI, " & _
                           "UNI_NOMUNI, PIE_YUTM30, PIE_XUTM30,PIE_Z,PIE_PROF, UTM_X_ET,UTM_Y_ET,PIE_CUNIDH,PIE_CPROVI,PIE.PIE_CMASA,MAS.NOM_MSBT,PIE_CMUNI " & _
                           "ORDER BY PIE_NOMBRE"
            Else

                Dim strMesAnios As String()
                Dim strMes As String, strAnio As String, strFechaDesde As String, strFechaHasta As String
                Dim intMes As Integer, intAnio As Integer
                strMesAnios = strFecha.Split("/")
                If (strMesAnios.Length() = 3) Then
                    strMes = strMesAnios(1)
                    If (IsNumeric(strMes)) Then
                        intMes = CInt(strMes)
                        If (intMes < 1 Or intMes > 12) Then
                            Return Nothing
                        End If
                    Else
                        Return Nothing
                    End If
                    strAnio = strMesAnios(2)
                    If (IsNumeric(strAnio)) Then
                        intAnio = CInt(strAnio)
                    Else
                        Return Nothing
                    End If
                Else
                    Return Nothing
                End If
                strFechaDesde = "01/" + strMes + "/" + strAnio
                If (intMes = 12) Then
                    intMes = 1
                    intAnio += 1
                Else
                    intMes += 1
                End If
                strMes = CStr(intMes)
                strAnio = CStr(intAnio)
                If (strMes.Length() = 1) Then
                    strMes = "0" + strMes
                End If
                strFechaHasta = "01/" + strMes + "/" + strAnio
                Dim strWhereFecha As String
                strWhereFecha = " AND NIV_FECPIE>='" + strFechaDesde + "' AND NIV_FECPIE<'" + strFechaHasta + "' "
                strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + "," + strCampoDescripcion + ", PRO." + strCampoProvincia + "," + strIdUH + "," + strCampoMunicipio + "," + strProf + "," + strCampoUH + "," + strCampoCota + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strIdProvincia + "," + "DEM." + strIdDemar + "," + strIdMasa + "," + "DEM." + strCampoDemar + "," + "MAS." + strCampoMasa + "," + strIdMunicipio + "," + strCampoXUtmET + "," + strCampoYUtmET + "," + strCampoFecha + "," + strCampoMedidas + "," + strCampoNivel + "," + strCampoFechaNivel + " FROM " + strTabla & _
                           " WHERE " + strIdDemarPiezom + "='" + codDemar + "' " + strWhereMasa + strWhereFecha & _
                           "AND PIE.PIE_NUMPIE = PME.IDPIEZ (+) " & _
                           "AND PIE.PIE_CPROVI = PRO.PROVC (+) " & _
                           "AND PIE.PIE_CPROVI = PRO.PROVC (+) " & _
                           "AND PIE.PIE_CPROVMUNI  = MUN.INE (+) " & _
                           "AND PIE.PIE_CUNIDH = UNI.UNI_CUNIDH (+) " & _
                           "AND PIE.PIE_CDEMH  = DEM.COD_DEMAR (+) " & _
                           "AND PIE.PIE_CMASA = MAS.COD_MSBTDM (+) " & _
                           "AND PIE.PIE_NUMPIE = NIV.NIV_NUMPIE (+) " & _
                           "GROUP BY PIE_NUMPIE,PIE_NOMBRE,PIE_DESCRI,PRO.PROV_NOM,MUN_NOM,DEM.COD_DEMAR,DEM.NOM_DEMAR,PIE_CPROVMUNI, " & _
                           "UNI_NOMUNI, PIE_YUTM30, PIE_XUTM30,PIE_PROF, PIE_Z,NIV_FECPIE,NIV_MEDIDA,UTM_X_ET,UTM_Y_ET,PIE_CUNIDH,PIE_CPROVI,PIE.PIE_CMASA,MAS.NOM_MSBT,PIE_CMUNI " & _
                           "ORDER BY PIE_NOMBRE"


            End If

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aPiezometro As New Piezometro()

                aPiezometro.Id = IIf(aRow(strCampoId).Equals(System.DBNull.Value), "", aRow(strCampoId))
                aPiezometro.nombre = IIf(aRow(strCampoNombre).Equals(System.DBNull.Value), "", aRow(strCampoNombre))
                aPiezometro.Descripcion = IIf(aRow(strCampoDescripcion).Equals(System.DBNull.Value), "", aRow(strCampoDescripcion))
                aPiezometro.Provincia = IIf(aRow(strCampoProvincia).Equals(System.DBNull.Value), "", aRow(strCampoProvincia))
                aPiezometro.Municipio = IIf(aRow(strCampoMunicipio).Equals(System.DBNull.Value), "", aRow(strCampoMunicipio))
                aPiezometro.Demarcacion = IIf(aRow(strCampoDemar).Equals(System.DBNull.Value), "", aRow(strCampoDemar))
                aPiezometro.Masa = IIf(aRow(strCampoMasa).Equals(System.DBNull.Value), "", aRow(strCampoMasa))
                aPiezometro.Cota = IIf(aRow(strCampoCota).Equals(System.DBNull.Value), 0, aRow(strCampoCota))
                aPiezometro.URLFicha = strUrlBaseFicha + aPiezometro.Id
                aPiezometro.Nivel = IIf(aRow(strCampoNivel).Equals(System.DBNull.Value), 0, aRow(strCampoNivel))
                Dim aDate As Date
                aDate = IIf(aRow(strCampoFecha).Equals(System.DBNull.Value), Nothing, aRow(strCampoFecha))
                If Not aDate = Nothing Then
                    aPiezometro.FechaInicio = aDate.ToString("dd/MM/yyyy")
                Else
                    aPiezometro.FechaInicio = ""
                End If
                aPiezometro.FechaNivel = aPiezometro.FechaInicio
                aPiezometro.NumMedidas = IIf(aRow(strCampoMedidas).Equals(System.DBNull.Value), 0, aRow(strCampoMedidas))
                aPiezometro.XUtm30 = IIf(aRow(strCampoXUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoXUtm30))
                aPiezometro.YUtm30 = IIf(aRow(strCampoYUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoYUtm30))
                aPiezometro.XUtmET = IIf(aRow(strCampoXUtmET).Equals(System.DBNull.Value), 0, aRow(strCampoXUtmET))
                aPiezometro.YUtmET = IIf(aRow(strCampoYUtmET).Equals(System.DBNull.Value), 0, aRow(strCampoYUtmET))
                aPiezometro.IdDemar = IIf(aRow(strIdDemar).Equals(System.DBNull.Value), "", aRow(strIdDemar))
                aPiezometro.IdMunicipio = IIf(aRow(strIdMunicipio).Equals(System.DBNull.Value), 0, aRow(strIdMunicipio))
                aPiezometro.IdProvincia = IIf(aRow(strIdProvincia).Equals(System.DBNull.Value), 0, aRow(strIdProvincia))
                aPiezometro.IdMasa = IIf(aRow(strIdMasa).Equals(System.DBNull.Value), "", aRow(strIdMasa))
                aPiezometro.Profundidad = IIf(aRow(strProf).Equals(System.DBNull.Value), "", aRow(strProf))
                aPiezometro.UH = IIf(aRow(strCampoUH).Equals(System.DBNull.Value), "", aRow(strCampoUH))
                aPiezometro.IdUH = IIf(aRow(strIdUH).Equals(System.DBNull.Value), "", aRow(strIdUH))
                arrValores(i) = aPiezometro
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Sondeos por UH", EnableSession:=True)> _
    Public Function DameSondeosUH(ByVal idCuenca As String, ByVal idUH As String) As Sondeo()
        Dim strQuery As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim strCampoDescripcion As String, strCampoProvincia As String, strCampoMunicipio As String, strCampoCuenca As String, strCampoUH As String, strCampoCota As String
        Dim strCampoFecha As String, strCampoMedidas As String, strIdCuenca As String, strIdUH As String, strIdProv As String, strIdMun As String
        Dim strWhereUH As String, strUrlBaseFicha As String, strFicha As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Sondeo

        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaSondeos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdSondeos")
            strCampoProvincia = System.Configuration.ConfigurationManager.AppSettings("NombreProvSondeos")
            strCampoMunicipio = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipioSondeos")
            strCampoCuenca = System.Configuration.ConfigurationManager.AppSettings("NombreCuencaSondeos")
            strCampoUH = System.Configuration.ConfigurationManager.AppSettings("NombreUHSondeos")
            strCampoXUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordXSondeos")
            strCampoYUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordYSondeos")
            strIdProv = System.Configuration.ConfigurationManager.AppSettings("IdProvSondeos")
            strIdMun = System.Configuration.ConfigurationManager.AppSettings("IdMunicSondeos")
            strIdCuenca = System.Configuration.ConfigurationManager.AppSettings("IdCuencasSondeos")
            strIdUH = System.Configuration.ConfigurationManager.AppSettings("IdUHidrogeologicasSondeos")
            strUrlBaseFicha = System.Configuration.ConfigurationManager.AppSettings("URLBaseSondeos")
            strDatasetName = strTabla

            If idUH <> "" Then
                strWhereUH = " AND OS." + strIdUH + " ='" + idUH + "'"
            Else
                strWhereUH = ""
            End If

            strQuery = "SELECT DISTINCT(OS." + strCampoId + ")," + strCampoMunicipio + "," + strCampoCuenca + "," + strCampoUH + "," + strCampoXUtm30 + "," + strCampoYUtm30 + ",PRO." + strCampoProvincia + ",PRO." + strIdProv + ",OS." + strIdMun + ",OS." + strIdCuenca + ",OS." + strIdUH + " FROM " + strTabla & _
                       " WHERE OS." + strIdCuenca + "='" + idCuenca + "' " + strWhereUH & _
                       "AND OS.N_SONDEO = DGSON.N_SONDEO " & _
                       "AND OS.COD_MUNI = MUN.COD_MUNI " & _
                       "AND OS.COD_CUENCA = CUEN.COD_CUENCA " & _
                       "AND (OS.COD_UNIDAD = UNI.COD_UNIDAD " & _
                       "AND OS.COD_CUENCA = UNI.COD_CUENCA) " & _
                       "AND SUBSTRING(OS.COD_MUNI FROM 1 FOR 2) = PRO.COD_PROVIN"



            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aSondeo As New Sondeo()

                aSondeo.Id = IIf(aRow(strCampoId).Equals(System.DBNull.Value), "", aRow(strCampoId))
                aSondeo.Municipio = IIf(aRow(strCampoMunicipio).Equals(System.DBNull.Value), "", aRow(strCampoMunicipio))
                aSondeo.Cuenca = IIf(aRow(strCampoCuenca).Equals(System.DBNull.Value), "", aRow(strCampoCuenca))
                aSondeo.UH = IIf(aRow(strCampoUH).Equals(System.DBNull.Value), "", aRow(strCampoUH))
                aSondeo.URLFicha = strUrlBaseFicha + aSondeo.Id

                aSondeo.Provincia = IIf(aRow(strCampoProvincia).Equals(System.DBNull.Value), "", aRow(strCampoProvincia))
                aSondeo.IdCuenca = IIf(aRow(strIdCuenca).Equals(System.DBNull.Value), "", aRow(strIdCuenca))
                aSondeo.IdMunicipio = IIf(aRow(strIdMun).Equals(System.DBNull.Value), "", aRow(strIdMun))
                aSondeo.IdProvincia = IIf(aRow(strIdProv).Equals(System.DBNull.Value), "", aRow(strIdProv))
                aSondeo.IdUH = IIf(aRow(strIdUH).Equals(System.DBNull.Value), "", aRow(strIdUH))

                aSondeo.XUtmET = IIf(aRow(strCampoXUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoXUtm30))
                aSondeo.YUtmET = IIf(aRow(strCampoYUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoYUtm30))
                arrValores(i) = aSondeo
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Obtener Playa por Id", EnableSession:=True)> _
    Public Function ObtenerPlayaPorID(ByVal idPlaya As String) As Playa()

        Dim strQuery As String
        Dim strTabla As String, strTablaGrafica As String, strDatasetName As String
        Dim strCampoId As String
        Dim strCampoNombre As String
        Dim strCampoDescProvincia As String
        Dim strCampoCodProvincia As String
        Dim strCampoCodMunicipio As String
        Dim strCampoCodCCAA As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTablaProvincias As String
        Dim strTablaMunicipios As String
        Dim strCampoDescMunicipio As String


        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Playa

        Try
            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaPlayas")
            strTablaGrafica = System.Configuration.ConfigurationManager.AppSettings("TablaGraficaPlayas")
            strTablaProvincias = System.Configuration.ConfigurationManager.AppSettings("TablaProvinciasGPL")
            strTablaMunicipios = System.Configuration.ConfigurationManager.AppSettings("TablaMunicipiosGPL")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdPlaya")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombrePlaya")
            strCampoDescProvincia = System.Configuration.ConfigurationManager.AppSettings("NombreProvinciasGPL")
            strCampoXUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordXPlaya")
            strCampoYUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordYPlaya")
            strCampoCodProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasGPL")
            strCampoCodMunicipio = System.Configuration.ConfigurationManager.AppSettings("IdMunicipiosGPL")
            strCampoCodCCAA = System.Configuration.ConfigurationManager.AppSettings("IdCCAAProvGPL")
            strCampoDescMunicipio = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipiosGPL")
            strDatasetName = strTabla

            strQuery = "SELECT " + strTabla + "." + strCampoId + ", " + strTabla + "." + strCampoNombre & _
             ", " + strTablaGrafica + "." + strCampoXUtm30 + ", " + strTablaGrafica + "." + strCampoYUtm30 & _
             ", " + strTablaProvincias + "." + strCampoCodCCAA + ", " + strTablaProvincias + "." + strCampoCodProvincia & _
             ", " + strTablaGrafica + "." + strCampoDescProvincia + ", " + strTablaMunicipios + "." + strCampoCodMunicipio & _
             ", " + strTablaMunicipios + "." + strCampoDescMunicipio & _
             " FROM " + strTabla & _
             " INNER JOIN " + strTablaGrafica + " ON " + strTabla + "." + strCampoId + " = " + strTablaGrafica + "." + strCampoId & _
             " INNER JOIN " + strTablaMunicipios + " ON " + strTablaMunicipios + "." + strCampoCodMunicipio + " = " + strTabla + "." + strCampoCodMunicipio & _
             " INNER JOIN " + strTablaProvincias + " ON " + strTablaProvincias + "." + strCampoCodProvincia + " = " + strTablaMunicipios + "." + strCampoCodProvincia & _
             " WHERE " + strTabla + "." + strCampoId + " = '" + idPlaya + "' AND " + strTabla + "." + strCampoId & _
             " = " + strTablaGrafica + "." + strCampoId

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aPlaya As New Playa()

                aPlaya.Id = IIf(aRow(strCampoId).Equals(System.DBNull.Value), "", aRow(strCampoId))
                aPlaya.nombre = IIf(aRow(strCampoNombre).Equals(System.DBNull.Value), "", aRow(strCampoNombre))
                aPlaya.provincia = IIf(aRow(strCampoDescProvincia).Equals(System.DBNull.Value), "", aRow(strCampoDescProvincia))
                aPlaya.XUtmET = IIf(aRow(strCampoXUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoXUtm30))
                aPlaya.YUtmET = IIf(aRow(strCampoYUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoYUtm30))
                aPlaya.codCCAA = IIf(aRow(strCampoCodCCAA).Equals(System.DBNull.Value), "", aRow(strCampoCodCCAA))
                aPlaya.codMunicipio = IIf(aRow(strCampoCodMunicipio).Equals(System.DBNull.Value), "", aRow(strCampoCodMunicipio))
                aPlaya.codProvincia = IIf(aRow(strCampoCodProvincia).Equals(System.DBNull.Value), "", aRow(strCampoCodProvincia))
                aPlaya.municipio = IIf(aRow(strCampoDescMunicipio).Equals(System.DBNull.Value), "", aRow(strCampoDescMunicipio))
                arrValores(i) = aPlaya
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function





    <System.Web.Services.WebMethod(Description:="Obtener Playas", EnableSession:=True)> _
    Public Function DamePlayas(ByVal Nombre As String, ByVal idCCAA As String, ByVal idProvincia As String, ByVal idMunicipio As String, _
                               ByVal BandAzul As Boolean, ByVal AccesoMinusv As Boolean, ByVal Aparc As Boolean, _
                               ByVal PaseoMarit As Boolean, ByVal ZonaInf As Boolean, ByVal ZonaSubm As Boolean, _
                                 ByVal Aseos As Boolean, ByVal Duchas As Boolean, ByVal ServicioLimp As Boolean, ByVal ClubNaut As Boolean, _
                                 ByVal TodosCampos As Boolean) As Playa()
        Dim strQuery As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim strTablaMuni As String, strTablaProv As String, strTablaGrafica As String
        Dim strCampoDescripcion As String
        Dim strIdCCAA As String, strIdProv As String, strIdMun As String, strCampoDescProvincia As String, strCampoDescMunicipio As String
        Dim strCampoBandAzul As String, strCampoAccesoMinusv As String, strCampoAparc As String
        Dim strCampoPaseoMarit As String, strCampoZonaInf As String, strCampoZonaSubm As String
        Dim strCampoAseos As String, strCampoDuchas As String, strCampoServicioLimp As String, strCampoClubNaut As String
        Dim strWhere As String, strSeparador As String
        Dim strNombre As String

        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Playa

        Try
            '        <add key="TablaProvinciasGPL" value="DGC.GPL_PROV_PROVINCIA"/>
            '<add key="IdProvinciasGPL" value="PRO_CO_PROVINCIA"/>
            '<add key="NombreProvinciasGPL" value="PRO_DS_PROVINCIA"/>
            '<add key="IdCCAAProvGPL" value="AUT_CO_CCAA"/>
            '<add key="TablaComunidadesGPL" value="DGC.GPL_AUT_CCAA"/>
            '<add key="NombreComunidadesGPL" value="AUT_DS_CCAA"/>
            '<add key="IdComunidadesGPL" value="AUT_CO_CCAA"/>
            '<add key="TablaMunicipiosGPL" value="DGC.GPL_MUN_MUNICIPIO"/>
            '<add key="NombreMunicipiosGPL" value="MUN_DS_TERM_MUNI"/>
            '<add key="IdMunicipiosGPL" value="MUN_CO_INE"/>
            '<add key="TablaPlayas" value="DGC.GPL_PLY_PLAYAS"/>
            '<add key="IdPlaya" value="PLY_CO_PLAYA"/>
            '<add key="NombrePlaya" value="PLY_DS_NOMBRE"/>
            '<add key="BandAzul" value="PLY_BO_BANDERA_AZUL"/>
            '<add key="AccesoMinusv" value="PLY_BO_ACCESO_MINUSV"/>
            '<add key="Aparc" value="PLY_BO_APARCAMIENTO"/>
            '<add key="PaseoMarit" value="PLY_BO_PASEO_MARITIMO"/>
            '<add key="ZonaInf" value="PLY_BO_ZONA_INF"/>
            '<add key="ZonaSubm" value="PLY_BO_SUBMARINISMO"/>
            '<add key="Aseos" value="PLY_BO_ASEOS"/>
            '<add key="Duchas" value="PLY_BO_DUCHAS"/>
            '<add key="ServicioLimp" value="PLY_BO_SERV_LIMPIEZA"/>
            '<add key="ClubNaut" value="PLY_BO_CLUB_NAU"/>
            '        <add key="CoordXPlaya" value="PLY_NM_COORDX"/>
            '<add key="CoordYPlaya" value="PLY_NM_COORDY"/>
            '<add key="CoordYHuso" value="PLY_NM_HUSO"/>

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaPlayas")
            strTablaMuni = System.Configuration.ConfigurationManager.AppSettings("TablaMunicipiosGPL")
            strTablaProv = System.Configuration.ConfigurationManager.AppSettings("TablaProvinciasGPL")
            strTablaGrafica = System.Configuration.ConfigurationManager.AppSettings("TablaGraficaPlayas")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdPlaya")
            strCampoDescProvincia = System.Configuration.ConfigurationManager.AppSettings("NombreProvinciasGPL")
            strCampoDescMunicipio = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipiosGPL")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombrePlaya")
            strCampoXUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordXPlaya")
            strCampoYUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordYPlaya")
            strIdCCAA = System.Configuration.ConfigurationManager.AppSettings("IdComunidadesGPL")
            strIdProv = System.Configuration.ConfigurationManager.AppSettings("IdProvinciasGPL")
            strIdMun = System.Configuration.ConfigurationManager.AppSettings("IdMunicipiosGPL")

            strCampoBandAzul = System.Configuration.ConfigurationManager.AppSettings("BandAzul")
            strCampoAccesoMinusv = System.Configuration.ConfigurationManager.AppSettings("AccesoMinusv")
            strCampoAparc = System.Configuration.ConfigurationManager.AppSettings("Aparc")
            strCampoPaseoMarit = System.Configuration.ConfigurationManager.AppSettings("PaseoMarit")
            strCampoZonaInf = System.Configuration.ConfigurationManager.AppSettings("ZonaInf")
            strCampoZonaSubm = System.Configuration.ConfigurationManager.AppSettings("ZonaSubm")
            strCampoAseos = System.Configuration.ConfigurationManager.AppSettings("Aseos")
            strCampoDuchas = System.Configuration.ConfigurationManager.AppSettings("Duchas")
            strCampoServicioLimp = System.Configuration.ConfigurationManager.AppSettings("ServicioLimp")
            strCampoClubNaut = System.Configuration.ConfigurationManager.AppSettings("ClubNaut")

            strDatasetName = strTabla


            strWhere = ""
            strSeparador = " "

            'agm 19/20/2012: ahora obtenemos siempre el nombre del municipio, lo ponemos en los campos del select
            strQuery = "SELECT " + strTabla + "." + strCampoId + ", " + strTabla + "." + strCampoNombre + ", " + strTablaGrafica + "." + strCampoXUtm30 + ", " + strTablaGrafica + "." + strCampoYUtm30 + ", " + strTablaGrafica + "." + strCampoDescProvincia + ", " + strTablaMuni + "." + strCampoDescMunicipio + " FROM " + strTabla & _
                " INNER JOIN " + strTablaGrafica + " ON " + strTabla + "." + strCampoId + " = " + strTablaGrafica + "." + strCampoId + " "

            'agm 19/20/2012: ahora obtenemos siempre el nombre del municipio, necesitamos hacer el join con la tabla de municipios siempre
            strQuery += " INNER JOIN " + strTablaMuni + " ON " + strTablaMuni + "." + strIdMun + " = " + strTabla + "." + strIdMun

            If (idProvincia <> "" And idMunicipio = "") Or (idCCAA <> "" And idProvincia = "" And idMunicipio = "") Then
                'agm 19/20/2012: ahora obtenemos siempre el nombre del municipio, necesitamos hacer el join con la tabla de municipios siempre, comentamos aqu�
                'strQuery += " INNER JOIN " + strTablaMuni + " ON " + strTablaMuni + "." + strIdMun + " = " + strTabla + "." + strIdMun

                If idCCAA <> "" And idProvincia = "" And idMunicipio = "" Then
                    strQuery += " INNER JOIN " + strTablaProv + " ON " + strTablaProv + "." + strIdProv + " = " + strTablaMuni + "." + strIdProv
                    strWhere = strTablaProv + "." + strIdCCAA + " =" + idCCAA
                Else
                    strWhere = strTablaMuni + "." + strIdProv + " =" + idProvincia
                End If

                strSeparador = " AND "
            Else
                If idMunicipio <> "" Then
                    strWhere = strTabla + "." + strIdMun + " = " + idMunicipio
                    strSeparador = " AND "
                End If
            End If



            If Nombre <> "" Then

                'arg 26/07/2016: soporte al ap�strofo de nombres de playa en catal�n y derivados.
                strNombre = Nombre.ToUpper().Replace("'", "''")

                strWhere += strSeparador + "( translate(UPPER(" + strTabla + "." + strCampoNombre + "),  '�����', 'AEIOU') LIKE translate('%" + strNombre + "%',  '�����', 'AEIOU')"

                If TodosCampos = True Then
                    strWhere += "OR translate(UPPER(" + strTabla + "." + strCampoNombre + "2),  '�����', 'AEIOU') LIKE translate('%" + strNombre + "%',  '�����', 'AEIOU')"
                    strWhere += "OR translate(UPPER(" + strTabla + "." + strCampoNombre + "3),  '�����', 'AEIOU') LIKE translate('%" + strNombre + "%',  '�����', 'AEIOU')"
                End If

                strWhere += ")"

                strSeparador = " AND "
            End If

            If BandAzul = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoBandAzul + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If AccesoMinusv = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoAccesoMinusv + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If Aparc = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoAparc + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If PaseoMarit = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoPaseoMarit + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If ZonaInf = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoZonaInf + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If ZonaSubm = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoZonaSubm + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If Aseos = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoAseos + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If Duchas = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoDuchas + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If ServicioLimp = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoServicioLimp + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If ClubNaut = True Then
                strWhere += strSeparador + "translate(UPPER(" + strTabla + "." + strCampoClubNaut + "),  '�����', 'AEIOU') = 'SI'"
                strSeparador = " AND "
            End If

            If strWhere <> "" Then
                strQuery += " WHERE " + strWhere
            End If

            strQuery += " ORDER BY  " + strTabla + "." + strCampoNombre + " ASC"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aPlaya As New Playa()

                aPlaya.Id = IIf(aRow(strCampoId).Equals(System.DBNull.Value), "", aRow(strCampoId))
                aPlaya.nombre = IIf(aRow(strCampoNombre).Equals(System.DBNull.Value), "", aRow(strCampoNombre))
                aPlaya.provincia = IIf(aRow(strCampoDescProvincia).Equals(System.DBNull.Value), "", aRow(strCampoDescProvincia))
                aPlaya.municipio = IIf(aRow(strCampoDescMunicipio).Equals(System.DBNull.Value), "", aRow(strCampoDescMunicipio))
                aPlaya.XUtmET = IIf(aRow(strCampoXUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoXUtm30))
                aPlaya.YUtmET = IIf(aRow(strCampoYUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoYUtm30))
                arrValores(i) = aPlaya
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Obtener sondeos por Hoja", EnableSession:=True)> _
    Public Function DameSondeosHoja(ByVal idHoja As String) As Sondeo()
        Dim strQuery As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim strCampoDescripcion As String, strCampoProvincia As String, strCampoMunicipio As String, strCampoCuenca As String, strCampoUH As String, strCampoCota As String
        Dim strCampoFecha As String, strCampoMedidas As String, strIdHoja As String, strIdProv As String, strIdMun As String, strIdCuenca As String, strIdUH As String
        Dim strWhereUH As String, strUrlBaseFicha As String, strFicha As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Sondeo

        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaSondeos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdSondeos")
            strCampoMunicipio = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipioSondeos")
            strCampoCuenca = System.Configuration.ConfigurationManager.AppSettings("NombreCuencaSondeos")
            strCampoUH = System.Configuration.ConfigurationManager.AppSettings("NombreUHSondeos")
            strCampoXUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordXSondeos")
            strCampoYUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordYSondeos")
            strCampoProvincia = System.Configuration.ConfigurationManager.AppSettings("NombreProvSondeos")
            strIdProv = System.Configuration.ConfigurationManager.AppSettings("IdProvSondeos")
            strIdMun = System.Configuration.ConfigurationManager.AppSettings("IdMunicSondeos")
            strIdCuenca = System.Configuration.ConfigurationManager.AppSettings("IdCuencasSondeos")
            strIdUH = System.Configuration.ConfigurationManager.AppSettings("IdUHidrogeologicasSondeos")

            strIdHoja = System.Configuration.ConfigurationManager.AppSettings("IdHojaSondeos")

            strUrlBaseFicha = System.Configuration.ConfigurationManager.AppSettings("URLBaseSondeos")
            strDatasetName = strTabla


            strQuery = "SELECT DISTINCT(OS." + strCampoId + ")," + strCampoMunicipio + "," + strCampoCuenca + "," + strCampoUH + "," + strCampoXUtm30 + "," + strCampoYUtm30 + ",PRO." + strCampoProvincia + ",PRO." + strIdProv + ",OS." + strIdMun + ",OS." + strIdCuenca + ",OS." + strIdUH + " FROM " + strTabla & _
                       " WHERE OS." + strIdHoja + "='" + idHoja + "' " & _
                       "AND OS.N_SONDEO = DGSON.N_SONDEO " & _
                       "AND OS.COD_MUNI = MUN.COD_MUNI " & _
                       "AND OS.COD_CUENCA = CUEN.COD_CUENCA " & _
                       "AND (OS.COD_UNIDAD = UNI.COD_UNIDAD " & _
                       "AND OS.COD_CUENCA = UNI.COD_CUENCA) " & _
                       "AND SUBSTRING(OS.COD_MUNI FROM 1 FOR 2) = PRO.COD_PROVIN"



            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aSondeo As New Sondeo()

                aSondeo.Id = IIf(aRow(strCampoId).Equals(System.DBNull.Value), "", aRow(strCampoId))
                aSondeo.Municipio = IIf(aRow(strCampoMunicipio).Equals(System.DBNull.Value), "", aRow(strCampoMunicipio))
                aSondeo.Cuenca = IIf(aRow(strCampoCuenca).Equals(System.DBNull.Value), "", aRow(strCampoCuenca))
                aSondeo.UH = IIf(aRow(strCampoUH).Equals(System.DBNull.Value), "", aRow(strCampoUH))
                aSondeo.URLFicha = strUrlBaseFicha + aSondeo.Id

                aSondeo.Provincia = IIf(aRow(strCampoProvincia).Equals(System.DBNull.Value), "", aRow(strCampoProvincia))
                aSondeo.IdCuenca = IIf(aRow(strIdCuenca).Equals(System.DBNull.Value), "", aRow(strIdCuenca))
                aSondeo.IdMunicipio = IIf(aRow(strIdMun).Equals(System.DBNull.Value), "", aRow(strIdMun))
                aSondeo.IdProvincia = IIf(aRow(strIdProv).Equals(System.DBNull.Value), "", aRow(strIdProv))
                aSondeo.IdUH = IIf(aRow(strIdUH).Equals(System.DBNull.Value), "", aRow(strIdUH))

                aSondeo.XUtmET = IIf(aRow(strCampoXUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoXUtm30))
                aSondeo.YUtmET = IIf(aRow(strCampoYUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoYUtm30))
                arrValores(i) = aSondeo
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener sondeos por ID", EnableSession:=True)> _
    Public Function DameSondeosId(ByVal idSondeo As String) As Sondeo()
        Dim strQuery As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim strCampoDescripcion As String, strCampoProvincia As String, strCampoMunicipio As String, strCampoCuenca As String, strCampoUH As String, strCampoCota As String
        Dim strCampoFecha As String, strCampoMedidas As String, strIdHoja As String, strIdProv As String, strIdMun As String, strIdCuenca As String, strIdUH As String
        Dim strWhereUH As String, strUrlBaseFicha As String, strFicha As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Sondeo

        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaSondeos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdSondeos")
            strCampoMunicipio = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipioSondeos")
            strCampoCuenca = System.Configuration.ConfigurationManager.AppSettings("NombreCuencaSondeos")
            strCampoUH = System.Configuration.ConfigurationManager.AppSettings("NombreUHSondeos")
            strCampoXUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordXSondeos")
            strCampoYUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordYSondeos")
            strCampoProvincia = System.Configuration.ConfigurationManager.AppSettings("NombreProvSondeos")
            strIdProv = System.Configuration.ConfigurationManager.AppSettings("IdProvSondeos")
            strIdMun = System.Configuration.ConfigurationManager.AppSettings("IdMunicSondeos")
            strIdCuenca = System.Configuration.ConfigurationManager.AppSettings("IdCuencasSondeos")
            strIdUH = System.Configuration.ConfigurationManager.AppSettings("IdUHidrogeologicasSondeos")

            strIdHoja = System.Configuration.ConfigurationManager.AppSettings("IdHojaSondeos")

            strUrlBaseFicha = System.Configuration.ConfigurationManager.AppSettings("URLBaseSondeos")
            strDatasetName = strTabla


            strQuery = "SELECT DISTINCT(OS." + strCampoId + ")," + strCampoMunicipio + "," + strCampoCuenca + "," + strCampoUH + "," + strCampoXUtm30 + "," + strCampoYUtm30 + ",PRO." + strCampoProvincia + ",PRO." + strIdProv + ",OS." + strIdMun + ",OS." + strIdCuenca + ",OS." + strIdUH + " FROM " + strTabla & _
                       " WHERE OS." + strCampoId + "='" + idSondeo + "' " & _
                       "AND OS.N_SONDEO = DGSON.N_SONDEO " & _
                       "AND OS.COD_MUNI = MUN.COD_MUNI " & _
                       "AND OS.COD_CUENCA = CUEN.COD_CUENCA " & _
                       "AND (OS.COD_UNIDAD = UNI.COD_UNIDAD " & _
                       "AND OS.COD_CUENCA = UNI.COD_CUENCA) " & _
                       "AND SUBSTRING(OS.COD_MUNI FROM 1 FOR 2) = PRO.COD_PROVIN"



            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aSondeo As New Sondeo()

                aSondeo.Id = IIf(aRow(strCampoId).Equals(System.DBNull.Value), "", aRow(strCampoId))
                aSondeo.Municipio = IIf(aRow(strCampoMunicipio).Equals(System.DBNull.Value), "", aRow(strCampoMunicipio))
                aSondeo.Cuenca = IIf(aRow(strCampoCuenca).Equals(System.DBNull.Value), "", aRow(strCampoCuenca))
                aSondeo.UH = IIf(aRow(strCampoUH).Equals(System.DBNull.Value), "", aRow(strCampoUH))
                aSondeo.URLFicha = strUrlBaseFicha + aSondeo.Id

                aSondeo.Provincia = IIf(aRow(strCampoProvincia).Equals(System.DBNull.Value), "", aRow(strCampoProvincia))
                aSondeo.IdCuenca = IIf(aRow(strIdCuenca).Equals(System.DBNull.Value), "", aRow(strIdCuenca))
                aSondeo.IdMunicipio = IIf(aRow(strIdMun).Equals(System.DBNull.Value), "", aRow(strIdMun))
                aSondeo.IdProvincia = IIf(aRow(strIdProv).Equals(System.DBNull.Value), "", aRow(strIdProv))
                aSondeo.IdUH = IIf(aRow(strIdUH).Equals(System.DBNull.Value), "", aRow(strIdUH))

                aSondeo.XUtmET = IIf(aRow(strCampoXUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoXUtm30))
                aSondeo.YUtmET = IIf(aRow(strCampoYUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoYUtm30))
                arrValores(i) = aSondeo
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtener Piez�metros por UH", EnableSession:=True)> _
    Public Function DameSondeosProvMun(ByVal idProv As String, ByVal idMun As String) As Sondeo()
        Dim strQuery As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String
        Dim strCampoDescripcion As String, strCampoProvincia As String, strCampoMunicipio As String, strCampoCuenca As String, strCampoUH As String, strCampoCota As String
        Dim strCampoFecha As String, strCampoMedidas As String, strIdProv As String, strIdMun As String, strIdCuenca As String, strIdUH As String
        Dim strWhereUH As String, strUrlBaseFicha As String, strFicha As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Sondeo

        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaSondeos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdSondeos")
            strCampoProvincia = System.Configuration.ConfigurationManager.AppSettings("NombreProvSondeos")
            strCampoMunicipio = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipioSondeos")
            strCampoCuenca = System.Configuration.ConfigurationManager.AppSettings("NombreCuencaSondeos")
            strCampoUH = System.Configuration.ConfigurationManager.AppSettings("NombreUHSondeos")
            strCampoXUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordXSondeos")
            strCampoYUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordYSondeos")
            strIdProv = System.Configuration.ConfigurationManager.AppSettings("IdProvSondeos")
            strIdMun = System.Configuration.ConfigurationManager.AppSettings("IdMunicSondeos")
            strIdCuenca = System.Configuration.ConfigurationManager.AppSettings("IdCuencasSondeos")
            strIdUH = System.Configuration.ConfigurationManager.AppSettings("IdUHidrogeologicasSondeos")
            strUrlBaseFicha = System.Configuration.ConfigurationManager.AppSettings("URLBaseSondeos")
            strDatasetName = strTabla

            If idMun <> "" Then
                strWhereUH = " AND OS." + strIdMun + " ='" + idMun + "'"
            Else
                strWhereUH = ""
            End If

            strQuery = "SELECT DISTINCT(OS." + strCampoId + ")," + strCampoMunicipio + "," + strCampoCuenca + "," + strCampoUH + "," + strCampoXUtm30 + "," + strCampoYUtm30 + ",PRO." + strCampoProvincia + ",PRO." + strIdProv + ",OS." + strIdMun + ",OS." + strIdCuenca + ",OS." + strIdUH + " FROM " + strTabla & _
                       " WHERE SUBSTRING(OS." + strIdMun + "FROM 1 FOR 2)='" + idProv + "' " + strWhereUH & _
                       "AND OS.N_SONDEO = DGSON.N_SONDEO " & _
                       "AND OS.COD_MUNI = MUN.COD_MUNI " & _
                       "AND OS.COD_CUENCA = CUEN.COD_CUENCA " & _
                       "AND (OS.COD_UNIDAD = UNI.COD_UNIDAD " & _
                       "AND OS.COD_CUENCA = UNI.COD_CUENCA) " & _
                       "AND SUBSTRING(OS.COD_MUNI FROM 1 FOR 2) = PRO.COD_PROVIN"



            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aSondeo As New Sondeo()

                aSondeo.Id = IIf(aRow(strCampoId).Equals(System.DBNull.Value), "", aRow(strCampoId))
                aSondeo.Municipio = IIf(aRow(strCampoMunicipio).Equals(System.DBNull.Value), "", aRow(strCampoMunicipio))
                aSondeo.Cuenca = IIf(aRow(strCampoCuenca).Equals(System.DBNull.Value), "", aRow(strCampoCuenca))
                aSondeo.UH = IIf(aRow(strCampoUH).Equals(System.DBNull.Value), "", aRow(strCampoUH))
                aSondeo.URLFicha = strUrlBaseFicha + aSondeo.Id

                aSondeo.Provincia = IIf(aRow(strCampoProvincia).Equals(System.DBNull.Value), "", aRow(strCampoProvincia))
                aSondeo.IdCuenca = IIf(aRow(strIdCuenca).Equals(System.DBNull.Value), "", aRow(strIdCuenca))
                aSondeo.IdMunicipio = IIf(aRow(strIdMun).Equals(System.DBNull.Value), "", aRow(strIdMun))
                aSondeo.IdProvincia = IIf(aRow(strIdProv).Equals(System.DBNull.Value), "", aRow(strIdProv))
                aSondeo.IdUH = IIf(aRow(strIdUH).Equals(System.DBNull.Value), "", aRow(strIdUH))

                aSondeo.XUtmET = IIf(aRow(strCampoXUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoXUtm30))
                aSondeo.YUtmET = IIf(aRow(strCampoYUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoYUtm30))
                arrValores(i) = aSondeo
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Obtener Piez�metros por Masa", EnableSession:=True)> _
    Public Function DamePiezometrosProvMun(ByVal idProvincia As String, ByVal idMunicipio As String, ByVal strFecha As String) As Piezometro()
        Dim strQuery As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String, strCampoXUtmET As String, strCampoYUtmET As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strCampoUH As String
        Dim strCampoDescripcion As String, strCampoProvincia As String, strCampoMunicipio As String, strCampoDemar As String, strCampoMasa As String, strCampoCota As String
        Dim strCampoFecha As String, strCampoMedidas As String, strIdProvincia As String, strIdMunicipio As String, strIdDemarPiezom As String
        Dim strWhereMunicipio As String, strProf As String, strIdUH As String, strUrlBaseFicha As String, strCampoNivel As String, strCampoFechaNivel As String, strIdMasa As String, strIdDemar As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Piezometro

        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaPiezometros")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdPiezometros")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombrePiezometros")
            strCampoDescripcion = System.Configuration.ConfigurationManager.AppSettings("DescPiezometros")
            strCampoProvincia = System.Configuration.ConfigurationManager.AppSettings("NombreProvincias")
            strCampoMunicipio = System.Configuration.ConfigurationManager.AppSettings("NombreMunicipios")
            strCampoDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strCampoUH = System.Configuration.ConfigurationManager.AppSettings("NombreUHidrogeologicas")
            strIdUH = System.Configuration.ConfigurationManager.AppSettings("IdUHPiezometros")
            strCampoXUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordXPiezometros")
            strCampoYUtm30 = System.Configuration.ConfigurationManager.AppSettings("CoordYPiezometros")
            strCampoXUtmET = System.Configuration.ConfigurationManager.AppSettings("CoordXPiezometrosET")
            strCampoYUtmET = System.Configuration.ConfigurationManager.AppSettings("CoordYPiezometrosET")
            strCampoMasa = System.Configuration.ConfigurationManager.AppSettings("NombreMasaAgua")
            strCampoFecha = System.Configuration.ConfigurationManager.AppSettings("FechaPiezometros")
            strCampoMedidas = System.Configuration.ConfigurationManager.AppSettings("NMedidasPiezometros")
            strIdDemarPiezom = System.Configuration.ConfigurationManager.AppSettings("idDemarPiezometros")
            strIdMasa = System.Configuration.ConfigurationManager.AppSettings("IdMasaPiezometros")
            strIdDemar = System.Configuration.ConfigurationManager.AppSettings("idDemarcaciones")
            strCampoCota = System.Configuration.ConfigurationManager.AppSettings("CotaPiezometros")
            strIdProvincia = System.Configuration.ConfigurationManager.AppSettings("IdProvinciaPiezometros")
            strIdMunicipio = System.Configuration.ConfigurationManager.AppSettings("IdMunicipioPiezometros")
            strCampoNivel = System.Configuration.ConfigurationManager.AppSettings("NivelPiezometros")
            strCampoFechaNivel = System.Configuration.ConfigurationManager.AppSettings("FechaNivelPiezometros")
            strUrlBaseFicha = System.Configuration.ConfigurationManager.AppSettings("URLBasePiezometros")
            strProf = System.Configuration.ConfigurationManager.AppSettings("ProfundidadPiezom")
            strDatasetName = strTabla
            If (idMunicipio <> 0) Then
                strWhereMunicipio = " AND " + strIdMunicipio + " ='" + idMunicipio + "'"
            Else
                strWhereMunicipio = ""
            End If

            If strFecha = "" Then
                strQuery = "SELECT DISTINCT(" + strCampoId + ")," + strCampoNombre + "," + strCampoDescripcion + "," + strIdUH + "," + "PRO." + strCampoProvincia + "," + strProf + "," + strCampoMunicipio + "," + "DEM." + strIdDemar + "," + strIdMasa + "," + "DEM." + strCampoDemar + "," + "MAS." + strCampoMasa + "," + strCampoUH + "," + strCampoCota + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strIdProvincia + "," + strIdMunicipio + "," + strCampoXUtmET + "," + strCampoYUtmET + "," + strCampoFecha + "," + strCampoMedidas + ", 0 AS " + strCampoNivel + ", '' AS " + strCampoFechaNivel + " FROM " + strTabla & _
                           " WHERE " + strIdProvincia + "='" + idProvincia + "' " + strWhereMunicipio & _
                           "AND PIE.PIE_NUMPIE = PME.NUMPIE (+) " & _
                           "AND PIE.PIE_CPROVI = PRO.PROVC (+) " & _
                           "AND PIE.PIE_CPROVMUNI  = MUN.INE (+) " & _
                           "AND PIE.PIE_CUNIDH = UNI.UNI_CUNIDH (+) " & _
                           "AND PIE.PIE_CDEMH  = DEM.COD_DEMAR (+) " & _
                           "AND PIE.PIE_CMASA = MAS.COD_MSBTDM (+) " & _
                           "AND PIE.PIE_NUMPIE = NIV.NIV_NUMPIE (+) " & _
                           "GROUP BY PIE_NUMPIE,PIE_NOMBRE,PIE_DESCRI,PRO.PROV_NOM,PIE_CPROVMUNI,MUN_NOM,DEM.COD_DEMAR,DEM.NOM_DEMAR, " & _
                           "UNI_NOMUNI, PIE_YUTM30,PIE_PROF, PIE_XUTM30, PIE_Z,UTM_X_ET,UTM_Y_ET,PIE_CUNIDH,PIE_CPROVI,PIE.PIE_CMASA,MAS.NOM_MSBT,PIE_CMUNI " & _
                           "ORDER BY PIE_NOMBRE"
                '  "AND PIE.PIE_CPROVI = MUN.MUN_CPROVI (+) " & _
            Else

                Dim strMesAnios As String()
                Dim strMes As String, strAnio As String, strFechaDesde As String, strFechaHasta As String
                Dim intMes As Integer, intAnio As Integer
                strMesAnios = strFecha.Split("/")
                If (strMesAnios.Length() = 3) Then
                    strMes = strMesAnios(1)
                    If (IsNumeric(strMes)) Then
                        intMes = CInt(strMes)
                        If (intMes < 1 Or intMes > 12) Then
                            Return Nothing
                        End If
                    Else
                        Return Nothing
                    End If
                    strAnio = strMesAnios(2)
                    If (IsNumeric(strAnio)) Then
                        intAnio = CInt(strAnio)
                    Else
                        Return Nothing
                    End If
                Else
                    Return Nothing
                End If
                strFechaDesde = "01/" + strMes + "/" + strAnio
                If (intMes = 12) Then
                    intMes = 1
                    intAnio += 1
                Else
                    intMes += 1
                End If
                strMes = CStr(intMes)
                strAnio = CStr(intAnio)
                If (strMes.Length() = 1) Then
                    strMes = "0" + strMes
                End If
                strFechaHasta = "01/" + strMes + "/" + strAnio
                Dim strWhereFecha As String
                strWhereFecha = " AND NIV_FECPIE>='" + strFechaDesde + "' AND NIV_FECPIE<'" + strFechaHasta + "' "

                strQuery = "SELECT DISTINCT(" + strCampoId + "), NIV_FECPIE," + strCampoNombre + "," + strCampoDescripcion + "," + strIdUH + "," + "PRO." + strCampoProvincia + "," + strProf + "," + strCampoMunicipio + "," + "DEM." + strIdDemar + "," + strIdMasa + "," + "DEM." + strCampoDemar + "," + "MAS." + strCampoMasa + "," + strCampoUH + "," + strCampoCota + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strIdProvincia + "," + strIdMunicipio + "," + strCampoXUtmET + "," + strCampoYUtmET + "," + strCampoFecha + "," + strCampoMedidas + "," + strCampoNivel + "," + strCampoFechaNivel + " FROM " + strTabla & _
                                           " WHERE " + strIdProvincia + "='" + idProvincia + "' " + strWhereMunicipio + strWhereFecha & _
                                           "AND PIE.PIE_NUMPIE = PME.NUMPIE (+) " & _
                                           "AND PIE.PIE_CPROVI = PRO.PROVC (+) " & _
                                           "AND PIE.PIE_CPROVMUNI = MUN.INE (+) " & _
                                           "AND PIE.PIE_CUNIDH = UNI.UNI_CUNIDH (+) " & _
                                           "AND PIE.PIE_CDEMH  = DEM.COD_DEMAR (+) " & _
                                           "AND PIE.PIE_CMASA = MAS.COD_MSBTDM (+) " & _
                                           "AND PIE.PIE_NUMPIE = NIV.NIV_NUMPIE (+) " & _
                                           "GROUP BY PIE_NUMPIE,PIE_NOMBRE,PIE_DESCRI,PRO.PROV_NOM,PIE_CPROVMUNI,MUN_NOM,DEM.COD_DEMAR,DEM.NOM_DEMAR, " & _
                                           "UNI_NOMUNI,PIE_PROF, PIE_YUTM30, PIE_XUTM30, PIE_Z,NIV_FECPIE,NIV_MEDIDA,UTM_X_ET,UTM_Y_ET,PIE_CUNIDH,PIE_CPROVI,PIE.PIE_CMASA,MAS.NOM_MSBT,PIE_CMUNI " & _
                                           "ORDER BY PIE_NOMBRE"
                '"AND PIE.PIE_CPROVI = MUN.MUN_CPROVI (+) " & _
            End If

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aPiezometro As New Piezometro()

                aPiezometro.Id = IIf(aRow(strCampoId).Equals(System.DBNull.Value), "", aRow(strCampoId))
                aPiezometro.nombre = IIf(aRow(strCampoNombre).Equals(System.DBNull.Value), "", aRow(strCampoNombre))
                aPiezometro.Descripcion = IIf(aRow(strCampoDescripcion).Equals(System.DBNull.Value), "", aRow(strCampoDescripcion))
                aPiezometro.Provincia = IIf(aRow(strCampoProvincia).Equals(System.DBNull.Value), "", aRow(strCampoProvincia))
                aPiezometro.Municipio = IIf(aRow(strCampoMunicipio).Equals(System.DBNull.Value), "", aRow(strCampoMunicipio))
                aPiezometro.Demarcacion = IIf(aRow(strCampoDemar).Equals(System.DBNull.Value), "", aRow(strCampoDemar))
                aPiezometro.Masa = IIf(aRow(strCampoMasa).Equals(System.DBNull.Value), "", aRow(strCampoMasa))
                aPiezometro.Cota = IIf(aRow(strCampoCota).Equals(System.DBNull.Value), 0, aRow(strCampoCota))
                aPiezometro.URLFicha = strUrlBaseFicha + aPiezometro.Id
                aPiezometro.Nivel = IIf(aRow(strCampoNivel).Equals(System.DBNull.Value), 0, aRow(strCampoNivel))


                Dim aDate As Date
                aDate = IIf(aRow(strCampoFecha).Equals(System.DBNull.Value), Nothing, aRow(strCampoFecha))
                If Not aDate = Nothing Then
                    aPiezometro.FechaInicio = aDate.ToString("dd/MM/yyyy")
                Else
                    aPiezometro.FechaInicio = ""
                End If
                'Realmente siempre son iguales, porque agrupamos por fecha en la segunda opci�n
                aPiezometro.FechaNivel = aPiezometro.FechaInicio
                aPiezometro.NumMedidas = IIf(aRow(strCampoMedidas).Equals(System.DBNull.Value), 0, aRow(strCampoMedidas))
                aPiezometro.XUtm30 = IIf(aRow(strCampoXUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoXUtm30))
                aPiezometro.YUtm30 = IIf(aRow(strCampoYUtm30).Equals(System.DBNull.Value), 0, aRow(strCampoYUtm30))
                aPiezometro.XUtmET = IIf(aRow(strCampoXUtmET).Equals(System.DBNull.Value), 0, aRow(strCampoXUtmET))
                aPiezometro.YUtmET = IIf(aRow(strCampoYUtmET).Equals(System.DBNull.Value), 0, aRow(strCampoYUtmET))
                aPiezometro.IdDemar = IIf(aRow(strIdDemar).Equals(System.DBNull.Value), "", aRow(strIdDemar))
                aPiezometro.IdMunicipio = IIf(aRow(strIdMunicipio).Equals(System.DBNull.Value), 0, aRow(strIdMunicipio))
                aPiezometro.IdProvincia = IIf(aRow(strIdProvincia).Equals(System.DBNull.Value), 0, aRow(strIdProvincia))
                aPiezometro.IdMasa = IIf(aRow(strIdMasa).Equals(System.DBNull.Value), "", aRow(strIdMasa))
                aPiezometro.Profundidad = IIf(aRow(strProf).Equals(System.DBNull.Value), "", aRow(strProf))
                aPiezometro.UH = IIf(aRow(strCampoUH).Equals(System.DBNull.Value), "", aRow(strCampoUH))
                aPiezometro.IdUH = IIf(aRow(strIdUH).Equals(System.DBNull.Value), "", aRow(strIdUH))
                arrValores(i) = aPiezometro
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    <System.Web.Services.WebMethod(Description:="Obtener Pluviometros", EnableSession:=True)> _
    Public Function DamePluviometros(ByVal codDemar As String, ByVal filtro As String) As Pluviometro()
        Dim strQuery As String
        Dim strCampoXUtm30 As String, strCampoYUtm30 As String
        Dim strTabla As String, strTabla2 As String, strCampoNomDemar As String, strCampoCodDemar As String, strDatasetName As String, strCampoIdPunto As String, strCampoIdTipo As String, strCampoTipo As String, strCampoId As String, strCampoNombre As String, strCampoIdSaih As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Pluviometro
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaPluviometros")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdPluviometros")
            strCampoIdTipo = System.Configuration.ConfigurationManager.AppSettings("IdTipo")
            strCampoTipo = System.Configuration.ConfigurationManager.AppSettings("Tipo")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombrePluviometros")
            strCampoIdSaih = System.Configuration.ConfigurationManager.AppSettings("IdSaih")
            strCampoIdPunto = System.Configuration.ConfigurationManager.AppSettings("IdPunto")
            strTabla2 = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strCampoCodDemar = System.Configuration.ConfigurationManager.AppSettings("idDemarcaciones")
            strCampoNomDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")
            strCampoXUtm30 = "UTM_X_ET"
            strCampoYUtm30 = "UTM_Y_ET"
            strDatasetName = strTabla

            strQuery = "SELECT  * FROM ("
            strQuery += "SELECT DISTINCT(" + strTabla + "." + strCampoId + ")," + strCampoNomDemar + "," + strCampoNombre + "," + strCampoXUtm30 + "," + strCampoYUtm30 + "," + strCampoIdPunto + "," + strCampoIdTipo + "," + strCampoTipo + "," + strTabla + "." + strCampoCodDemar + "," + strCampoIdSaih + " FROM " + strTabla & _
             " INNER JOIN " + strTabla2 + " ON " + strTabla + "." + strCampoCodDemar + " = " + strTabla2 + "." + strCampoCodDemar + " AND " + strTabla + "." + strCampoCodDemar

            If Not (codDemar.Equals("-1")) Then
                strQuery += "='" + codDemar + "'"
            Else
                strQuery += " IS NOT NULL "
            End If

            strQuery += " AND " + strCampoNombre + " IS NOT NULL AND LENGTH(TRIM(" + strCampoNombre + "))>0) a"

            If Not (filtro.Equals("")) Then
                strQuery += " WHERE " + filtro
            End If

            strQuery += " ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aPluviom As New Pluviometro()
                aPluviom.Id = aRow(strCampoId)
                aPluviom.nombre = aRow(strCampoNombre)
                aPluviom.Tipo = aRow(strCampoTipo)
                aPluviom.IdTipo = aRow(strCampoIdTipo)
                aPluviom.IdPunto = aRow(strCampoIdPunto)
                aPluviom.Cuenca = aRow(strCampoIdSaih)
                aPluviom.XUtm30 = aRow(strCampoXUtm30)
                aPluviom.YUtm30 = aRow(strCampoYUtm30)
                aPluviom.CodDemar = aRow(strCampoCodDemar)
                aPluviom.NomDemar = aRow(strCampoNomDemar)
                arrValores(i) = aPluviom
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    Private Function ObtenerAutenticacion() As String
        Dim arrRoles As ArrayList
        Dim strUsuario As String, strAutenticacion As String, strRoles As String
        Dim i As Integer
        strAutenticacion = System.Configuration.ConfigurationManager.AppSettings("Autenticacion")

        arrRoles = Me.Session("Rol")
        strUsuario = Me.Session("Usuario")
        If (arrRoles Is Nothing) Then
            strRoles = "= 'ALL')"
        ElseIf (arrRoles.Count = 0) Then
            strRoles = "= 'ALL')"
        ElseIf (arrRoles(0).Equals("Error")) Then
            strRoles = "= 'ALL')"
        ElseIf (arrRoles(0).Equals("NO_ROL")) Then
            strRoles = "= 'ALL')"
        Else
            strRoles = " IN ('ALL',"
            For i = 0 To arrRoles.Count - 1
                strRoles += "'" + arrRoles(i) + "'"
                If (i = arrRoles.Count - 1) Then
                    strRoles += ")"
                Else
                    strRoles += ","
                End If
            Next
            strRoles += ")"
        End If
        Return strRoles
    End Function
    Private Function ObtenerAutenticacionFlex(ByVal userInfo As UserInfo) As String
        Dim arrRoles As ArrayList
        Dim strUsuario As String, strAutenticacion As String, strRoles As String
        Dim i As Integer
        strAutenticacion = System.Configuration.ConfigurationManager.AppSettings("Autenticacion")

        arrRoles = userInfo.grupos      'Me.Session("Rol")
        strUsuario = userInfo.nombre    'Session("Usuario")

        'agm: infame
        If arrRoles.Count > 0 Then
            If TypeOf arrRoles(0) Is Array Then
                For i = 0 To arrRoles.Count - 1
                    arrRoles(i) = CType(CType(arrRoles(i), Array)(0), XmlText).InnerText
                Next
            End If
        End If

        If (arrRoles Is Nothing) Then
            strRoles = "= 'ALL')"
        ElseIf (arrRoles.Count = 0) Then
            strRoles = "= 'ALL')"
        ElseIf (arrRoles(0).Equals("Error")) Then
            strRoles = "= 'ALL')"
        ElseIf (arrRoles(0).Equals("NO_ROL")) Then
            strRoles = "= 'ALL')"
        Else
            strRoles = " IN ('ALL',"
            For i = 0 To arrRoles.Count - 1
                strRoles += "'" + arrRoles(i) + "'"
                If (i = arrRoles.Count - 1) Then
                    strRoles += ")"
                Else
                    strRoles += ","
                End If
            Next
            strRoles += ")"
        End If
        Return strRoles
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Campos", EnableSession:=True)> _
    Public Function DameEnfermedades() As Enfermedad()
        Dim strQuery As String, strTableName As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim arrValores() As Enfermedad
        Dim strEsquema As String
        Try
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")

            strTableName = "SAN_ENF_ENFERMEDADES"
            strDatasetName = "SAN_ENF_ENFERMEDADES"
            strQuery = "SELECT ENF_CO_ID,ENF_DS_DESCRIPCION FROM " & strEsquema & strTableName & " ORDER BY ENF_DS_DESCRIPCION"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)



            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                arrValores(i) = New Enfermedad()
                arrValores(i).Id = aRow("ENF_CO_ID")
                arrValores(i).nombre = aRow("ENF_DS_DESCRIPCION")
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Campos", EnableSession:=True)> _
    Public Function DameEspecies() As Familia()
        Dim strQuery As String, strTableName As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim arrValores() As Familia
        Dim strEsquema As String
        Try
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")

            strTableName = "TC_FAMILIA_REGA"
            strDatasetName = "TC_FAMILIA_REGA"
            strQuery = "SELECT ID FROM DGG." & strTableName & " ORDER BY ID"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)



            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                arrValores(i) = New Familia()
                arrValores(i).Cargar(aRow("ID"))
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Campos", EnableSession:=True)> _
    Public Function ObtenerFocosInfeccion(ByVal idAlerta As Long) As PuntoInfeccion()
        Dim strQuery As String, strTableName As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim arrValores() As PuntoInfeccion
        Dim strEsquema As String
        Try
            strEsquema = System.Configuration.ConfigurationManager.AppSettings("Esquema")

            strTableName = "SAN_PTO_PUNTOS_ALERTAS"
            strDatasetName = "SAN_PTO_PUNTOS_ALERTAS"
            strQuery = "SELECT PTO_CO_ID, ALE_CO_ID, PTO_CO_SUBEXPLOTACION, PTO_NM_LATITUD, PTO_NM_LONGITUD  FROM " & strEsquema & strTableName & _
                       " WHERE ALE_CO_ID=" & idAlerta
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)



            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                arrValores(i) = New PuntoInfeccion()
                arrValores(i).id = aRow("PTO_CO_ID")
                arrValores(i).idAlerta = aRow("ALE_CO_ID")
                arrValores(i).idExplotacion = IIf(aRow("PTO_CO_SUBEXPLOTACION").Equals(System.DBNull.Value), "", aRow("PTO_CO_SUBEXPLOTACION"))
                arrValores(i).latitud = IIf(aRow("PTO_NM_LATITUD").Equals(System.DBNull.Value), Nothing, aRow("PTO_NM_LATITUD"))
                arrValores(i).longitud = IIf(aRow("PTO_NM_LONGITUD").Equals(System.DBNull.Value), Nothing, aRow("PTO_NM_LONGITUD"))
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function GetPatternedFiles(ByVal path As String, ByVal patterns() As String) As String()
        Dim ar As New ArrayList
        For Each pattern As String In patterns
            If (Directory.Exists(path)) Then
                Dim temp() As String = Directory.GetFiles(path, pattern)
                If temp.Length > 0 Then
                    ar.AddRange(temp)
                End If
            Else
                Return Nothing
            End If
        Next
        If (ar.Count > 0) Then
            Dim ret(ar.Count - 1) As String
            Array.Copy(ar.ToArray, ret, ar.Count)  '/// copy the arraylist as an array object to a string array
            Return ret
        Else
            Return Nothing
        End If
    End Function


    <System.Web.Services.WebMethod(Description:="Obtencion de Estaciones", EnableSession:=True)> _
    Public Function DameEstaciones(ByVal idProvincia As String, ByVal filtroCriterios As String, _
     ByVal tablasJoin As String(), ByVal camposJoin As String()) As Estacion()
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String, strCampoId As String, strCampoNombre As String, strTablaJoin As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Estacion
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaEstaciones")
            strTablaJoin = System.Configuration.ConfigurationManager.AppSettings("TablaEstacionesDatos")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdEstaciones")
            strCampoNombre = System.Configuration.ConfigurationManager.AppSettings("NombreEstaciones")
            strDatasetName = strTabla
            filtroCriterios = Replace(filtroCriterios, "&lt;", "<")




            strQuery = "SELECT DISTINCT(" + strTabla + "." + strCampoId + ")," + strTablaJoin + "." + strCampoNombre + " FROM " + strTabla + _
             " inner join " + strTablaJoin + " on " + strTabla + "." + strCampoId + "=" + strTablaJoin + "." + strCampoId
            For j As Integer = 0 To tablasJoin.Length - 1
                strQuery += " INNER JOIN " + tablasJoin(j) + " on " + strTabla + "." + strCampoId + "=" + tablasJoin(j) + "." + camposJoin(j)
            Next
            strQuery += " WHERE " + _
             strTablaJoin + ".PROVINCIA='" + idProvincia + "' AND " + _
             strTabla + "." + strCampoId + " IS NOT NULL AND " + strCampoNombre + " IS NOT NULL " + _
             filtroCriterios + _
             " ORDER BY(" + strCampoNombre + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)

            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer

            For Each aRow In aDS.Tables(0).Rows
                Dim aEstacion As New Estacion
                aEstacion.Id = aRow(strCampoId)
                aEstacion.nombre = aRow(strCampoNombre)
                arrValores(i) = aEstacion
                i += 1

            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de Estaciones", EnableSession:=True)> _
    Public Function DameMaxMinCriteriosEstaciones(ByVal idProvincia As String) As CriteriosEstaciones
        Dim strQuery As String
        Dim strTabla As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Estacion
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaEstaciones")
            strDatasetName = strTabla

            strQuery = "select MAX(TO_NUMBER(COTA,'9999')) MAXCOTA, MIN(TO_NUMBER(COTA,'9999')) MINCOTA, " + _
             "MAX(A�O) MAXTEMP,MIN(A�O) MINTEMP,MAX(PA�O) MAXPRECIP,MIN(PA�O) MINPRECIP " + _
            " from DGA.CH_EST_DATOS LEFT join DGA.CH_EST_TMEMEME on DGA.CH_EST_TMEMEME.CLAVE = DGA.CH_EST_DATOS.CLAVE " + _
            " LEFT join DGA.CH_EST_PLUMEME on DGA.CH_EST_PLUMEME.CLAVE = DGA.CH_EST_DATOS.CLAVE "
            strQuery += " WHERE DGA.CH_EST_DATOS.PROVINCIA='" + idProvincia + "'"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)

            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer

            Dim aCriteriosEstacion As New CriteriosEstaciones

            For Each aRow In aDS.Tables(0).Rows
                aCriteriosEstacion.maxAltitud = aRow("MAXCOTA")
                aCriteriosEstacion.minAltitud = aRow("MINCOTA")
                aCriteriosEstacion.maxTemperatura = aRow("MAXTEMP")
                aCriteriosEstacion.minTemperatura = aRow("MINTEMP")
                aCriteriosEstacion.maxPrecicitacion = aRow("MAXPRECIP")
                aCriteriosEstacion.minPrecicitacion = aRow("MINPRECIP")

                i += 1
            Next
            Return aCriteriosEstacion
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    <System.Web.Services.WebMethod(Description:="Obtener Embalses CX", EnableSession:=True)> _
    Public Function DameEmbalsesCX(ByVal strWhere As String) As EmbalsesCX()
        Dim strQuery As String
        Dim strTabla As String
        Dim strTablaMunicipios As String
        Dim strref_ceh As String
        Dim strnomEmbalse As String
        Dim strcorriente As String
        Dim strpropietario As String
        Dim strnombreMunicipio As String
        Dim strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As EmbalsesCX
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaEmbalsesDGAGUA")
            strTablaMunicipios = System.Configuration.ConfigurationManager.AppSettings("TablaMunicipios")
            strref_ceh = System.Configuration.ConfigurationManager.AppSettings("REF_CEH")
            strnomEmbalse = System.Configuration.ConfigurationManager.AppSettings("nomEmbalse")
            strcorriente = System.Configuration.ConfigurationManager.AppSettings("corriente")
            strpropietario = System.Configuration.ConfigurationManager.AppSettings("propietario")
            strnombreMunicipio = System.Configuration.ConfigurationManager.AppSettings("nombreMunicipio")

            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT "
            strQuery = strQuery + "CAST(a." + strref_ceh + " AS INTEGER), "
            strQuery = strQuery + "a." + strnomEmbalse + ", "
            strQuery = strQuery + "a." + strcorriente + ", "
            strQuery = strQuery + "a." + strpropietario + ", "
            strQuery = strQuery + "COALESCE(b." + strnombreMunicipio + ",'NaN') MUN_NOM "
            strQuery = strQuery + " FROM " + strTabla + " a "
            strQuery = strQuery + " LEFT JOIN " + strTablaMunicipios + " b "
            strQuery = strQuery + " ON b.INE = a.MUNI_ID "
            If (strWhere <> String.Empty) Then
                'strQuery += " WHERE TRANSLATE(UPPER(a." + strnomEmbalse + "), '��������������������', 'AEIOUAEIOUAEIOUAEIOU') "
                'strQuery += " LIKE TRANSLATE(UPPER('%" + strWhere + "), '��������������������', 'AEIOUAEIOUAEIOUAEIOU') "
                strQuery += " WHERE " + strWhere
            End If
            strQuery += " ORDER BY(a." + strnomEmbalse + ")"


            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)

            Dim i As Integer
            Dim aEmbalses As EmbalsesCX

            For Each aRow In aDS.Tables(0).Rows
                aEmbalses = New EmbalsesCX
                aEmbalses.ref_ceh = aRow(strref_ceh)
                aEmbalses.nomEmbalse = aRow(strnomEmbalse)
                aEmbalses.corriente = aRow(strcorriente)
                aEmbalses.propietario = aRow(strpropietario)
                aEmbalses.nombreMunicipio = aRow(strnombreMunicipio)
                arrValores(i) = aEmbalses
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    'JCM: 29/11/2010 Modificaci�n Sondeos. Demarcaci�n que "tire" de la tabla de sondeos
    <System.Web.Services.WebMethod(Description:="Obtener las demarcaciones Hidrogr�ficas  con sondeos", EnableSession:=True)> _
    Public Function DameSondeosDemarcaciones() As Cuenca()
        Dim strQuery As String
        Dim strTablaSondeos As String, strTablaDemar As String
        Dim strDatasetName As String, strCampoNombreDemar As String
        Dim strCampoIdDemar As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Cuenca
        Try

            strTablaSondeos = System.Configuration.ConfigurationManager.AppSettings("TablaSondeosSolo")
            strTablaDemar = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strCampoIdDemar = System.Configuration.ConfigurationManager.AppSettings("SondeosIdDemar")
            strCampoNombreDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")

            strDatasetName = strTablaSondeos

            strQuery = "SELECT DISTINCT("
            strQuery = strQuery + strTablaSondeos + "." + strCampoIdDemar + "),"
            strQuery = strQuery + strCampoNombreDemar
            strQuery = strQuery + " FROM " + strTablaDemar
            strQuery = strQuery + "," + strTablaSondeos
            strQuery = strQuery + " WHERE " + strTablaSondeos + "." + strCampoIdDemar
            strQuery = strQuery + " = " + strTablaDemar + "." + strCampoId
            strQuery = strQuery + " ORDER BY(" + strCampoNombreDemar + ")"

            aDS = ExecuteQuery(strQuery, strTablaSondeos, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCuenca As New Cuenca()
                aCuenca.Id = aRow(strCampoIdDemar)
                aCuenca.nombre = aRow(strCampoNombreDemar)
                arrValores(i) = aCuenca
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    'JCM: 29/11/2010 Modificaci�n Control del Estado Qu�mico.
    <System.Web.Services.WebMethod(Description:="Obtener las demarcaciones de calidad de aguas subterraneas", EnableSession:=True)> _
    Public Function DameCalidadAguasSubterraneasDemarcaciones() As Cuenca()
        Dim strQuery As String
        Dim strTabla As String, strTablaDemar As String
        Dim strDatasetName As String, strCampoNombreDemar As String
        Dim strCampoIdDemar As String, strCampoId As String, strCampoNombre As String, strPrefijo As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As Cuenca
        Try

            strTabla = System.Configuration.ConfigurationManager.AppSettings("TablaCalidadAguaSub")
            strTablaDemar = System.Configuration.ConfigurationManager.AppSettings("TablaDemarcacion")
            strCampoId = System.Configuration.ConfigurationManager.AppSettings("IdDemarcaciones")
            strCampoIdDemar = System.Configuration.ConfigurationManager.AppSettings("CalidadAguaSubIdDemar")
            strCampoNombreDemar = System.Configuration.ConfigurationManager.AppSettings("NombreDemarcaciones")

            strDatasetName = strTabla

            strQuery = "SELECT DISTINCT("
            strQuery = strQuery + strCampoIdDemar + "),"
            strQuery = strQuery + strCampoNombreDemar
            strQuery = strQuery + " FROM " + strTablaDemar
            strQuery = strQuery + "," + strTabla
            strQuery = strQuery + " WHERE " + strTabla + "." + strCampoIdDemar
            strQuery = strQuery + " = " + strTablaDemar + "." + strCampoId
            strQuery = strQuery + " ORDER BY(" + strCampoNombreDemar + ")"

            aDS = ExecuteQuery(strQuery, strTabla, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aCuenca As New Cuenca()
                aCuenca.Id = aRow(strCampoIdDemar)
                aCuenca.nombre = aRow(strCampoNombreDemar)
                arrValores(i) = aCuenca
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    'DameNombreUltimoPDF'
    <System.Web.Services.WebMethod(Description:="Obtener Nombre PDF Memoria Anuario", EnableSession:=True)> _
    Public Function DameEstacionesAforo_NombreUltimoPDF() As String
        Dim strQuery As String
        Dim strTablaRoanAuxMemAnuario As String
        Dim strDatasetName As String
        Dim strCampoNomMem As String
        Dim strCampoCodMem As String
        Dim aDS As Data.DataSet
        Dim nombreUltimoPDF As String
        Try
            strTablaRoanAuxMemAnuario = System.Configuration.ConfigurationManager.AppSettings("TablaRoanAuxMemAnuario")
            strCampoNomMem = System.Configuration.ConfigurationManager.AppSettings("NOM_MEM")
            strCampoCodMem = System.Configuration.ConfigurationManager.AppSettings("COD_MEM")
            strDatasetName = strTablaRoanAuxMemAnuario

            'SELECT nom_mem FROM DGA_USH.ROAN_AUX_MEM_ANUARIO where cod_mem = (select max(cod_mem) from DGA_USH.ROAN_AUX_MEM_ANUARIO)

            strQuery = "SELECT " + strCampoNomMem + ""
            strQuery = strQuery + " FROM " + strTablaRoanAuxMemAnuario + ""
            strQuery = strQuery + " WHERE " + strCampoCodMem + " = (SELECT MAX (" + strCampoCodMem + ")" + ""
            strQuery = strQuery + " FROM " + strTablaRoanAuxMemAnuario + ")"

            aDS = ExecuteQuery(strQuery, strTablaRoanAuxMemAnuario, strDatasetName)
            nombreUltimoPDF = aDS.Tables(strTablaRoanAuxMemAnuario).Rows(0).Item(strCampoNomMem)

            Return nombreUltimoPDF


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    'ObtenerIDDesdeURL'
    <System.Web.Services.WebMethod(Description:="Obtener ID de Servicio a partir de la URL", EnableSession:=True)> _
    Public Function ObtenerIDDesdeURL(ByVal url As String) As Integer
        Dim strQuery As String
        Dim strTablaVisSewServiciosWMS As String
        Dim strDatasetName As String
        Dim strCampoSerCoiId As String
        Dim strCampoSewDsUrl As String

        Dim aDS As Data.DataSet
        Dim id As Integer
        Try
            strTablaVisSewServiciosWMS = System.Configuration.ConfigurationManager.AppSettings("TablaVisSewServiciosWMS")
            strCampoSerCoiId = System.Configuration.ConfigurationManager.AppSettings("SER_CO_ID")
            strCampoSewDsUrl = System.Configuration.ConfigurationManager.AppSettings("SEW_DS_URL")
            strDatasetName = strTablaVisSewServiciosWMS

            'SELECT ser_co_id FROM visorintranet.vis_gn_geonetwork WHERE gn_ds_url = url'

            strQuery = "SELECT " + strCampoSerCoiId + ""
            strQuery = strQuery + " FROM " + strTablaVisSewServiciosWMS + ""
            strQuery = strQuery + " WHERE " + strCampoSewDsUrl + " = '" + url + "'"

            aDS = ExecuteQuery(strQuery, strTablaVisSewServiciosWMS, strDatasetName)
            If aDS.Tables(strTablaVisSewServiciosWMS).Rows.Count > 0 Then
                id = aDS.Tables(strTablaVisSewServiciosWMS).Rows(0).Item(strCampoSerCoiId)
            End If

            Return id


        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    'agm: ObtenerIDDesdeUUID
    <System.Web.Services.WebMethod(Description:="Obtener ID de Servicio a partir del UUID del metadato", EnableSession:=True)> _
    Public Function ObtenerIDDesdeUUID(ByVal uuid As String) As Integer
        Dim strQuery As String
        Dim strTablaVisSewServiciosWMS As String
        Dim strDatasetName As String
        Dim strCampoSerCoiId As String
        Dim strCampoSewDsUrl As String

        Dim aDS As Data.DataSet
        Dim id As Integer
        Try
            strTablaVisSewServiciosWMS = System.Configuration.ConfigurationManager.AppSettings("TablaVisSewServiciosWMS")
            strCampoSerCoiId = System.Configuration.ConfigurationManager.AppSettings("SER_CO_ID")
            strDatasetName = strTablaVisSewServiciosWMS

            'SELECT ser_co_id FROM visorintranet.vis_gn_geonetwork WHERE gn_ds_url = url'

            strQuery = "SELECT " + strCampoSerCoiId + ""
            strQuery = strQuery + " FROM " + strTablaVisSewServiciosWMS + ""
            strQuery = strQuery + " WHERE SEW_CO_UID = '" + uuid + "'"

            aDS = ExecuteQuery(strQuery, strTablaVisSewServiciosWMS, strDatasetName)
            If aDS.Tables(strTablaVisSewServiciosWMS).Rows.Count > 0 Then
                id = aDS.Tables(strTablaVisSewServiciosWMS).Rows(0).Item(strCampoSerCoiId)
            End If

            Return id

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    'aaa: ObtenerIDsDesdeUUID
    <System.Web.Services.WebMethod(Description:="Obtener array de IDs de Servicio a partir del UUID del metadato", EnableSession:=True)> _
    Public Function ObtenerIDsDesdeUUID(ByVal uuid As String, ByVal codIdioma As String) As Data.DataTable()
        Dim strQuery As String
        Dim strTablaVisSewServiciosWMS As String
        Dim strDatasetName As String
        Dim strCampoSerCoiId As String
        Dim strCampoSewDsUrl As String
        Dim arrDT() As Data.DataTable

        Dim aDS As Data.DataSet
        Dim id As Integer
        Try
            strTablaVisSewServiciosWMS = System.Configuration.ConfigurationManager.AppSettings("TablaVisSewServiciosWMS")
            strCampoSerCoiId = System.Configuration.ConfigurationManager.AppSettings("SER_CO_ID")
            strDatasetName = strTablaVisSewServiciosWMS

            strQuery = "SELECT " + strTablaVisSewServiciosWMS + "." + strCampoSerCoiId + ", CASE WHEN VIS_SDS_DESC_SERVICIOS.ser_ds_alias IS null THEN VIS_SER_SERVICIOS.SER_DS_ALIAS ELSE VIS_SDS_DESC_SERVICIOS.ser_ds_alias END as descServicio, " + strTablaVisSewServiciosWMS + ".SEW_DS_DESCRIPCION as descMetadato, " + strTablaVisSewServiciosWMS + ".SEW_CO_UID"
            strQuery = strQuery + " FROM (VIS_SER_SERVICIOS left join VIS_SDS_DESC_SERVICIOS on VIS_SER_SERVICIOS.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id"
            strQuery = strQuery + " AND VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "'), " + strTablaVisSewServiciosWMS + ""
            strQuery = strQuery + " WHERE " + strTablaVisSewServiciosWMS + ".SER_CO_ID = VIS_SER_SERVICIOS.SER_CO_ID AND " + strTablaVisSewServiciosWMS + ".SEW_CO_UID IN ('" + uuid + "') order by " + strTablaVisSewServiciosWMS + ".SEW_CO_UID"

            aDS = ExecuteQuery(strQuery, strTablaVisSewServiciosWMS, strDatasetName)

            ReDim arrDT(1)
            arrDT(0) = aDS.Tables(0).Copy()
            datatableFieldsToUpper(arrDT(0))
            arrDT(1) = aDS.Tables(0).Copy()
            datatableFieldsToUpper(arrDT(1))
            If (arrDT(0).Rows.Count = 0) Then
                Dim dsNewRow As DataRow
                dsNewRow = arrDT(1).NewRow()
                dsNewRow.Item("SEW_CO_UID") = uuid
                arrDT(1).Rows.Add(dsNewRow)
            End If
            'arrIDs(0) = New ArrayList()
            'arrIDs(1) = New ArrayList()

            'If aDS.Tables(strTablaVisSewServiciosWMS).Rows.Count > 0 Then 
            'For i As Integer = 0 To aDS.Tables(strTablaVisSewServiciosWMS).Rows.Count - 1
            'arrIDs(0).Add(aDS.Tables(strTablaVisSewServiciosWMS).Rows(i)(strCampoSerCoiId))
            'arrIDs(1).Add(aDS.Tables(strTablaVisSewServiciosWMS).Rows(i)(strCampoSerCoiId))
            'Next
            'End If

            'Return arrIDs
            Return arrDT
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    'JCM: 26/01/2011 Visor GISROEA
    <System.Web.Services.WebMethod(Description:="Obtener Nombre y C�digo de EstacionAforo", EnableSession:=True)> _
    Public Function DameEstacionAforo_Confederaciones() As EstacionAforo()
        Dim strQuery As String
        Dim strTablaEstacionesEt As String
        Dim strDatasetName As String, strCampoNombreConfederacion As String
        Dim strCampoIdConfe As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As EstacionAforo
        Try

            strTablaEstacionesEt = System.Configuration.ConfigurationManager.AppSettings("TablaRoeaEstacionesET")
            strCampoIdConfe = System.Configuration.ConfigurationManager.AppSettings("COD_AMBITO")
            strCampoNombreConfederacion = System.Configuration.ConfigurationManager.AppSettings("ORG_CUENCA")

            strDatasetName = strTablaEstacionesEt

            strQuery = "SELECT DISTINCT "
            'strQuery = strQuery + "COALESCE(TO_CHAR(" + strCampoIdConfe + "),' ') Cod_Conf,"
            'strQuery = strQuery + "INITCAP(translate(UPPER(" + strCampoNombreConfederacion + "), '�����', 'AEIOU')) " + strCampoNombreConfederacion
            strQuery = strQuery + "INITCAP(UPPER(" + strCampoNombreConfederacion + ")) " + strCampoNombreConfederacion
            strQuery = strQuery + " FROM " + strTablaEstacionesEt
            strQuery = strQuery + "  WHERE CAST(INITCAP(UPPER(TRIM(" + strCampoNombreConfederacion + "))) AS VARCHAR)!=CAST(INITCAP(UPPER(TRIM('A.A.A. -  D.H. Guadalete - Barbate'))) AS VARCHAR)"
            strQuery = strQuery + " ORDER BY(" + strCampoNombreConfederacion + ")"



            aDS = ExecuteQuery(strQuery, strTablaEstacionesEt, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer

            For Each aRow In aDS.Tables(0).Rows
                Dim aConf As New EstacionAforo()
                aConf.nombreConfederacion = aRow(strCampoNombreConfederacion)
                arrValores(i) = aConf
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    'JCM: 26/01/2011 Visor GISROEA
    <System.Web.Services.WebMethod(Description:="Obtener Estado de EstacionAforo", EnableSession:=True)> _
    Public Function DameEstacionAforo_Estados() As EstacionAforo()
        Dim strQuery As String
        Dim strTablaEstacionesEt As String
        Dim strDatasetName As String
        Dim strCampoEstado As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As EstacionAforo
        Try

            strTablaEstacionesEt = System.Configuration.ConfigurationManager.AppSettings("TablaRoeaEstacionesET")
            strCampoEstado = System.Configuration.ConfigurationManager.AppSettings("ESTADO")

            strDatasetName = strTablaEstacionesEt
            ' JCM: 04/02/2011 Estados: Alta los que sean Alta, Baja los que sean Baja y el resto OTROS
            strQuery = "SELECT DISTINCT CAST(INITCAP(UPPER(TRIM(ESTADO))) AS VARCHAR) ESTADO"
            ' strQuery = strQuery + " CASE WHEN TO_CHAR(INITCAP(UPPER(TRIM(ESTADO)))) = 'Alta' "
            ' strQuery = strQuery + " THEN TO_CHAR(INITCAP(UPPER(ESTADO)))"
            ' strQuery = strQuery + " WHEN TO_CHAR(INITCAP(UPPER(TRIM(ESTADO)))) = 'Baja'"
            ' strQuery = strQuery + " THEN TO_CHAR(INITCAP(UPPER(ESTADO)))"
            'strQuery = strQuery + " ELSE 'Otros'"
            'strQuery = strQuery + " END ESTADO "
            strQuery = strQuery + " FROM " + strTablaEstacionesEt
            strQuery = strQuery + "  WHERE (CAST(INITCAP(UPPER(TRIM(" + strCampoEstado + "))) AS VARCHAR)=CAST(INITCAP(UPPER(TRIM('Alta'))) AS VARCHAR) OR CAST(INITCAP(UPPER(TRIM(" + strCampoEstado + "))) AS VARCHAR)=CAST(INITCAP(UPPER(TRIM('Baja'))) AS VARCHAR))"  'WHERE TO_CHAR(INITCAP(UPPER(TRIM(" + strCampoEstado + "))))!='-'"
            strQuery = strQuery + " ORDER BY(" + strCampoEstado + ")"
            ' FIN: JCM: 04/02/2011 Estados: Alta los que sean Alta, Baja los que sean Baja y el resto OTROS

            'strQuery = "SELECT DISTINCT "
            'strQuery = strQuery + "CASE WHEN " + strCampoEstado + " = '-' THEN 'Otros'"
            'strQuery = strQuery + " ELSE TO_CHAR(INITCAP(translate(UPPER(" + strCampoEstado + "), '�����', 'AEIOU'))) END " + strCampoEstado
            'strQuery = strQuery + " FROM " + strTablaEstacionesEt
            'strQuery = strQuery + " ORDER BY(" + strCampoEstado + ")"

            aDS = ExecuteQuery(strQuery, strTablaEstacionesEt, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aConf As New EstacionAforo()
                aConf.Id = " "
                aConf.estado = IIf(aRow(strCampoEstado).Equals(System.DBNull.Value), Nothing, aRow(strCampoEstado))
                arrValores(i) = aConf
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    'JCM: 26/01/2011 Visor GISROEA
    <System.Web.Services.WebMethod(Description:="Obtener Situaci�n de Estacion de EstacionAforo", EnableSession:=True)> _
    Public Function DameEstacionAforo_SituacionesEstacion() As EstacionAforo()
        Dim strQuery As String
        Dim strTablaEstacionesEt As String
        Dim strDatasetName As String
        Dim strCampoSituacionEstacion As String
        Dim strCampoEstado As String
        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As EstacionAforo
        Try

            strTablaEstacionesEt = System.Configuration.ConfigurationManager.AppSettings("TablaRoeaEstacionesET")
            strCampoSituacionEstacion = System.Configuration.ConfigurationManager.AppSettings("SITUACION_ESTACION")
            strCampoEstado = System.Configuration.ConfigurationManager.AppSettings("ESTADO")

            strDatasetName = strTablaEstacionesEt

            strQuery = "SELECT DISTINCT "
            strQuery = strQuery + " CAST(INITCAP(UPPER(" + strCampoSituacionEstacion + ")) AS VARCHAR) " + strCampoSituacionEstacion
            strQuery = strQuery + " FROM " + strTablaEstacionesEt
            strQuery = strQuery + " WHERE " + strCampoSituacionEstacion + " IS NOT NULL"
            strQuery = strQuery + " AND " + strCampoEstado + " IN ('Alta','Baja')"
            strQuery = strQuery + " ORDER BY(" + strCampoSituacionEstacion + ")"

            aDS = ExecuteQuery(strQuery, strTablaEstacionesEt, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aConf As New EstacionAforo()
                aConf.Id = " "
                aConf.situacionEstacion = aRow(strCampoSituacionEstacion)
                arrValores(i) = aConf
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    'JCM: 26/01/2011 Visor GISROEA
    <System.Web.Services.WebMethod(Description:="Obtener Situaci�n de Estacion de EstacionAforo", EnableSession:=True)> _
    Public Function DameEstacionAforo_Todo() As EstacionAforo()
        Dim strQuery As String
        Dim strTablaEstacionesEt As String
        Dim strDatasetName As String
        Dim strCampoIdConfe As String
        Dim strCampoNombreConfederacion As String
        Dim strCampoEstado As String
        Dim strCampoSituacionEstacion As String

        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As EstacionAforo

        Try
            strTablaEstacionesEt = System.Configuration.ConfigurationManager.AppSettings("TablaRoeaEstacionesET")
            strCampoIdConfe = System.Configuration.ConfigurationManager.AppSettings("COD_AMBITO")
            strCampoNombreConfederacion = System.Configuration.ConfigurationManager.AppSettings("ORG_CUENCA")

            strCampoEstado = System.Configuration.ConfigurationManager.AppSettings("ESTADO")


            strCampoSituacionEstacion = System.Configuration.ConfigurationManager.AppSettings("SITUACION_ESTACION")


            strDatasetName = strTablaEstacionesEt

            'DameEstacionAforo_Confederaciones
            strQuery = "SELECT DISTINCT "
            strQuery = strQuery + "COALESCE(CAST(" + strCampoIdConfe + " AS VARCHAR),' ') Cod_Ambito,"
            strQuery = strQuery + "CAST(INITCAP(UPPER(" + strCampoNombreConfederacion + ")) AS VARCHAR) " + strCampoNombreConfederacion
            strQuery = strQuery + ",' ' " + strCampoEstado + ", ' '" + strCampoSituacionEstacion
            strQuery = strQuery + " FROM " + strTablaEstacionesEt
            'strQuery = strQuery + " ORDER BY(" + strCampoNombreConfederacion + ")"

            strQuery = strQuery + " UNION ALL "

            'DameEstacionAforo_Estados
            strQuery = strQuery + "SELECT DISTINCT 'Estados_CBO' Cod_Ambito, ' ' " + strCampoNombreConfederacion + ","
            strQuery = strQuery + "CASE WHEN " + strCampoEstado + " = '-' THEN 'Otros'"
            strQuery = strQuery + " ELSE CAST(INITCAP(UPPER(" + strCampoEstado + ")) AS VARCHAR) END " + strCampoEstado
            strQuery = strQuery + ", ' '" + strCampoSituacionEstacion
            strQuery = strQuery + " FROM " + strTablaEstacionesEt
            'strQuery = strQuery + " ORDER BY(" + strCampoEstado + ")"

            strQuery = strQuery + " UNION ALL "

            'DameEstacionAforo_SituacionesEstacion
            strQuery = strQuery + "SELECT DISTINCT 'SituacionesEstacion_CBO' Cod_Ambito, ' ' " + strCampoNombreConfederacion + ", ' '" + strCampoEstado + ","
            strQuery = strQuery + " CAST(INITCAP(UPPER(" + strCampoSituacionEstacion + ")) AS VARCHAR) " + strCampoSituacionEstacion
            strQuery = strQuery + " FROM " + strTablaEstacionesEt
            'strQuery = strQuery + " ORDER BY(" + strCampoSituacionEstacion + ")"

            aDS = ExecuteQuery(strQuery, strTablaEstacionesEt, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aConf As New EstacionAforo()

                aConf.Id = IIf(aRow(strCampoIdConfe).Equals(System.DBNull.Value), "", aRow(strCampoIdConfe).ToString())
                aConf.nombreConfederacion = IIf(aRow(strCampoNombreConfederacion).Equals(System.DBNull.Value), "", aRow(strCampoNombreConfederacion))
                aConf.estado = IIf(aRow(strCampoEstado).Equals(System.DBNull.Value), "", aRow(strCampoEstado))
                aConf.situacionEstacion = IIf(aRow(strCampoSituacionEstacion).Equals(System.DBNull.Value), "", aRow(strCampoSituacionEstacion))
                arrValores(i) = aConf
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    'JCM: 27/01/2011 Visor GISROEA

    <System.Web.Services.WebMethod(Description:="Obtener Estaciones de Aforo", EnableSession:=True)> _
    Public Function obtenerEstacionAforo(ByVal strWhere As String) As EstacionAforo()
        Dim strQuery As String
        Dim strTablaEstacionesEt As String
        Dim strDatasetName As String
        Dim strCampoCodHidro As String
        Dim strCampoNombreEstacion As String
        Dim strCampoSituacionEstacion As String
        Dim strCampoCodSituacionEstacion As String
        Dim strCampoEstado As String
        Dim strCampoNombreConfederacion As String
        Dim strCampoUTMX_H30_ETRS8 As String
        Dim strCampoUTMY_H30_ETRS89 As String
        Dim strCampoFotografia As String
        Dim strCampoSeccion As String
        Dim strCampoPlano As String
        Dim strCampoCodDemarcacion As String


        Dim aDS As Data.DataSet
        Dim aRow As System.Data.DataRow
        Dim arrValores() As EstacionAforo

        Try

            strTablaEstacionesEt = System.Configuration.ConfigurationManager.AppSettings("TablaRoeaEstacionesET")
            strCampoCodHidro = System.Configuration.ConfigurationManager.AppSettings("COD_HIDRO")
            strCampoNombreEstacion = System.Configuration.ConfigurationManager.AppSettings("NOM_ANUARIO")
            strCampoSituacionEstacion = System.Configuration.ConfigurationManager.AppSettings("SITUACION_ESTACION")
            strCampoCodSituacionEstacion = System.Configuration.ConfigurationManager.AppSettings("COD_SITUACION_ESTACION")
            strCampoEstado = System.Configuration.ConfigurationManager.AppSettings("ESTADO")
            strCampoNombreConfederacion = System.Configuration.ConfigurationManager.AppSettings("ORG_CUENCA")
            strCampoUTMX_H30_ETRS8 = System.Configuration.ConfigurationManager.AppSettings("UTMX_H30_ETRS89")
            strCampoUTMY_H30_ETRS89 = System.Configuration.ConfigurationManager.AppSettings("UTMY_H30_ETRS89")
            strCampoFotografia = System.Configuration.ConfigurationManager.AppSettings("FOTOGRAFIA")
            strCampoSeccion = System.Configuration.ConfigurationManager.AppSettings("SECCION")
            strCampoPlano = System.Configuration.ConfigurationManager.AppSettings("PLANO")
            strCampoCodDemarcacion = System.Configuration.ConfigurationManager.AppSettings("COD_DEMAR")

            strDatasetName = strTablaEstacionesEt

            strQuery = "SELECT DISTINCT "
            strQuery = strQuery + strCampoCodHidro + ","
            strQuery = strQuery + " CAST(" + strCampoNombreEstacion + " AS VARCHAR)" + strCampoNombreEstacion + ","
            strQuery = strQuery + " CAST(INITCAP(UPPER(" + strCampoSituacionEstacion + ")) AS VARCHAR)" + strCampoSituacionEstacion + ","
            strQuery = strQuery + " CAST(INITCAP(UPPER(" + strCampoCodSituacionEstacion + ")) AS VARCHAR)" + strCampoCodSituacionEstacion + ","
            strQuery = strQuery + " CAST(INITCAP(UPPER(" + strCampoEstado + ")) AS VARCHAR)" + strCampoEstado + ","
            strQuery = strQuery + " CAST(INITCAP(UPPER(" + strCampoNombreConfederacion + ")) AS VARCHAR)" + strCampoNombreConfederacion + ","
            strQuery = strQuery + " " + strCampoUTMX_H30_ETRS8 + ","
            strQuery = strQuery + " " + strCampoUTMY_H30_ETRS89 + ","
            strQuery = strQuery + " " + strCampoFotografia + ","
            strQuery = strQuery + " " + strCampoSeccion + ","
            strQuery = strQuery + " " + strCampoPlano + ","
            strQuery = strQuery + " " + strCampoCodDemarcacion

            strQuery = strQuery + " FROM DGA_USH.ROEA_ESTACIONES_ET"

            If (strWhere <> String.Empty) Then
                strQuery += " WHERE (CAST(INITCAP(UPPER(TRIM(" + strCampoEstado + "))) AS VARCHAR)=CAST(INITCAP(UPPER(TRIM('Alta'))) AS VARCHAR) OR CAST(INITCAP(UPPER(TRIM(" + strCampoEstado + "))) AS VARCHAR)=CAST(INITCAP(UPPER(TRIM('Baja'))) AS VARCHAR)) AND " + strWhere
            End If

            strQuery &= " ORDER BY " & strCampoNombreEstacion & " ASC"

            aDS = ExecuteQuery(strQuery, strTablaEstacionesEt, strDatasetName)
            ReDim arrValores(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            For Each aRow In aDS.Tables(0).Rows
                Dim aConf As New EstacionAforo()

                aConf.Id = IIf(aRow(strCampoCodHidro).Equals(System.DBNull.Value), "", aRow(strCampoCodHidro))
                aConf.nombreConfederacion = IIf(aRow(strCampoNombreConfederacion).Equals(System.DBNull.Value), "", aRow(strCampoNombreConfederacion))
                aConf.estado = IIf(aRow(strCampoEstado).Equals(System.DBNull.Value), "", aRow(strCampoEstado))
                aConf.situacionEstacion = IIf(aRow(strCampoSituacionEstacion).Equals(System.DBNull.Value), "", aRow(strCampoSituacionEstacion))
                aConf.codSituacionEstacion = IIf(aRow(strCampoCodSituacionEstacion).Equals(System.DBNull.Value), "", aRow(strCampoCodSituacionEstacion))
                aConf.codEstacion = IIf(aRow(strCampoCodHidro).Equals(System.DBNull.Value), "", aRow(strCampoCodHidro))
                aConf.nombreEstacion = IIf(aRow(strCampoNombreEstacion).Equals(System.DBNull.Value), "", aRow(strCampoNombreEstacion))
                aConf.UTMX_H30_ETRS89 = IIf(aRow(strCampoUTMX_H30_ETRS8).Equals(System.DBNull.Value), 0, aRow(strCampoUTMX_H30_ETRS8))
                aConf.UTMY_H30_ETRS89 = IIf(aRow(strCampoUTMY_H30_ETRS89).Equals(System.DBNull.Value), 0, aRow(strCampoUTMY_H30_ETRS89))
                aConf.fotografia = IIf(aRow(strCampoFotografia).Equals(System.DBNull.Value), "", aRow(strCampoFotografia))
                aConf.seccion = IIf(aRow(strCampoSeccion).Equals(System.DBNull.Value), "", aRow(strCampoSeccion))
                aConf.plano = IIf(aRow(strCampoPlano).Equals(System.DBNull.Value), "", aRow(strCampoPlano))
                aConf.CodDemarcacion = IIf(aRow(strCampoCodDemarcacion).Equals(System.DBNull.Value), "", aRow(strCampoCodDemarcacion))

                arrValores(i) = aConf
                i += 1
            Next
            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try

    End Function








    'JCM: 26/01/2011 Visor GISROEA
    <System.Web.Services.WebMethod(Description:="Obtener Situaci�n de Estacion de EstacionAforo", EnableSession:=True)> _
    Public Function DameEstacionAforo_ConfederacionEstadoSituacion() As Data.DataTable()
        Dim strQuery As String
        Dim strTablaEstacionesEt As String
        Dim strDatasetName As String
        Dim strCampoIdConfe As String
        Dim strCampoNombreConfederacion As String
        Dim strCampoEstado As String
        Dim strCampoSituacionEstacion As String

        Dim aDS As Data.DataSet
        Dim aDT As Data.DataTable

        Dim arrValores() As Data.DataTable

        Try
            strTablaEstacionesEt = System.Configuration.ConfigurationManager.AppSettings("TablaRoeaEstacionesET")
            strCampoIdConfe = System.Configuration.ConfigurationManager.AppSettings("Cod_AMBITO")
            strCampoNombreConfederacion = System.Configuration.ConfigurationManager.AppSettings("ORG_CUENCA")
            strCampoEstado = System.Configuration.ConfigurationManager.AppSettings("ESTADO")
            strCampoSituacionEstacion = System.Configuration.ConfigurationManager.AppSettings("SITUACION_ESTACION")

            strDatasetName = strTablaEstacionesEt

            ReDim arrValores(3)

            'DameEstacionAforo_Confederaciones
            strQuery = "SELECT DISTINCT "
            strQuery = strQuery + "COALESCE(CAST(" + strCampoIdConfe + " AS VARCHAR),' ') Cod_Ambito,"
            strQuery = strQuery + "INITCAP(UPPER(" + strCampoNombreConfederacion + "))) " + strCampoNombreConfederacion
            strQuery = strQuery + " FROM " + strTablaEstacionesEt
            strQuery = strQuery + " ORDER BY(" + strCampoNombreConfederacion + ")"

            aDS = ExecuteQuery(strQuery, strTablaEstacionesEt, strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(0) = aDT


            'DameEstacionAforo_Estados
            strQuery = "SELECT DISTINCT "
            strQuery = strQuery + "CASE WHEN " + strCampoEstado + " = '-' THEN 'Otros'"
            strQuery = strQuery + " ELSE CAST(INITCAP(UPPER(" + strCampoEstado + ")) AS VARCHAR) END " + strCampoEstado
            strQuery = strQuery + " FROM " + strTablaEstacionesEt
            strQuery = strQuery + " ORDER BY(" + strCampoEstado + ")"

            aDS = ExecuteQuery(strQuery, strTablaEstacionesEt, strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(1) = aDT


            'DameEstacionAforo_SituacionesEstacion
            strQuery = "SELECT DISTINCT "
            strQuery = strQuery + " CAST(INITCAP(UPPER(" + strCampoSituacionEstacion + ")) AS VARCHAR) " + strCampoSituacionEstacion
            strQuery = strQuery + " FROM " + strTablaEstacionesEt
            strQuery = strQuery + " ORDER BY(" + strCampoSituacionEstacion + ")"

            aDS = ExecuteQuery(strQuery, strTablaEstacionesEt, strDatasetName)
            aDT = aDS.Tables(0).Copy()
            datatableFieldsToUpper(aDT)
            arrValores(2) = aDT

            Return arrValores
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    <System.Web.Services.WebMethod(Description:="Obtencion de Servicios por Tema", EnableSession:=True)> _
    Public Function DameRamaTemasServico(ByVal idTema As String, ByVal idVisor As String, ByVal nombreServicio As String) As String
        Try

            Dim strQuery As String, strTableName As String, strDatasetName As String
            Dim aDS As Data.DataSet
            Dim strRoles As String
            Dim temasConcat As String
            strRoles = ObtenerAutenticacion()

            strTableName = "vis_tem_temas"
            strDatasetName = "vis_tem_temas"

            strQuery = " With Recursive X (TEM_CO_ID, TEM_CO_IDPADRE, TEM_DS_DESCRIPCION, VTM_NM_ORDEN) as "
            strQuery = strQuery + "("
            strQuery = strQuery + "select TT.TEM_CO_ID, TT.TEM_CO_IDPADRE, "
            strQuery = strQuery + "TT.TEM_DS_DESCRIPCION, "
            strQuery = strQuery + "VV.VTM_NM_ORDEN "
            strQuery = strQuery + "from VIS_TEM_TEMAS TT "
            strQuery = strQuery + "JOIN VIS_VTM_VISORES_TEMA VV "
            strQuery = strQuery + "ON TT.TEM_CO_ID=VV.TEM_CO_ID "
            strQuery = strQuery + "where TT.TEM_CO_ID in (SELECT MAX(TEMA_CO_ID) FROM VIS_TEMS_TEMAS_SERVICIO "
            strQuery = strQuery + "WHERE SER_CO_ID ='" & idTema & "') "
            strQuery = strQuery + "and "
            strQuery = strQuery + "VV.VIS_CO_ID = '" & idVisor & "' "
            strQuery = strQuery + "union all "
            strQuery = strQuery + "select TT.TEM_CO_ID, TT.TEM_CO_IDPADRE, "
            strQuery = strQuery + "TT.TEM_DS_DESCRIPCION, "
            strQuery = strQuery + "VV.VTM_NM_ORDEN "
            strQuery = strQuery + "from X INNER JOIN VIS_TEM_TEMAS TT "
            strQuery = strQuery + "ON X.TEM_CO_ID = TT.TEM_CO_IDPADRE "
            strQuery = strQuery + "JOIN VIS_VTM_VISORES_TEMA VV "
            strQuery = strQuery + "ON TT.TEM_CO_ID=VV.TEM_CO_ID "
            strQuery = strQuery + "where TT.TEM_CO_ID in (SELECT MAX(TEMA_CO_ID) FROM VIS_TEMS_TEMAS_SERVICIO "
            strQuery = strQuery + "WHERE SER_CO_ID ='" & idTema & "') "
            strQuery = strQuery + "and "
            strQuery = strQuery + "VV.VIS_CO_ID = '" & idVisor & "' "
            strQuery = strQuery + ") "
            strQuery = strQuery + "SELECT TEM_CO_ID,TEM_DS_DESCRIPCION,TEM_DS_DESCRIPCION DESCRIPCION_COMPLETA,VTM_NM_ORDEN,TEM_CO_IDPADRE "
            strQuery = strQuery + "FROM X"

            '            strQuery = "SELECT T.TEM_CO_ID,T.TEM_DS_DESCRIPCION, T.TEM_DS_DESCRIPCION DESCRIPCION_COMPLETA,T.VTM_NM_ORDEN,T.TEM_CO_IDPADRE"
            '            strQuery = strQuery + " FROM  (select TT.TEM_CO_ID,tt.TEM_DS_DESCRIPCION,TT.TEM_DS_DESCRIPCION DESCRIPCION_COMPLETA , VV.VTM_NM_ORDEN, VV.TEM_CO_IDPADRE "
            '           strQuery = strQuery + " FROM   VIS_TEM_TEMAS TT"
            '          strQuery = strQuery + " JOIN VIS_VTM_VISORES_TEMA  VV ON TT.TEM_CO_ID=VV.TEM_CO_ID  WHERE VV.VIS_CO_ID = '" & idVisor & "' ) T"
            '         strQuery = strQuery + " START WITH T.TEM_CO_ID in (SELECT MAX(TEMA_CO_ID) FROM VIS_TEMS_TEMAS_SERVICIO "
            '        strQuery = strQuery + " WHERE SER_CO_ID ='" & idTema & "')"
            '       strQuery = strQuery + " CONNECT BY PRIOR T.TEM_CO_IDPADRE= T.TEM_CO_ID"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow

            For Each aRow In aDS.Tables(0).Rows
                temasConcat = temasConcat + aRow("DESCRIPCION_COMPLETA") + "/"
            Next
            'temasConcat = temasConcat.Substring(0, temasConcat.Length - 1)
            Return temasConcat + nombreServicio

        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    <System.Web.Services.WebMethod(Description:="Obtencion xml GISPE", EnableSession:=True)> _
    Public Function crearXMLParametros(ByVal strXmlParams As String) As String
        Return crearXMLParametros_Generic(strXmlParams, "GISPE")
    End Function
    <System.Web.Services.WebMethod(Description:="Obtencion xml RAG", EnableSession:=True)> _
    Public Function crearXMLParametrosRAG(ByVal strXmlParams As String) As String
        Return crearXMLParametros_Generic(strXmlParams, "RAG")
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion xml DUNA y otros Sistemas", EnableSession:=True)> _
    Public Function crearXMLParametrosCOMUN(ByVal strXmlParams As String) As String
        Return crearXMLParametros_Generic(strXmlParams, "COMUN")
    End Function


    Public Function crearXMLParametros_Generic(ByVal strXmlParams As String, Optional ByVal BBDD As String = "GISPE") As String
        '<System.Web.Services.WebMethod(Description:="Obtencion xml", EnableSession:=True)> _
        'Public Function crearXMLParametros_Generic(ByVal strXmlParams As String) As String

        Try
            Dim xmlParams As XmlDocument = New XmlDocument()
            xmlParams.LoadXml(strXmlParams)
            Dim result As String = ""
            Dim m_nodelist As XmlNodeList
            Dim m_node As XmlNode
            Dim aDS As Data.DataSet
            Dim idOperacion As String, strTablaOperaciones As String, strDatasetName As String, strQuery As String, strCampoIdOperacion As String
            Dim strTablaParametros As String, strCampoGrupo As String
            Dim IDCapa_Query As New Hashtable()
            Dim nodoCapasConsulta As XmlNodeList

            If BBDD = "GISPE" Then
                m_nodelist = xmlParams.GetElementsByTagName("operacion")
            ElseIf BBDD = "RAG" Or BBDD = "COMUN" Then
                m_nodelist = xmlParams.GetElementsByTagName("CONFIGVISOR")
                'm_nodelist = xmlParams.GetElementsByTagName("EVENTO")
                'If m_nodelist(0).Attributes("TIPO").Value = "ALTA" Then
                '    idOperacion = "1"
                'ElseIf m_nodelist(0).Attributes("TIPO").Value = "MODIFICACION" Then
                '    idOperacion = "2"
                'End If
            End If

            idOperacion = m_nodelist(0).InnerText

            strTablaOperaciones = System.Configuration.ConfigurationManager.AppSettings("TablaOperaciones")
            strTablaParametros = System.Configuration.ConfigurationManager.AppSettings("TablaParametros")
            strCampoIdOperacion = System.Configuration.ConfigurationManager.AppSettings("CampoIdOperacion")
            strCampoGrupo = System.Configuration.ConfigurationManager.AppSettings("CampoGrupo")
            strDatasetName = strTablaOperaciones

            strQuery = "SELECT * "
            strQuery = strQuery + " FROM " + strTablaOperaciones + " operaciones "
            strQuery = strQuery + " LEFT JOIN " + strTablaParametros + " parametros "
            strQuery = strQuery + " ON operaciones." + strCampoIdOperacion + " = parametros." + strCampoIdOperacion + " "
            strQuery = strQuery + " WHERE parametros." + strCampoIdOperacion + " = '" + idOperacion + "'"
            '   strQuery = strQuery + " GROUP BY  " + strCampoGrupo


            If BBDD = "GISPE" Then
                aDS = ExecuteQueryGISPE(strQuery, strTablaOperaciones, strDatasetName)
            ElseIf BBDD = "RAG" Then
                aDS = ExecuteQueryRAG(strQuery, strTablaOperaciones, strDatasetName)
            ElseIf BBDD = "COMUN" Then
                aDS = ExecuteQuery(strQuery, strTablaOperaciones, strDatasetName, "bbddComun")
            End If

            If aDS.Tables(strTablaOperaciones).Rows.Count > 0 Then
                result = parsearXML(aDS.Tables(strTablaOperaciones))
            End If


            If BBDD = "GISPE" Then
                If Not result.Equals("") Then
                    Dim lastParam As String = ""
                    While result.IndexOf("#") <> -1   'parametros obligatorios #
                        Dim paramAux As String
                        Dim indexIni As Integer = result.IndexOf("#")
                        Dim resultAux As String = result.Substring(indexIni + 1)
                        paramAux = resultAux.Substring(0, resultAux.IndexOf("#"))
                        Dim nextChar As String = resultAux.Substring(0, 1)
                        If lastParam.Equals(paramAux) And Not nextChar.Equals("@") Then
                            Throw New Exception("Falta alg�n parametro " + paramAux)
                        Else
                            lastParam = paramAux
                            Dim tagname As String
                            If nextChar.Equals("@") Then
                                tagname = paramAux.Substring(1, paramAux.Length - 2)
                            Else
                                tagname = paramAux
                            End If
                            Dim nodeListAux As XmlNodeList = xmlParams.GetElementsByTagName(tagname)
                            If nodeListAux.Count = 0 Then
                                If nextChar.Equals("@") Then 'parametro opcional
                                    While result.IndexOf("#" + paramAux) <> -1
                                        result = Replace(result, "#" + paramAux + "#", Chr(39) + "" + Chr(39), , 1)
                                    End While
                                Else
                                    Throw New Exception("El parametro " + paramAux + " es necesario")
                                End If

                            Else
                                For Each aNode As XmlNode In nodeListAux
                                    If Not aNode Is Nothing And Not aNode.InnerText.Equals("") Then              'comilla simple
                                        result = Replace(result, "#" + paramAux + "#", Chr(39) + aNode.InnerText + Chr(39), , 1)
                                    End If
                                Next
                            End If

                        End If
                    End While

                End If
            Else
                'RAG Y DUNA

                Dim xmlEntidadString As String = ""
                Dim strTablaEntidades As String = "RAG_ENT_ENTIDADES"
                Dim strCampoXMLEntidad As String = "ENT_DS_XMLENTIDAD"

                If BBDD = "COMUN" Then
                    strTablaEntidades = "VIS_CAE_CATALOGOENTIDADES"
                End If


                'Creamos un nodo para las CAPAS DE VALIDACI�N
                Dim nodoCapasValidacion As XmlNodeList
                Dim nodoParams As XmlNode
                Dim strCapasValidacion As StringBuilder = New StringBuilder()
                Dim tipoOperacion As String
                Dim idCapaValidacion As String
                Dim hashtableCapas As Hashtable ' Registra informaci�n de las capas obtenidas

                nodoCapasValidacion = xmlParams.GetElementsByTagName("EVENTOS")
                If nodoCapasValidacion.Count > 0 Then
                    tipoOperacion = nodoCapasValidacion(0).ChildNodes(0).Attributes("TIPO").Value
                    If Not (nodoCapasValidacion(0).ChildNodes(0).Attributes("CAPA") Is Nothing) Then
                        idCapaValidacion = nodoCapasValidacion(0).ChildNodes(0).Attributes("CAPA").Value
                    End If
                End If

                If BBDD = "RAG" Then
                    If tipoOperacion = "CONSULTA_MULTIPLE" Then

                        nodoCapasConsulta = xmlParams.GetElementsByTagName("PARAM")

                        For i As Integer = 0 To nodoCapasConsulta.Count - 1
                            If nodoCapasConsulta(i).Attributes("NAME").Value = "CAPASCONSULTA" Then
                                For j As Integer = 0 To nodoCapasConsulta(i).ChildNodes.Count - 1
                                    Dim IDCAPACONS As String = nodoCapasConsulta(i).ChildNodes(j).Attributes("ID").Value
                                    Dim QUERYCAPA As String = nodoCapasConsulta(i).ChildNodes(j).ChildNodes(0).InnerText
                                    'nodoCapasConsulta(i).ChildNodes(j).Attributes("ID_CAPA")
                                    IDCapa_Query(IDCAPACONS) = QUERYCAPA
                                Next
                            End If
                        Next
                    End If


                    strDatasetName = strTablaEntidades
                    strQuery = "SELECT " + strCampoXMLEntidad
                    strQuery = strQuery + " FROM " + strTablaEntidades
                    strQuery = strQuery + " WHERE ENT_CO_ID  = '" + idCapaValidacion + "'"

                    aDS = ExecuteQueryRAG(strQuery, strTablaEntidades, strDatasetName)
                    If aDS.Tables(strTablaEntidades).Rows.Count > 0 Then
                        xmlEntidadString = aDS.Tables(strTablaEntidades).Rows(0).Item(strCampoXMLEntidad)
                    End If

                    If (xmlEntidadString <> "") Then
                        Dim XMLEntidadValidacion As XmlDocument = New XmlDocument()
                        XMLEntidadValidacion.LoadXml(xmlEntidadString)
                        Dim nodoEntidadValidacion As XmlNodeList = XMLEntidadValidacion.GetElementsByTagName("ENTIDAD")

                        ' Eliminamos el SERVICE_ENTIDAD (si existe)
                        If (result.IndexOf("<SERVICE_ENTIDAD") > -1) Then
                            Dim inicio As Integer = result.IndexOf("<SERVICE_ENTIDAD")
                            Dim fin As Integer = result.IndexOf("/>", inicio) + 2
                            result = result.Substring(fin, result.Length - (fin - inicio))
                        End If

                        ' Recuperamos todos los valores de RAG_ENT_ENTIDADES
                        strCapasValidacion.Append("<SERVICE_ENTIDAD ")
                        strCapasValidacion.Append("idServicio='")
                        strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("idServicio").Value)
                        strCapasValidacion.Append("' ")
                        If nodoEntidadValidacion(0).Attributes("auxAGS") Is Nothing = False Then
                            strCapasValidacion.Append("nombre=""")
                            'strCapasValidacion.Append("25830/" + nodoEntidadValidacion(0).Attributes("auxAGS").Value)
                            Dim longSR As Integer = nodoEntidadValidacion(0).Attributes("nombre").Value.IndexOf("/")
                            Dim sr As String = nodoEntidadValidacion(0).Attributes("nombre").Value.Substring(0, longSR)
                            strCapasValidacion.Append(sr + "/" + nodoEntidadValidacion(0).Attributes("auxAGS").Value)
                            strCapasValidacion.Append(""" ")
                        Else
                            strCapasValidacion.Append("nombre=""")
                            strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("nombre").Value)
                            strCapasValidacion.Append(""" ")
                        End If

                        If nodoEntidadValidacion(0).Attributes("tagServidorArcGIS") Is Nothing = False Then

                            Dim tagServidorArcGIS As String = nodoEntidadValidacion(0).Attributes("tagServidorArcGIS").Value

                            strCapasValidacion.Append("tagServidorArcGIS=""")
                            strCapasValidacion.Append(tagServidorArcGIS)
                            strCapasValidacion.Append(""" ")
                        End If

                        strCapasValidacion.Append("index=""")
                        strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("index").Value)
                        strCapasValidacion.Append(""" ")
                        strCapasValidacion.Append("campo=""")
                        strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("campo").Value)
                        strCapasValidacion.Append(""" ")
                        strCapasValidacion.Append("tipoCampo=""")
                        strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("tipoCampo").Value)
                        strCapasValidacion.Append(""" ")
                        strCapasValidacion.Append("/>")
                    End If

                    'arg 26/03/2014 vamos a interceptar los identificadores de propuestas
                    Dim m_nodePropuestas As XmlNodeList

                    m_nodePropuestas = xmlParams.GetElementsByTagName("PROPUESTAS")
                    If m_nodePropuestas.Count > 0 Then
                        ' Hay propuestas definidas en el XML de entrada al visor
                        For i As Integer = 0 To m_nodePropuestas(0).ChildNodes.Count - 1
                            Try
                                Dim idPropuesta As String = m_nodePropuestas(0).ChildNodes(i).Attributes("ID").Value
                                If Not String.IsNullOrEmpty(idPropuesta) Then

                                    ' Ejecuci�n de la consulta por cada vista
                                    Dim strTableName As String
                                    Dim strEsquemaPropuesta As String = "RA_PROPUESTA"
                                    Dim strVistaPropuestaConcesiones As String = "RAPRO_VIEW_GEO_CONCESIONES"
                                    Dim strVistaPropuestaCesiones As String = "RAPRO_VIEW_GEO_CESIONES"
                                    Dim strVistaPropuestaTrasvases As String = "RAPRO_VIEW_GEO_TRASVASES"
                                    Dim strVistaPropuestaLimitaciones As String = "RAPRO_VIEW_GEO_LIMITACIONES"
                                    Dim vistasImplicadas() As String = {strVistaPropuestaConcesiones, strVistaPropuestaCesiones,
                                                                        strVistaPropuestaLimitaciones, strVistaPropuestaTrasvases}

                                    Dim strCampoPropuesta As String = "ID_PROPUESTA"
                                    Dim strCampoGeoPropuesta As String = "ID_GEOGRAFICO_PROPUESTA"
                                    Dim strCampoGeoRegistro As String = "ID_GEOGRAFICO_REGISTRO"
                                    Dim strCampoCapaPropuesta As String = "ID_CAPA"

                                    Dim idCapa As String
                                    Dim idEntidadP As String
                                    Dim idEntidadG As String

                                    ' Se registran las capas en una hashtable
                                    If IsNothing(hashtableCapas) Then
                                        hashtableCapas = New Hashtable()
                                    End If

                                    ' Se ejecuta una consulta por vista y se revisan las capas
                                    For j As Integer = 0 To vistasImplicadas.Length - 1
                                        Try
                                            Dim fields As String = "*"

                                            'Se genera la consulta
                                            strDatasetName = vistasImplicadas(j)
                                            strTableName = strEsquemaPropuesta + "." + vistasImplicadas(j)
                                            strQuery = "SELECT " + fields
                                            strQuery = strQuery + " FROM " + strTableName
                                            strQuery = strQuery + " WHERE " + strCampoPropuesta + " = " + idPropuesta

                                            ' Se ejecuta la consulta
                                            aDS = ExecuteQueryRAG(strQuery, strTableName, strDatasetName)
                                            If aDS.Tables(strTableName).Rows.Count > 0 Then

                                                ' Se sustrae la informaci�n de capa y entidades de inter�s
                                                ' la lista de entidades funciona como filtro
                                                For Each row As DataRow In aDS.Tables(strTableName).Rows

                                                    If row.Table.Columns.Contains(strCampoCapaPropuesta) Then
                                                        idCapa = row(strCampoCapaPropuesta).ToString()

                                                        ' Id entidad propuesta
                                                        If row.Table.Columns.Contains(strCampoGeoPropuesta) Then
                                                            idEntidadP = row(strCampoGeoPropuesta).ToString()
                                                            If Not String.IsNullOrEmpty(idEntidadP) Then
                                                                If hashtableCapas.ContainsKey(idCapa) Then
                                                                    ' Se obtiene el listado con los ids de entidades
                                                                    Dim strEntidades As String = hashtableCapas.Item(idCapa).ToString()
                                                                    Dim separador As String = ","
                                                                    strEntidades += separador + idEntidadP
                                                                    ' Se a�ade el idEntidad a la lista
                                                                    hashtableCapas.Item(idCapa) = strEntidades
                                                                Else
                                                                    'Se da de alta una nueva capa con un nuevo idEntidad
                                                                    hashtableCapas.Add(idCapa, idEntidadP)
                                                                End If
                                                            End If
                                                        End If

                                                        ' Id entidad registro
                                                        If row.Table.Columns.Contains(strCampoGeoRegistro) Then
                                                            idEntidadG = row(strCampoGeoRegistro).ToString()
                                                            If Not String.IsNullOrEmpty(idEntidadG) Then
                                                                If hashtableCapas.ContainsKey(idCapa) Then
                                                                    ' Se obtiene el idEntidad listado
                                                                    Dim strEntidades As String = hashtableCapas.Item(idCapa).ToString()
                                                                    Dim separador As String = ","
                                                                    strEntidades += separador + idEntidadG
                                                                    ' Se a�ade el idEntidad a la lista
                                                                    hashtableCapas.Item(idCapa) = strEntidades
                                                                Else
                                                                    'Se da de alta una nueva capa con un nuevo idEntidad
                                                                    hashtableCapas.Add(idCapa, idEntidadG)
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                Next
                                            End If
                                        Catch ex As Exception
                                            ' Consulta a vista fallida, se prueba con la siguiente
                                        End Try
                                    Next
                                End If
                            Catch ex As Exception

                            End Try
                        Next
                    End If

                    'arg 26/03/2014 vamos a interceptar los identificadores de inscripciones
                    Dim m_nodeInscripciones As XmlNodeList
                    m_nodeInscripciones = xmlParams.GetElementsByTagName("INSCRIPCIONES")
                    If m_nodeInscripciones.Count > 0 Then
                        ' Hay inscripciones definidas en el XML de entrada al visor
                        For i As Integer = 0 To m_nodeInscripciones(0).ChildNodes.Count - 1
                            Try
                                Dim idInscripcion As String = m_nodeInscripciones(0).ChildNodes(i).Attributes("ID").Value.ToString()
                                'Dim idAsiento As String = m_nodeInscripciones(0).ChildNodes(i).Attributes("ID_ASIENTO").Value.ToString()
                                Dim idAsiento As String = m_nodeInscripciones(0).ChildNodes(i).Attributes("ASIENTO").Value.ToString()
                                If Not String.IsNullOrEmpty(idInscripcion) And Not String.IsNullOrEmpty(idAsiento) Then

                                    Dim strEsquema As String = "RA_REGISTRO"
                                    Dim strVistaConcesiones As String = "RA_VIEW_GEO_CONCESIONES"
                                    Dim strVistaCesiones As String = "RA_VIEW_GEO_CESIONES"
                                    Dim strVistaTrasvases As String = "RA_VIEW_GEO_TRASVASES"
                                    Dim strVistaLimitaciones As String = "RA_VIEW_GEO_LIMITACIONES"
                                    Dim vistasImplicadas() As String = {strVistaConcesiones, strVistaCesiones,
                                                                        strVistaLimitaciones, strVistaTrasvases}

                                    Dim strCampoInscripcion As String = "ID_INSCRIPCION"
                                    Dim strCampoInscripcionCesion As String = String.Format("{0}_CESION", strCampoInscripcion)
                                    Dim strCampoInscripcionLimitacion As String = String.Format("{0}_LIMITACION", strCampoInscripcion)
                                    Dim strCampoAsiento As String = "ID_ASIENTO"
                                    Dim strCampoAsientoCesion As String = String.Format("{0}_CESION", strCampoAsiento)
                                    Dim strCampoAsientoLimitacion As String = String.Format("{0}_LIMITACION", strCampoAsiento)
                                    Dim strCampoGeoRegistro As String = "ID_GEOGRAFICO"
                                    Dim strCampoCapaRegistro As String = "ID_CAPA"

                                    Dim idCapa As String
                                    Dim idEntidad As String ' Aqu� solo hay un id, el geogr�fico

                                    Dim strTableName As String

                                    ' Se registran las capas en una hashtable
                                    If IsNothing(hashtableCapas) Then
                                        hashtableCapas = New Hashtable()
                                    End If

                                    ' Se ejecuta una consulta por vista y se revisan las capas
                                    For j As Integer = 0 To vistasImplicadas.Length - 1
                                        Try
                                            Dim strBWhere As New StringBuilder()
                                            Select Case vistasImplicadas(j)
                                                Case strVistaCesiones
                                                    strBWhere.Append(strCampoAsientoCesion + "=" + idAsiento)
                                                    strBWhere.Append(" AND ")
                                                    strBWhere.Append(strCampoInscripcionCesion + "='" + idInscripcion + "'")
                                                Case strVistaLimitaciones
                                                    strBWhere.Append(strCampoAsientoLimitacion + "=" + idAsiento)
                                                    strBWhere.Append(" AND ")
                                                    strBWhere.Append(strCampoInscripcionLimitacion + "='" + idInscripcion + "'")
                                                Case Else
                                                    strBWhere.Append(strCampoAsiento + "=" + idAsiento)
                                                    strBWhere.Append(" AND ")
                                                    strBWhere.Append(strCampoInscripcion + "='" + idInscripcion + "'")
                                            End Select

                                            Dim fields As String = "*"

                                            'Se genera la consulta
                                            strDatasetName = vistasImplicadas(j)
                                            strTableName = strEsquema + "." + vistasImplicadas(j)
                                            strQuery = "SELECT " + fields
                                            strQuery = strQuery + " FROM " + strTableName
                                            strQuery = strQuery + " WHERE " + strBWhere.ToString()

                                            ' Se ejecuta la consulta
                                            aDS = ExecuteQueryRAG(strQuery, strTableName, strDatasetName)

                                            If aDS.Tables(strTableName).Rows.Count > 0 Then

                                                If Not IsNothing(hashtableCapas) Then
                                                    ' Se sustrae la informaci�n de capa y entidades de inter�s
                                                    ' la lista entidades funciona como filtro
                                                    For Each row As DataRow In aDS.Tables(strTableName).Rows

                                                        If row.Table.Columns.Contains(strCampoCapaRegistro) Then
                                                            idCapa = row(strCampoCapaRegistro).ToString()

                                                            'Id entidad registro (geogr�fico)
                                                            If row.Table.Columns.Contains(strCampoGeoRegistro) Then
                                                                idEntidad = row(strCampoGeoRegistro).ToString()
                                                                If Not String.IsNullOrEmpty(idEntidad) Then
                                                                    If hashtableCapas.ContainsKey(idCapa) Then
                                                                        ' Se obtiene el listado con los ids de entidades
                                                                        Dim strEntidades As String = hashtableCapas.Item(idCapa).ToString()
                                                                        Dim separador As String = ","
                                                                        strEntidades += separador + idEntidad
                                                                        ' Se a�ade el idEntidad a la lista
                                                                        hashtableCapas.Item(idCapa) = strEntidades
                                                                    Else
                                                                        ' Se da de alta una nueva capa con un nuevo idEntidad
                                                                        hashtableCapas.Add(idCapa, idEntidad)
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                    Next
                                                End If

                                            End If

                                        Catch ex As Exception
                                            ' Consulta a vista fallida, se prueba con la siguiente
                                        End Try
                                    Next
                                End If
                            Catch ex As Exception

                            End Try
                        Next
                    End If


                ElseIf BBDD = "COMUN" Then
                    strDatasetName = strTablaEntidades
                    strQuery = "SELECT " + strCampoXMLEntidad
                    strQuery = strQuery + " FROM " + strTablaEntidades
                    strQuery = strQuery + " WHERE ENT_CO_ID  = '" + idCapaValidacion + "'"

                    aDS = ExecuteQuery(strQuery, strTablaEntidades, strDatasetName, "bbddComun")
                    If aDS.Tables(strTablaEntidades).Rows.Count > 0 Then
                        xmlEntidadString = aDS.Tables(strTablaEntidades).Rows(0).Item(strCampoXMLEntidad)
                    End If

                    If (xmlEntidadString <> "") Then
                        Dim XMLEntidadValidacion As XmlDocument = New XmlDocument()
                        XMLEntidadValidacion.LoadXml(xmlEntidadString)
                        Dim nodoEntidadValidacion As XmlNodeList = XMLEntidadValidacion.GetElementsByTagName("ENTIDAD")

                        ' Eliminamos el SERVICE_ENTIDAD (si existe)
                        If (result.IndexOf("<SERVICE_ENTIDAD") > -1) Then
                            Dim inicio As Integer = result.IndexOf("<SERVICE_ENTIDAD")
                            Dim fin As Integer = result.IndexOf("/>", inicio) + 2
                            result = result.Substring(fin, result.Length - (fin - inicio))
                        End If

                        ' Recuperamos todos los valores de RAG_ENT_ENTIDADES
                        strCapasValidacion.Append("<SERVICE_ENTIDAD ")
                        strCapasValidacion.Append("idServicio='")
                        strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("idServicio").Value)
                        strCapasValidacion.Append("' ")
                        If nodoEntidadValidacion(0).Attributes("auxAGS") Is Nothing = False Then
                            strCapasValidacion.Append("nombre=""")
                            'strCapasValidacion.Append("25830/" + nodoEntidadValidacion(0).Attributes("auxAGS").Value)
                            Dim longSR As Integer = nodoEntidadValidacion(0).Attributes("nombre").Value.IndexOf("/")
                            Dim sr As String = nodoEntidadValidacion(0).Attributes("nombre").Value.Substring(0, longSR)
                            strCapasValidacion.Append(sr + "/" + nodoEntidadValidacion(0).Attributes("auxAGS").Value)
                            strCapasValidacion.Append(""" ")
                        Else
                            strCapasValidacion.Append("nombre=""")
                            strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("nombre").Value)
                            strCapasValidacion.Append(""" ")
                        End If

                        If nodoEntidadValidacion(0).Attributes("tagServidorArcGIS") Is Nothing = False Then

                            Dim tagServidorArcGIS As String = nodoEntidadValidacion(0).Attributes("tagServidorArcGIS").Value

                            strCapasValidacion.Append("tagServidorArcGIS=""")
                            strCapasValidacion.Append(tagServidorArcGIS)
                            strCapasValidacion.Append(""" ")
                        End If

                        strCapasValidacion.Append("index=""")
                        strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("index").Value)
                        strCapasValidacion.Append(""" ")
                        strCapasValidacion.Append("campo=""")
                        strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("campo").Value)
                        strCapasValidacion.Append(""" ")
                        strCapasValidacion.Append("tipoCampo=""")
                        strCapasValidacion.Append(nodoEntidadValidacion(0).Attributes("tipoCampo").Value)
                        strCapasValidacion.Append(""" ")
                        strCapasValidacion.Append("/>")
                    End If
                End If

                'arg 26/03/2014 se convierten las capas a hijos del nodo de capas a visualizar
                Dim m_nodeCapas As XmlNodeList
                m_nodeCapas = xmlParams.GetElementsByTagName("CAPASVISOR")

                If Not IsNothing(hashtableCapas) Then

                    For Each capaInfo In hashtableCapas

                        Dim capaExistente As Boolean = False
                        ' Si ya existe un elemento capa con el mismo id se deja igual
                        If m_nodeCapas.Count > 0 Then
                            ' Se busca si se ha solicitado una capa con el mismo id
                            For i As Integer = 0 To m_nodeCapas.Count - 1
                                Dim capaAnalizar As XmlNode = m_nodeCapas(0).ChildNodes(i)
                                If capaAnalizar.Attributes("ID").Value.ToString() = capaInfo.Key.ToString() Then
                                    ' Se sobreescribe
                                    capaAnalizar.InnerText = capaInfo.Value.ToString()
                                    capaExistente = True
                                    Exit For
                                End If
                            Next
                        Else
                            'No existe un CAPASVISOR (se crea uno)
                            Dim xmlNodeCapas As XmlNode = xmlParams.CreateElement("CAPASVISOR")
                            xmlParams.DocumentElement.AppendChild(xmlNodeCapas)
                            m_nodeCapas = xmlParams.GetElementsByTagName("CAPASVISOR")
                        End If

                        If Not capaExistente Then
                            ' Se crea el elemento CAPA
                            Dim xmlNodeCapa As XmlNode = xmlParams.CreateElement("CAPA")
                            Dim attrCapa As XmlAttribute = xmlParams.CreateAttribute("ID")
                            Dim attrAlpha As XmlAttribute = xmlParams.CreateAttribute("ALPHA")
                            attrAlpha.Value = "1"       ' Por defecto sin transparencia
                            attrCapa.Value = capaInfo.Key
                            xmlNodeCapa.Attributes.Append(attrCapa)
                            xmlNodeCapa.Attributes.Append(attrAlpha)

                            ' Se a�ade el filtro, de existir, a la capa
                            If Not String.IsNullOrEmpty(capaInfo.Value) Then
                                Dim xmlNodeFiltro As XmlNode = xmlParams.CreateElement("FILTRO")
                                xmlNodeFiltro.InnerText = capaInfo.Value.ToString()
                                xmlNodeCapa.AppendChild(xmlNodeFiltro)
                            End If

                            ' Se a�ade la capa a la lista 
                            m_nodeCapas(0).AppendChild(xmlNodeCapa)

                            ' arg 27/03/2014 Descomnetar para abrir las capas consultadas en el visor directamente
                            '' Si se solicita una consulta m�ltiple se actualiza la hashtable
                            'If tipoOperacion = "CONSULTA_MULTIPLE" Then
                            '    ' Todas las capas modificadas han de incluirse aqu�
                            '    If IDCapa_Query.ContainsKey(capaInfo.Key) Then
                            '        IDCapa_Query(capaInfo.Key) = capaInfo.Value
                            '    Else
                            '        IDCapa_Query.Add(capaInfo.Key, capaInfo.Value)
                            '    End If
                            'End If

                        End If
                    Next
                End If

                'agm 14/11/2013 vamos a incluir las capas solicitadas desde el XML de entrada para RAG
                'Dim m_nodeCapas As XmlNodeList
                Dim strBCapas As StringBuilder = New StringBuilder()
                'm_nodeCapas = xmlParams.GetElementsByTagName("CAPASVISOR")
                If m_nodeCapas.Count > 0 Then
                    'Hay capas definidas en el XML de entrada al visor
                    For i As Integer = 0 To m_nodeCapas(0).ChildNodes.Count - 1
                        Dim idCapa As String = m_nodeCapas(0).ChildNodes(i).Attributes("ID").Value
                        strBCapas.Append("<SERVICIO ")
                        Dim alpha As String = "100"

                        If Not m_nodeCapas(0).ChildNodes(i).Attributes("ALPHA") Is Nothing Then
                            alpha = (Double.Parse(m_nodeCapas(0).ChildNodes(i).Attributes("ALPHA").Value.Replace(".", ",")) * 100).ToString().Replace(",", ".")
                        End If

                        strBCapas.Append("alpha='")
                        strBCapas.Append(alpha)
                        strBCapas.Append("' ")


                        Dim auxAGS As String = ""
                        Dim index As String = ""
                        Dim filtro As String = ""
                        'tenemos filtro

                        If m_nodeCapas(0).ChildNodes(i).ChildNodes.Count > 0 Then
                            filtro = m_nodeCapas(0).ChildNodes(i).ChildNodes(0).InnerText
                        End If

                        ' NUEVO
                        ' Recuperamos el valor del nombre del CAMPO del XML_ENTIDAD
                        'Dim xmlEntidadString As String = ""
                        'Dim strTablaEntidades As String = "RAG_ENT_ENTIDADES"
                        'Dim strCampoXMLEntidad As String = "ENT_DS_XMLENTIDAD"
                        strDatasetName = strTablaEntidades
                        strQuery = "SELECT " + strCampoXMLEntidad
                        strQuery = strQuery + " FROM " + strTablaEntidades
                        strQuery = strQuery + " WHERE ENT_CO_ID  = '" + idCapa + "'"

                        If BBDD = "RAG" Then
                            aDS = ExecuteQueryRAG(strQuery, strTablaEntidades, strDatasetName)
                        ElseIf BBDD = "COMUN" Then
                            aDS = ExecuteQuery(strQuery, strTablaEntidades, strDatasetName, "bbddComun")
                        End If

                        If aDS.Tables(strTablaEntidades).Rows.Count > 0 Then
                            xmlEntidadString = aDS.Tables(strTablaEntidades).Rows(0).Item(strCampoXMLEntidad)
                        End If

                        If (xmlEntidadString <> "") Then
                            Dim XMLEntidad As XmlDocument = New XmlDocument()
                            XMLEntidad.LoadXml(xmlEntidadString)
                            Dim nodoEntidad As XmlNodeList = XMLEntidad.GetElementsByTagName("ENTIDAD")
                            Dim nombreCampoFiltro As String = nodoEntidad(0).Attributes("campo").Value
                            Dim tipoCampo As String = nodoEntidad(0).Attributes("tipoCampo").Value

                            strBCapas.Append(" id='")
                            strBCapas.Append(nodoEntidad(0).Attributes("idServicio").Value)
                            strBCapas.Append("' ")


                            index = nodoEntidad(0).Attributes("index").Value
                            strBCapas.Append("index=""")
                            strBCapas.Append(index)
                            strBCapas.Append(""" ")

                            If Not IDCapa_Query(idCapa) Is Nothing Then
                                Dim consulta As String = IDCapa_Query(idCapa)
                                Dim filtroComillas As String = ""
                                Dim arrValoresFiltro As Array
                                Dim noNumerico As Boolean
                                ' Si filtro es un conjunto de valores separados por coma
                                If (consulta.Contains("SELECT") = False) Then
                                    ' Si el campo del filtro es de tipo num�rico cogemos filtro tal cual,
                                    ' si es no num�rico a�adimos comillas
                                    If (tipoCampo = "VARCHAR" Or tipoCampo = "VARCHAR2" Or tipoCampo = "NVARCHAR" _
                                        Or tipoCampo = "NVARCHAR2" Or tipoCampo = "NCHAR") Then
                                        noNumerico = True
                                    Else
                                        noNumerico = False
                                    End If
                                    If (noNumerico) Then
                                        arrValoresFiltro = consulta.Split(",")
                                        For cont As Integer = 0 To arrValoresFiltro.Length - 2
                                            filtroComillas = filtroComillas + "'" + arrValoresFiltro(cont).ToString() + "',"
                                        Next
                                        filtroComillas = filtroComillas + "'" + arrValoresFiltro(arrValoresFiltro.Length - 1).ToString() + "'"
                                        consulta = filtroComillas
                                    End If
                                End If
                                consulta = nombreCampoFiltro + " IN (" + consulta + ")"

                                strBCapas.Append("query=""")
                                strBCapas.Append(consulta)
                                strBCapas.Append(""" ")
                            End If

                            If nodoEntidad(0).Attributes("auxAGS") Is Nothing = False Then

                                auxAGS = nodoEntidad(0).Attributes("auxAGS").Value

                                strBCapas.Append("auxAGS=""")
                                strBCapas.Append(auxAGS)
                                strBCapas.Append(""" ")



                                'Nuevo: a�adimos siempre en la respuesta el nombre del campo y su tipo de dato
                                'strBCapas.Append("nombreCampo=""")
                                'strBCapas.Append(nombreCampoFiltro)
                                'strBCapas.Append(""" ")
                                'strBCapas.Append("tipoCampo=""")
                                'strBCapas.Append(tipoCampo)
                                'strBCapas.Append(""" ")


                            End If

                            If nodoEntidad(0).Attributes("tagServidorArcGIS") Is Nothing = False Then

                                Dim tagServidorArcGIS As String = nodoEntidad(0).Attributes("tagServidorArcGIS").Value

                                strBCapas.Append("tagServidorArcGIS=""")
                                strBCapas.Append(tagServidorArcGIS)
                                strBCapas.Append(""" ")

                            End If

                            '- Si filtro es un conjunto de valores separados por coma (Ej.:"18,21,22") --> filtro: campo IN (18,21,22)
                            '- Si filtro es una SELECT (con los valores del filtro) --> filtro: campo IN ("SELECT proporcionada en XML Entrada")
                            If Not String.IsNullOrEmpty(filtro) Then
                                Dim filtroComillas As String = ""
                                Dim arrValoresFiltro As Array
                                Dim noNumerico As Boolean
                                ' Si filtro es un conjunto de valores separados por coma
                                If (filtro.Contains("SELECT") = False) Then
                                    ' Si el campo del filtro es de tipo num�rico cogemos filtro tal cual,
                                    ' si es no num�rico a�adimos comillas
                                    If (tipoCampo = "VARCHAR" Or tipoCampo = "VARCHAR2" Or tipoCampo = "NVARCHAR" _
                                        Or tipoCampo = "NVARCHAR2" Or tipoCampo = "NCHAR") Then
                                        noNumerico = True
                                    Else
                                        noNumerico = False
                                    End If
                                    If (noNumerico) Then
                                        arrValoresFiltro = filtro.Split(",")
                                        For cont As Integer = 0 To arrValoresFiltro.Length - 2
                                            filtroComillas = filtroComillas + "'" + arrValoresFiltro(cont).ToString() + "',"
                                        Next
                                        filtroComillas = filtroComillas + "'" + arrValoresFiltro(arrValoresFiltro.Length - 1).ToString() + "'"
                                        filtro = filtroComillas
                                    End If
                                End If
                                filtro = nombreCampoFiltro + " IN (" + filtro + ")"
                                strBCapas.Append("filter=""")
                                strBCapas.Append(filtro)
                                strBCapas.Append("""")
                            End If

                        End If

                        'End If

                        ' arg 27/03/2014 Se a�ade un nuevo campo para indicar el c�lculo del extent
                        strBCapas.Append(" computaExtent=")
                        If Not IsNothing(hashtableCapas) AndAlso hashtableCapas.ContainsKey(idCapa) Then
                            strBCapas.Append("'true'")
                        Else
                            strBCapas.Append("'false'")
                        End If

                        strBCapas.Append(" visible='true' />")

                    Next

                    result = result + "<CAPAS>" + strBCapas.ToString() + "</CAPAS>"
                End If
                result = result + strCapasValidacion.ToString()

            End If

            Return "<RAG>" + result + "</RAG>"
        Catch ex As Exception
            Return ex.ToString()
        End Try
    End Function
    Public Function parsearXML(ByVal entrada As DataTable) As String
        Dim xmlText As String, lastGrupo As String
        lastGrupo = ""
        Dim grupoOpen As Boolean = False
        For Each aRow As Object In entrada.Rows
            If Not aRow("GRUPO").Equals(System.DBNull.Value) And Not aRow("GRUPO").Equals("") Then
                If Not lastGrupo.Equals(aRow("GRUPO")) Then
                    If Not lastGrupo.Equals("") And grupoOpen Then
                        xmlText = xmlText + "</" + lastGrupo + ">"
                        grupoOpen = False
                    End If
                    lastGrupo = aRow("GRUPO")
                    xmlText = xmlText + "<" + aRow("GRUPO") + ">"
                    grupoOpen = True
                End If
            Else
                If grupoOpen Then
                    xmlText = xmlText + "</" + lastGrupo + ">"
                    grupoOpen = False
                End If
            End If
            xmlText = xmlText + aRow("PARAMS")
        Next
        If grupoOpen Then
            xmlText = xmlText + "</" + lastGrupo + ">"
            grupoOpen = False
        End If
        Return xmlText
    End Function

    <System.Web.Services.WebMethod(Description:="Obtener lista de IDs de entidades en capa por consulta.", EnableSession:=True)> _
    Public Function EjecutarFiltro(ByVal strquery As String, ByVal servidor As String, ByVal limiteResultados As Integer) As String

        ' arg 02/12/2013
        ' Recupera la informaci�n de la primera columna de la tabla resultante y devuelve los valores separados por comas
        ' Si ocurre un error devuelve null

        Dim strIds As String = String.Empty
        Try
            Dim strBuilder As StringBuilder = New StringBuilder()

            ' Limite de entidades a devolver -------------------------------------------
            'Se aplica, de existir, el l�mite de entidades
            'Si no se pasa el par�metro por valor, se recurre al archivo de configuraci�n
            Dim limiteConfig As Integer
            If Integer.TryParse(System.Configuration.ConfigurationManager.AppSettings("limiteResultadosConsultas"), limiteConfig) Then
                If limiteResultados = 0 Then
                    If (limiteConfig <> 0) Then
                        limiteResultados = limiteConfig
                    End If
                End If
            End If

            ' Si existe un valor l�mite (0 es que no existe) se aplica a la subconsulta
            If limiteResultados > 0 Then
                strBuilder.Append("SELECT * FROM (")
                strBuilder.Append(strquery.Trim())
                strBuilder.Append(") WHERE ROWNUM <=" + limiteResultados.ToString())
            Else
                strBuilder.Append(strquery.Trim())
            End If
            ' ----------------------------------------------------------------------------

            'Se solicita informaci�n al servidor
            Dim strDataset As String = "dsConsulta"
            Dim strTableName As String = "tableConsulta"
            Dim aDS As Data.DataSet
            Select Case servidor 'Listar aqu� los distintos servidores para los que se vaya a aplicar este m�todo
                Case "ServidorRAG"
                    aDS = ExecuteQueryRAG(strBuilder.ToString(), strDataset, strTableName)
                Case Else
                    aDS = ExecuteQuery(strBuilder.ToString(), strDataset, strTableName) ' ServidorArcGIS
            End Select

            ' Se reincia el constructor de cadenas
            'strBuilder.Clear() 'arg versionNET = 4.0
            strBuilder = New System.Text.StringBuilder() 'versionNET = 2.0

            'S�lo interesa la primera columna
            If aDS.Tables(0).Rows.Count > 0 Then
                'Si hay resultados

                Dim fila As Data.DataRow = aDS.Tables(0).Rows(0)
                ' Se recorre la tabla anotando los valores del campo devuelto (solo primera columna)
                Dim separador As String = String.Empty
                Dim valor As String = String.Empty
                For Each fila In fila.Table.Rows
                    valor = fila.Item(0).ToString()
                    strBuilder.Append(separador)
                    strBuilder.Append(valor)
                    separador = ","
                Next
                strIds = strBuilder.ToString()
            End If

            Return strIds
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtiene las capas de consulta de informaci�n administrativa.", EnableSession:=True)> _
    Public Function ObtenerCapasInformacionAdministrativaRAG() As String

        'arg 04/12/2013 Accede a los servicios de mapas necesarios para la consulta administrativa
        Try
            Dim result As String = String.Empty
            Dim strTablaParametros As String, strCampoIdOperacion As String, strCampoGrupo As String, strValorInforAdministrativa As String
            Dim strCampoInfoAdministrativa As String

            strTablaParametros = System.Configuration.ConfigurationManager.AppSettings("TablaParametros")
            strCampoIdOperacion = System.Configuration.ConfigurationManager.AppSettings("CampoIdOperacion")
            strCampoGrupo = System.Configuration.ConfigurationManager.AppSettings("CampoGrupo")
            strCampoInfoAdministrativa = "PARAMS"
            strValorInforAdministrativa = "SERVICESIDENTIFY"

            Dim strBuilder As System.Text.StringBuilder = New System.Text.StringBuilder()
            strBuilder.Append("SELECT " & strCampoInfoAdministrativa & " FROM ")
            strBuilder.Append(strTablaParametros)
            strBuilder.Append(" WHERE ")
            strBuilder.Append(strCampoIdOperacion)
            strBuilder.Append(" IS NULL ")
            strBuilder.Append(" AND ")
            strBuilder.Append(strCampoGrupo)
            strBuilder.Append(" = ")
            strBuilder.Append("'" & strValorInforAdministrativa & "'")

            Dim aDS As System.Data.DataSet
            aDS = ExecuteQueryRAG(strBuilder.ToString(), "InfoAdministrativa", "dsInfoAdministrativa")

            ' Se reincia el constructor de cadenas
            'strBuilder.Clear() 'arg versionNET = 4.0
            strBuilder = New System.Text.StringBuilder() 'versionNET = 2.0

            If aDS.Tables(0).Rows.Count > 0 Then

                ' Se abre el nodo principal
                strBuilder.Append("<CONSULTAS>" + vbLf)

                Dim row As System.Data.DataRow
                Dim numeroResultados As Integer = aDS.Tables(0).Rows.Count
                For i As Integer = 0 To numeroResultados - 1
                    row = aDS.Tables(0).Rows(i)
                    strBuilder.Append(row(strCampoInfoAdministrativa))
                    strBuilder.Append(vbLf)
                Next

                ' Se cierra el nodo principal
                strBuilder.Append("</CONSULTAS>")

                result = strBuilder.ToString()
            End If

            Return result
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    <System.Web.Services.WebMethod(Description:="Obtencion de la capas intervinientes en propuestas e inscripciones RAG", EnableSession:=True)> _
    Public Function ObtenerCapasRAG(ByVal strEntrada As String) As String

        Dim xmlParams As XmlDocument = New XmlDocument()
        Dim result As String = ""
        Dim m_nodelist As XmlNodeList
        Dim aDS As Data.DataSet
        Dim idOperacion As String, strTablaOperaciones As String, strDatasetName As String, strQuery As String, strCampoIdOperacion As String
        Dim strTablaParametros As String, strCampoGrupo As String
        Dim hashtableCapas As New Hashtable()
        Dim strBuilder As New StringBuilder()

        Dim id As String
        Dim asiento As String

        Try
            xmlParams.LoadXml(strEntrada)

            ' Se interceptan las propuestas
            m_nodelist = xmlParams.GetElementsByTagName("PROPUESTAS")
            If m_nodelist.Count > 0 Then
                ' Existen propuestas
                For i As Integer = 0 To m_nodelist(0).ChildNodes.Count - 1
                    id = m_nodelist(0).ChildNodes(i).Attributes("ID").Value
                    DamePropuestaRAG(id, hashtableCapas)
                Next

                ' Se limpian las variables
                id = Nothing
            End If

            ' Se vac�a la variable
            m_nodelist = Nothing

            ' Se interceptan las inscripciones (registros)
            m_nodelist = xmlParams.GetElementsByTagName("INSCRIPCIONES")
            If m_nodelist.Count > 0 Then
                ' Existen inscripciones
                For i As Integer = 0 To m_nodelist(0).ChildNodes.Count - 1
                    id = m_nodelist(0).ChildNodes(i).Attributes("ID").Value
                    asiento = m_nodelist(0).ChildNodes(i).Attributes("ASIENTO").Value
                    DameInscripcionRAG(id, asiento, hashtableCapas)
                Next
            End If

            ' Se vac�a la variable
            m_nodelist = Nothing

            ' Se busca el nodo capas
            m_nodelist = xmlParams.GetElementsByTagName("CAPAS")

            If Not IsNothing(hashtableCapas) Then
                ' Se han encontrado capas asociadas a las consultas (propuestas e inscripciones)
                For Each capaInfo In hashtableCapas

                    Dim capaExistente As Boolean = False
                    'Si ya existe un elemento con el mismo id se deja igual
                    If m_nodelist.Count > 0 Then
                        ' Se busca si se ha solicitado una capa con el mismo id
                        For i As Integer = 0 To m_nodelist.Count - 1
                            Dim capaAnalizar As XmlNode = m_nodelist(0).ChildNodes(i)
                            If capaAnalizar.Attributes("ID").Value.ToString() = capaInfo.Key.ToString() Then
                                ' Se sobreescribe el filtro
                                ' capaAnalizar.InnerText = capaInfo.Value.ToString()
                                Dim xmlNodeFiltro As XmlNode = capaAnalizar.Item("FILTRO")
                                xmlNodeFiltro.InnerText = capaInfo.Value
                                capaExistente = True
                                Exit For
                            End If
                        Next
                    Else
                        ' No existe un CAPAS (se crea uno)
                        Dim xmlNodeCapas As XmlNode = xmlParams.CreateElement("CAPAS")
                        xmlParams.DocumentElement.AppendChild(xmlNodeCapas)
                        m_nodelist = xmlParams.GetElementsByTagName("CAPAS")
                    End If

                    If Not capaExistente Then
                        ' Se crea el elemento CAPA
                        Dim xmlNodeCapa As XmlNode = xmlParams.CreateElement("CAPA")
                        Dim xmlNodeFiltro As XmlNode = xmlParams.CreateElement("FILTRO")
                        Dim attrCapa As XmlAttribute = xmlParams.CreateAttribute("ID")
                        Dim attrAlpha As XmlAttribute = xmlParams.CreateAttribute("ALPHA")

                        ' Se a�aden los atributos
                        attrAlpha.Value = "1"       ' Por defecto sin transparencia
                        attrCapa.Value = capaInfo.Key
                        xmlNodeCapa.Attributes.Append(attrCapa)
                        xmlNodeCapa.Attributes.Append(attrAlpha)

                        ' Se a�ade el filtro
                        xmlNodeFiltro.InnerText = capaInfo.Value 'Aqu� se almacena el filtro
                        xmlNodeCapa.AppendChild(xmlNodeFiltro)

                        ' Se a�ade la capa a la lista 
                        m_nodelist(0).AppendChild(xmlNodeCapa)
                    End If
                Next
            End If

        Catch ex As Exception
            Return Nothing
        End Try

        Return xmlParams.InnerXml

    End Function

    Private Sub DamePropuestaRAG(ByVal idPropuesta As String, Optional ByRef hashtableCapas As Hashtable = Nothing)
        If Not String.IsNullOrEmpty(idPropuesta) Then

            ' Ejecuci�n de la consulta por cada vista
            Dim strEsquemaPropuesta As String = "RA_PROPUESTA"
            Dim strVistaPropuestaConcesiones As String = "RAPRO_VIEW_GEO_CONCESIONES"
            Dim strVistaPropuestaCesiones As String = "RAPRO_VIEW_GEO_CESIONES"
            Dim strVistaPropuestaTrasvases As String = "RAPRO_VIEW_GEO_TRASVASES"
            Dim strVistaPropuestaLimitaciones As String = "RAPRO_VIEW_GEO_LIMITACIONES"
            Dim vistasImplicadas() As String = {strVistaPropuestaConcesiones, strVistaPropuestaCesiones,
                                                strVistaPropuestaLimitaciones, strVistaPropuestaTrasvases}

            Dim strCampoPropuesta As String = "ID_PROPUESTA"
            Dim strCampoGeoPropuesta As String = "ID_GEOGRAFICO_PROPUESTA"
            Dim strCampoGeoRegistro As String = "ID_GEOGRAFICO_REGISTRO"
            Dim strCampoCapaPropuesta As String = "ID_CAPA"

            Dim idCapa As String
            Dim idEntidadP As String
            Dim idEntidadG As String

            Dim strDatasetName As String
            Dim strTableName As String
            Dim strQuery As String
            Dim aDS As DataSet

            ' Se ejecuta una consulta por vista y se revisan las capas
            For j As Integer = 0 To vistasImplicadas.Length - 1

                Try
                    Dim fields As String = "*"

                    'Se genera la consulta
                    strDatasetName = vistasImplicadas(j)
                    strTableName = strEsquemaPropuesta + "." + vistasImplicadas(j)
                    strQuery = "SELECT " + fields
                    strQuery = strQuery + " FROM " + strTableName
                    strQuery = strQuery + " WHERE " + strCampoPropuesta + " = " + idPropuesta

                    ' Se ejecuta la consulta
                    aDS = ExecuteQueryRAG(strQuery, strTableName, strDatasetName)

                    If aDS.Tables(strTableName).Rows.Count > 0 Then

                        If Not IsNothing(hashtableCapas) Then
                            ' Se sustrae la informaci�n de capa y entidades de inter�s
                            ' la lista de entidades funciona como filtro
                            For Each row As DataRow In aDS.Tables(strTableName).Rows

                                If row.Table.Columns.Contains(strCampoCapaPropuesta) Then
                                    idCapa = row(strCampoCapaPropuesta).ToString()

                                    ' Id entidad propuesta
                                    If row.Table.Columns.Contains(strCampoGeoPropuesta) Then
                                        idEntidadP = row(strCampoGeoPropuesta).ToString()

                                        If Not String.IsNullOrEmpty(idEntidadP) Then

                                            If hashtableCapas.ContainsKey(idCapa) Then
                                                ' Se obtiene el listado con los ids de entidades
                                                Dim strEntidades As String = hashtableCapas.Item(idCapa).ToString()
                                                Dim separador As String = ","
                                                strEntidades += separador + idEntidadP
                                                ' Se a�ade el idEntidad a la lista
                                                hashtableCapas.Item(idCapa) = strEntidades
                                            Else
                                                'Se da de alta una nueva capa con un nuevo idEntidad
                                                hashtableCapas.Add(idCapa, idEntidadP)
                                            End If

                                        End If

                                    End If

                                    ' Id entidad registro
                                    If row.Table.Columns.Contains(strCampoGeoRegistro) Then
                                        idEntidadG = row(strCampoGeoRegistro).ToString()

                                        If Not String.IsNullOrEmpty(idEntidadG) Then

                                            If hashtableCapas.ContainsKey(idCapa) Then
                                                ' Se obtiene el idEntidad listado
                                                Dim strEntidades As String = hashtableCapas.Item(idCapa).ToString()
                                                Dim separador As String = ","
                                                strEntidades += separador + idEntidadG
                                                ' Se a�ade el idEntidad a la lista
                                                hashtableCapas.Item(idCapa) = strEntidades
                                            Else
                                                'Se da de alta una nueva capa con un nuevo idEntidad
                                                hashtableCapas.Add(idCapa, idEntidadG)
                                            End If

                                        End If

                                    End If

                                End If
                            Next
                        End If
                    End If
                Catch ex As Exception
                    ' Consulta a vista fallida, se prueba con la siguiente
                End Try
            Next
        End If
    End Sub

    Private Sub DameInscripcionRAG(ByVal idInscripcion As String, ByVal idAsiento As String, Optional ByRef hashtableCapas As Hashtable = Nothing)
        If Not String.IsNullOrEmpty(idInscripcion) AndAlso Not String.IsNullOrEmpty(idAsiento) Then

            Dim strEsquema As String = "RA_REGISTRO"
            Dim strVistaConcesiones As String = "RA_VIEW_GEO_CONCESIONES"
            Dim strVistaCesiones As String = "RA_VIEW_GEO_CESIONES"
            Dim strVistaTrasvases As String = "RA_VIEW_GEO_TRASVASES"
            Dim strVistaLimitaciones As String = "RA_VIEW_GEO_LIMITACIONES"
            Dim vistasImplicadas() As String = {strVistaConcesiones, strVistaCesiones,
                                                strVistaLimitaciones, strVistaTrasvases}

            Dim strCampoInscripcion As String = "ID_INSCRIPCION"
            Dim strCampoInscripcionCesion As String = String.Format("{0}_CESION", strCampoInscripcion)
            Dim strCampoInscripcionLimitacion As String = String.Format("{0}_LIMITACION", strCampoInscripcion)
            Dim strCampoAsiento As String = "ID_ASIENTO"
            Dim strCampoAsientoCesion As String = String.Format("{0}_CESION", strCampoAsiento)
            Dim strCampoAsientoLimitacion As String = String.Format("{0}_LIMITACION", strCampoAsiento)
            Dim strCampoGeoRegistro As String = "ID_GEOGRAFICO"
            Dim strCampoCapaRegistro As String = "ID_CAPA"

            Dim idCapa As String
            Dim idEntidad As String ' Aqu� solo hay un id geogr�fico

            Dim strDatasetName As String
            Dim strTableName As String
            Dim strQuery As String
            Dim aDS As DataSet


            ' Se ejecuta una consulta por vista y se revisan las capas
            For j As Integer = 0 To vistasImplicadas.Length - 1
                Try
                    Dim strBWhere As New StringBuilder()
                    Select Case vistasImplicadas(j)
                        Case strVistaCesiones
                            strBWhere.Append(strCampoAsientoCesion + "=" + idAsiento)
                            strBWhere.Append(" AND ")
                            strBWhere.Append(strCampoInscripcionCesion + "='" + idInscripcion + "'")
                        Case strVistaLimitaciones
                            strBWhere.Append(strCampoAsientoLimitacion + "=" + idAsiento)
                            strBWhere.Append(" AND ")
                            strBWhere.Append(strCampoInscripcionLimitacion + "='" + idInscripcion + "'")
                        Case Else
                            strBWhere.Append(strCampoAsiento + "=" + idAsiento)
                            strBWhere.Append(" AND ")
                            strBWhere.Append(strCampoInscripcion + "='" + idInscripcion + "'")
                    End Select

                    Dim fields As String = "*"

                    'Se genera la consulta
                    strDatasetName = vistasImplicadas(j)
                    strTableName = strEsquema + "." + vistasImplicadas(j)
                    strQuery = "SELECT " + fields
                    strQuery = strQuery + " FROM " + strTableName
                    strQuery = strQuery + " WHERE " + strBWhere.ToString()

                    ' Se ejecuta la consulta
                    aDS = ExecuteQueryRAG(strQuery, strTableName, strDatasetName)

                    If aDS.Tables(strTableName).Rows.Count > 0 Then

                        If Not IsNothing(hashtableCapas) Then
                            ' Se sustrae la informaci�n de capa y entidades de inter�s
                            ' la lista entidades funciona como filtro
                            For Each row As DataRow In aDS.Tables(strTableName).Rows

                                If row.Table.Columns.Contains(strCampoCapaRegistro) Then
                                    idCapa = row(strCampoCapaRegistro).ToString()

                                    'Id entidad registro (geogr�fico)
                                    If row.Table.Columns.Contains(strCampoGeoRegistro) Then
                                        idEntidad = row(strCampoGeoRegistro).ToString()
                                        If Not String.IsNullOrEmpty(idEntidad) Then
                                            If hashtableCapas.ContainsKey(idCapa) Then
                                                ' Se obtiene el listado con los ids de entidades
                                                Dim strEntidades As String = hashtableCapas.Item(idCapa).ToString()
                                                Dim separador As String = ","
                                                strEntidades += separador + idEntidad
                                                ' Se a�ade el idEntidad a la lista
                                                hashtableCapas.Item(idCapa) = strEntidades
                                            Else
                                                ' Se da de alta una nueva capa con un nuevo idEntidad
                                                hashtableCapas.Add(idCapa, idEntidad)
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        End If

                    End If

                Catch ex As Exception
                    ' Consulta a vista fallida, se prueba con la siguiente
                End Try
            Next
        End If
    End Sub

End Class