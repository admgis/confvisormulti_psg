Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service


Public Class Servicio

    Public Enum tipoPrivacidad
        Publico = 0
        Accesible = 1
        Privado = 2
    End Enum
    Private _id As Integer
    Private _host As String
    Private _nombre As String
    Private _nombrelargo As String
    Private _descripcion As String
    Private _url As String
    Private _metodo As String
    Private _tipo As String
    Private _info As String
    Private _alpha As Integer
    Private _departamento As Integer
    Private _departamentoPadre As Integer
    Private _mostrarLeyenda As String
    Private _escalaMinima As Integer
    Private _escalaMaxima As Integer
    Private _escalaMinimaImpresion As Integer
    Private _idtemapadre As Integer
    Private _privacidad As tipoPrivacidad
    Private _cabeceraLeyenda As Boolean
    Private _identificarRest As Boolean
    Private _capas As String
    Private _uuid(0 To 1) As String
    Private _rutaTemas As String
    Private _uuidArray As Object
    Private _tiempoDesde As String
    Private _tiempoHasta As String
    Private _tipoTiempo As String
    Private _activarPlayTiempo As String

    Private _nomTabla As String = "VIS_SER_SERVICIOS"

#Region "Propiedades de la Clase"

    Public Property id() As Integer
        Get
            id = _id
        End Get
        Set(ByVal aId As Integer)
            _id = aId
        End Set
    End Property

    Public Property host() As String
        Get
            host = _host
        End Get
        Set(ByVal aHost As String)
            _host = aHost
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property nombrelargo() As String
        Get
            nombrelargo = _nombrelargo
        End Get
        Set(ByVal aNombrelargo As String)
            _nombrelargo = aNombrelargo
        End Set
    End Property

    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDescripcion As String)
            _descripcion = aDescripcion
        End Set
    End Property

    Public Property url() As String
        Get
            url = _url
        End Get
        Set(ByVal aUrl As String)
            _url = aUrl
        End Set
    End Property

    Public Property uuid() As String()
        Get
            uuid = _uuid
        End Get
        Set(ByVal aUuid() As String)
            _uuid = aUuid
        End Set
    End Property

    Public Property metodo() As String
        Get
            metodo = _metodo
        End Get
        Set(ByVal aMetodo As String)
            _metodo = aMetodo
        End Set
    End Property

    Public Property tipo() As String
        Get
            tipo = _tipo
        End Get
        Set(ByVal aTipo As String)
            _tipo = aTipo
        End Set
    End Property

    Public Property info() As String
        Get
            info = _info
        End Get
        Set(ByVal ainfo As String)
            _info = ainfo
        End Set
    End Property

    Public Property alpha() As Integer
        Get
            alpha = _alpha
        End Get
        Set(ByVal aAlpha As Integer)
            _alpha = aAlpha
        End Set
    End Property
    Public Property departamento() As Integer
        Get
            departamento = _departamento
        End Get
        Set(ByVal aDepartamento As Integer)
            _departamento = aDepartamento
        End Set
    End Property
    Public Property departamentoPadre() As Integer
        Get
            departamentoPadre = _departamentoPadre
        End Get
        Set(ByVal aDepartamentoPadre As Integer)
            _departamentoPadre = aDepartamentoPadre
        End Set
    End Property
    Public Property mostrarLeyenda() As String
        Get
            mostrarLeyenda = _mostrarLeyenda
        End Get
        Set(ByVal aMostrarLeyenda As String)
            _mostrarLeyenda = aMostrarLeyenda
        End Set
    End Property

    Public Property tiempoDesde() As String
        Get
            tiempoDesde = _tiempoDesde
        End Get
        Set(ByVal value As String)
            _tiempoDesde = value
        End Set
    End Property

    Public Property tiempoHasta() As String
        Get
            tiempoHasta = _tiempoHasta
        End Get
        Set(ByVal value As String)
            _tiempoHasta = value
        End Set
    End Property

    Public Property tipoTiempo() As String
        Get
            tipoTiempo = _tipoTiempo
        End Get
        Set(ByVal value As String)
            _tipoTiempo = value
        End Set
    End Property

    Public Property activarPlayTiempo() As String
        Get
            activarPlayTiempo = _activarPlayTiempo
        End Get
        Set(ByVal value As String)
            _activarPlayTiempo = value
        End Set
    End Property
    Public Property escalaMinima() As Integer
        Get
            escalaMinima = _escalaMinima
        End Get
        Set(ByVal aEscalaMinima As Integer)
            _escalaMinima = aEscalaMinima
        End Set
    End Property
    Public Property escalaMaxima() As Integer
        Get
            escalaMaxima = _escalaMaxima
        End Get
        Set(ByVal aEscalaMaxima As Integer)
            _escalaMaxima = aEscalaMaxima
        End Set
    End Property

    Public Property escalaMinimaImpresion() As Integer
        Get
            escalaMinimaImpresion = _escalaMinimaImpresion
        End Get
        Set(ByVal aEscalaMinimaImpresion As Integer)
            _escalaMinimaImpresion = aEscalaMinimaImpresion
        End Set
    End Property
    Public Property IdPadre() As Integer
        Get
            IdPadre = _idtemapadre
        End Get
        Set(ByVal avalue As Integer)
            _idtemapadre = avalue
        End Set
    End Property
    Public Property Privacidad() As tipoPrivacidad
        Get
            Privacidad = _privacidad
        End Get
        Set(ByVal aPrivacidad As tipoPrivacidad)
            _privacidad = aPrivacidad
        End Set
    End Property

    Public Property CabeceraLeyenda() As Boolean
        Get
            CabeceraLeyenda = _cabeceraLeyenda
        End Get
        Set(ByVal aCabeceraLeyenda As Boolean)
            _cabeceraLeyenda = aCabeceraLeyenda
        End Set
    End Property

    Public Property IdentificarRest() As Boolean
        Get
            IdentificarRest = _identificarRest
        End Get
        Set(ByVal aIdentificarRest As Boolean)
            _identificarRest = aIdentificarRest
        End Set
    End Property

    Public Property capas() As String
        Get
            capas = _capas
        End Get
        Set(ByVal aCapas As String)
            _capas = aCapas
        End Set
    End Property

    Public Property rutaTemas() As String
        Get
            rutaTemas = _rutaTemas
        End Get
        Set(ByVal aRutaTemas As String)
            _rutaTemas = aRutaTemas
        End Set
    End Property
    Public Property uuidArray() As Object
        Get
            uuidArray = _uuidArray
        End Get
        Set(ByVal aUuidArray As Object)
            _uuidArray = aUuidArray
        End Set
    End Property
#End Region


#Region "Funciones de la Clase"
    Public Function Cargar(ByVal idServicio As Integer, ByVal codIdioma As String, Optional ByVal usuario As String = "") As Servicio
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "VIS_SER_SERVICIOS"
            strDatasetName = "VIS_SER_SERVICIOS"

            strQuery = "select SER_NM_ESCALAIMP,SER_NM_ESCALAMIN,SER_BO_MOSTRARLEYENDA,CASE WHEN VIS_DEP_DEPARTAMENTOS.DEP_CO_IDPADRE IS null THEN 0 ELSE VIS_DEP_DEPARTAMENTOS.DEP_CO_IDPADRE END as DEP_CO_IDPADRE,vis_ser_servicios.dep_co_id, hos_ds_nombre,ser_ds_nombre,CASE WHEN VIS_SDS_DESC_SERVICIOS.ser_ds_descripcion IS null THEN vis_ser_servicios.SER_DS_DESCRIPCION ELSE vis_sds_desc_servicios.SER_DS_DESCRIPCION END as ser_ds_descripcion,ser_ds_url,ser_ds_path," & _
            "vis_tis_tipo_servicio.TIS_DS_DESCRIPCION,ser_ds_info, ser_co_alpha, CASE WHEN VIS_SDS_DESC_SERVICIOS.ser_ds_alias IS null THEN vis_ser_servicios.SER_DS_ALIAS ELSE VIS_SDS_DESC_SERVICIOS.ser_ds_alias END as ser_ds_alias, SER_BO_CABECERALEYENDA, SER_BO_IDENTIFICAR_REST, SER_DS_CAPASWMS, " & _
            "SER_DS_TIEMPO_DESDE, SER_DS_TIEMPO_HASTA, SER_BO_ACTIVAR_PLAY_TIEMPO, SER_DS_TIPO_TIEMPO " & _
            "from ((vis_ser_servicios left join VIS_SDS_DESC_SERVICIOS on vis_ser_servicios.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id " & _
            "and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') left join vis_hos_hosts on vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
            "inner join vis_tis_tipo_servicio on vis_ser_servicios.tis_co_id=vis_tis_tipo_servicio.tis_co_id " & _
            "left join VIS_DEP_DEPARTAMENTOS on vis_ser_servicios.dep_co_id=VIS_DEP_DEPARTAMENTOS.dep_co_id " & _
            " where (vis_ser_servicios.ser_co_id=" + idServicio.ToString + ") "



            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            For Each aRow In aDS.Tables(0).Rows
                Me.id = idServicio
                Me.host = IIf(aRow("HOS_DS_NOMBRE").Equals(System.DBNull.Value), "", aRow("HOS_DS_NOMBRE"))
                Me.nombre = IIf(aRow("SER_DS_NOMBRE").Equals(System.DBNull.Value), "", aRow("SER_DS_NOMBRE"))
                Me.descripcion = IIf(aRow("SER_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("SER_DS_DESCRIPCION"))
                Me.url = IIf(aRow("SER_DS_URL").Equals(System.DBNull.Value), "", aRow("SER_DS_URL"))
                Me.metodo = IIf(aRow("SER_DS_PATH").Equals(System.DBNull.Value), "", aRow("SER_DS_PATH"))
                Me.info = IIf(aRow("SER_DS_INFO").Equals(System.DBNull.Value), "", aRow("SER_DS_INFO"))
                Me.tipo = IIf(aRow("TIS_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TIS_DS_DESCRIPCION"))
                Me.alpha = IIf(aRow("SER_CO_ALPHA").Equals(System.DBNull.Value), 100, aRow("SER_CO_ALPHA"))
                Me.nombrelargo = IIf(aRow("SER_DS_ALIAS").Equals(System.DBNull.Value), "", aRow("SER_DS_ALIAS"))
                Me.departamento = IIf(aRow("DEP_CO_ID").Equals(System.DBNull.Value), 0, aRow("DEP_CO_ID"))
                Me.departamentoPadre = IIf(aRow("DEP_CO_IDPADRE").Equals(System.DBNull.Value), 0, aRow("DEP_CO_IDPADRE"))
                Me.mostrarLeyenda = IIf(aRow("SER_BO_MOSTRARLEYENDA").Equals(System.DBNull.Value), "Y", aRow("SER_BO_MOSTRARLEYENDA"))
                Me.activarPlayTiempo = IIf(aRow("SER_BO_ACTIVAR_PLAY_TIEMPO").Equals(System.DBNull.Value), "N", aRow("SER_BO_ACTIVAR_PLAY_TIEMPO"))
                Me.tipoTiempo = IIf(aRow("SER_DS_TIPO_TIEMPO").Equals(System.DBNull.Value), "", aRow("SER_DS_TIPO_TIEMPO"))
                Me.tiempoDesde = IIf(aRow("SER_DS_TIEMPO_DESDE").Equals(System.DBNull.Value), "", aRow("SER_DS_TIEMPO_DESDE"))
                Me.tiempoHasta = IIf(aRow("SER_DS_TIEMPO_HASTA").Equals(System.DBNull.Value), "", aRow("SER_DS_TIEMPO_HASTA"))
                Me.escalaMinima = IIf(aRow("SER_NM_ESCALAMIN").Equals(System.DBNull.Value), 0, aRow("SER_NM_ESCALAMIN"))
                Me.escalaMaxima = 0
                'Me.escalaMaxima = IIf(aRow("SER_NM_ESCALAMAX").Equals(System.DBNull.Value), 0, aRow("SER_NM_ESCALAMAX"))
                Me.escalaMinimaImpresion = IIf(aRow("SER_NM_ESCALAIMP").Equals(System.DBNull.Value), 0, aRow("SER_NM_ESCALAIMP"))
                Me.CabeceraLeyenda = IIf(aRow("SER_BO_CABECERALEYENDA").Equals(System.DBNull.Value), False, IIf(aRow("SER_BO_CABECERALEYENDA").Equals("Y"), True, False))
                Me.IdentificarRest = IIf(aRow("SER_BO_IDENTIFICAR_REST").Equals(System.DBNull.Value), False, IIf(aRow("SER_BO_IDENTIFICAR_REST").Equals("Y"), True, False))
                Me.capas = IIf(aRow("SER_DS_CAPASWMS").Equals(System.DBNull.Value), "", aRow("SER_DS_CAPASWMS"))
                'Me.uuid = IIf(aRow("SEW_CO_UID").Equals(System.DBNull.Value), "", aRow("SEW_CO_UID"))

                Me.uuid = Me.GetUuids(Me.id)

            Next

            Me.Privacidad = Me.GetPrivacidad(usuario)


            Return Me
        Catch
            Return Nothing
        End Try

    End Function

    Public Function GetPrivacidad(ByVal usuario As String) As tipoPrivacidad
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Dim numFilas As Integer
        Try
            strTableName = "VIS_SER_SERVICIOS"
            strDatasetName = "VIS_SER_SERVICIOS"

            ' Para saber si es p�blico
            strQuery = "SELECT count(vis_rol_roles_servicio.ser_co_id) FROM vis_rol_roles_servicio,vis_rol_roles WHERE vis_rol_roles_servicio.ser_co_id=" & Me.id & _
            " AND (vis_rol_roles.rol_co_id = vis_rol_roles_servicio.rol_co_id) AND vis_rol_roles.rol_ds_nombre='ALL'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows
                numFilas = IIf(aRow(0).Equals(System.DBNull.Value), 0, aRow(0))
            Next
            If numFilas <> 0 Then
                Me.Privacidad = tipoPrivacidad.Publico
                Return tipoPrivacidad.Publico
            End If

            If (usuario <> "") Then

                strQuery = "SELECT count(*) FROM vis_rol_roles_servicio,vis_rol_roles,vis_usr_usuarios_rol,vis_usu_usuarios " & _
                           " WHERE vis_rol_roles_servicio.ser_co_id=" & Me.id & _
                           " AND vis_usu_usuarios.USU_DS_NOMBRE = '" & usuario & "'" & _
                           " AND (vis_rol_roles.rol_co_id = vis_rol_roles_servicio.rol_co_id) AND (vis_rol_roles.rol_co_id = vis_usr_usuarios_rol.rol_co_id) AND (vis_usu_usuarios.usu_co_id = vis_usr_usuarios_rol.usu_co_id) AND vis_rol_roles.rol_ds_nombre<>'ALL' "
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                For Each aRow In aDS.Tables(0).Rows
                    numFilas = IIf(aRow(0).Equals(System.DBNull.Value), 0, aRow(0))
                Next
                If numFilas = 0 Then
                    Me.Privacidad = tipoPrivacidad.Privado
                    Return tipoPrivacidad.Privado
                Else
                    Me.Privacidad = tipoPrivacidad.Accesible
                    Return tipoPrivacidad.Accesible
                End If
            Else
                Me.Privacidad = tipoPrivacidad.Privado
                Return tipoPrivacidad.Privado
            End If
        Catch ex As Exception
            Me.Privacidad = tipoPrivacidad.Privado
            Return tipoPrivacidad.Privado
        End Try

    End Function


    Public Function GetUuids(ByVal idServicio As Integer) As String()
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Dim arrAux(0) As String
        Try
            strTableName = "VIS_SEW_SERVICIOS_WMS"
            strDatasetName = "VIS_SEW_SERVICIOS_WMS"

            strQuery = "SELECT sew_co_uid FROM VIS_SEW_SERVICIOS_WMS WHERE ser_co_id=" & idServicio
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            If aDS.Tables(0).Rows.Count <> 0 Then
                Dim cont As Integer, numFilas As Integer
                cont = 0
                numFilas = 0
                For Each aRow In aDS.Tables(0).Rows
                    If aDS.Tables(0).Rows(cont)("sew_co_uid").ToString() <> "" And aDS.Tables(0).Rows(cont)("sew_co_uid").ToString() <> " " Then
                        numFilas = numFilas + 1
                    End If
                Next

                If numFilas = 0 Then
                    Return Nothing
                End If

                ReDim arrAux(0 To numFilas - 1)
                cont = 0
                For Each aRow In aDS.Tables(0).Rows
                    If aDS.Tables(0).Rows(cont)("sew_co_uid") <> "" Then
                        arrAux(cont) = aDS.Tables(0).Rows(cont)("sew_co_uid")
                        cont = cont + 1
                    End If
                Next
            Else
                Return Nothing
            End If

            Return arrAux
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

#End Region

End Class
