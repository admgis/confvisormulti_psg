Imports Microsoft.VisualBasic

Public Class Sondeo

#Region "Propiedades de la Clase"
    Private _id As String
    Private _idMunicipio As String
    Private _municipio As String
    Private _idUH As String
    Private _UH As String
    Private _idCuenca As String
    Private _cuenca As String
    Private _idProvincia As String
    Private _provincia As String
    Private _xUtmET As Double
    Private _yUtmET As Double
    Private _urlFicha As String



    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property IdCuenca() As String
        Get
            IdCuenca = _idCuenca
        End Get
        Set(ByVal mIdCuenca As String)
            _idCuenca = mIdCuenca
        End Set
    End Property
    Public Property Cuenca() As String
        Get
            Cuenca = _cuenca
        End Get
        Set(ByVal mCuenca As String)
            _cuenca = mCuenca
        End Set
    End Property
    Public Property IdProvincia() As String
        Get
            IdProvincia = _idProvincia
        End Get
        Set(ByVal mIdProvincia As String)
            _idProvincia = mIdProvincia
        End Set
    End Property
    Public Property Provincia() As String
        Get
            Provincia = _provincia
        End Get
        Set(ByVal mProvincia As String)
            _provincia = mProvincia
        End Set
    End Property
    Public Property IdMunicipio() As String
        Get
            IdMunicipio = _idMunicipio
        End Get
        Set(ByVal mIdMunicipio As String)
            _idMunicipio = mIdMunicipio
        End Set
    End Property
    Public Property Municipio() As String
        Get
            Municipio = _municipio
        End Get
        Set(ByVal mMunicipio As String)
            _municipio = mMunicipio
        End Set
    End Property
    
    Public Property IdUH() As String
        Get
            IdUH = _idUH
        End Get
        Set(ByVal mIdUH As String)
            _idUH = mIdUH
        End Set
    End Property

    Public Property UH() As String
        Get
            UH = _UH
        End Get
        Set(ByVal mUH As String)
            _UH = mUH
        End Set
    End Property
    
    Public Property XUtmET() As Double
        Get
            XUtmET = _xUtmET
        End Get
        Set(ByVal mXUtmET As Double)
            _xUtmET = mXUtmET
        End Set
    End Property
    Public Property YUtmET() As Double
        Get
            YUtmET = _yUtmET
        End Get
        Set(ByVal mYUtmET As Double)
            _yUtmET = mYUtmET
        End Set
    End Property
    Public Property URLFicha() As String
        Get
            URLFicha = _urlFicha
        End Get
        Set(ByVal mURLFicha As String)
            _urlFicha = mURLFicha
        End Set
    End Property
#End Region





End Class
