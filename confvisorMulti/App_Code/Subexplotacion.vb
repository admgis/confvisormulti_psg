Imports Microsoft.VisualBasic
Imports Service

Public Class Subexplotacion : Implements IComparable
    Private _Id As String
    Private _IdExplotacion As String
    'Private _direccion As String
    'Private _codPostal As String
    'Private _estado As String
    'Private _fechaEstado As Date
    Private _autoConsumo As Boolean
    Private _especie As String
    Private _idEspecie As String
    Private _longitud As Double
    Private _latitud As Double
    Private _provincia As String
    Private _municipio As String
    Private _comarca As String
    Private _censo As Integer

#Region "Propiedades de la Clase"
    Public Property id() As String
        Get
            id = _Id
        End Get
        Set(ByVal value As String)
            _Id = value
        End Set
    End Property
    Public Property idExplotacion() As String
        Get
            idExplotacion = _IdExplotacion
        End Get
        Set(ByVal value As String)
            _IdExplotacion = value
        End Set
    End Property
    'Public Property direccion() As String
    '    Get
    '        direccion = _direccion
    '    End Get
    '    Set(ByVal value As String)
    '        _direccion = value
    '    End Set
    'End Property
    'Public Property codPostal() As String
    '    Get
    '        codPostal = _codPostal
    '    End Get
    '    Set(ByVal value As String)
    '        _codPostal = value
    '    End Set
    'End Property
    'Public Property estado() As String
    '    Get
    '        estado = _estado
    '    End Get
    '    Set(ByVal value As String)
    '        _estado = value
    '    End Set
    'End Property
    'Public Property fechaEstado() As Date
    '    Get
    '        fechaEstado = _fechaEstado
    '    End Get
    '    Set(ByVal value As Date)
    '        _fechaEstado = value
    '    End Set
    'End Property
    Public Property autoConsumo() As Boolean
        Get
            autoConsumo = _autoConsumo
        End Get
        Set(ByVal value As Boolean)
            _autoConsumo = value
        End Set
    End Property
    Public Property especie() As String
        Get
            especie = _especie
        End Get
        Set(ByVal value As String)
            _especie = value
        End Set
    End Property
    Public Property idEspecie() As String
        Get
            idEspecie = _idEspecie
        End Get
        Set(ByVal value As String)
            _idEspecie = value
        End Set
    End Property
    Public Property longitud() As Double
        Get
            longitud = _longitud
        End Get
        Set(ByVal value As Double)
            _longitud = value
        End Set
    End Property
    Public Property latitud() As Double
        Get
            latitud = _latitud
        End Get
        Set(ByVal value As Double)
            _latitud = value
        End Set
    End Property
    Public Property provincia() As String
        Get
            provincia = _provincia
        End Get
        Set(ByVal value As String)
            _provincia = value
        End Set
    End Property
    Public Property municipio() As String
        Get
            municipio = _municipio
        End Get
        Set(ByVal value As String)
            _municipio = value
        End Set
    End Property
    Public Property comarca() As String
        Get
            comarca = _comarca
        End Get
        Set(ByVal value As String)
            _comarca = value
        End Set
    End Property

    Public Property censo() As Integer
        Get
            censo = _censo
        End Get
        Set(ByVal value As Integer)
            _censo = value
        End Set
    End Property

#End Region

#Region "Funciones de la Clase"

    Public Function Comparar(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Try
            Return Me.id = obj.id
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function Cargar(ByVal strCodigo As String) As Subexplotacion
        Dim strTableName As String
        Dim strDatasetName As String
        Dim strQuery As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        
        Try
            'agm 23/03/2012 Migraci�n Ganader�a REGA
            strQuery = "SELECT DISTINCT * from DGG.REGA_MV_UBICACION_ESPECIES WHERE SE_CO_SUBEXPLOT = '" + strCodigo + "'"
            'strQuery = "SELECT DISTINCT * from DGG.VUB_MV_UBICACION_ESPECIES WHERE SE_CLAVE = '" + strCodigo + "'"
            strTableName = "VUC_UBICACION_ESPECIES_COMP"
            strDatasetName = "VUC_UBICACION_ESPECIES_COMP"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)


            'Debemos a�adir los elementos a la colecci�n de Id's de explotaciones            
            For Each aRow In aDS.Tables(0).Rows

                Me.id = IIf(aRow("SE_CO_SUBEXPLOT").Equals(System.DBNull.Value), 0, aRow("SE_CO_SUBEXPLOT"))
                Me.idExplotacion = IIf(aRow("SE_CE_EXPLOT").Equals(System.DBNull.Value), "", aRow("SE_CE_EXPLOT"))
                'me.direccion = IIf(aRow("SE_DIREC").Equals(System.DBNull.Value), "", aRow("SE_DIREC"))
                'me.codPostal = IIf(aRow("SE_CP").Equals(System.DBNull.Value), "", aRow("SE_CP"))
                'me.estado = IIf(aRow("ESTADO").Equals(System.DBNull.Value), "", aRow("ESTADO"))
                'me.fechaEstado = IIf(aRow("SE_FC_EST").Equals(System.DBNull.Value), "", aRow("SE_FC_EST"))
                Me.autoConsumo = IIf(aRow("SE_AUTOC").Equals("S"), True, False)
                Me.especie = IIf(aRow("ESPECIE").Equals(System.DBNull.Value), "", aRow("ESPECIE"))
                'me.idEspecie = IIf(aRow("SE_CO_EXPECIE").Equals(System.DBNull.Value), "", aRow("SE_CO_EXPECIE"))
                Me.longitud = IIf(aRow("UB_LON").Equals(System.DBNull.Value), 0, aRow("UB_LON"))
                Me.latitud = IIf(aRow("UB_LAT").Equals(System.DBNull.Value), 0, aRow("UB_LAT"))
                Me.provincia = IIf(aRow("PROV_NOM").Equals(System.DBNull.Value), "", aRow("PROV_NOM"))
                Me.municipio = IIf(aRow("MUN_NOM").Equals(System.DBNull.Value), "", aRow("MUN_NOM"))
                Me.comarca = IIf(aRow("COMARCA_SGSA").Equals(System.DBNull.Value), "", aRow("COMARCA_SGSA"))

            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

#End Region

End Class
