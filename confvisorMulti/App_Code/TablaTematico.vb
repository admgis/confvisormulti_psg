Imports Microsoft.VisualBasic

Public Class TablaTematico
    Private _nombre As String
    Private _colCampos() As CampoTematico
    
#Region "Propiedades de la clase"
   
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property campos() As CampoTematico()
        Get
            campos = _colCampos
        End Get
        Set(ByVal colcampos As CampoTematico())
            _colCampos = colcampos
        End Set
    End Property

#End Region
End Class
