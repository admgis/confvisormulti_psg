Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service

Public Class Tema
    Private _Id As Integer
    Private _nombre As String
    Private _desccompleta As String
    Private _numTemasHijos As Integer ' JCP: VERSION 2
    Private _numServiciosHijos As Integer
    Private _IdPadre As Integer
    Private _colTemas() As Tema
    Private _colServicios() As Servicio
    Private _nNombreTabla As String = "VIS_TEM_TEMAS"

#Region "Propiedades de la clase"
    Public Property Id() As Integer
        Get
            Id = _Id
        End Get
        Set(ByVal aId As Integer)
            _Id = aId
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property descripcion() As String
        Get
            descripcion = _desccompleta
        End Get
        Set(ByVal aValue As String)
            _desccompleta = aValue
        End Set
    End Property
    Public Property temas() As Tema()
        Get
            temas = _colTemas
        End Get
        Set(ByVal colTemas As Tema())
            _colTemas = colTemas
        End Set
    End Property

    Public Property servicios() As Servicio()
        Get
            servicios = _colServicios
        End Get
        Set(ByVal colServicios As Servicio())
            _colServicios = colServicios
        End Set
    End Property
    Public Property numTemasHijos() As Integer
        Get
            numTemasHijos = _numTemasHijos
        End Get
        Set(ByVal value As Integer)
            _numTemasHijos = value
        End Set
    End Property

    Public Property numServiciosHijos() As Integer
        Get

            numServiciosHijos = _numServiciosHijos


        End Get

        Set(ByVal value As Integer)
            _numServiciosHijos = value
        End Set

    End Property

    Public Property IdPadre() As Integer
        Get
            IdPadre = _IdPadre
        End Get
        Set(ByVal aValue As Integer)
            _IdPadre = aValue
        End Set
    End Property
#End Region

#Region "Funciones de la Clase"
    Public Function CargarTema(ByVal id As Integer, ByVal strRoles As String, ByVal codIdioma As String, Optional ByVal usuario As String = "", _
     Optional ByVal cargarRecursivo As Boolean = True, Optional ByVal cargarServicios As Boolean = True, Optional ByVal idVisor As Integer = 0, _
     Optional ByVal filtrarRoles As Boolean = True) As Tema
        Dim strQuery As String

        Dim aDS As Data.DataSet
        Dim strTableName As String, strDatasetName As String
        Try

            strTableName = "VIS_TEM_TEMAS"
            strDatasetName = "VIS_TEM_TEMAS"

            strQuery = "SELECT T.TEM_CO_ID,CASE WHEN VIS_TDS_DESC_TEMAS.TEM_DS_DESCRIPCION IS NULL THEN T.TEM_DS_DESCRIPCION ELSE VIS_TDS_DESC_TEMAS.TEM_DS_DESCRIPCION END AS TEM_DS_DESCRIPCION" & _
            " FROM (SELECT TT.TEM_CO_ID,TT.TEM_DS_DESCRIPCION  FROM VIS_TEM_TEMAS TT JOIN VIS_VTM_VISORES_TEMA  VV ON TT.TEM_CO_ID=VV.TEM_CO_ID  WHERE VV.VIS_CO_ID =" & idVisor & ") T" & _
            " LEFT JOIN VIS_TDS_DESC_TEMAS ON  T.TEM_CO_ID= VIS_TDS_DESC_TEMAS.TEM_CO_ID " & _
            " AND VIS_TDS_DESC_TEMAS.IDI_DS_CODIGO='" & codIdioma & "'" & _
            " WHERE T.TEM_CO_ID ='" & id & "'"

            'strQuery = "SELECT VIS_TEM_TEMAS.TEM_CO_ID,DECODE(VIS_TDS_DESC_TEMAS.TEM_DS_DESCRIPCION,NULL,VIS_TEM_TEMAS.TEM_DS_DESCRIPCION,VIS_TDS_DESC_TEMAS.TEM_DS_DESCRIPCION) AS TEM_DS_DESCRIPCION" & _
            '  " FROM VIS_TEM_TEMAS LEFT JOIN VIS_TDS_DESC_TEMAS ON  VIS_TEM_TEMAS.TEM_CO_ID= VIS_TDS_DESC_TEMAS.TEM_CO_ID AND VIS_TDS_DESC_TEMAS.IDI_DS_CODIGO='" & codIdioma & "'" & _
            '  " WHERE VIS_TEM_TEMAS.TEM_CO_ID='" & id & "'"

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow
            For Each aRow In aDS.Tables(0).Rows
                Me.Id = aRow("TEM_CO_ID")
                Me.nombre = aRow("TEM_DS_DESCRIPCION")
            Next

            If cargarServicios Then

                'Obtenemos los Servicios (s�lo los que disponemos permisos sobre ellos)
                strTableName = "VIS_SER_SERVICIOS"
                strDatasetName = "VIS_SER_SERVICIOS"
                If idVisor = 0 Then
                    'strQuery = "SELECT vis_ser_servicios.ser_co_id FROM vis_rol_roles_servicio,vis_ser_servicios, vis_hos_hosts " & _
                    ' " WHERE (vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
                    ' " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                    ' " and (vis_ser_servicios.tem_co_id=" + id.ToString + ") and(vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
                    ' " order by(vis_ser_servicios.ser_ds_descripcion)"
                    'strQuery = "SELECT DISTINCT vis_ser_servicios.ser_co_id, vis_ser_servicios.ser_ds_descripcion FROM vis_rol_roles_servicio,vis_ser_servicios " & _
                    '           " WHERE (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
                    '           " and (vis_ser_servicios.tem_co_id=" + id.ToString + ") and(vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
                    '           " order by(vis_ser_servicios.ser_ds_descripcion)"

                    'strQuery = "SELECT DISTINCT vis_ser_servicios.ser_co_id, DECODE (vis_sds_desc_servicios.ser_ds_descripcion, null, vis_ser_servicios.ser_ds_descripcion, vis_sds_desc_servicios.ser_ds_descripcion) AS ser_ds_descripcion " & _
                    '" FROM vis_rol_roles_servicio,vis_ser_servicios INNER JOIN vis_tems_temas_servicio ON vis_ser_servicios.ser_co_id = vis_tems_temas_servicio.ser_co_id " & _
                    '" LEFT JOIN vis_sds_desc_servicios ON vis_ser_servicios.ser_co_id = vis_sds_desc_servicios.ser_co_id AND vis_sds_desc_servicios.idi_ds_codigo='" + codIdioma + "' " & _
                    '" WHERE (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
                    '" AND (vis_tems_temas_servicio.tema_co_id=" + id.ToString + ") AND (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
                    '" ORDER BY (ser_ds_descripcion)"

                    'arg: orden especificado en base de datos
                    strQuery = "SELECT DISTINCT vis_ser_servicios.ser_co_id id, vis_tems_temas_servicio.tems_nm_orden tems_nm_orden, CASE WHEN vis_sds_desc_servicios.ser_ds_descripcion IS null THEN vis_ser_servicios.ser_ds_descripcion ELSE vis_sds_desc_servicios.ser_ds_descripcion END ser_ds_descripcion " & _
                        " FROM vis_rol_roles_servicio,vis_ser_servicios INNER JOIN vis_tems_temas_servicio ON vis_ser_servicios.ser_co_id = vis_tems_temas_servicio.ser_co_id " & _
                        " LEFT JOIN vis_sds_desc_servicios ON vis_ser_servicios.ser_co_id = vis_sds_desc_servicios.ser_co_id AND vis_sds_desc_servicios.idi_ds_codigo='" + codIdioma + "' " & _
                        " WHERE (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
                        " AND (vis_tems_temas_servicio.tema_co_id=" + id.ToString + ") AND (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
                        " ORDER BY tems_nm_orden ASC, ser_ds_descripcion ASC, id ASC"
                Else


                    If (filtrarRoles) Then
                        'strQuery = "SELECT DISTINCT vis_ser_servicios.ser_co_id, DECODE (vis_sds_desc_servicios.ser_ds_descripcion, null, vis_ser_servicios.ser_ds_descripcion, vis_sds_desc_servicios.ser_ds_descripcion) AS ser_ds_descripcion " & _
                        '    " FROM vis_rol_roles_servicio, vis_ser_servicios INNER JOIN vis_tems_temas_servicio ON vis_ser_servicios.ser_co_id = vis_tems_temas_servicio.ser_co_id INNER JOIN vis_vsr_visores_servicio ON (vis_ser_servicios.ser_co_id = vis_vsr_visores_servicio.ser_co_id OR vis_ser_servicios.tis_co_id = '2') " & _
                        '    " LEFT JOIN vis_sds_desc_servicios ON vis_ser_servicios.ser_co_id = vis_sds_desc_servicios.ser_co_id AND vis_sds_desc_servicios.idi_ds_codigo='" + codIdioma + "' " & _
                        '    " WHERE (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
                        '    " AND ((vis_vsr_visores_servicio.vis_co_id = '" + idVisor.ToString() + "') OR (vis_ser_servicios.tis_co_id = 2))" & _
                        '    " AND (vis_tems_temas_servicio.tema_co_id=" + id.ToString + ") AND (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
                        '    " ORDER BY (ser_ds_descripcion)"

                        'arg: orden especificado en base de datos
                        strQuery = "SELECT DISTINCT vis_ser_servicios.ser_co_id id, vis_tems_temas_servicio.tems_nm_orden tems_nm_orden, CASE WHEN vis_sds_desc_servicios.ser_ds_descripcion IS null THEN vis_ser_servicios.ser_ds_descripcion ELSE vis_sds_desc_servicios.ser_ds_descripcion END ser_ds_descripcion " & _
                            " FROM vis_rol_roles_servicio, vis_ser_servicios INNER JOIN vis_tems_temas_servicio ON vis_ser_servicios.ser_co_id = vis_tems_temas_servicio.ser_co_id INNER JOIN VIS_VSR_VISORES_SERVICIO ON (vis_ser_servicios.ser_co_id = VIS_VSR_VISORES_SERVICIO.SER_CO_ID OR vis_ser_servicios.tis_co_id = '2') " & _
                           " LEFT JOIN vis_sds_desc_servicios ON vis_ser_servicios.ser_co_id = vis_sds_desc_servicios.ser_co_id and vis_sds_desc_servicios.idi_ds_codigo='" + codIdioma + "' " & _
                           " WHERE (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
                           " AND ((vis_vsr_visores_servicio.vis_co_id = '" + idVisor.ToString() + "') OR (vis_ser_servicios.tis_co_id = 2))" & _
                           " AND (vis_tems_temas_servicio.tema_co_id=" + id.ToString + ") AND (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
                           " ORDER BY tems_nm_orden ASC, ser_ds_descripcion ASC, id ASC"
                    Else


                        'strQuery = "SELECT DISTINCT vis_ser_servicios.ser_co_id , DECODE (vis_sds_desc_servicios.ser_ds_descripcion, null, vis_ser_servicios.ser_ds_descripcion, vis_sds_desc_servicios.ser_ds_descripcion) AS ser_ds_descripcion " & _
                        '      " FROM vis_rol_roles_servicio,vis_ser_servicios INNER JOIN vis_tems_temas_servicio ON vis_ser_servicios.ser_co_id = vis_tems_temas_servicio.ser_co_id INNER JOIN vis_vsr_visores_servicio ON (vis_ser_servicios.ser_co_id = vis_vsr_visores_servicio.ser_co_id OR vis_ser_servicios.tis_co_id = '2') " & _
                        '      " LEFT JOIN vis_sds_desc_servicios ON vis_ser_servicios.ser_co_id = vis_sds_desc_servicios.ser_co_id AND vis_sds_desc_servicios.idi_ds_codigo='" + codIdioma + "' " & _
                        '      " WHERE (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
                        '      " AND ((vis_vsr_visores_servicio.vis_co_id = '" + idVisor.ToString() + "') OR (vis_ser_servicios.tis_co_id = 2))" & _
                        '      " AND (vis_tems_temas_servicio.tema_co_id=" + id.ToString + ") " & _
                        '      " ORDER BY (ser_ds_descripcion)"

                        'arg: orden especificado en base de datos
                        strQuery = "SELECT DISTINCT vis_ser_servicios.ser_co_id id, vis_tems_temas_servicio.tems_nm_orden tems_nm_orden, CASE WHEN vis_sds_desc_servicios.ser_ds_descripcion IS null THEN vis_ser_servicios.ser_ds_descripcion ELSE vis_sds_desc_servicios.ser_ds_descripcion END ser_ds_descripcion " & _
                               " FROM vis_rol_roles_servicio,vis_ser_servicios INNER JOIN vis_tems_temas_servicio ON vis_ser_servicios.ser_co_id = vis_tems_temas_servicio.ser_co_id INNER JOIN VIS_VSR_VISORES_SERVICIO ON (vis_ser_servicios.ser_co_id = vis_vsr_visores_servicio.ser_co_id OR vis_ser_servicios.tis_co_id = '2') " & _
                               " LEFT JOIN vis_sds_desc_servicios ON vis_ser_servicios.ser_co_id = vis_sds_desc_servicios.ser_co_id AND vis_sds_desc_servicios.idi_ds_codigo='" + codIdioma + "' " & _
                               " WHERE (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
                               " AND ((vis_vsr_visores_servicio.vis_co_id = '" + idVisor.ToString() + "') OR (vis_ser_servicios.tis_co_id = 2))" & _
                               " AND (vis_tems_temas_servicio.tema_co_id=" + id.ToString + ") " & _
                               " ORDER BY tems_nm_orden ASC, ser_ds_descripcion ASC, id ASC"


                        'strQuery = "SELECT DISTINCT vis_ser_servicios.ser_co_id, vis_ser_servicios.ser_nm_orden DECODE (vis_sds_desc_servicios.ser_ds_descripcion, null, vis_ser_servicios.ser_ds_descripcion, vis_sds_desc_servicios.ser_ds_descripcion) AS ser_ds_descripcion " & _
                        '      " FROM vis_rol_roles_servicio,vis_ser_servicios INNER JOIN vis_tems_temas_servicio ON vis_ser_servicios.ser_co_id = vis_tems_temas_servicio.ser_co_id INNER JOIN VIS_VSR_VISORES_SERVICIO ON (vis_ser_servicios.ser_co_id = VIS_VSR_VISORES_SERVICIO.SER_CO_ID or vis_ser_servicios.tis_co_id = '2') " & _
                        '       " LEFT JOIN vis_sds_desc_servicios ON vis_ser_servicios.ser_co_id = vis_sds_desc_servicios.ser_co_id and vis_sds_desc_servicios.idi_ds_codigo='" + codIdioma + "' " & _
                        '      " WHERE (vis_ser_servicios.ser_co_id = vis_rol_roles_servicio.ser_co_id) " & _
                        '      " and ((vis_vsr_visores_servicio.vis_co_id = '" + idVisor.ToString() + "') or (vis_ser_servicios.tis_co_id = 2))" & _
                        '      " and (vis_tems_temas_servicio.tema_co_id=" + id.ToString + ") " & _
                        '      " order by(ser_ds_descripcion)"
                    End If




                End If

                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                ReDim Me.servicios(aDS.Tables(0).Rows.Count - 1)
                Dim i As Integer
                i = 0
                For Each aRow In aDS.Tables(0).Rows
                    Dim nServicio As New Servicio
                    nServicio.Cargar(aRow("ID"), codIdioma, usuario)
                    Me.servicios(i) = nServicio
                    i += 1
                Next

                'If cargarRecursivo Then
                'Obtenemos los hijos
                strTableName = "VIS_TEM_TEMAS"
                strDatasetName = "VIS_TEM_TEMAS"
                If idVisor = 0 Then  'strQuery = "SELECT TEM_CO_ID FROM VIS_TEM_TEMAS WHERE TEM_CO_IDPADRE='" & id & "' order by VIS_TEM_TEMAS.TEM_NM_ORDEN ASC"
                    strQuery = "SELECT t.TEM_CO_ID TEM_CO_ID FROM VIS_TEM_TEMAS t inner join vis_vtm_visores_tema v on t.tem_co_id=v.tem_co_id WHERE  v.TEM_CO_IDPADRE='" & id & "' order by V.VTM_NM_ORDEN ASC"
                Else
                    strQuery = "SELECT t.TEM_CO_ID TEM_CO_ID FROM VIS_TEM_TEMAS t inner join vis_vtm_visores_tema v on t.tem_co_id=v.tem_co_id WHERE v.vis_co_id=" & idVisor & " and v.TEM_CO_IDPADRE='" & id & "' order by V.VTM_NM_ORDEN ASC"
                End If
                'strQuery = "SELECT TEM_CO_ID FROM VIS_TEM_TEMAS WHERE TEM_CO_IDPADRE=" & id 
                aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
                ReDim Preserve Me.temas(aDS.Tables(0).Rows.Count - 1)
                i = 0
                For Each aRow In aDS.Tables(0).Rows
                    Dim nTema As New Tema
                    nTema.CargarTema(aRow("TEM_CO_ID"), strRoles, codIdioma, usuario, cargarRecursivo, cargarRecursivo, idVisor)
                    Me.temas.SetValue(nTema, i)
                    i += 1
                Next
                'Else
                '	ReDim Preserve Me.temas(-1)
                'End If
            Else
                ReDim Me.servicios(-1)
                ReDim Preserve Me.temas(-1)
            End If
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

	Public Sub CargarNumTemasHijo(ByVal idTema As Integer, ByVal strRoles As String, ByVal codIdioma As String)
		Dim strQuery As String

		Dim aDS As Data.DataSet
		Dim strTableName As String, strDatasetName As String

		Try



			'Obtenemos los hijos
			strTableName = "VIS_TEM_TEMAS"
			strDatasetName = "VIS_TEM_TEMAS"
			strQuery = "SELECT count(TEM_CO_ID) FROM VIS_TEM_TEMAS WHERE TEM_CO_IDPADRE='" & idTema & "'"

			aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
			_numTemasHijos = aDS.Tables(0).Rows(0).Item(0)



		Catch ex As Exception

		End Try

	End Sub


	Public Sub CargarNumServiciosHijo(ByVal idTema As Integer, ByVal strRoles As String, ByVal codIdioma As String)
		Dim strQuery As String

		Dim aDS As Data.DataSet
		Dim strTableName As String, strDatasetName As String

		Try



			'Obtenemos los hijos
			strTableName = "VIS_SER_SERVICIOS"
			strDatasetName = "VIS_SER_SERVICIOS"
			'strQuery = "SELECT count(SER_CO_ID) FROM VIS_SER_SERVICIOS WHERE TEM_CO_ID='" & idTema & "'"


			strQuery = "select count(vis_ser_servicios.SER_CO_ID) from vis_rol_roles_servicio,vis_ser_servicios, vis_hos_hosts " & _
				" where (vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
				" and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
				" and (vis_ser_servicios.tem_co_id=" + idTema.ToString() + ") "	'LFMC: quitado, ahora devolvemos los servicios con su privacidad and (vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles


			aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
			_numServiciosHijos = aDS.Tables(0).Rows(0).Item(0)



		Catch ex As Exception

		End Try

	End Sub

	Public Sub CargarNumDptosHijo(ByVal idDep As Integer, ByVal strRoles As String, ByVal codIdioma As String)
		Dim strQuery As String

		Dim aDS As Data.DataSet
		Dim strTableName As String, strDatasetName As String

		Try



			'Obtenemos los hijos
			strTableName = "VIS_DEP_DEPARTAMENTOS"
			strDatasetName = "VIS_DEP_DEPARTAMENTOS"
			strQuery = "SELECT count(DEP_CO_ID) FROM VIS_DEP_DEPARTAMENTOS WHERE DEP_CO_IDPADRE='" & idDep & "'"

			aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
			_numTemasHijos = aDS.Tables(0).Rows(0).Item(0)



		Catch ex As Exception

		End Try

	End Sub
    Public Function CargarTemaSinServicios(ByVal id As Integer, ByVal codIdioma As String, Optional ByVal idVisor As Integer = -1) As Tema
        Dim strQuery As String

        Dim aDS As Data.DataSet
        Dim strTableName As String, strDatasetName As String
        Try

            strTableName = "VIS_TEM_TEMAS"
            strDatasetName = "VIS_TEM_TEMAS"
            strQuery = "SELECT VIS_TEM_TEMAS.TEM_CO_ID,VIS_TDS_DESC_TEMAS.TEM_DS_DESCRIPCION FROM VIS_TEM_TEMAS,VIS_TDS_DESC_TEMAS WHERE VIS_TEM_TEMAS.TEM_CO_ID='" & id & "'" & _
                 " AND VIS_TDS_DESC_TEMAS.TEM_CO_ID=VIS_TEM_TEMAS.TEM_CO_ID AND VIS_TDS_DESC_TEMAS.IDI_DS_CODIGO='" & codIdioma & "'"
            'strQuery = "SELECT TEM_CO_ID,TEM_DS_DESCRIPCION FROM VIS_TEM_TEMAS WHERE TEM_CO_ID='" & id & "'"
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            Dim aRow As Data.DataRow
            For Each aRow In aDS.Tables(0).Rows
                Me.Id = aRow("TEM_CO_ID")
                Me.nombre = aRow("TEM_DS_DESCRIPCION")
            Next

            '' '' ''Obtenemos los Servicios (s�lo los que disponemos permisos sobre ellos)
            ' '' ''strTableName = "VIS_SER_SERVICIOS"
            ' '' ''strDatasetName = "VIS_SER_SERVICIOS"

            ' '' ''strQuery = "select vis_ser_servicios.ser_co_id from vis_rol_roles_servicio,vis_ser_servicios, vis_hos_hosts " & _
            ' '' ''       " where (vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
            ' '' ''       " and (vis_ser_servicios.ser_co_id=vis_rol_roles_servicio.ser_co_id) " & _
            ' '' ''       " and (vis_ser_servicios.tem_co_id=" + id.ToString + ") and(vis_rol_roles_servicio.rol_ds_rol_servicio " + strRoles & _
            ' '' ''        " order by(vis_ser_servicios.ser_ds_descripcion)"
            ' '' ''aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ' '' ''ReDim Me.servicios(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            ' '' ''For Each aRow In aDS.Tables(0).Rows
            ' '' ''    Dim nServicio As New Servicio
            ' '' ''    nServicio.Cargar(aRow("SER_CO_ID"))
            ' '' ''    Me.servicios(i) = nServicio
            ' '' ''    i += 1
            ' '' ''Next
            'Obtenemos los hijos
            strTableName = "VIS_TEM_TEMAS"
            strDatasetName = "VIS_TEM_TEMAS"
            If idVisor = -1 Then
                strQuery = "SELECT TEM_CO_ID FROM VIS_TEM_TEMAS WHERE TEM_CO_IDPADRE=" & id & " order by TEM_NM_ORDEN ASC"
            Else
                strQuery = "SELECT t.TEM_CO_ID FROM VIS_TEM_TEMAS t inner join vis_vtm_visores_tema v on t.tem_co_id = v.tem_co_id WHERE v.vis_co_id = " & idVisor & " and v.TEM_CO_IDPADRE=" & id & " order by v.VTM_NM_ORDEN ASC"
            End If

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim Preserve Me.temas(aDS.Tables(0).Rows.Count - 1)
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim nTema As New Tema
                nTema.CargarTemaSinServicios(aRow("TEM_CO_ID"), codIdioma)
                Me.temas.SetValue(nTema, i)
                i += 1
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
#End Region

End Class
