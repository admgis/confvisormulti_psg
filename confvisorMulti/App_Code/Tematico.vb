Imports Microsoft.VisualBasic
Imports Service

Public Class Tematico
    ''Public Enum tipoTematico
    ''    Comunidades = 1
    ''    Provincias = 2
    ''    Municipios = 3
    ''    Comarcas = 4
    ''    Cuencas = 5
    ''    ComarcasVeterinarias = 6
    ''End Enum

    Public Enum estadoTematico
        Cargado = 1
        Generado = 2
        Aprobado = 3
    End Enum

    Public Enum privacidadTematico
        Publico = 1
        Privado = 2
        Compartido = 3
    End Enum
    Private _Id As Integer
    Private _ficheroEntrada As String
    Private _tablaEntrada As String
    Private _campoValores As String
    Private _tipo As TipoTematico
    Private _tablaDestino As String
    Private _estado As estadoTematico
    Private _campoJoin As String
    Private _nombre As String
    Private _resultado As String
    Private _privacidad As privacidadTematico
    Private _autor As String
    Private _editable As Boolean
    Private _idLayer As String


#Region "Propiedades de la Clase"
    Public Property Id() As Integer
        Get
            Id = _Id
        End Get
        Set(ByVal aId As Integer)
            _Id = aId
        End Set
    End Property

    Public Property ficheroEntrada() As String
        Get
            ficheroEntrada = _ficheroEntrada
        End Get
        Set(ByVal aFichero As String)
            _ficheroEntrada = aFichero
        End Set
    End Property

    Public Property tablaEntrada() As String
        Get
            tablaEntrada = _tablaEntrada
        End Get
        Set(ByVal aTabla As String)
            _tablaEntrada = aTabla
        End Set
    End Property

    Public Property campoValores() As String
        Get
            campoValores = _campoValores
        End Get
        Set(ByVal aCampo As String)
            _campoValores = aCampo
        End Set
    End Property

    Public Property tipo() As tipoTematico
        Get
            tipo = _tipo
        End Get
        Set(ByVal aTipo As tipoTematico)
            _tipo = aTipo
        End Set
    End Property

    Public Property tablaDestino() As String
        Get
            tablaDestino = _tablaDestino
        End Get
        Set(ByVal aTabla As String)
            _tablaDestino = aTabla
        End Set
    End Property

    Public Property estado() As estadoTematico
        Get
            estado = _estado
        End Get
        Set(ByVal aEstado As estadoTematico)
            _estado = aEstado
        End Set
    End Property

    Public Property campoJoin() As String
        Get
            campoJoin = _campoJoin
        End Get
        Set(ByVal aCampo As String)
            _campoJoin = aCampo
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property

    Public Property resultado() As String
        Get
            resultado = _resultado
        End Get
        Set(ByVal aResultado As String)
            _resultado = aResultado
        End Set
    End Property

    Public Property privacidad() As privacidadTematico
        Get
            privacidad = _privacidad
        End Get
        Set(ByVal aPriv As privacidadTematico)
            _privacidad = aPriv
        End Set
    End Property

    Public Property autor() As String
        Get
            autor = _autor
        End Get
        Set(ByVal aAutor As String)
            _autor = aAutor
        End Set
    End Property

    Public Property editable() As Boolean
        Get
            editable = _editable
        End Get
        Set(ByVal aEditable As Boolean)
            _editable = aEditable
        End Set
    End Property

    Public Property idLayer() As String
        Get
            idLayer = _idLayer
        End Get
        Set(ByVal aIdLayer As String)
            _idLayer = aIdLayer
        End Set
    End Property
#End Region

#Region "Funciones de la Clase"
    Public Function Cargar(ByVal idTematico As Integer, ByVal usuario As String) As Tematico
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"

            strQuery = "SELECT TAD_CO_ID, TAD_DS_FICHERO_ENTRADA, TAD_DS_TABLA_ENTRADA, TAD_DS_CAMPO_TEM_ENTRADA, TIT_CO_ID, " & _
                       "EST_CO_ID, TAD_DS_NOMBRE_ANALISIS, TAD_DS_CAMPO_TEM_JOIN, TAD_DS_RESULTADO, TPR_CO_ID, TAD_DS_USUARIO " & _
                       "FROM VIS_TAD_TEMATICOSDEMANDA WHERE TAD_CO_ID=" + idTematico.ToString
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            For Each aRow In aDS.Tables(0).Rows
                Me.Id = aRow("TAD_CO_ID")
                Me.ficheroEntrada = IIf(aRow("TAD_DS_FICHERO_ENTRADA").Equals(System.DBNull.Value), "", aRow("TAD_DS_FICHERO_ENTRADA"))
                Me.tablaEntrada = IIf(aRow("TAD_DS_TABLA_ENTRADA").Equals(System.DBNull.Value), "", aRow("TAD_DS_TABLA_ENTRADA"))
                Me.campoValores = IIf(aRow("TAD_DS_CAMPO_TEM_ENTRADA").Equals(System.DBNull.Value), "", aRow("TAD_DS_CAMPO_TEM_ENTRADA"))
                Me.tipo = New TipoTematico()
                Me.tipo.Cargar(IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), 1, aRow("TIT_CO_ID")))
                Me.idLayer = IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), "1", aRow("TIT_CO_ID").ToString())
                Me.estado = IIf(aRow("EST_CO_ID").Equals(System.DBNull.Value), 1, aRow("EST_CO_ID"))
                Me.nombre = IIf(aRow("TAD_DS_NOMBRE_ANALISIS").Equals(System.DBNull.Value), "", aRow("TAD_DS_NOMBRE_ANALISIS"))
                Me.campoJoin = IIf(aRow("TAD_DS_CAMPO_TEM_JOIN").Equals(System.DBNull.Value), "", aRow("TAD_DS_CAMPO_TEM_JOIN"))
                Me.privacidad = IIf(aRow("TPR_CO_ID").Equals(System.DBNull.Value), 1, aRow("TPR_CO_ID"))
                Me.resultado = IIf(aRow("TAD_DS_RESULTADO").Equals(System.DBNull.Value), "", aRow("TAD_DS_RESULTADO"))
                Me.autor = IIf(aRow("TAD_DS_USUARIO").Equals(System.DBNull.Value), "", aRow("TAD_DS_USUARIO"))

                If (Len(Trim(usuario)) > 0) And (Trim(UCase(Me.autor)) = Trim(UCase(usuario))) Then
                    Me.editable = True
                Else
                    Me.editable = False
                End If
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function CargarMemoria(ByVal idTematico As Integer, ByVal usuario As String) As Tematico
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "VIS_TAD_TEMATICOS_GEOMETRIA"
            strDatasetName = "VIS_TAD_TEMATICOS_GEOMETRIA"

            strQuery = "SELECT TAD_CO_ID, TAD_DS_FICHERO_ENTRADA,  TIT_CO_ID, " & _
                       "TAD_DS_NOMBRE_ANALISIS,  TAD_DS_RESULTADO, TPR_CO_ID, TAD_DS_USUARIO " & _
                       "FROM VIS_TAD_TEMATICOS_GEOMETRIA WHERE TAD_CO_ID=" + idTematico.ToString
            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

            For Each aRow In aDS.Tables(0).Rows
                Me.Id = aRow("TAD_CO_ID")
                Me.ficheroEntrada = IIf(aRow("TAD_DS_FICHERO_ENTRADA").Equals(System.DBNull.Value), "", aRow("TAD_DS_FICHERO_ENTRADA"))
                Me.tablaEntrada = ""
                Me.campoValores = ""
                Me.tipo = New TipoTematico()
                Me.tipo.Cargar(IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), 1, aRow("TIT_CO_ID")))
                Me.idLayer = IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), "1", aRow("TIT_CO_ID").ToString())
                Me.estado = 1
                Me.nombre = IIf(aRow("TAD_DS_NOMBRE_ANALISIS").Equals(System.DBNull.Value), "", aRow("TAD_DS_NOMBRE_ANALISIS"))
                Me.campoJoin = ""
                Me.privacidad = IIf(aRow("TPR_CO_ID").Equals(System.DBNull.Value), 1, aRow("TPR_CO_ID"))
                Me.resultado = IIf(aRow("TAD_DS_RESULTADO").Equals(System.DBNull.Value), "", aRow("TAD_DS_RESULTADO"))
                Me.autor = IIf(aRow("TAD_DS_USUARIO").Equals(System.DBNull.Value), "", aRow("TAD_DS_USUARIO"))

                If (Len(Trim(usuario)) > 0) And (Trim(UCase(Me.autor)) = Trim(UCase(usuario))) Then
                    Me.editable = True
                Else
                    Me.editable = False
                End If
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
#End Region

End Class
