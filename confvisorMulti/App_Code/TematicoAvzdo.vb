﻿Imports Microsoft.VisualBasic

Public Class TematicoAvzdo
    Private _Id As Integer
    Private _nombre As String
    Private _fecha As String
    Private _idTipo As Integer
    Private _tipo As String
    Private _layerDrawingOptions As String
    Private _layerDataSource As String
    Private _pointClustering As Boolean
    Private _renderer As String
    Private _definitionExp As String
    Private _properties As String

#Region "Propiedades de la Clase"
    Public Property Id() As Integer
        Get
            Id = _Id
        End Get
        Set(ByVal value As Integer)
            _Id = value
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Public Property fecha() As String
        Get
            fecha = _fecha
        End Get
        Set(ByVal value As String)
            _fecha = value
        End Set
    End Property

    Public Property IdTipo() As Integer
        Get
            Return _idTipo
        End Get
        Set(ByVal value As Integer)
            _idTipo = value
        End Set
    End Property

    Public Property Tipo() As String
        Get
            Return _tipo
        End Get
        Set(ByVal value As String)
            _tipo = value
        End Set
    End Property

    Public Property LayerDrawingOptions() As String
        Get
            Return _layerDrawingOptions
        End Get
        Set(ByVal value As String)
            _layerDrawingOptions = value
        End Set
    End Property

    Public Property LayerDataSource() As String
        Get
            Return _layerDataSource
        End Get
        Set(ByVal value As String)
            _layerDataSource = value
        End Set
    End Property

    Public Property PointClustering() As Boolean
        Get
            Return _pointClustering
        End Get
        Set(ByVal value As Boolean)
            _pointClustering = value
        End Set
    End Property

    Public Property definitionExp() As String
        Get
            Return _definitionExp
        End Get
        Set(ByVal value As String)
            _definitionExp = value
        End Set
    End Property

    Public Property Renderer() As String
        Get
            Return _renderer
        End Get
        Set(ByVal value As String)
            _renderer = value
        End Set
    End Property

    Public Property Properties() As String
        Get
            Return _properties
        End Get
        Set(ByVal value As String)
            _properties = value
        End Set
    End Property


#End Region

End Class
