Imports Microsoft.VisualBasic
Imports Service

Public Class TematicoLista
	''Public Enum tipoTematico
	''    Comunidades = 1
	''    Provincias = 2
	''    Municipios = 3
	''    Comarcas = 4
	''    Cuencas = 5
	''    ComarcasVeterinarias = 6
	''End Enum

	Public Enum estadoTematico
		Cargado = 1
		Generado = 2
		Aprobado = 3
	End Enum

	Public Enum privacidadTematicoLista
		Publico = 1
		Privado = 2
		Compartido = 3
	End Enum
	Private _Id As Integer
	Private _nombre As String
	Private _descripcion As String
	Private _fecha As String
	Private _privacidad As privacidadTematicoLista
	Private _autor As String
	Private _idLayer As String


#Region "Propiedades de la Clase"
	Public Property Id() As Integer
		Get
			Id = _Id
		End Get
		Set(ByVal aId As Integer)
			_Id = aId
		End Set
	End Property



	Public Property nombre() As String
		Get
			nombre = _nombre
		End Get
		Set(ByVal aNombre As String)
			_nombre = aNombre
		End Set
	End Property

	Public Property descripcion() As String
		Get
			descripcion = _descripcion
		End Get
		Set(ByVal aDescripcion As String)
			_descripcion = aDescripcion
		End Set
	End Property

	Public Property fecha() As String
		Get
			fecha = _fecha
		End Get
		Set(ByVal aFecha As String)
			_fecha = aFecha
		End Set
	End Property


	Public Property privacidad() As privacidadTematicoLista
		Get
			privacidad = _privacidad
		End Get
		Set(ByVal aPriv As privacidadTematicoLista)
			_privacidad = aPriv
		End Set
	End Property

	Public Property autor() As String
		Get
			autor = _autor
		End Get
		Set(ByVal aAutor As String)
			_autor = aAutor
		End Set
	End Property

	Public Property idLayer() As String
		Get
			idLayer = _idLayer
		End Get
		Set(ByVal aIdLayer As String)
			_idLayer = aIdLayer
		End Set
	End Property
#End Region

#Region "Funciones de la Clase"
	Public Function Cargar(ByVal idTematico As Integer, ByVal usuario As String) As TematicoLista
		Dim aDS As Data.DataSet
		Dim strQuery As String
		Dim strTableName As String, strDatasetName As String
		Dim aRow As Data.DataRow
		Try
			strTableName = "VIS_TAD_TEMATICOSDEMANDA"
			strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"

			strQuery = "SELECT TAD_CO_ID, TAD_DS_FICHERO_ENTRADA, TAD_DS_TABLA_ENTRADA, TAD_DS_CAMPO_TEM_ENTRADA, TIT_CO_ID, " & _
				 "EST_CO_ID, TAD_DS_NOMBRE_ANALISIS, TAD_DS_CAMPO_TEM_JOIN, TAD_DS_RESULTADO, TPR_CO_ID, TAD_DS_USUARIO, TAD_DS_DESCRIPCION, TAD_DS_FECHA " & _
				 "FROM VIS_TAD_TEMATICOSDEMANDA WHERE TAD_CO_ID=" + idTematico.ToString
			aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)

			For Each aRow In aDS.Tables(0).Rows
				Me.Id = aRow("TAD_CO_ID")
				Me.nombre = IIf(aRow("TAD_DS_NOMBRE_ANALISIS").Equals(System.DBNull.Value), "", aRow("TAD_DS_NOMBRE_ANALISIS"))
				Me.privacidad = IIf(aRow("TPR_CO_ID").Equals(System.DBNull.Value), 1, aRow("TPR_CO_ID"))
				Me.autor = IIf(aRow("TAD_DS_USUARIO").Equals(System.DBNull.Value), "", aRow("TAD_DS_USUARIO"))
				Me.idLayer = IIf(aRow("TIT_CO_ID").Equals(System.DBNull.Value), "1", aRow("TIT_CO_ID").ToString())
				Me.descripcion = IIf(aRow("TAD_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TAD_DS_DESCRIPCION"))
				Me.fecha = IIf(aRow("TAD_DS_FECHA").Equals(System.DBNull.Value), "", aRow("TAD_DS_FECHA"))

			Next
			Return Me
		Catch ex As Exception
			Return Nothing
		End Try
	End Function
#End Region

End Class
