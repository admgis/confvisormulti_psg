Imports Microsoft.VisualBasic
Imports Service

Public Class TematicoPredefinido

	Private _Id As Integer
	Private _descripcion As String
	Private _tipoTematico As Integer
	Private _campoJoinTematico As Integer
	Private _tablaExterna As String
	Private _nombreCampoJoinTematico As String

	Public Property Id() As Integer
		Get
			Id = _Id
		End Get
		Set(ByVal aId As Integer)
			_Id = aId
		End Set
	End Property

	Public Property descripcion() As String
		Get
			descripcion = _descripcion
		End Get
		Set(ByVal aDescripcion As String)
			_descripcion = aDescripcion
		End Set
	End Property

	Public Property tipoTematico() As Integer
		Get
			tipoTematico = _tipoTematico
		End Get
		Set(ByVal aTipoTematico As Integer)
			_tipoTematico = aTipoTematico
		End Set
	End Property

	Public Property campoJoinTematico() As Integer
		Get
			campoJoinTematico = _campoJoinTematico
		End Get
		Set(ByVal aCampoJoinTematico As Integer)
			_campoJoinTematico = aCampoJoinTematico
		End Set
	End Property

	Public Property tablaExterna() As String
		Get
			tablaExterna = _tablaExterna
		End Get
		Set(ByVal aTablaExterna As String)
			_tablaExterna = atablaExterna
		End Set
	End Property

	Public Property nombreCampoJoinTematico() As String
		Get
			nombreCampoJoinTematico = _nombreCampoJoinTematico
		End Get
		Set(ByVal aNombreCampoJoinTematico As String)
			_nombreCampoJoinTematico = anombreCampoJoinTematico
		End Set
	End Property

	Public Function Cargar(ByVal id As Integer) As TematicoPredefinido
		Dim strQuery As String, strConn As String
		Dim strTablename As String, strDatasetName As String
		Dim aDS As Data.DataSet
		Dim aRow As Data.DataRow
		Try
			strConn = System.Configuration.ConfigurationManager.AppSettings("bbdd")
			strTablename = "VIS_TPF_TEMATICOSPREDEFINIDOS"
			strDatasetName = "VIS_TPF_TEMATICOSPREDEFINIDOS"
			strQuery = "SELECT TPF_CO_ID, TPF_DS_DESCRIPCION, TIT_CO_ID, CEX_CO_ID FROM VIS_TPF_TEMATICOSPREDEFINIDOS WHERE TPF_CO_ID=" & id.ToString
			aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
			For Each aRow In aDS.Tables(0).Rows
				Me.Id = aRow("TPF_CO_ID")
				Me.descripcion = aRow("TPF_DS_DESCRIPCION")
				Me.tipoTematico = aRow("TIT_CO_ID")
				Me.campoJoinTematico = aRow("CEX_CO_ID")
			Next
			aDS.Clear()
			aDS = Nothing
			Return Me
		Catch ex As Exception
			Return Nothing
		End Try

	End Function

End Class
