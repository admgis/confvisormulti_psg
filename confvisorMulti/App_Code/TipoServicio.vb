Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service

Public Class TipoServicio

    Private _id As Integer
    Private _descripcion As String
    Private _colServicios() As Servicio
    Private _nNombreTabla As String = "VIS_TIS_TIPO_SERVICIO"

#Region "Propiedades de la Clase"

    Public Property id() As Integer
        Get
            id = _id
        End Get
        Set(ByVal aId As Integer)
            _id = aId
        End Set
    End Property
    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDescripcion As String)
            _descripcion = aDescripcion
        End Set
    End Property
    Public Property servicios() As Servicio()
        Get
            servicios = _colServicios
        End Get
        Set(ByVal colServicios As Servicio())
            _colServicios = colServicios
        End Set
    End Property
#End Region

#Region "Funciones de la Clase"
    Public Function Cargar(ByVal idTipoServicio As Integer, ByVal codIdioma As String, Optional ByVal idVisor As Integer = 0) As TipoServicio
        Dim aDS As Data.DataSet
        Dim strQuery As String
        Dim strTableName As String, strDatasetName As String
        Dim aRow As Data.DataRow
        Try
            strTableName = "VIS_TIS_TIPO_SERVICIO"
            strDatasetName = "VIS_TIS_TIPO_SERVICIO"
            strQuery = "select VIS_TIS_TIPO_SERVICIO.TIS_CO_ID, TIS_DS_DESCRIPCION " & _
            "from VIS_TIS_TIPO_SERVICIO " & _
            "where VIS_TIS_TIPO_SERVICIO.TIS_CO_ID = " + idTipoServicio.ToString + " "

            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            aRow = aDS.Tables(0).Rows(0)
            Me.id = idTipoServicio
            Me.descripcion = IIf(aRow("TIS_DS_DESCRIPCION").Equals(System.DBNull.Value), "", aRow("TIS_DS_DESCRIPCION"))


            'Obtenemos los Servicios (s�lo los que disponemos permisos sobre ellos)
            'strTableName = "VIS_SER_SERVICIOS"
            'strDatasetName = "VIS_SER_SERVICIOS"
            'strQuery = "select vis_ser_servicios.SER_CO_ID,SER_NM_ESCALAIMP,SER_NM_ESCALAMIN,SER_BO_MOSTRARLEYENDA,DECODE (VIS_DEP_DEPARTAMENTOS.DEP_CO_IDPADRE ,null,0,VIS_DEP_DEPARTAMENTOS.DEP_CO_IDPADRE)as DEP_CO_IDPADRE,vis_ser_servicios.dep_co_id, hos_ds_nombre,ser_ds_nombre,DECODE (VIS_SDS_DESC_SERVICIOS.ser_ds_descripcion,null,vis_ser_servicios.SER_DS_DESCRIPCION, vis_sds_desc_servicios.SER_DS_DESCRIPCION )as ser_ds_descripcion,ser_ds_url,ser_ds_path," & _
            '"vis_tis_tipo_servicio.TIS_DS_DESCRIPCION,ser_ds_info, ser_co_alpha, DECODE (VIS_SDS_DESC_SERVICIOS.ser_ds_alias, null, vis_ser_servicios.SER_DS_ALIAS, VIS_SDS_DESC_SERVICIOS.ser_ds_alias) as ser_ds_alias, SER_BO_CABECERALEYENDA, SER_BO_IDENTIFICAR_REST " & _
            '"from ((vis_ser_servicios left join VIS_SDS_DESC_SERVICIOS on vis_ser_servicios.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id " & _
            '"and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') left join vis_hos_hosts on vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
            '"inner join vis_tis_tipo_servicio on vis_ser_servicios.tis_co_id=vis_tis_tipo_servicio.tis_co_id " & _
            '"left join VIS_DEP_DEPARTAMENTOS on vis_ser_servicios.dep_co_id=VIS_DEP_DEPARTAMENTOS.dep_co_id " & _
            '" where (vis_ser_servicios.tis_co_id=" + idTipoServicio.ToString + " and ) "

            strTableName = "VIS_SER_SERVICIOS"
            strDatasetName = "VIS_SER_SERVICIOS"
            strQuery = "select vis_ser_servicios.SER_CO_ID,SER_NM_ESCALAIMP,SER_NM_ESCALAMIN,SER_BO_MOSTRARLEYENDA,CASE WHEN VIS_DEP_DEPARTAMENTOS.DEP_CO_IDPADRE IS null THEN 0 ELSE VIS_DEP_DEPARTAMENTOS.DEP_CO_IDPADRE END as DEP_CO_IDPADRE,vis_ser_servicios.dep_co_id, hos_ds_nombre,ser_ds_nombre,case when VIS_SDS_DESC_SERVICIOS.ser_ds_descripcion is null then vis_ser_servicios.SER_DS_DESCRIPCION else vis_sds_desc_servicios.SER_DS_DESCRIPCION end as ser_ds_descripcion,ser_ds_url,ser_ds_path," & _
            "vis_tis_tipo_servicio.TIS_DS_DESCRIPCION,ser_ds_info, ser_co_alpha, CASE WHEN VIS_SDS_DESC_SERVICIOS.ser_ds_alias IS null THEN vis_ser_servicios.SER_DS_ALIAS ELSE VIS_SDS_DESC_SERVICIOS.ser_ds_alias END as ser_ds_alias, SER_BO_CABECERALEYENDA, SER_BO_IDENTIFICAR_REST, SER_DS_CAPASWMS " & _
            "from (((vis_ser_servicios left join VIS_SDS_DESC_SERVICIOS on vis_ser_servicios.ser_co_id=VIS_SDS_DESC_SERVICIOS.ser_co_id " & _
            "and VIS_SDS_DESC_SERVICIOS.idi_ds_codigo='" + codIdioma + "') left join vis_hos_hosts on vis_ser_servicios.hos_co_id=vis_hos_hosts.hos_co_id) " & _
            "inner join vis_tis_tipo_servicio on vis_ser_servicios.tis_co_id=vis_tis_tipo_servicio.tis_co_id " & _
            "left join VIS_DEP_DEPARTAMENTOS on vis_ser_servicios.dep_co_id=VIS_DEP_DEPARTAMENTOS.dep_co_id) " & _
            "LEFT JOIN VIS_VSR_VISORES_SERVICIO ON VIS_SER_SERVICIOS.SER_CO_ID=VIS_VSR_VISORES_SERVICIO.SER_CO_ID " & _
            " where (vis_ser_servicios.tis_co_id=" + idTipoServicio.ToString + " and vis_vsr_visores_servicio.vis_co_id=" + idVisor.ToString + ") ORDER BY SER_CO_ID"


            aDS = ExecuteQuery(strQuery, strTableName, strDatasetName)
            ReDim Me.servicios(aDS.Tables(0).Rows.Count - 1)
            Dim i As Integer
            i = 0
            For Each aRow In aDS.Tables(0).Rows
                Dim nServicio As New Servicio
                nServicio.Cargar(aRow("SER_CO_ID"), codIdioma)
                Me.servicios(i) = nServicio
                i += 1
            Next

            Return Me
        Catch
            Return Nothing
        End Try

    End Function

#End Region

End Class
