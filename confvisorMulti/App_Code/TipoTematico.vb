Imports Microsoft.VisualBasic
Imports Service

Public Class TipoTematico
    Private _Id As Integer
    Private _nombre As String
    Private _tabla As String
    Private _campo As String
   
#Region "Propiedades de la clase"
    Public Property Id() As Integer
        Get
            Id = _Id
        End Get
        Set(ByVal aId As Integer)
            _Id = aId
        End Set
    End Property

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property tabla() As String
        Get
            tabla = _tabla
        End Get
        Set(ByVal aTabla As String)
            _tabla = aTabla
        End Set
    End Property
    Public Property campo() As String
        Get
            campo = _campo
        End Get
        Set(ByVal aCampo As String)
            _campo = aCampo
        End Set
    End Property
#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal idEntrada As Integer) As TipoTematico
        Dim strQuery As String, strConn As String
        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Try
            strConn = System.Configuration.ConfigurationManager.AppSettings("bbdd")
            strTablename = "VIS_TAD_TEMATICOSDEMANDA"
            strDatasetName = "VIS_TAD_TEMATICOSDEMANDA"
            strQuery = "SELECT TIT_CO_ID, TIT_DS_DESCRIPCION, TIT_DS_TABLA_TEMATICO, TIT_DS_CAMPO_TEMATICO FROM VIS_TIT_TIPO_TEMATICO "
            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            For Each aRow In aDS.Tables(0).Rows                
                Me.Id = aRow("TIT_CO_ID")
                Me.nombre = aRow("TIT_DS_DESCRIPCION")
                Me.tabla = aRow("TIT_DS_TABLA_TEMATICO")
                Me.campo = aRow("TIT_DS_CAMPO_TEMATICO")
            Next
            aDS.Clear()
            aDS = Nothing
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
#End Region
End Class
