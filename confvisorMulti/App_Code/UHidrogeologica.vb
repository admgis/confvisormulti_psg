Imports Microsoft.VisualBasic

Public Class UHidrogeologica
  
#Region "Propiedades de la Clase"
    Private _id As String
    Private _uh As String
    Private _nombre As String
    Private _idCuenca As String
    Public Property Id() As String
        Get
            Id = _id
        End Get
        Set(ByVal mId As String)
            _id = mId
        End Set
    End Property
    Public Property UH() As String
        Get
            UH = _uh
        End Get
        Set(ByVal mUH As String)
            _uh = mUH
        End Set
    End Property

    Public Property IdCuenca() As String
        Get
            IdCuenca = _idCuenca
        End Get
        Set(ByVal mIdCuenca As String)
            _idCuenca = mIdCuenca
        End Set
    End Property
    
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property    
#End Region


End Class
