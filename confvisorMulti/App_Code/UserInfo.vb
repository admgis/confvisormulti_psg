Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OracleClient
Imports Service


Public Class UserInfo
#Region "Propiedades de la Clase"    
    Private _nombre As String
    Private _descripcion As String
    Private _lstGrupos As ArrayList
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property descripcion() As String
        Get
            descripcion = _descripcion
        End Get
        Set(ByVal aDescripcion As String)
            _descripcion = aDescripcion
        End Set
    End Property

    Public Property grupos() As ArrayList
        Get
            grupos = _lstGrupos
        End Get
        Set(ByVal aLstGrupos As ArrayList)
            _lstGrupos = aLstGrupos
        End Set
    End Property

#End Region
#Region "Funciones de la Clase"
    Public Function Cargar(ByVal user As String) As UserInfo

        Dim strTablename As String, strDatasetName As String
        Dim aDS As Data.DataSet
        Dim aRow As Data.DataRow
        Dim strQuery As String
        Dim arrValores As New ArrayList
        Dim i As Integer
        Try
            'Debemos obtener los tem�ticos a los que puede acceder el usuario, tanto si son 
            'privados, compartidos o p�blicos

            strTablename = "VIS_USR_USUARIOS_ROL"
            strDatasetName = "VIS_USR_USUARIOS_ROL"
            strQuery = "SELECT DISTINCT VIS_USU_USUARIOS.USU_DS_NOMBRE,VIS_USU_USUARIOS.USU_DS_DESCRIPCION,VIS_USR_USUARIOS_ROL.ROL_CO_ID, VIS_ROL_ROLES.ROL_DS_NOMBRE" & _
              " FROM VIS_USU_USUARIOS,VIS_USR_USUARIOS_ROL, VIS_ROL_ROLES " & _
              " WHERE VIS_USU_USUARIOS.USU_CO_ID = VIS_USR_USUARIOS_ROL.USU_CO_ID " & _
              " AND VIS_ROL_ROLES.ROL_CO_ID = VIS_USR_USUARIOS_ROL.ROL_CO_ID " & _
              " AND VIS_USU_USUARIOS.USU_DS_NOMBRE = '" & user & "'"

            aDS = ExecuteQuery(strQuery, strTablename, strDatasetName)
            i = 0
            Me.grupos = New ArrayList
            Me.nombre = user
            For Each aRow In aDS.Tables(0).Rows
                If (i = 0) Then                    
                    Me.descripcion = aRow("USU_DS_DESCRIPCION")
                End If
                i += 1
                Me.grupos.Add(aRow("ROL_DS_NOMBRE"))
            Next
            Return Me
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

#End Region
End Class
