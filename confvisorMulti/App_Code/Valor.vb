Imports Microsoft.VisualBasic

Public Class Valor
    Private _nombre As String
    Private _valor As String
    Private _exportar As Boolean = True

    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal aNombre As String)
            _nombre = aNombre
        End Set
    End Property
    Public Property valor() As String
        Get
            valor = _valor
        End Get
        Set(ByVal value As String)
            _valor = value
        End Set
    End Property
    Public Property exportar() As Boolean
        Get
            exportar = _exportar
        End Get
        Set(ByVal aExp As Boolean)
            _exportar = aExp
        End Set
    End Property
End Class
