Imports Microsoft.VisualBasic

Public Class ValoresCampo
#Region "Propiedades de la Clase"
    Private _nombre As String
    Private _alias As String
    Private _minimo As String
    Private _maximo As String
    Private _colValores As String()
    '' ''Private _tipo As String
#End Region
#Region "Funciones de la Clase"
    Public Property nombre() As String
        Get
            nombre = _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property
    Public Property aliasCampo() As String
        Get
            aliasCampo = _alias
        End Get
        Set(ByVal value As String)
            _alias = value
        End Set
    End Property
    Public Property minimo() As String
        Get
            minimo = _minimo
        End Get
        Set(ByVal value As String)
            _minimo = value
        End Set
    End Property
    Public Property maximo() As String
        Get
            maximo = _maximo
        End Get
        Set(ByVal value As String)
            _maximo = value
        End Set
    End Property
    
    Public Property valores() As String()
        Get
            valores = _colValores
        End Get
        Set(ByVal value As String())
            _colValores = value
        End Set
    End Property

    ' '' ''Public Property tipo() As String
    ' '' ''    Get
    ' '' ''        tipo = _tipo
    ' '' ''    End Get
    ' '' ''    Set(ByVal value As String)
    ' '' ''        _tipo = value
    ' '' ''    End Set
    ' '' ''End Property
#End Region

End Class
