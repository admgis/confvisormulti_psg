# confvisorMulti
## Perfil de publicación
La ruta de publicación respeta la estructura del despliegue en {{maquina}}, sustituyéndose __\\\{{maquina}}\wwwrooot\\__ por __C:\publicaciones\\__. A continuación se incluye la configuración empleada para la publicación (perfilCombinado.pubxml):
```
<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup>
    <WebPublishMethod>FileSystem</WebPublishMethod>
    <LastUsedBuildConfiguration>Release</LastUsedBuildConfiguration>
    <LastUsedPlatform>Any CPU</LastUsedPlatform>
    <SiteUrlToLaunchAfterPublish />
    <LaunchSiteAfterPublish>True</LaunchSiteAfterPublish>
    <PrecompileBeforePublish>True</PrecompileBeforePublish>
    <EnableUpdateable>True</EnableUpdateable>
    <DebugSymbols>False</DebugSymbols>
    <WDPMergeOption>MergeAllOutputsToASingleAssembly</WDPMergeOption>
    <UseMerge>True</UseMerge>
    <SingleAssemblyName>confvisorMulti</SingleAssemblyName>
    <DeleteAppCodeCompiledFiles>True</DeleteAppCodeCompiledFiles>
    <ExcludeApp_Data>True</ExcludeApp_Data>
    <publishUrl>C:\publicaciones\WebServices\confvisorMulti</publishUrl>
    <DeleteExistingFiles>True</DeleteExistingFiles>
  </PropertyGroup>
</Project>
```

## Árbol de directorios
```
\---WebServices
    |   
    \---confvisorMulti
        |   Global.asax
        |   jsonService.aspx
        |   PrecompiledApp.config
        |   README.md
        |   Service.asmx
        |   web.config
        |   
        \---bin
                App_global.asax.compiled
                confvisorMulti.dll
                Devart.Data.dll
                Devart.Data.PostgreSql.dll
                Newtonsoft.Json.dll
```

**El archivo Global.asax ha de estar presente en el despliegue y hay que publicarlo manualmente ya que el perfil de despliegue no lo traslada al directorio de salida.**