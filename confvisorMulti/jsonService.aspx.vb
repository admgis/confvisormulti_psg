﻿Imports Newtonsoft.Json

Partial Class jsonService
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim queryStr As Specialized.NameValueCollection = Request.QueryString
        Dim res As String
        Dim ws As Service = New Service()
        If Not queryStr("test") Is Nothing Then
            'OK
            'Dim testObj As CCAA() = ws.DameCCAA_ET()
            Dim testObj As Tema() = ws.ObtenerTemasVisor("es", 1)
            res = JsonConvert.SerializeObject(testObj)
        Else
            res = ""
        End If

        Response.Clear()
        Response.AddHeader("Access-Control-Allow-Origin", "*")
        Response.Write(res)

    End Sub
End Class
